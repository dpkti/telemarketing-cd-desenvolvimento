VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmCadEmail 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cadastro de E-mail"
   ClientHeight    =   5265
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8805
   ClipControls    =   0   'False
   Icon            =   "frmCadEmail.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5265
   ScaleWidth      =   8805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid msfEmail 
      Height          =   1935
      Left            =   30
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   3270
      Width           =   8745
      _ExtentX        =   15425
      _ExtentY        =   3413
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      WordWrap        =   -1  'True
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
   End
   Begin VB.TextBox txtDtAtual 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7470
      Locked          =   -1  'True
      MaxLength       =   8
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   1050
      Width           =   1260
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1335
      Left            =   30
      TabIndex        =   5
      Top             =   1440
      Width           =   8715
      Begin VB.TextBox txtEmail 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   100
         TabIndex        =   15
         Top             =   900
         Width           =   7680
      End
      Begin VB.TextBox txtDtCadastro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7410
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   540
         Width           =   1260
      End
      Begin VB.TextBox txtContato 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   70
         TabIndex        =   13
         Top             =   540
         Width           =   5220
      End
      Begin VB.TextBox txtSeq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         Locked          =   -1  'True
         MaxLength       =   100
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   180
         Width           =   540
      End
      Begin VB.ComboBox cboDepto 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   4380
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   180
         Width           =   4275
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "E-mail:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   420
         TabIndex        =   20
         Top             =   990
         Width           =   525
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dt.Cadastro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   6360
         TabIndex        =   19
         Top             =   630
         Width           =   1020
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Contato:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   240
         TabIndex        =   18
         Top             =   630
         Width           =   720
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Sequ�ncia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   60
         TabIndex        =   17
         Top             =   270
         Width           =   900
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3120
         TabIndex        =   16
         Top             =   270
         Width           =   1245
      End
   End
   Begin VB.TextBox txtCodCliente 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      Locked          =   -1  'True
      MaxLength       =   100
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1050
      Width           =   960
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   960
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   855
      Left            =   7920
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   1508
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":0CCA
      PICN            =   "frmCadEmail.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAdd 
      Height          =   405
      Left            =   3000
      TabIndex        =   6
      ToolTipText     =   "Salvar E-mail"
      Top             =   2820
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Salvar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":19C0
      PICN            =   "frmCadEmail.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   405
      Left            =   7650
      TabIndex        =   10
      ToolTipText     =   "Excluir E-mail selecionado"
      Top             =   2820
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Excluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":1F76
      PICN            =   "frmCadEmail.frx":1F92
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNovo 
      Height          =   405
      Left            =   4050
      TabIndex        =   7
      ToolTipText     =   "Adicionar E-mail"
      Top             =   2820
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Novo"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":252C
      PICN            =   "frmCadEmail.frx":2548
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   405
      Left            =   6450
      TabIndex        =   9
      ToolTipText     =   "Limpar"
      Top             =   2820
      Width           =   1185
      _ExtentX        =   2090
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Limpar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":2A04
      PICN            =   "frmCadEmail.frx":2A20
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSemEmail 
      Height          =   405
      Left            =   5100
      TabIndex        =   8
      ToolTipText     =   "Cliente n�o possui email"
      Top             =   2820
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Sem Email"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":2D3A
      PICN            =   "frmCadEmail.frx":2D56
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEnviaSenha 
      Height          =   405
      Left            =   1920
      TabIndex        =   25
      ToolTipText     =   "Enviar Senha por E-Mail"
      Top             =   2820
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Enviar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":31C0
      PICN            =   "frmCadEmail.frx":31DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Dt.Atual:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   6720
      TabIndex        =   23
      Top             =   1140
      Width           =   735
   End
   Begin VB.Label lblNomeCliente 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   2100
      TabIndex        =   21
      Top             =   1050
      Width           =   4515
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "E-mails Cadastrados:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   4
      Top             =   3030
      Width           =   1635
   End
   Begin VB.Label lblCodCliente 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "C�d.Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   3
      Top             =   1140
      Width           =   990
   End
End
Attribute VB_Name = "frmCadEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmCadEmail
' Author    : c.samuel.oliveira
' Date      : 07/05/2018
' Purpose   : TI-6488
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmCadEmail
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 16/03/16
' Purpose   : TI-4046
'---------------------------------------------------------------------------------------
Option Explicit

Dim vLoad As Boolean
Dim vRst As Object
Dim vHoje As Date

'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
Dim vstrEmail_Selecionado As String

Private Sub cmdAdd_Click()

    If cboDepto.Text = "" Then
        MessageBox 0, "O Campo Departamento n�o pode estar em branco.", "Aten��o", &H40000
        cboDepto.SetFocus
        Exit Sub
    End If
    
    If txtCodCliente = "" Then
        MessageBox 0, "O Campo C�digo do Cliente n�o pode estar em branco." & vbCrLf & "Entre em contato com o Helpdesk.", "Aten��o", &H40000
        Exit Sub
    End If
    
    If Trim(txtEmail) = "" Then
        MessageBox 0, "Informe o E-mail.", "Aten��o", &H40000
        txtEmail.SetFocus
        Exit Sub
    End If
        
    'Validar Email
    If isEmail(txtEmail) = False Then
       MessageBox 0, "E-mail inv�lido.", "Aten��o", &H40000
       txtEmail.SetFocus
       Exit Sub
    End If

    'TI-6488
    For i = 1 To msfEmail.rows - 1
        If txtSeq <> msfEmail.TextMatrix(i, 1) And (cboDepto.ItemData(cboDepto.ListIndex) = "19" And cboDepto = msfEmail.TextMatrix(i, 4)) Then
            MessageBox 0, "J� existe E-Mail Comercial cadastrado!", "Aten��o", &H40000
            Exit Sub
        End If
    Next
    'FIM TI-6488
        
'Verificar se existe um igual, se existir sair da rotina
    For i = 1 To msfEmail.rows - 1
        If txtEmail = msfEmail.TextMatrix(i, 2) And txtContato = msfEmail.TextMatrix(i, 3) And cboDepto = msfEmail.TextMatrix(i, 4) And txtDtCadastro = msfEmail.TextMatrix(i, 5) Then 'TI-4046
            MessageBox 0, "Este email j� existe.", "Aten��o", &H40000
            Exit Sub
        End If
    Next
    
    'Verificar se existe uma sequencia igual, se existir dar update
    If UCase(Frame1.Caption) = "ALTERAR" Then
        If MessageBox(0, "Confirma a altera��o deste e-mail ?", "Aten��o", &H40000 + 36) = vbYes Then
           For i = 1 To msfEmail.rows
               If txtSeq = msfEmail.TextMatrix(i, 1) Then 'TI-4046
                  Exit For
               End If
           Next
           
           msfEmail.TextMatrix(i, 1) = txtSeq 'TI-4046
           msfEmail.TextMatrix(i, 2) = txtEmail 'TI-4046
           msfEmail.TextMatrix(i, 3) = txtContato 'TI-4046
           msfEmail.TextMatrix(i, 4) = cboDepto 'TI-4046
           msfEmail.TextMatrix(i, 5) = txtDtCadastro 'TI-4046
        Else
            Exit Sub
        End If
    ElseIf UCase(Frame1.Caption) = "NOVO" Then
       msfEmail.rows = msfEmail.rows + 1
       msfEmail.TextMatrix(i, 1) = txtSeq 'TI-4046
       msfEmail.TextMatrix(i, 2) = txtEmail 'TI-4046
       msfEmail.TextMatrix(i, 3) = txtContato 'TI-4046
       msfEmail.TextMatrix(i, 4) = cboDepto 'TI-4046
       msfEmail.TextMatrix(i, 5) = txtDtCadastro 'TI-4046
    End If
    
    'Inserir no banco
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Remove "Sequencia"
    vBanco.Parameters.Remove "Email"
    vBanco.Parameters.Remove "CodDepto"
    vBanco.Parameters.Remove "Contato"
    
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    vBanco.Parameters.Add "Sequencia", txtSeq, 1
    vBanco.Parameters.Add "Email", txtEmail, 1
    vBanco.Parameters.Add "CodDepto", cboDepto.ItemData(cboDepto.ListIndex), 1
    vBanco.Parameters.Add "Contato", txtContato, 1
    
    If UCase(Frame1.Caption) = "NOVO" Then
    
       'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
       'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       
       'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
       If Trim(vstrEmail_Selecionado) <> "" Then
          vBanco.Parameters.Remove "Cod_Repres"
          vBanco.Parameters.Remove "Email_DE"
          vBanco.Parameters.Remove "Email_PARA"
        
          vBanco.Parameters.Add "Cod_Repres", Trim(Mid(frmVenda.lblRepres, 1, 4)), 1
          vBanco.Parameters.Add "Email_DE", vstrEmail_Selecionado, 1
          vBanco.Parameters.Add "Email_PARA", txtEmail, 1
        
          'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
          'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_LOG_CLIENTE_EMAIL(:Cod_Repres, :Cod_Cliente, :Email_DE, :Email_PARA); END;"
          vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_LOG_CLIENTE_EMAIL(:Cod_Repres, :Cod_Cliente, :Email_DE, :Email_PARA); END;"
          
       End If
       'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK

    ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
    
       'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
       'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       
       'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
       If Trim(vstrEmail_Selecionado) <> Trim(txtEmail) Then
            vBanco.Parameters.Remove "Cod_Repres"
            vBanco.Parameters.Remove "Email_DE"
            vBanco.Parameters.Remove "Email_PARA"
       
            vBanco.Parameters.Add "Cod_Repres", Trim(Mid(frmVenda.lblRepres, 1, 4)), 1
            vBanco.Parameters.Add "Email_DE", vstrEmail_Selecionado, 1
            vBanco.Parameters.Add "Email_PARA", txtEmail, 1
       
            'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
            'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_LOG_CLIENTE_EMAIL(:Cod_Repres, :Cod_Cliente, :Email_DE, :Email_PARA); END;"
            vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_LOG_CLIENTE_EMAIL(:Cod_Repres, :Cod_Cliente, :Email_DE, :Email_PARA); END;"
            
        End If
       'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK

    End If
    
    If cboDepto.ItemData(cboDepto.ListIndex) = "19" Then EnviarParaAprovacaoCAD060 UCase(Trim(txtEmail)), Val(txtCodCliente) 'TI-6488
    
    txtContato = ""
    txtEmail = ""
    txtSeq = ""
    cboDepto.ListIndex = -1
     
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = ""
     
    Frame1.Enabled = False
    
    Preencher_Email
    
    Botoes True, False, False, False, False, True
        
End Sub


Private Sub cmdAlterar_Click()
    Frame1.Enabled = True
    Frame1.Caption = "Alterar"
    txtDtCadastro = txtDtAtual
    Botoes True, True, True, True, True, True
End Sub

Private Sub cmdExcluir_Click()
    If Val(txtSeq) = 0 Then
       MessageBox 0, "Selecione um item da lista.", "Aten��o", &H40000
       Exit Sub
    End If
    
    If MessageBox(0, "Confirma a exclus�o do e-mail selecionado ?", "Aten��o", &H40000 + 36) = vbYes Then
        vBanco.Parameters.Remove "COD_CLIENTE"
        vBanco.Parameters.Remove "Sequencia"
        vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
        vBanco.Parameters.Add "Sequencia", txtSeq, 1
        
        'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
        'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_EXCLUIR_EMAIL(:Cod_Cliente,:Sequencia); END;"
        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_EXCLUIR_EMAIL(:Cod_Cliente,:Sequencia); END;"
        
        'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
        'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_RESEQUENCIAR(:Cod_Cliente); END;"
        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_RESEQUENCIAR(:Cod_Cliente); END;"
        
        Preencher_Email
    End If
    
    txtSeq = ""
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
        
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = ""
        
    Frame1.Enabled = False
    
    Botoes True, False, False, False, False, True
    
    cboDepto = "COMERCIAL"
    
End Sub

Private Sub cmdLimpar_Click()
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    Me.Frame1.Caption = ""
    
    'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
    vstrEmail_Selecionado = ""
    
    Frame1.Enabled = False
    
    Botoes True, False, True, False, False, False
End Sub

Private Sub cmdNovo_Click()
    
    On Error GoTo Trata_Erro
    
    Frame1.Enabled = True
    
    If msfEmail.rows = 1 Then
        txtSeq = 1
    Else
        txtSeq = msfEmail.TextMatrix(msfEmail.rows - 1, 1) + 1 'TI-4046
    End If
    
    'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
    vstrEmail_Selecionado = "NOVO"
    
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    
    txtDtCadastro = txtDtAtual
    
    cboDepto = "COMERCIAL"
    
    txtContato.SetFocus
    
    Frame1.Caption = "Novo"
    
    Botoes False, False, True, True, False, True
    
Trata_Erro:
    If Err.Number = 5 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MessageBox 0, "Sub CmdNovo_CLick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
    
End Sub

Private Sub cmdSair_Click()
    Unload Me
    frmVenda.Verificar_Email
End Sub

Private Sub cmdSemEmail_Click()

    NonStayOnTop frmCadEmail

    If MsgBox("Confirma que este cliente n�o possui email ?", vbYesNo + vbQuestion, "Aten��o") = vbNo Then Exit Sub
    
    If msfEmail.rows > 1 Then
        MsgBox "Este cliente j� possui email cadastrado." & vbCrLf & "Caso queria informar que o cliente n�o possui email" & vbCrLf & "exclua os emails da lista.", vbInformation, "Aten��o"
        StayOnTop frmCadEmail
        Exit Sub
    End If
    
    If cboDepto.Text = "" Then
        MessageBox 0, "O Campo Departamento n�o pode estar em branco.", "Aten��o", &H40000
        StayOnTop frmCadEmail
        Exit Sub
    End If
    
    If txtCodCliente = "" Then
        MessageBox 0, "O Campo C�digo do Cliente n�o pode estar em branco." & vbCrLf & "Entre em contato com o Helpdesk.", "Aten��o", &H40000
        StayOnTop frmCadEmail
        Exit Sub
    End If
    
    Frame1.Caption = "Novo"
    
    If msfEmail.rows = 1 Then
        txtSeq = 1
    Else
        txtSeq = msfEmail.TextMatrix(msfEmail.rows - 1, 1) + 1 'TI-4046
    End If
    
    txtEmail = ""
    
'Verificar se existe um igual, se existir sair da rotina
    For i = 1 To msfEmail.rows - 1
                If txtEmail = msfEmail.TextMatrix(i, 2) And txtContato = msfEmail.TextMatrix(i, 3) And cboDepto = msfEmail.TextMatrix(i, 4) And txtDtCadastro = msfEmail.TextMatrix(i, 5) Then 'TI-4046
            MessageBox 0, "Este email j� existe.", "Aten��o", &H40000
            StayOnTop frmCadEmail
            Exit Sub
        End If
    Next
    
    'Verificar se existe uma sequencia igual, se existir dar update
    If UCase(Frame1.Caption) = "ALTERAR" Then
        If MessageBox(0, "Confirma a altera��o deste e-mail ?", "Aten��o", &H40000 + 36) = vbYes Then
           For i = 1 To msfEmail.rows
               If txtSeq = msfEmail.TextMatrix(i, 1) Then 'TI-4046
                  Exit For
               End If
           Next
           
           msfEmail.TextMatrix(i, 0) = "N�o" 'TI-4046
           msfEmail.TextMatrix(i, 1) = txtSeq 'TI-4046
           msfEmail.TextMatrix(i, 2) = txtEmail 'TI-4046
           msfEmail.TextMatrix(i, 3) = txtContato 'TI-4046
           msfEmail.TextMatrix(i, 4) = cboDepto 'TI-4046
           msfEmail.TextMatrix(i, 5) = txtDtCadastro 'TI-4046
        Else
            StayOnTop frmCadEmail
            Exit Sub
        End If
    ElseIf UCase(Frame1.Caption) = "NOVO" Then
       msfEmail.rows = msfEmail.rows + 1
       msfEmail.TextMatrix(i, 0) = "N�o" 'TI-4046
       msfEmail.TextMatrix(i, 1) = txtSeq 'TI-4046
       msfEmail.TextMatrix(i, 2) = txtEmail 'TI-4046
       msfEmail.TextMatrix(i, 3) = txtContato 'TI-4046
       msfEmail.TextMatrix(i, 4) = cboDepto 'TI-4046
       msfEmail.TextMatrix(i, 5) = txtDtCadastro 'TI-4046
    End If
    
    'Inserir no banco
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Remove "Sequencia"
    vBanco.Parameters.Remove "Email"
    vBanco.Parameters.Remove "CodDepto"
    vBanco.Parameters.Remove "Contato"
    
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    vBanco.Parameters.Add "Sequencia", txtSeq, 1
    vBanco.Parameters.Add "Email", Null, 1
    vBanco.Parameters.Add "CodDepto", cboDepto.ItemData(cboDepto.ListIndex), 1
    vBanco.Parameters.Add "Contato", txtContato, 1
    
    If UCase(Frame1.Caption) = "NOVO" Then
    
       'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
       'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       
    ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
    
       'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
       'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
       
    End If
    
    txtContato = ""
    txtEmail = ""
    txtSeq = ""
    cboDepto.ListIndex = -1
     
    Frame1.Enabled = False
    
    Preencher_Email
    
    Botoes True, False, False, False, False, False
    

End Sub

Private Sub Form_Activate()
    If msfEmail.rows = 1 Then cmdNovo_Click
End Sub

Private Sub Form_Load()
    
    vLoad = True
        
    Me.txtCodCliente = frmVenda.txtCOD_CLIENTE
        
    Botoes True, False, False, False, False, False

    Preencher_Data
        
    Preencher_Depto
    
    Preencher_Email
    
    Pegar_nome_Cliente
    
    vLoad = False
    
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = ""
    
End Sub

Private Sub msfEmail_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeq = Item.SubItems(1) 'TI-4046
    txtEmail = Item.SubItems(2) 'TI-4046
    txtContato = Item.SubItems(3) 'TI-4046
    cboDepto = Item.SubItems(4) 'TI-4046
    txtDtCadastro = Item.SubItems(5) 'TI-4046
        
    If Trim(txtDtCadastro) = "" Then txtDtCadastro = txtDtAtual
        
    Frame1.Enabled = False
    Frame1.Caption = ""
    Botoes False, True, True, False, True, True
        
End Sub

Sub Preencher_Data()
    
    'Criar_Cursor

    'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
    'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_DATA_CADASTRO(:vCursor); END;"
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_SELECT_DATA_CADASTRO(:vCursor); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
            
    txtDtAtual = Data_Faturamento
    vHoje = Data_Faturamento

End Sub

Sub Preencher_Depto()
    
    'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
    'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_DEPTO(:vCursor); end;"
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_SELECT_DEPTO(:vCursor); end;"

    Set vRst = vBanco.Parameters("vCursor").Value
            
    For i = 1 To vRst.RecordCount
        cboDepto.AddItem vRst!Descricao
        cboDepto.ItemData(cboDepto.NewIndex) = vRst!Cod_Depto
        vRst.MoveNext
    Next

    vRst.Close
    Set vRst = Nothing

End Sub

Sub Preencher_Email()
    Dim lItem As ListItem
    Dim ii As Integer
    
    'Criar_Cursor
    
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Add "Cod_cliente", txtCodCliente, 1
    
    'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
    'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_EMAIL(:vCursor, :Cod_Cliente); END;"
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_SELECT_EMAIL(:vCursor, :Cod_Cliente); END;"
    
    Set vRst = vBanco.Parameters("vCursor").Value
    
    'vBanco.Parameters.Remove "vCursor"
    
    msfEmail.Clear
    
    Montar_Grid vRst
    
    For i = 1 To vRst.RecordCount
        With msfEmail
            .TextMatrix(i, 0) = "N�o" 'TI-4046
            .TextMatrix(i, 1) = vRst!Sequencia 'TI-4046
            .TextMatrix(i, 2) = IIf(IsNull(vRst!Email), "", vRst!Email) 'TI-4046
            .TextMatrix(i, 3) = IIf(IsNull(vRst!NOME_CONTATO), "", vRst!NOME_CONTATO) 'TI-4046
            .TextMatrix(i, 4) = vRst!Descricao 'TI-4046
            .TextMatrix(i, 5) = IIf(IsNull(vRst!Dt_Email), "", vRst!Dt_Email) 'TI-4046
        
            If .TextMatrix(i, 5) = "" Then 'TI-4046
               For ii = 1 To 5 'TI-4046
                    .Row = i
                    .Col = ii
                    .CellFontBold = True
                    .CellForeColor = vbRed
               Next
            Else
                If DateAdd("m", -6, vHoje) > CDate(.TextMatrix(i, 5)) Then 'TI-4046
                    For ii = 1 To 5 'TI-4046
                         .Row = i
                         .Col = ii
                         .CellFontBold = True
                         .CellForeColor = vbRed
                    Next
                End If
            End If
        End With
        vRst.MoveNext
    Next
    Set vRst = Nothing
End Sub

Sub Pegar_nome_Cliente()

    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    
    'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
    'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_NOME_CLIENTE(:vCursor, :Cod_Cliente); END;"
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_SELECT_NOME_CLIENTE(:vCursor, :Cod_Cliente); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
    
    'vBanco.Parameters.Remove "vCursor"
    
    If vRst.EOF = False Then Me.lblNomeCliente = vRst!NOME_CLIENTE
    
    Set vRst = Nothing

End Sub

Sub Botoes(pNovo As Boolean, pAlterar As Boolean, pDesfazer As Boolean, pGravar As Boolean, pExcluir As Boolean, pSemEmail As Boolean)

    cmdNovo.Enabled = pNovo
    cmdLimpar.Enabled = pDesfazer
    cmdAdd.Enabled = pGravar
    cmdExcluir.Enabled = pExcluir
    cmdSemEmail.Enabled = pSemEmail
End Sub

Function isEmail(ByVal pEmail As String) As Boolean
        
    Dim Conta As Integer, Flag As Integer, cValido As String
    
    isEmail = False
    
    If Len(pEmail) < 3 Then
        Exit Function
    End If

    
    'Verifica se existe caracter inv�lido
    
    For Conta = 1 To Len(pEmail)
        cValido = Mid(pEmail, Conta, 1)
        If Not (LCase(cValido) Like "[a-z]" Or cValido = _
            "@" Or cValido = "." Or cValido = "-" Or _
            cValido = "_" Or cValido Like "[0-9]") Then
            Exit Function
        End If
    Next
  

    'Verifica a exist�ncia de (@)
    
    If InStr(pEmail, "@") = 0 Then
        Exit Function
    Else
        Flag = 0
        
        For Conta = 1 To Len(pEmail)
            If Mid(pEmail, Conta, 1) = "@" Then
                Flag = Flag + 1
            End If
        Next
        
        If Flag > 1 Then Exit Function
    End If
  
    If Left(pEmail, 1) = "@" Then
        Exit Function
    ElseIf Right(pEmail, 1) = "@" Then
        Exit Function
    ElseIf InStr(pEmail, ".@") > 0 Then
        Exit Function
    ElseIf InStr(pEmail, "@.") > 0 Then
        Exit Function
    End If
  
  
    'Verifica a exist�ncia de (.)
    
    If InStr(pEmail, ".") = 0 Then
        Exit Function
    ElseIf Left(pEmail, 1) = "." Then
        Exit Function
    ElseIf Right(pEmail, 1) = "." Then
        Exit Function
    ElseIf InStr(pEmail, "..") > 0 Then
        Exit Function
    End If
    
    'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
    If InStr(UCase$(pEmail), "DPK") > 0 Then
        'Se n�o for Funcion�rio n�o permite email DPK...
        If Not Verifica_Funcionario Then
            Exit Function
        End If
    End If

    isEmail = True

End Function

Sub Montar_Grid(rst As Object)
    With msfEmail
        .rows = rst.RecordCount + 1
        .TextMatrix(0, 0) = "Enviar" 'TI-4046
        .TextMatrix(0, 1) = "Seq" 'TI-4046
        .TextMatrix(0, 2) = "E-mail" 'TI-4046
        .TextMatrix(0, 3) = "Contato" 'TI-4046
        .TextMatrix(0, 4) = "Departamento" 'TI-4046
        .TextMatrix(0, 5) = "Dt Cadastro" 'TI-4046
        .ColAlignment(1) = vbCenter 'TI-4046
        .ColWidth(0) = 550 'TI-4046
        .ColWidth(1) = 500 'TI-4046
        .ColWidth(2) = 2800 'TI-4046
        .ColWidth(3) = 2000 'TI-4046
        .ColWidth(4) = 1850 'TI-4046
        .ColWidth(5) = 1000 'TI-4046

    End With
End Sub

Private Sub msfEmail_Click()
    If msfEmail.TextMatrix(0, 1) = "" Or vLoad = True Then Exit Sub 'TI-4046
    msfEmail_EnterCell
End Sub

Private Sub msfEmail_EnterCell()
    If msfEmail.Text = "" Or vLoad = True Or msfEmail.rows < 2 Then Exit Sub 'TI-4046
    

    txtSeq = msfEmail.TextMatrix(msfEmail.Row, 1) 'TI-4046
    txtEmail = msfEmail.TextMatrix(msfEmail.Row, 2) 'TI-4046
    txtContato = msfEmail.TextMatrix(msfEmail.Row, 3) 'TI-4046
    cboDepto = msfEmail.TextMatrix(msfEmail.Row, 4) 'TI-4046
    txtDtCadastro = msfEmail.TextMatrix(msfEmail.Row, 5) 'TI-4046
        
    If Trim(txtDtCadastro) = "" Then txtDtCadastro = txtDtAtual
        
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = txtEmail
            
    Frame1.Enabled = False
    Frame1.Caption = ""
    Botoes True, True, True, True, True, True
    
    cmdAlterar_Click
    
    txtContato.SetFocus
    
End Sub

Private Sub txtEmail_LostFocus()
    Me.cmdAdd.SetFocus
End Sub
'TI-4046
Private Sub msfEmail_DblClick()
    If msfEmail.rows < 2 Then Exit Sub
    
    If msfEmail.TextMatrix(msfEmail.Row, 0) = "N�o" Then
        msfEmail.TextMatrix(msfEmail.Row, 0) = "Sim"
    Else
        msfEmail.TextMatrix(msfEmail.Row, 0) = "N�o"
    End If
End Sub
Private Sub cmdEnviaSenha_Click()

    On Error GoTo Trata_Erro
    Dim i As Integer, intMarcado As Integer, intErro As Integer, strErro As String, intContErro As Integer
    
    intMarcado = 0
    intContErro = 0
    For i = 1 To msfEmail.rows - 1
        If msfEmail.TextMatrix(i, 0) = "Sim" Then
            intMarcado = intMarcado + 1
            vBanco.Parameters.Remove "PM_COD_CLIENTE"
            vBanco.Parameters.Add "PM_COD_CLIENTE", Val(txtCodCliente.Text), 1
            vBanco.Parameters.Remove "PM_NOME_CLIENTE"
            vBanco.Parameters.Add "PM_NOME_CLIENTE", lblNomeCliente.Caption, 1
            vBanco.Parameters.Remove "PM_EMAIL"
            vBanco.Parameters.Add "PM_EMAIL", msfEmail.TextMatrix(i, 2), 1
            vBanco.Parameters.Remove "PM_COD_ERRO"
            vBanco.Parameters.Add "PM_COD_ERRO", 0, 2
            vBanco.Parameters.Remove "PM_ERRO"
            vBanco.Parameters.Add "PM_ERRO", "", 2
        
            vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA230.PR_SEL_ENVIAR_SENHA(:PM_COD_CLIENTE,:PM_NOME_CLIENTE,:PM_EMAIL,:PM_COD_ERRO,:PM_ERRO); END;"
            
            strErro = IIf(IsNull(vBanco.Parameters("PM_ERRO").Value), "", vBanco.Parameters("PM_ERRO").Value)
            intErro = vBanco.Parameters("PM_COD_ERRO").Value
            If intErro <> 0 Then
                 intContErro = intContErro + 1
                 MessageBox 0, "Erro no envio para o e-mail: " & msfEmail.TextMatrix(i, 2) & vbCrLf & "Erro: " & strErro, "Aten��o", &H40000
            End If
        End If
    Next
    
    If intMarcado = 0 Then
        MessageBox 0, "Nenhum E-Mail foi selecionado para envio de senha.", "Aten��o", &H40000
    Else
        If intContErro = 0 Then MessageBox 0, "E-mail(s) enviado(s) com sucesso!", "Aten��o", &H40000
    End If

  
    Exit Sub
    
Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub cmdEnviaSenha_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Sub
'FIM TI-4046

'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
Function Verifica_Funcionario() As Boolean

    Verifica_Funcionario = False

    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    
    'Emerson - 14/03/18 - TI-6440 - Utilizar Owner PRODUCAO na package PCK_CAD_EMAIL...
    'vBanco.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_VERIFICA_FUNCIONARIO(:vCursor, :Cod_Cliente); END;"
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_VERIFICA_FUNCIONARIO(:vCursor, :Cod_Cliente); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
    
    If vRst!Funcionario = "SIM" Then Verifica_Funcionario = True
    
    Set vRst = Nothing

End Function
'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK

'TI-6488
Sub EnviarParaAprovacaoCAD060(pEmail As String, pCodCliente As Long)

On Error GoTo Trata_Erro

    vBanco.Parameters.Remove "PM_COD_CLIENTE"
    vBanco.Parameters.Remove "PM_EMAIL"
    vBanco.Parameters.Remove "PM_COD_USUARIO"
    
    vBanco.Parameters.Add "PM_COD_CLIENTE", pCodCliente, 1
    vBanco.Parameters.Add "PM_EMAIL", pEmail, 1
    vBanco.Parameters.Add "PM_COD_USUARIO", sCOD_VEND, 1
    
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_INS_CLIENTE_WEB(:PM_COD_CLIENTE,:PM_EMAIL, :PM_COD_USUARIO); END;"
    
Trata_Erro:

End Sub
'FIM TI-6488
