VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCesta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "LISTA DE EXCEL�NCIA (ITENS DIFERENCIADOS, PRIORIT�RIOS)"
   ClientHeight    =   5955
   ClientLeft      =   585
   ClientTop       =   1530
   ClientWidth     =   10125
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5955
   ScaleWidth      =   10125
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame ssfraPesq 
      Caption         =   "Pesquisa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   705
      Left            =   60
      TabIndex        =   14
      Top             =   5190
      Visible         =   0   'False
      Width           =   6465
      Begin VB.TextBox txtPesquisa 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   60
         TabIndex        =   15
         Top             =   300
         Width           =   5175
      End
      Begin Bot�o.cmd sscmdDesc 
         Height          =   315
         Left            =   5310
         TabIndex        =   16
         ToolTipText     =   "Buscar"
         Top             =   270
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "Descri��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCesta.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.TextBox txtVeiculo 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1650
      MaxLength       =   20
      TabIndex        =   10
      Top             =   1470
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Frame framCesta 
      Caption         =   "SEGMENTO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   615
      Left            =   30
      TabIndex        =   0
      Top             =   750
      Visible         =   0   'False
      Width           =   4695
      Begin VB.OptionButton optAcessorios 
         Appearance      =   0  'Flat
         Caption         =   "ACESSORIOS"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2880
         TabIndex        =   3
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton optFrotas 
         Appearance      =   0  'Flat
         Caption         =   "FROTAS"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton optPecas 
         Appearance      =   0  'Flat
         Caption         =   "PECAS"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   240
         Width           =   1215
      End
   End
   Begin MSGrid.Grid grdItem 
      Height          =   3135
      Left            =   60
      TabIndex        =   7
      Top             =   1770
      Visible         =   0   'False
      Width           =   10035
      _Version        =   65536
      _ExtentX        =   17701
      _ExtentY        =   5530
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmCesta.frx":001C
   End
   Begin Bot�o.cmd Command1 
      Height          =   675
      Left            =   30
      TabIndex        =   11
      ToolTipText     =   "Voltar"
      Top             =   0
      Visible         =   0   'False
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCesta.frx":0038
      PICN            =   "frmCesta.frx":0054
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   12
      Top             =   690
      Width           =   10110
      _ExtentX        =   17833
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdBuscar 
      Height          =   675
      Left            =   9330
      TabIndex        =   13
      ToolTipText     =   "Buscar"
      Top             =   0
      Visible         =   0   'False
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCesta.frx":0D2E
      PICN            =   "frmCesta.frx":0D4A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "VE�CULO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   690
      TabIndex        =   9
      Top             =   1470
      Visible         =   0   'False
      Width           =   810
   End
   Begin VB.Label lblObs 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ITENS CADASTRADOS NO �LTIMO M�S COM ESTOQUE DISPON�VEL"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   60
      TabIndex        =   8
      Top             =   4950
      Visible         =   0   'False
      Width           =   6270
   End
   Begin VB.Label lblSIGLA 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "WWWWWWWWWW"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2340
      TabIndex        =   6
      Top             =   1410
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.Label lblCod_fornecedor 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1620
      TabIndex        =   5
      Top             =   1410
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblForn 
      BackStyle       =   0  'Transparent
      Caption         =   "FORNECEDOR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   4
      Top             =   1410
      Visible         =   0   'False
      Width           =   1455
   End
End
Attribute VB_Name = "frmCesta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmCesta
' Author    : c.samuel.oliveira
' Date      : 08/09/2017
' Purpose   : TI-6092
'---------------------------------------------------------------------------------------

Option Explicit

Public bTipo_Cesta As Byte
Public strPesquisa As String

'VARIAVEIS UTILIZADAS PARA PESQUISA
Public CONTINUAPESQUISA As Integer
Public OCORRENCIA As Integer
Public CONTADOROCORRENCIA As Long
Public TEXTOPARAPROCURA As String
Public INICIOPESQUISA As Long
Public INILINHA As Long
Public tot_grdfabr As Long

Private Sub cmdBuscar_Click()

1         On Error GoTo Trata_Erro

2         If strTipo_Tela = "V" And txtVeiculo = "" Then
3             NonStayOnTop Me
4             MessageBox 0, "Digite um ve�culo, para efetuar a consulta", "Aten�ao", &H40000 + 0
5             StayOnTop Me

6             Exit Sub
7         End If

8         If strTipo_Tela = "V" And txtVeiculo <> "" Then
9             Screen.MousePointer = 11
10            Call Carrega_Kit(txtVeiculo)
11            ssfraPesq.Visible = True
12            Screen.MousePointer = 0
13        Else
14            ssfraPesq.Visible = False
15            If bTipo_Cesta = 0 Then
16                NonStayOnTop Me
17                MessageBox 0, "Selecione um segmento para fazer a busca", "Aten�ao", &H40000 + 0
18                StayOnTop Me

19                Exit Sub
20            End If

21            If Verifica_Cesta(bTipo_Cesta) Then
22                Screen.MousePointer = 11
23                Call Carrega_Itens(bTipo_Cesta)
24                Screen.MousePointer = 0
25            Else
26                NonStayOnTop Me
27                MessageBox 0, "Lista de Itens n�o dispon�vel", "Aten�ao", &H40000 + 0
28                StayOnTop Me

29                Exit Sub
30            End If
31        End If

Trata_Erro:
32        If Err.Number <> 0 Then
33           MessageBox 0, "Sub cmdBuscar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
34        End If

End Sub

Private Sub Command1_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    
10        On Error GoTo Trata_Erro

          Dim ss As Object
20        Screen.MousePointer = 11

30        If strTipo_Tela = "D" Then
40            If intSegmento_Vend = 1 Then
50              optPecas.Value = 1
'                bTipo_Cesta = 1
60            ElseIf intSegmento_Vend = 2 Then
70              optFrotas.Value = 1
80              bTipo_Cesta = 2
90            Else
100             optAcessorios = 1
110             bTipo_Cesta = 3
120           End If
               'cmdBuscar_Click



130           frmCesta.Caption = "LISTA DE EXCEL�NCIA (ITENS DIFERENCIADOS, PRIORIT�RIOS)"
140           framCesta.Visible = True
150           lblCod_fornecedor.Visible = True
160           lblForn = True
170           lblSIGLA = True
180           cmdBuscar.Visible = True
190           Command1.Visible = True
200           lblObs.Visible = False
210           Label1.Visible = False
220           txtVeiculo.Visible = False

230           bTipo_Cesta = 0
240           If frmVenda.txtCOD_FORNECEDOR <> "" Then
250               frmCesta.lblForn.Visible = False
260               frmCesta.lblSIGLA.Visible = False
270               frmCesta.lblCod_fornecedor.Visible = False

280               frmCesta.lblCod_fornecedor = frmVenda.txtCOD_FORNECEDOR
290               frmCesta.lblSIGLA = frmVenda.lblSIGLA
300               DoEvents
310           Else
320               frmCesta.lblForn.Visible = False
330               frmCesta.lblSIGLA.Visible = False
340               frmCesta.lblCod_fornecedor.Visible = False

350               frmCesta.lblCod_fornecedor = ""
360               frmCesta.lblSIGLA = ""
370               DoEvents
380           End If

390       ElseIf strTipo_Tela = "N" Then
400           frmCesta.Caption = "LAN�AMENTOS" 'TI-6092
410           framCesta.Visible = False
420           lblCod_fornecedor.Visible = False
430           lblForn = False
440           lblSIGLA = False
450           cmdBuscar.Visible = False
460           Command1.Visible = True
470           lblObs.Visible = True
480           lblObs.Caption = "Itens cadastrados nos �ltimos 6 meses com estoque dispon�vel" 'TI-6092
490           Label1.Visible = False
500           txtVeiculo.Visible = False

510           OraParameters.Remove "loja"
520           OraParameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1
530           OraParameters.Remove "vErro"
540           OraParameters.Add "vErro", 0, 2

550           Screen.MousePointer = 11

560           vSql = "Begin producao.pck_vda230.pr_itens_novos(:vCursor,:loja,:vErro);END;"

              'Criar_Cursor
570           oradatabase.ExecuteSQL vSql
580           Set ss = oradatabase.Parameters("vCursor").Value
              'oradatabase.Parameters.Remove ("vCursor")

590           If oradatabase.Parameters("vErro").Value <> 0 Then
600               Screen.MousePointer = 0
610               Call Process_Line_Errors(Sql)
620               Exit Sub
630           End If


640           If ss.EOF And ss.BOF Then
650               NonStayOnTop Me
660               MessageBox 0, "N�o h� lan�amentos", "Aten�ao", &H40000 + 0
670               StayOnTop Me

680           Else
690               With frmCesta.grdItem
700                   .Cols = 6 'TI-6092
710                   .rows = ss.RecordCount + 1
720                   .ColWidth(0) = 660
730                   .ColWidth(1) = 1500
740                   .ColWidth(2) = 1500
750                   .ColWidth(3) = 2500
760                   .ColWidth(4) = 800
770                   .ColWidth(5) = 2700 'TI-6092

780                   .Row = 0
790                   .Col = 0
800                   .Text = "Cod"
810                   .Col = 1
820                   .Text = "Forn"
830                   .Col = 2
840                   .Text = "Fabrica"
850                   .Col = 3
860                   .Text = "Descri��o"
870                   .Col = 4
880                   .Text = "DPK"
890                   .Col = 5 'TI-6092
900                   .Text = "Aplica��o" 'TI-6092

910                   ss.MoveFirst
920                   For i = 1 To .rows - 1
930                       .Row = i

940                       .Col = 0
950                       .Text = ss!cod_fornecedor
960                       .Col = 1
970                       .Text = ss!sigla
980                       .Col = 2
990                       .Text = ss!cod_fabrica
1000                      .Col = 3
1010                      .Text = ss!desc_item
1020                      .Col = 4
1030                      .Text = ss!cod_dpk
1040                      .Col = 5
1050                      .Text = IIf(IsNull(ss!APLICACAO_REDUZIDA), "", ss!APLICACAO_REDUZIDA) 'TI-6092

1060                      ss.MoveNext
1070                  Next
1080                  .Row = 1
1090              End With
1100              frmCesta.grdItem.Visible = True
1110          End If

1120      ElseIf strTipo_Tela = "V" Then
1130          frmCesta.Caption = "KIT POR VEICULO"
1140          framCesta.Visible = False
1150          lblCod_fornecedor.Visible = False
1160          lblForn = False
1170          lblSIGLA = False
1180          cmdBuscar.Visible = True
1190          Command1.Visible = True
1200          lblObs.Visible = False
1210          Label1.Visible = True
1220          txtVeiculo.Visible = True

1230          bTipo_Cesta = 9

1240      End If

1250      Screen.MousePointer = 0

1260    Set ss = Nothing

Trata_Erro:
1270      If Err.Number <> 0 Then
1280   MessageBox 0, "Sub Form_load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
1290      End If
Screen.MousePointer = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmCesta = Nothing
End Sub

Private Sub grdItem_DblClick()
    grdItem.Col = 4

    frmVenda.txtCOD_DPK = grdItem.Text
    frmVenda.txtCOD_DPK_LostFocus

    If strTipo_Tela = "V" Then
        frmCesta.Hide
    Else
        Unload Me
    End If

End Sub

Private Sub optAcessorios_Click()
    grdItem.Visible = False
    bTipo_Cesta = 3
     cmdBuscar_Click
End Sub

Private Sub optFrotas_Click()
    grdItem.Visible = False
    bTipo_Cesta = 2
     cmdBuscar_Click
End Sub

Private Sub optPecas_Click()
    grdItem.Visible = False
    bTipo_Cesta = 1
     cmdBuscar_Click
End Sub

Public Function Verifica_Cesta(tipo_cesta As Byte) As Boolean
              
    On Error GoTo Trata_Erro
          
          Dim ss As Object

1         OraParameters.Remove "tp"
2         OraParameters.Add "tp", tipo_cesta, 1
3         OraParameters.Remove "loja"
4         OraParameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1

5         'Criar_Cursor
6         oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Select_Dt_Vigencia(:vCursor, :Tp, :Loja); END;"
7         Set ss = oradatabase.Parameters("vCursor").Value
8         'oradatabase.Parameters.Remove ("vCursor")

9         If (ss.BOF And ss.EOF) Or IsNull(ss!dt_vigencia) Then
10            Verifica_Cesta = False
11        Else
12            If ss!dt_vigencia > Format(Data_Faturamento, "YYYYMMDD") Then
13                Verifica_Cesta = False
14            Else
15                Verifica_Cesta = True
16            End If
17        End If

    Set ss = Nothing

Trata_Erro:
    If Err.Number <> 0 Then
       MessageBox 0, "Sub Verifica_Cesta" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
    End If

End Function

Public Sub Carrega_Itens(tipo_cesta As Byte)
          
    On Error GoTo Trata_Erro
          
          Dim ss As Object

1         OraParameters.Remove "loja"
2         OraParameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1
3         OraParameters.Remove "tp"
4         OraParameters.Add "tp", tipo_cesta, 1
5         OraParameters.Remove "forn"
6         OraParameters.Add "forn", lblCod_fornecedor, 1
7         OraParameters.Remove "vErro"
8         OraParameters.Add "vErro", 0, 2

9         vSql = "Begin producao.pck_vda230.pr_cesta_item(:vCursor,:loja,:tp,:vErro);END;"

10        'Criar_Cursor
11        oradatabase.ExecuteSQL vSql
12        Set ss = oradatabase.Parameters("vCursor").Value
13        'oradatabase.Parameters.Remove ("vCursor")

14        If oradatabase.Parameters("vErro").Value <> 0 Then
15            Call Process_Line_Errors(Sql)
16            Exit Sub
17        End If

18        If ss.EOF And ss.BOF Then
19            NonStayOnTop Me
20            MessageBox 0, "Lista n�o dispon�vel, em breve itens com condi��es diferenciadas", "Aten�ao", &H40000 + 0
21            StayOnTop Me

22        Else
23            With frmCesta.grdItem
24                .Cols = 5
25                .rows = ss.RecordCount + 1
26                .ColWidth(0) = 660
27                .ColWidth(1) = 1500
28                .ColWidth(2) = 1500
29                .ColWidth(3) = 2500
30                .ColWidth(4) = 930

31                .Row = 0
32                .Col = 0
33                .Text = "Cod"
34                .Col = 1
35                .Text = "Forn"
36                .Col = 2
37                .Text = "Fabrica"
38                .Col = 3
39                .Text = "Descri��o"
40                .Col = 4
41                .Text = "DPK"

42                ss.MoveFirst
43                For i = 1 To .rows - 1
44                    .Row = i

45                    .Col = 0
46                    .Text = ss!cod_fornecedor
47                    .Col = 1
48                    .Text = ss!sigla
49                    .Col = 2
50                    .Text = ss!cod_fabrica
51                    .Col = 3
52                    .Text = ss!desc_item
53                    .Col = 4
54                    .Text = ss!cod_dpk

55                    ss.MoveNext
56                Next
57                .Row = 1
58            End With
59            frmCesta.grdItem.Visible = True
60        End If

    Set ss = Nothing

Trata_Erro:
    If Err.Number <> 0 Then
       MessageBox 0, "Sub Carrega_Itens" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
    End If

End Sub

Public Sub Carrega_Kit(strVeiculo As String)
          
    On Error GoTo Trata_Erro
          
          Dim ss As Object
          Dim i As Long
          Dim strFabr As String
          Dim bAltura As Byte

1         OraParameters.Remove "loja"
2         OraParameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1
3         OraParameters.Remove "veiculo"
4         OraParameters.Add "veiculo", "%" & strVeiculo & "%", 1
5         OraParameters.Remove "vErro"
6         OraParameters.Add "vErro", 0, 2

7         tot_grdfabr = 0

8         Screen.MousePointer = 11

9         vSql = "Begin producao.pck_vda230.pr_kit_veiculo(:vCursor,:loja,:veiculo,:vErro);END;"

10        'Criar_Cursor
11        oradatabase.ExecuteSQL vSql
12        Set ss = oradatabase.Parameters("vCursor").Value
13        'oradatabase.Parameters.Remove ("vCursor")

14        If oradatabase.Parameters("vErro").Value <> 0 Then
15            Screen.MousePointer = 0
16            Call Process_Line_Errors(Sql)
17            Exit Sub
18        End If

19        tot_grdfabr = ss.RecordCount
20        If ss.EOF And ss.BOF Then
21            NonStayOnTop Me
22            MessageBox 0, "N�o h� itens para este ve�culo", "Aten�ao", &H40000 + 0
23            StayOnTop Me

24            Screen.MousePointer = 0
25        Else
26            With frmCesta.grdItem
27                .Cols = 6
                  '.Rows = ss.RecordCount + 1
28                .rows = 1
29                .ColWidth(0) = 660
30                .ColWidth(1) = 1500
31                .ColWidth(2) = 1500
32                .ColWidth(3) = 2500
33                .ColWidth(4) = 800
34                .ColWidth(5) = 3870


35                .Row = 0
36                .Col = 0
37                .Text = "Cod"
38                .Col = 1
39                .Text = "Forn"
40                .Col = 2
41                .Text = "Fabrica"
42                .Col = 3
43                .Text = "Descri��o"
44                .Col = 4
45                .Text = "DPK"
46                .Col = 5
47                .Text = "APLICA��O"

48                ss.MoveFirst
49                strFabr = ""

50                For i = 1 To ss.RecordCount
                      'cria linha
51                    If strFabr <> ss!cod_fabrica Then
                          'cria linha
52                        .rows = .rows + 1
53                        .Row = .rows - 1
54                        .Col = 0
55                        .Text = ss!cod_fornecedor
56                        .Col = 1
57                        .Text = ss!sigla
58                        .Col = 2
59                        .Text = ss!cod_fabrica
60                        .Col = 3
61                        .Text = ss!desc_item
62                        .Col = 4
63                        .Text = ss!cod_dpk
64                        .Col = 5
65                        If IsNull(ss!desc_aplicacao) Then
66                            .Text = ""
67                        Else
68                            .Text = Trim$(ss!desc_aplicacao)
69                        End If
                          'carrega codigo de fabrica
70                        strFabr = ss!cod_fabrica
71                    Else
                          'carrega aplicacao
72                        If Not IsNull(ss!desc_aplicacao) Then
73                            .Col = 5
74                            .Text = .Text & " " & Trim$(ss!desc_aplicacao)
75                            bAltura = Len(Trim$(.Text)) \ 30
76                            If Len(Trim$(.Text)) > 30 And Len(Trim$(.Text)) Mod 30 > 0 Then
77                                bAltura = bAltura + 1
78                            End If
79                            If bAltura > 1 Then
80                                .RowHeight(.Row) = bAltura * 225
81                            End If
82                        End If
83                    End If

84                    ss.MoveNext

85                Next
86                .Row = 1
87                .FixedRows = 1
88            End With
89            Screen.MousePointer = 0
90            frmCesta.grdItem.Visible = True
91        End If

    Set ss = Nothing

Trata_Erro:
    If Err.Number <> 0 Then
       MessageBox 0, "Sub GRid1_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
    End If

End Sub

Private Sub sscmdDesc_Click()
1         On Error GoTo TopRowError
          'N�mero da coluna onde a pesquisa deve agir
2         grdItem.Col = 3

          'texto que dever� ser encontrado
3         TEXTOPARAPROCURA = Trim(txtPesquisa.Text)

          'desabilita o highlight
4         grdItem.HighLight = False

          'posiciona no in�cio do grid
5         grdItem.Row = 1
6         grdItem.TopRow = 1

7         For INICIOPESQUISA = CONTINUAPESQUISA + 1 To tot_grdfabr
8             grdItem.Row = INICIOPESQUISA

              'verifica se a string digitada est� contida no texto da aplica�ao
9             If InStr(grdItem.Text, TEXTOPARAPROCURA) <> 0 Then
                  'define nova ocorr�ncia
10                OCORRENCIA = OCORRENCIA + 1
11            End If

12            If InStr(grdItem.Text, TEXTOPARAPROCURA) <> 0 And OCORRENCIA > CONTADOROCORRENCIA Then
                  'vari�vel que controla quantas vezes determinado string j� foi localizado
13                CONTADOROCORRENCIA = CONTADOROCORRENCIA + 1

                  'liga o highlight
14                grdItem.HighLight = True
15                grdItem.SelStartRow = INICIOPESQUISA
16                grdItem.SelEndRow = INICIOPESQUISA
17                grdItem.SelStartCol = 0
18                grdItem.SelEndCol = 5

                  'se encontrou o texto posiciona a linha do grid
19                If grdItem.CellSelected Then
20                    If INICIOPESQUISA > 1 Then
21                        For INILINHA = 1 To INICIOPESQUISA - 1
                              'o m�ximo valor que toprow pode ter deve ser igual ao numero de
                              'linhas total do grid menos o numero de linhas visiveis do grid
22                            If grdItem.TopRow = grdItem.Row - tot_grdfabr Then
23                                grdItem.TopRow = grdItem.rows - tot_grdfabr
24                            Else
25                                grdItem.TopRow = grdItem.TopRow + 1
26                            End If
27                        Next
28                    End If
29                End If

                  'a proxima pesquisa continuara a partir desta variavel
30                CONTINUAPESQUISA = INICIOPESQUISA
31                sscmdDesc.Caption = "Pr�xima"
32                Exit Sub
33            End If
34        Next

35        NonStayOnTop Me
36        MessageBox 0, "Fim de Pesquisa", "Aten�ao", &H40000 + 0
37        StayOnTop Me

38        sscmdDesc.Caption = "Descri��o"
39        Exit Sub

TopRowError:
40        If Err = 30009 Then
41            grdItem.TopRow = 1
42            Resume Next
43         ElseIf Err.Number <> 0 Then
44            MessageBox 0, "Sub GRid1_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
45         End If


End Sub

Private Sub txtPesquisa_GotFocus()
    'reset nas posi�oes do grid
    frmCesta.grdItem.HighLight = False
    grdItem.TopRow = 1
    grdItem.Row = 1
    grdItem.Col = 2

    'Variaveis que definem inicio ou continuacao da pesquisa

    CONTINUAPESQUISA = 0
    OCORRENCIA = 0
    CONTADOROCORRENCIA = 0
End Sub


Private Sub txtPesquisa_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub

Private Sub txtVeiculo_Change()
    If txtVeiculo = "" Then
        grdItem.Visible = False
    End If
End Sub

Private Sub txtVeiculo_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

