VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDesem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Desempenho"
   ClientHeight    =   6705
   ClientLeft      =   165
   ClientTop       =   1320
   ClientWidth     =   11550
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6705
   ScaleWidth      =   11550
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid grdTime 
      Height          =   2010
      Left            =   60
      TabIndex        =   14
      Top             =   2580
      Width           =   11460
      _ExtentX        =   20214
      _ExtentY        =   3545
      _Version        =   393216
      ForeColor       =   12582912
      AllowUserResizing=   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "PEDIDOS"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1155
      Left            =   2910
      TabIndex        =   0
      Top             =   810
      Width           =   4905
      Begin VB.Image Image1 
         Height          =   480
         Left            =   4170
         Picture         =   "frmDesem.frx":0000
         Top             =   180
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label lblAtingido 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4200
         TabIndex        =   13
         Top             =   750
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "%ATINGIDO"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   3090
         TabIndex        =   12
         Top             =   810
         Visible         =   0   'False
         Width           =   1080
      End
      Begin VB.Label lblQuota 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1770
         TabIndex        =   11
         Top             =   810
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "VALOR QUOTA DIA"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   10
         Top             =   870
         Visible         =   0   'False
         Width           =   1650
      End
      Begin VB.Label lblValor 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1770
         TabIndex        =   4
         Top             =   540
         Width           =   1215
      End
      Begin VB.Label lblQtd 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1770
         TabIndex        =   3
         Top             =   270
         Width           =   1215
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "VALOR CONT�BIL"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   2
         Top             =   570
         Width           =   1545
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "QUANTIDADE"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   1
         Top             =   300
         Width           =   1170
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdFora_Time 
      Height          =   1830
      Left            =   90
      TabIndex        =   17
      Top             =   4860
      Width           =   11460
      _ExtentX        =   20214
      _ExtentY        =   3228
      _Version        =   393216
      ForeColor       =   12582912
   End
   Begin Bot�o.cmd SSCommand2 
      Height          =   675
      Left            =   30
      TabIndex        =   20
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDesem.frx":030A
      PICN            =   "frmDesem.frx":0326
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   21
      Top             =   720
      Width           =   11490
      _ExtentX        =   20267
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblDt_Base 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   2520
      TabIndex        =   19
      Top             =   2070
      Width           =   60
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "DATA DE REFER�NCIA"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   2070
      Width           =   2190
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "FORA DO TIME"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   45
      TabIndex        =   16
      Top             =   4620
      Width           =   1485
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "TIME"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   75
      TabIndex        =   15
      Top             =   2355
      Width           =   570
   End
   Begin VB.Label lblPseudonimo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   6000
      TabIndex        =   9
      Top             =   120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCod_vend 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5280
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "VENDEDOR"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3240
      TabIndex        =   7
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblDt_Faturamento 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5280
      TabIndex        =   6
      Top             =   390
      Width           =   855
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "DATA FATURAMENTO"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3240
      TabIndex        =   5
      Top             =   390
      Width           =   1815
   End
End
Attribute VB_Name = "frmDesem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub SSCommand1_Click()
          Dim Sql As String
          Dim i As Long

1         Screen.MousePointer = 11

2         On Error GoTo Trata_Erro

'3         Set OraParameters = oradatabase.Parameters

4         'OraParameters.Remove "vCursor"
5         OraParameters.Add "vCursor", 0, 2
6         OraParameters("vCursor").serverType = ORATYPE_CURSOR
7         OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
8         OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

9         OraParameters.Remove "fil"
10        OraParameters.Add "fil", Mid(FILIAL_PED, 1, 4), 1

11        OraParameters.Remove "vErro"
12        OraParameters.Add "vErro", 0, 2
13        OraParameters("vErro").serverType = ORATYPE_NUMBER

14        vSql = "Begin " & strTabela_Banco & "pck_vda630.pr_desempenho(:vCursor,:fil,:vErro);END;"

15        'Criar_Cursor
16        oradatabase.ExecuteSQL vSql
17        Set VarRec = oradatabase.Parameters("vCursor").Value
18        'oradatabase.Parameters.Remove ("vCursor")

19        If oradatabase.Parameters("vErro").Value <> 0 Then
20            Call Process_Line_Errors(Sql)
21            Exit Sub
22        End If

23        With frmRank.Grid1
24            .Cols = 5
25            .rows = VarRec.RecordCount + 1
26            .ColWidth(0) = 500
27            .ColWidth(1) = 1200
28            .ColWidth(2) = 900
29            .ColWidth(3) = 1000
30            .ColWidth(4) = 900

31            .Row = 0
32            .Col = 0
33            .Text = "RANK."
34            .Col = 1
35            .Text = "TLMKT"
36            .Col = 2
37            .Text = "  ATIVA"
38            .Col = 3
39            .Text = "RECEPTIVA"
40            .Col = 4
41            .Text = "  TOTAL"

42            VarRec.MoveFirst
43            For i = 1 To VarRec.RecordCount
                  ' .Rows -1
44                .Row = i

45                .Col = 0
                  '.ColAlignment(0) = 1
46                .Text = i
47                .Col = 1
48                .ColAlignment(1) = 0
49                .Text = CStr(VarRec!pseudonimo)
50                .Col = 2
51                .ColAlignment(2) = 1
52                .Text = CStr(VarRec!ativa)
53                .Col = 3
54                .ColAlignment(3) = 1
55                .Text = CStr(VarRec!receptiva)
56                .Col = 4
57                .ColAlignment(4) = 1
58                .Text = CStr(VarRec!total)
59                VarRec.MoveNext
60            Next
61            .Row = 1
62        End With
63        frmRank.Grid1.Visible = True
64        Screen.MousePointer = 0
65        frmRank.Show
66        StayOnTop frmRank

67        Exit Sub
Trata_Erro:
    If Err = 30009 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MessageBox 0, "Sub Grid1_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If

End Sub

Private Sub Form_Load()
          
1         On Error GoTo Trata_Erro

          Dim intLinha_Time As Integer
          Dim intLinha_Fora_Time As Integer
          Dim ss As Object
          Dim i As Long
          Dim dblVda_Diaria As Double
          Dim dblCota_Ajustada As Double
          Dim dblVda_Acum As Double
          Dim dblCota As Double
          Dim lngNF_Dia As Double
          Dim lngNF_Acum As Double

          Dim dblLC_Diario As Double
          Dim dblBVista_Diario As Double
          Dim dblLC_Acum As Double
          Dim dblBVista_Acum As Double
          Dim dblCota_Total As Double

        'Emerson - 15/01/18 - TI-6313-Branch 2 - Melhorias CRM - Comiss�o On Line - Mudan�a de ESCOPO...
        Dim wstrComissaoMensal As String
    
        wstrComissaoMensal = Busca_Comissao(sCOD_VEND)
    
        If Trim(wstrComissaoMensal) <> "" Then
            frmDesem.Caption = wstrComissaoMensal & "          Desempenho"
        End If
        'Emerson - 15/01/18 - TI-6313-Branch 2 - Melhorias CRM - Comiss�o On Line - Mudan�a de ESCOPO...

2         dblVda_Diaria = 0
3         dblCota_Ajustada = 0
4         dblVda_Acum = 0
5         dblCota = 0
6         lngNF_Dia = 0
7         lngNF_Acum = 0
8         dblCota_Total = 0

9         dblLC_Diario = 0
10        dblBVista_Diario = 0
11        dblLC_Acum = 0
12        dblBVista_Acum = 0

13        OraParameters.Remove "vend"
14        OraParameters.Add "vend", sCOD_VEND, 1

15        OraParameters.Remove "vErro"
16        OraParameters.Add "vErro", 0, 2

17        vSql = "Begin producao.pck_vda230.pr_sav(:vCursor,:vend,:vErro);END;"

18        'Criar_Cursor
19        oradatabase.ExecuteSQL vSql
20        Set ss = oradatabase.Parameters("vCursor").Value
21        'oradatabase.Parameters.Remove ("vCursor")

22        If OraParameters("vErro") <> 0 Then
23            MessageBox 0, "Sub Form_Load" & vbCrLf & "Descri��o:" & "C�digo:" & Err.Number & vbCrLf & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
24        End If

25        If ss.EOF And ss.BOF Then
26            grdTime.Visible = False
27            grdFora_Time.Visible = False
28            NonStayOnTop Me
29            MessageBox 0, "N�o h� informa��es dispon�veis sobre o desempenho mensal", "Aten�ao", &H40000 + 0
30            StayOnTop Me

31            Exit Sub
32        End If

33        For i = 1 To ss.RecordCount
34            If ss!Time = "1" Then
35                intLinha_Time = intLinha_Time + 1
36            Else
37                intLinha_Fora_Time = intLinha_Fora_Time + 1
38            End If
39            ss.MoveNext
40        Next
41        ss.MoveFirst
42        lblDt_Base = ss(21)
43        grdTime.Clear
44        grdFora_Time.Clear

45        With frmDesem.grdTime
46            .Cols = 15
47            .rows = intLinha_Time + 2
48            .ColWidth(0) = 600
49            .ColWidth(1) = 1100
50            .ColWidth(2) = 600
51            .ColWidth(3) = 1100
52            .ColWidth(4) = 1000
53            .ColWidth(5) = 1000
54            .ColWidth(6) = 400
55            .ColWidth(7) = 1000
56            .ColWidth(8) = 900
57            .ColWidth(9) = 400
58            .ColWidth(10) = 800
59            .ColWidth(11) = 700
60            .ColWidth(12) = 700
61            .ColWidth(13) = 400
62            .ColWidth(14) = 400

63            .Row = 0
64            .Col = 0
65            .Text = "VEND"
66            .Col = 1
67            .Text = "PSEUD"
68            .Col = 2
69            .Text = "REPR"
70            .Col = 3
71            .Text = "PSEUD"
72            .Col = 4
73            .Text = "VDA DIARIA"
74            .Col = 5
75            .Text = "COTA AJUS."
76            .Col = 6
77            .Text = "PC"
78            .Col = 7
79            .Text = "VDA ACUM"
80            .Col = 8
81            .Text = "COTA A."
82            .Col = 9
83            .Text = "PC"
84            .Col = 10
85            .Text = "COTA T."
86            .Col = 11
87            .Text = "QT NF D"
88            .Col = 12
89            .Text = "QT NF A"
90            .Col = 13
91            .Text = "MD"
92            .Col = 14
93            .Text = "MA"

94            For i = 1 To intLinha_Time
95                .Row = i
96                .Col = 0
97                If (ss(5) = 1 Or ss(5) = 3) Then
98                    .Text = ss(1)
99                Else
100                   .Text = ""
101               End If
102               .Col = 1
103               If (ss(5) = 1 Or ss(5) = 3) Then
104                   .Text = ss(2)
105               Else
106                   .Text = ""
107               End If
108               .Col = 2
109               .Text = ss(3)
110               .Col = 3
111               .Text = IIf(IsNull(ss(4)), "", ss(4))
112               .Col = 4
113               .Text = Round(CStr(ss(6)), 0)
114               dblVda_Diaria = dblVda_Diaria + CDbl(CStr(ss(6)))
115               .Col = 5
116               .Text = Round(CStr(ss(7)), 0)
117               dblCota_Ajustada = dblCota_Ajustada + CDbl(CStr(ss(7)))
118               .Col = 6
119               If ss(8) < 100 Then
120                   .CellForeColor = vbRed
121               Else
122                   .CellForeColor = vbBlue
123               End If
124               .Text = ss(8)
125               .Col = 7
126               .Text = Round(CStr(ss(9)), 0)
127               dblVda_Acum = dblVda_Acum + CDbl(CStr(ss(9)))
128               .Col = 8
129               .Text = Round(CStr(ss(10)), 0)
130               dblCota = dblCota + CDbl(CStr(ss(10)))
131               .Col = 9
132               If ss(11) < 100 Then
133                   .CellForeColor = vbRed
134               Else
135                   .CellForeColor = vbBlue
136               End If
137               .Text = ss(11)
138               .Col = 10
139               .Text = IIf(IsNull(ss(20)), 0, ss(20))
140               .Col = 11
141               .Text = IIf(IsNull(ss(14)), 0, ss(14))
142               lngNF_Dia = lngNF_Dia + IIf(IsNull(CStr(ss(14))), 0, CStr(ss(14)))
143               .Col = 12
144               .Text = IIf(IsNull(ss(15)), 0, ss(15))
145               lngNF_Acum = lngNF_Acum + IIf(IsNull(CStr(ss(15))), 0, CStr(ss(15)))
146               .Col = 13
147               .Text = Round(CStr(IIf(IsNull(ss(12)), 0, ss(12))), 0)
148               .Col = 14
149               .Text = Round(CStr(IIf(IsNull(ss(13)), 0, ss(13))), 0)

150               dblLC_Diario = dblLC_Diario + CDbl(ss(16))
151               dblBVista_Diario = dblBVista_Diario + CDbl(ss(18))
152               dblLC_Acum = dblLC_Acum + CDbl(ss(17))
153               dblBVista_Acum = dblBVista_Acum + CDbl(ss(19))
154               dblCota_Total = dblCota_Total + CDbl(ss(20))

155               ss.MoveNext
156           Next

157           .Row = intLinha_Time + 1
158           .Col = 0
159           .Text = ""
160           .Col = 1
161           .CellForeColor = vbBlue
162           .Text = "TOTAL"
163           .Col = 2
164           .Text = ""
165           .Col = 3
166           .Text = ""
167           .Col = 4
168           .Text = Round(dblVda_Diaria, 0)
169           .Col = 5
170           .Text = Round(dblCota_Ajustada, 0)
171           .Col = 6
172           If dblVda_Diaria = 0 Or dblCota_Ajustada = 0 Then
173               .Text = "0"
174               .CellForeColor = vbRed
175           Else
176               If Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0) < 100 Then
177                   .CellForeColor = vbRed
178               Else
179                   .CellForeColor = vbBlue
180               End If
181               .Text = Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0)
182           End If
183           .Col = 7
184           .Text = Round(dblVda_Acum, 0)
185           .Col = 8
186           .Text = Round(dblCota, 0)
187           .Col = 9
188           If dblVda_Acum = 0 Or dblCota = 0 Then
189               .CellForeColor = vbRed
190               .Text = "0"
191           Else
192               If Round((dblVda_Acum) / (dblCota) * 100, 0) < 100 Then
193                   .CellForeColor = vbRed
194               Else
195                   .CellForeColor = vbBlue
196               End If
197               .Text = Round((dblVda_Acum) / (dblCota) * 100, 0)
198           End If
199           .Col = 10
200           .Text = dblCota_Total
201           .Col = 11
202           .Text = lngNF_Dia
203           .Col = 12
204           .Text = lngNF_Acum
205           .Col = 13
206           If dblLC_Diario = 0 Or dblBVista_Diario = 0 Then
207               .Text = "0"
208           Else
209               .Text = Round((dblLC_Diario / dblBVista_Diario) * 100, 0)
210           End If
211           .Col = 14
212           If dblLC_Acum = 0 Or dblBVista_Acum = 0 Then
213               .Text = "0"
214           Else
215               .Text = Round((CDbl(dblLC_Acum) / CDbl(dblBVista_Acum)) * 100, 0)
216           End If

217       End With

218       dblVda_Diaria = 0
219       dblCota_Ajustada = 0
220       dblVda_Acum = 0
221       dblCota = 0
222       lngNF_Dia = 0
223       lngNF_Acum = 0
224       dblCota_Total = 0

225       dblLC_Diario = 0
226       dblBVista_Diario = 0
227       dblLC_Acum = 0
228       dblBVista_Acum = 0

229       With frmDesem.grdFora_Time
230           .Cols = 15
231           .rows = intLinha_Fora_Time + 2
232           .ColWidth(0) = 600
233           .ColWidth(1) = 1100
234           .ColWidth(2) = 600
235           .ColWidth(3) = 1100
236           .ColWidth(4) = 1000
237           .ColWidth(5) = 1000
238           .ColWidth(6) = 400
239           .ColWidth(7) = 1000
240           .ColWidth(8) = 900
241           .ColWidth(9) = 400
242           .ColWidth(10) = 800
243           .ColWidth(11) = 700
244           .ColWidth(12) = 700
245           .ColWidth(13) = 400
246           .ColWidth(14) = 400

247           .Row = 0
248           .Col = 0
249           .Text = "VEND"
250           .Col = 1
251           .Text = "PSEUD"
252           .Col = 2
253           .Text = "REPR"
254           .Col = 3
255           .Text = "PSEUD"
256           .Col = 4
257           .Text = "VDA DIARIA"
258           .Col = 5
259           .Text = "COTA AJUS."
260           .Col = 6
261           .Text = "PC"
262           .Col = 7
263           .Text = "VDA ACUM"
264           .Col = 8
265           .Text = "COTA A."
266           .Col = 9
267           .Text = "PC"
268           .Col = 10
269           .Text = "COTA T."
270           .Col = 11
271           .Text = "QT NF D"
272           .Col = 12
273           .Text = "QT NF A"
274           .Col = 13
275           .Text = "MD"
276           .Col = 14
277           .Text = "MA"

278           For i = 1 To intLinha_Fora_Time
279               .Row = i
280               .Col = 0
281               .Text = IIf(IsNull(ss(1)), 0, ss(1))
282               .Col = 1
283               .Text = IIf(IsNull(ss(2)), 0, ss(2))
284               .Col = 2
285               .Text = IIf(IsNull(ss(3)), 0, ss(3))
286               .Col = 3
287               .Text = IIf(IsNull(ss(4)), "", ss(4))
288               .Col = 4
289               .Text = Round(CStr(ss(6)), 0)
290               dblVda_Diaria = dblVda_Diaria + CDbl(CStr(ss(6)))
291               .Col = 5
292               .Text = Round(CStr(ss(7)), 0)
293               dblCota_Ajustada = dblCota_Ajustada + CDbl(CStr(ss(7)))
294               .Col = 6
295               If ss(8) < 100 Then
296                   .CellForeColor = vbRed
297               Else
298                   .CellForeColor = vbBlue
299               End If
300               .Text = ss(8)
301               .Col = 7
302               .Text = Round(CStr(ss(9)), 0)
303               dblVda_Acum = dblVda_Acum + CDbl(CStr(ss(9)))
304               .Col = 8
305               .Text = Round(CStr(ss(10)), 0)
306               dblCota = dblCota + CDbl(CStr(ss(10)))
307               .Col = 9
308               If ss(11) < 100 Then
309                   .CellForeColor = vbRed
310               Else
311                   .CellForeColor = vbBlue
312               End If
313               .Text = ss(11)
314               .Col = 10
315               .Text = dblCota_Total
316               .Col = 11
317               .Text = IIf(IsNull(ss(14)), 0, ss(14))
318               lngNF_Dia = lngNF_Dia + IIf(IsNull(CStr(ss(14))), 0, CStr(ss(14)))
319               .Col = 12
320               .Text = IIf(IsNull(ss(15)), 0, ss(15))
321               lngNF_Acum = lngNF_Acum + IIf(IsNull(CStr(ss(15))), 0, CStr(ss(15)))
322               .Col = 13
323               .Text = Round(CStr(IIf(IsNull(ss(12)), 0, ss(12))), 0)
324               .Col = 14
325               .Text = Round(CStr(IIf(IsNull(ss(13)), 0, ss(13))), 0)

326               dblLC_Diario = dblLC_Diario + CDbl(ss(16))
327               dblBVista_Diario = dblBVista_Diario + CDbl(ss(18))
328               dblLC_Acum = dblLC_Acum + CDbl(ss(17))
329               dblBVista_Acum = dblBVista_Acum + CDbl(ss(19))
330               dblCota_Total = dblCota_Total + CDbl(ss(20))
331               ss.MoveNext
332           Next

333           .Row = intLinha_Fora_Time + 1
334           .Col = 0
335           .Text = ""
336           .Col = 1
337           .CellForeColor = vbBlue
338           .Text = "TOTAL"
339           .Col = 2
340           .Text = ""
341           .Col = 3
342           .Text = ""
343           .Col = 4
344           .Text = Round(dblVda_Diaria, 0)
345           .Col = 5
346           .Text = Round(dblCota_Ajustada, 0)
347           .Col = 6
348           If dblVda_Diaria = 0 Or dblCota_Ajustada = 0 Then
349               .Text = "0"
350               .CellForeColor = vbRed
351           Else
352               If Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0) < 100 Then
353                   .CellForeColor = vbRed
354               Else
355                   .CellForeColor = vbBlue
356               End If
357               .Text = Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0)
358           End If
359           .Col = 7
360           .Text = Round(dblVda_Acum, 0)
361           .Col = 8
362           .Text = Round(dblCota, 0)
363           .Col = 9
364           If dblVda_Acum = 0 Or dblCota = 0 Then
365               .CellForeColor = vbRed
366               .Text = "0"
367           Else
368               If Round((dblVda_Acum) / (dblCota) * 100, 0) < 100 Then
369                   .CellForeColor = vbRed
370               Else
371                   .CellForeColor = vbBlue
372               End If
373               .Text = Round((dblVda_Acum) / (dblCota) * 100, 0)
374           End If
375           .Col = 10
376           .Text = dblCota_Total
377           .Col = 11
378           .Text = lngNF_Dia
379           .Col = 12
380           .Text = lngNF_Acum
381           .Col = 13
382           If dblLC_Diario = 0 Or dblBVista_Diario = 0 Then
383               .Text = "0"
384           Else
385               .Text = Round((dblLC_Diario / dblBVista_Diario) * 100, 0)
386           End If
387           .Col = 14
388           If dblLC_Acum = 0 Or dblBVista_Acum = 0 Then
389               .Text = "0"
390           Else
391               .Text = Round((dblLC_Acum / dblBVista_Acum) * 100, 0)
392           End If

393       End With

Trata_Erro:
394       If Err.Number <> 0 Then
395           MessageBox 0, "Sub Form_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
396       End If
          
          Set ss = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmDesem = Nothing
End Sub

Private Sub SSCommand2_Click()
    Unload Me
End Sub
