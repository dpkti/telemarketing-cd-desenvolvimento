VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmInfo_Adic 
   Caption         =   "Informacoes Adicionais do Cliente"
   ClientHeight    =   6795
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   9480
   LinkTopic       =   "Form1"
   ScaleHeight     =   6795
   ScaleWidth      =   9480
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin SHDocVwCtl.WebBrowser BrowseWeb 
      Height          =   7605
      Left            =   30
      TabIndex        =   0
      Top             =   780
      Visible         =   0   'False
      Width           =   11535
      ExtentX         =   20346
      ExtentY         =   13414
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   1
      Top             =   720
      Width           =   11430
      _ExtentX        =   20161
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd Command1 
      Height          =   675
      Left            =   0
      TabIndex        =   2
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmINFO_ADIC.frx":0000
      PICN            =   "frmINFO_ADIC.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmInfo_Adic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Form_Load()

1       On Error GoTo Trata_Erro
          
2       BrowseWeb.Navigate ("http://" & strDominioDpkNetWeb & "/Info_Adicionais/Menu.asp?Act=1&CodUsr=" & lngUser_Help & "&Sistema=VDA230&codcli=" & frmVenda.txtCOD_CLIENTE)

        BrowseWeb.Visible = True

        Exit Sub

Trata_Erro:
3         NonStayOnTop Me
4         MessageBox 0, "Cadastro de Informa��es Adicionais n�o dispon�vel no momento.", "Aten�ao", &H40000 + 0
5         StayOnTop Me

6         Unload Me

End Sub

Private Sub Form_Resize()

On Error GoTo Trata_Erro
    
    BrowseWeb.Height = Me.Height - (Command1.Height + Command1.Height * 0.8)
    BrowseWeb.Width = Me.Width - pkGradient1.Height * 7
    Exit Sub
    
Trata_Erro:
   Resume Next
   
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmInfo_Adic = Nothing
End Sub
