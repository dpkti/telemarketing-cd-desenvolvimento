VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{44B6B849-95EE-4828-980F-0659BDA86936}#4.0#0"; "MonExt.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{1A12816C-FD0B-46D0-A07B-C6424C858EC6}#1.0#0"; "TEL INTERFACE.OCX"
Object = "{81BD97DC-0629-499A-9690-8142149FEDF6}#3.0#0"; "conwaresdk.ocx"
Object = "{3571006E-80DB-49C6-A35D-CD1C59414C20}#1.0#0"; "CWareSDK2.ocx"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "VDA240 - Enviar Recados para Telemarketing"
   ClientHeight    =   4440
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   8460
   Icon            =   "MDIForm1.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin CWareSDK2.ctrlCWareSDK2 MONEXT9000 
      Left            =   4335
      Top             =   2085
      _ExtentX        =   1402
      _ExtentY        =   714
      CTIRetryPooling =   3
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   3
      Top             =   4095
      Width           =   8460
      _ExtentX        =   14923
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   6068
            MinWidth        =   6068
            Text            =   "VDA240 - Recados para Telemarketing "
            TextSave        =   "VDA240 - Recados para Telemarketing "
         EndProperty
      EndProperty
   End
   Begin ConWareSDK.ctrlCWareSDK MONEXT8000 
      Left            =   1920
      Top             =   1485
      _ExtentX        =   1588
      _ExtentY        =   1588
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1920
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":0B24
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picPABX 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   570
      Left            =   0
      ScaleHeight     =   540
      ScaleWidth      =   8430
      TabIndex        =   0
      Top             =   0
      Width           =   8460
      Begin VB.TextBox TxtRamal 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         MaxLength       =   4
         TabIndex        =   5
         Top             =   210
         Width           =   945
      End
      Begin VB.TextBox txtID 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1065
         MaxLength       =   5
         PasswordChar    =   "*"
         TabIndex        =   4
         Top             =   210
         Width           =   840
      End
      Begin Bot�o.cmd cmdMonitorar 
         Height          =   405
         Left            =   1965
         TabIndex        =   1
         Tag             =   "1"
         ToolTipText     =   "Voltar"
         Top             =   90
         Width           =   2790
         _ExtentX        =   4921
         _ExtentY        =   714
         BTYPE           =   3
         TX              =   "Ativar Monitoramento"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "MDIForm1.frx":10BE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ramal:"
         Height          =   195
         Left            =   60
         TabIndex        =   7
         Top             =   30
         Width           =   495
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ID:"
         Height          =   195
         Left            =   1050
         TabIndex        =   6
         Top             =   30
         Width           =   210
      End
      Begin VB.Label lblNaoEncontrou 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4830
         TabIndex        =   2
         Top             =   180
         Visible         =   0   'False
         Width           =   3495
      End
   End
   Begin MonExt.ctrlMonExt monExt4000 
      Left            =   90
      Top             =   900
      _ExtentX        =   2990
      _ExtentY        =   873
      EnderecoServidor=   ""
      PortaServidor   =   0
   End
   Begin TELINTERFACELib.TelInterface MonExt3000 
      Left            =   90
      Top             =   1500
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   1005
      _StockProps     =   0
   End
   Begin VB.Menu mnuRecado 
      Caption         =   "Recado"
   End
   Begin VB.Menu mnuJanelas 
      Caption         =   "Janelas"
      WindowList      =   -1  'True
   End
   Begin VB.Menu mnuOrganizarHorizontal 
      Caption         =   "Organizar Horizontal"
   End
   Begin VB.Menu mnuOrganizarVertical 
      Caption         =   "Organizar Vertical"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "Sobre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public vNew_Form     As frmRecado
Public vCaptForm     As String
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Sub cmdMonitorar_Click()
    
    Dim vAgent
    Dim vExt
    Dim vInc As Long
    
    If TxtRamal = "" And Val(cmdMonitorar.Tag) <> 0 Then
        MsgBox "Informe o Ramal.", vbInformation, "Aten��o"
        TxtRamal.SetFocus
        Exit Sub
    End If
    
'    If txtID = "" And Val(cmdMonitorar.Tag) <> 0 Then
'        MsgBox "Informe o ID.", vbInformation, "Aten��o"
'        txtID.SetFocus
'        Exit Sub
'    End If
    
    RegLogonUser.vId = txtID.Text
    
    Pegar_Valores_Parametros
    
    Parametros.IP4000 = ""
    
    If Parametros.IP3000 <> "" Then
        TxtRamal.Tag = "3000"
    ElseIf Parametros.IP4000 <> "" Then
        TxtRamal.Tag = "4000"
    ElseIf Parametros.IP8000 <> "" Then
        TxtRamal.Tag = "8000"
    End If
        
    If Val(cmdMonitorar.Tag) = 0 Then
        
        If TxtRamal.Tag = "3000" Then
            cmdMonitorar.Tag = 1
            MonExt3000.StopExtMonitor vIdRamal, TxtRamal
            MonExt3000.Terminate
            cmdMonitorar.Caption = "Ativar Monitoramento"
            Set cmdMonitorar.PictureNormal = ImageList1.ListImages(2).Picture
            Exit Sub
        
        ElseIf TxtRamal.Tag = "4000" Then
            cmdMonitorar.Tag = 1
            monExt4000.fncMonitorStop
            monExt4000.Disconnect
            cmdMonitorar.Caption = "Ativar Monitoramento"
            Set cmdMonitorar.PictureNormal = ImageList1.ListImages(2).Picture
            Exit Sub
        
        ElseIf TxtRamal.Tag = "8000" Then
            
            Screen.MousePointer = vbHourglass
            
            cmdMonitorar.Tag = 1
            'MONEXT8000.fncMonitorStop Parametros.Ramal8000, TIPO_RAMAL
            
            'Logoff
            'Mudan�a componente Siemens - 18/12/2013
            'vMonRet = MDIForm1.MONEXT8000.fncAgentLogoff(RegLogonUser.vId, MIDIA_VOZ)
            vMonRet = MDIForm1.MONEXT9000.fncAgentLogoff(RegLogonUser.vId, MIDIA_VOZ)
            
            Screen.MousePointer = vbDefault
            
            cmdMonitorar.Caption = "Ativar Monitoramento"
            Set cmdMonitorar.PictureNormal = Me.ImageList1.ListImages(2).Picture
            Exit Sub
        End If
        
    End If
    
    vIdRamal = Rnd(9999)
    vIDMonExt = Rnd(9999)
    
    If Parametros.Ativo = "1" Then
        
        If Parametros.IP3000 <> "" Then
            vMonRet = MonExt3000.Connect(Parametros.IP3000, Parametros.Porta3000, "$.�D$(3_�����ط����4D$�65����4��3��)A3�@�$.7(T$.�Dg��$���ۤ��@")
            vAgent = MDIForm1.MonExt3000.StartAgentMonitor(vIdRamal, TxtRamal)
            vExt = MDIForm1.MonExt3000.StartExtMonitor(vIDMonExt, TxtRamal)
              
            If vAgent = 1 And vExt = 1 Then vMonRet = 1
          
            'Verificar se a conexao est� OK
            Call MDIForm1.MonExt3000_StartExtMonitorResult(vIdRamal, 0)
            
        ElseIf Parametros.IP4000 <> "" Then
            monExt4000.EnderecoServidor = Parametros.IP4000
            monExt4000.PortaServidor = Parametros.Porta4000
            vMonRet = monExt4000.Connect
            
        '-------------------------------------------------------------
        ' Eduardo Diogo Garcia / 26-01-2010
        '-------------------------------------------------------------
        ElseIf Parametros.IP8000 <> "" Then
            
            vMonRet = -1
            vInc = 1
            'Mudan�a componente Siemens - 18/12/2013
            'MDIForm1.MONEXT8000.Disconnect
	    MDIForm1.MONEXT9000.Disconnect
            Screen.MousePointer = vbHourglass
            Do While vInc < 11 And vConectadoHP8000 = False
                'Mudan�a componente Siemens - 18/12/2013
                'MDIForm1.MONEXT8000.EnderecoServidor = Parametros.IP8000
                'MDIForm1.MONEXT8000.PortaServidor = Parametros.Porta8000
                'MDIForm1.MONEXT8000.TipoAplicacao = 80
                'vConectadoHP8000 = MDIForm1.MONEXT8000.Connect
                MDIForm1.MONEXT9000.CTIServerIP = Parametros.IP8000
                MDIForm1.MONEXT9000.CTIServerPort = Parametros.Porta8000
                MDIForm1.MONEXT9000.AppTipo = 80
                vConectadoHP8000 = MDIForm1.MONEXT9000.Connect		
                Sleep (1000)
                vInc = vInc + 1
            Loop
        '-------------------------------------------------------------
        
        
        End If
    
    End If
    
End Sub

Private Sub MDIForm_Activate()
    TxtRamal.SetFocus
End Sub

Private Sub MDIForm_Load()
    
    Set cmdMonitorar.PictureNormal = ImageList1.ListImages(2).Picture
    
    Call Get_CD(Mid(App.Path, 1, 1))

    'If lngCD = 1 Then
    '    TxtRamal.Tag = "8000"
    'Else
    '    TxtRamal.Tag = "3000"
    'End If

    If strTp_banco = "U" Then
        strTabela_Banco = "PRODUCAO."
    Else
        strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
    End If

    fl_banco = 0

    sCOD_VEND = 599

    'Conexao oracle
    Set vSessao = CreateObject("oracleinprocserver.xorasession")
    
    'Producao
    Set oradatabase = vSessao.OpenDatabase("PRODUCAO", "VDA020/PROD", 0&)
    'Set oradatabase = vSessao.OpenDatabase("SDPKT", "VDA020/PROD", 0&)
       
    Set vBanco = oradatabase
    Set OraParameters = oradatabase.Parameters
End Sub

Private Sub mnuOrganizarHorizontal_Click()
    MDIForm1.Arrange 1
End Sub

Private Sub mnuOrganizarVertical_Click()
    MDIForm1.Arrange 2
End Sub

Private Sub mnuRecado_Click()
    Set vNew_Form = New frmRecado
    vNew_Form.SetFocus
End Sub

'----------------------------------------------------------------------------------
'mnuSobre_Click():
'----------------------------------------------------------------------------------
Private Sub mnuSobre_Click()
    MsgBox "Recados para Telemarketing - HP8000", vbInformation, "Vers�o.: " & App.Major & "." & _
           Format(App.Minor, "00") & "." & Format(App.Revision, "00")
End Sub

Private Sub MonExt3000_OnConnectionStateChange(ByVal sState As Integer)

    If sState <> 102 Then
        MonExt3000.Connect Parametros.IP3000, Parametros.Porta3000, "$.�D$(3_�����ط����4D$�65����4��3��)A3�@�$.7(T$.�Dg��$���ۤ��@"
        MonExt3000.StartAgentMonitor vIdRamal, TxtRamal
        MonExt3000.StartExtMonitor vIDMonExt, TxtRamal
    End If
    
    If vMonRet = 1 Then
        cmdMonitorar.Tag = 0
        cmdMonitorar.Caption = "Desativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(1).Picture
    Else
        cmdMonitorar.Tag = 1
        cmdMonitorar.Caption = "Ativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(2).Picture
    End If

End Sub

'----------------------------------------------------------------------------------
'Telefone est� tocando em B, este B pode ser o fone do cliente ou o fone do Usuario
'----------------------------------------------------------------------------------
Private Sub MonExt3000_OnDelivered(ByVal lLocalConnState As Long, ByVal strCallingID As String, ByVal strExtension As String, ByVal strOriginalDNIS As String, ByVal strUniqueCallID As String, ByVal strPrimarySubCallKey As String, ByVal strSecundarySubCallKey As String, ByVal strTrunkID As String)

  On Error GoTo TrataErro
  Dim vFoneOrigem As String
  
  vFoneOrigem = Trim(strCallingID)
  
  If Val(vFoneOrigem) <> Val(TxtRamal) Then
     If Len(Trim(vFoneOrigem)) >= 7 Then
        If IsNumeric(Trim(vFoneOrigem)) Then
           Set vNew_Form = New frmRecado
           vNew_Form.SetFocus
           Set vForm_Atual = vNew_Form
           Cliente_Fone Trim(vFoneOrigem)
           cmdLocalizar_Click
           Me.vNew_Form.Visible = True
        End If
     End If
  End If

TrataErro:
  If Err.Number <> 0 Then
      MsgBox "Sub: MonExt3000_OnDelivered" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & "Linha:" & Erl, , "Aten�ao"
  End If
    
End Sub

Private Sub MonExt3000_OnTransferred(ByVal lLocalConnState As Long, ByVal strOriginalDNIS As String, ByVal strOwnerID As String, ByVal strParties As String, ByVal strPrimarySubCallKey As String, ByVal strSecondarySubCallKey As String, ByVal strTranferredID As String, ByVal strTranferringID As String, ByVal strTranferTargetID As String, ByVal strUniqueCallID As String)
  Dim vFoneOrigem As String
  
  vFoneOrigem = Trim(strTranferredID)
  
  If Val(vFoneOrigem) <> Val(TxtRamal) Then
     If Len(Trim(vFoneOrigem)) >= 7 Then
        If IsNumeric(Trim(vFoneOrigem)) Then
           Set vNew_Form = New frmRecado
           vNew_Form.SetFocus
           Set vForm_Atual = vNew_Form
           Cliente_Fone Trim(vFoneOrigem)
           cmdLocalizar_Click
           Me.vNew_Form.Visible = True
        End If
     End If
  End If
End Sub

Private Sub monExt4000_evtServerConnect()
    
    monExt4000.Ramal = Val(TxtRamal)
    
    vMonRet = monExt4000.fncMonitorStart
    
    If vMonRet = 1 Then
        cmdMonitorar.Tag = 0
        cmdMonitorar.Caption = "Desativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(1).Picture
    Else
        cmdMonitorar.Tag = 1
        cmdMonitorar.Caption = "Ativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(2).Picture
    End If
End Sub

'-----------------------------------------------------------------------------------------------------------------------
'                                                   HP 4000
'-----------------------------------------------------------------------------------------------------------------------
Private Sub MonExt4000_TelephonyEvent(ByVal TelephonyEvent As MonExt.enumTelephonyEvents, LineData As MonExt.tyDadosEventoLinha, ByVal AttachedData As MonExt.classAttachedData)

    On Error GoTo vda230_Erro

    Dim vEvento As String
    Dim vFoneOrigem As String
    
    vEvento = NomeEventTel(TelephonyEvent)
    
    'Debug.Print Now & " - " & vEvento & " - Party1: " & LineData.Party1 & " -Party2: " & LineData.Party2 & " -Party3:" & LineData.Party3 & " -OriginalDNS:" & LineData.OriginalDNIS & " -OwnerID:" & LineData.OwnerID
    'GravarNoArquivo Now & " - " & vEvento & " - Party1: " & LineData.Party1 & " -Party2: " & LineData.Party2 & " -Party3:" & LineData.Party3 & " -OriginalDNS:" & LineData.OriginalDNIS & " -OwnerID:" & LineData.OwnerID
    
    'Receptiva
    If Trim(IIf(Len(Trim(LineData.Party2)) > 3, Mid(Trim(LineData.Party2), 2), Trim(LineData.Party2))) = Trim(TxtRamal) Or Len(Trim(LineData.Party1)) >= 8 Then
        If vEvento = "Tocar" Then
                vFoneOrigem = LineData.Party1
                If Trim(vFoneOrigem) >= 7 Then
                   If IsNumeric(Trim(vFoneOrigem)) Then
                      Set vNew_Form = New frmRecado
                      vNew_Form.SetFocus
                      Set vForm_Atual = vNew_Form
                      Cliente_Fone Trim(vFoneOrigem)
                      cmdLocalizar_Click
                      Me.vNew_Form.Visible = True
                   End If
                End If
        ElseIf vEvento = "Transferido" Then
              vFoneOrigem = LineData.Party1
              If Len(Trim(vFoneOrigem)) >= 7 Then
                 If IsNumeric(Trim(vFoneOrigem)) Then
                    Set vNew_Form = New frmRecado
                    vNew_Form.SetFocus
                    Set vForm_Atual = vNew_Form
                    Cliente_Fone Trim(vFoneOrigem)
                    cmdLocalizar_Click
                 End If
              End If
        End If
    End If
    
vda230_Erro:
    If Err.Number = 5 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MessageBox 0, "Rotina: MonExt4000_TelephonyEvent" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
        Resume Next
    End If

End Sub

Public Sub cmdLocalizar_Click()
          
1         On Error GoTo Trata_Erro
          
          Dim i As Double
          Dim ss As Object
          
2         If vForm_Atual.txtCodCliente = "" Then Exit Sub

3         Me.MousePointer = vbHourglass

4         vBanco.Parameters.Remove "Cod"
5         vBanco.Parameters.Add "Cod", IIf(vForm_Atual.txtCodCliente = "", 0, vForm_Atual.txtCodCliente), 1

6         vBanco.Parameters.Remove "CGC"
7         vBanco.Parameters.Add "CGC", 0, 1

8         vBanco.Parameters.Remove "nome"
9         vBanco.Parameters.Add "nome", vForm_Atual.txtNomeCliente, 1

10        vSql = "Begin producao.pck_vda230.Pr_Select_Cliente_Fone(:vCursor,:Cod, :CGC, :nome);END;"

11        Criar_Cursor
12        vBanco.ExecuteSQL vSql
13        Set ss = vBanco.Parameters("vCursor").Value
14        vBanco.Parameters.Remove ("vCursor")

15        If ss.EOF = True Then
16            MessageBox 0, "Cliente n�o encontrado. " & vbCrLf & "Verifique se voc� digitou corretamente.", "Aten�ao", &H40000 + 0
17            Me.MousePointer = vbNormal
18            Exit Sub
19        End If

20        vForm_Atual.txtNomeCliente = ss("Nome_Cliente")

21        vForm_Atual.txtObs = vForm_Atual.txtCodCliente & " - " & vForm_Atual.txtNomeCliente

22        Set ss = oradatabase.createdynaset("SELECT COD_VEND FROM R_REPVEN WHERE COD_REPRES = (SELECT COD_REPRES FROM R_CLIE_REPRES WHERE COD_CLIENTE = " & vForm_Atual.txtCodCliente & ")", 0&)
                          
23        If ss.EOF = False Then
            vForm_Atual.txtVendedor = ss("COD_VEND")
            vForm_Atual.txtVendedor_LostFocus
          End If

24        Me.MousePointer = vbNormal

Trata_Erro:
25    If Err.Number <> 0 Then
26     MessageBox 0, "Sub cmdLocalizar_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
27    End If
End Sub

Public Sub Cliente_Fone(pNumFone As String)
1         On Error GoTo TrataErro
          
2         If IsNumeric(pNumFone) And Len(pNumFone) > 4 Then
              
3             Get_DDD_FONE Trim(pNumFone)
4             Criar_Cursor

5             oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_Cod_Cliente(:vCursor, :DDD, :FONE); END;"
6             Set vObjOracle = oradatabase.Parameters("vCursor").Value
7             oradatabase.Parameters.Remove ("vCursor")

8             If vObjOracle.EOF = False And vObjOracle.BOF = False Then
9                vForm_Atual.txtCodCliente = vObjOracle("Cod_Cliente")
                 
10               Set vObjOracle = oradatabase.createdynaset("SELECT COD_VEND FROM R_REPVEN WHERE COD_REPRES = (SELECT COD_REPRES FROM R_CLIE_REPRES WHERE COD_CLIENTE = " & vForm_Atual.txtCodCliente & ")", 0&)
                          
11               If vObjOracle.EOF = False Then vForm_Atual.txtVendedor = vObjOracle("COD_VEND")
                          
              Else
                 lblNaoEncontrou.Visible = True
                 lblNaoEncontrou.Caption = "Cliente n�o encontrado."
12            End If
13        End If

TrataErro:
14        If Err.Number <> 0 Then
15             MessageBox 0, "Cliente_Fone" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
16        End If
End Sub

Public Sub GravarNoArquivo(Texto As String)
    Dim arq As Integer
    
    arq = FreeFile
    Open "C:\Log_Ligacoes_Recado.txt" For Append As arq
    Print #arq, Texto
    
    Close arq
End Sub

Public Sub MonExt3000_StartExtMonitorResult(ByVal lRefID As Long, ByVal lResult As Long)
          
1         On Error GoTo Trata_Erro
          
          Dim vRetorno As Double
          
2         If lResult < 0 Then
3             vRetorno = MDIForm1.MonExt3000.Connect(Parametros.IP3000, Parametros.Porta3000, "$.�D$(3_�����ط����4D$�65����4��3��)A3�@�$.7(T$.�Dg��$���ۤ��@")
4             If vRetorno <> 1 And vRetorno <> 8 Then
5                 MsgBox 0, "Erro na conex�o com o PABX, informe o suporte.", "Aten��o", &H40000 + 0
6                 End
7             End If
8             vRetorno = MDIForm1.MonExt3000.QueryAgentState(222, vIdRamal, TxtRamal)
9             If vRetorno <> 1 Then
10                MDIForm1.MonExt3000.StartAgentMonitor vIdRamal, TxtRamal
11                MDIForm1.MonExt3000.StartExtMonitor vIDMonExt, TxtRamal
12            End If
13        End If
          
Trata_Erro:
14        If Err.Number <> 0 Then
15           MsgBox "Sub: MonExt3000_StarExtMonitorResult" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao"
16        End If
          
End Sub

'--------------------------------------------------------------------------------------------------------------------
' MONEXT8000_evtServerConnect()
'--------------------------------------------------------------------------------------------------------------------
' Autor: Eduardo Diogo
'  Data: 26-01-2010
'--------------------------------------------------------------------------------------------------------------------
Private Sub MONEXT8000_evtServerConnect()

    Parametros.Ramal8000 = Parametros.PrefixoFone & TxtRamal.Text & Parametros.SufixoFone
    

    MDIForm1.MONEXT8000.fncMonitorStop Parametros.Ramal8000, TIPO_RAMAL
    If MDIForm1.MONEXT8000.fncMonitorStart(Parametros.Ramal8000, TIPO_RAMAL) > 0 Then
        vMonRet = 0
    End If
    
    If vMonRet = 0 Then
        cmdMonitorar.Tag = 0
        cmdMonitorar.Caption = "Desativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(1).Picture
    Else
        cmdMonitorar.Tag = 1
        cmdMonitorar.Caption = "Ativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(2).Picture
    End If

    If vConectadoHP8000 Then
        
        'Logon
        vMonRet = MDIForm1.MONEXT8000.fncAgentLogon(RegLogonUser.vId, Parametros.Ramal8000, MIDIA_VOZ)
        
        'Dispon�vel
        vMonRet = MDIForm1.MONEXT8000.fncAgentAvailable(RegLogonUser.vId)
    
    End If
    
    TxtRamal.Tag = "8000"
    
    Screen.MousePointer = vbDefault

End Sub

'--------------------------------------------------------------------------------------------------------------------
'Mudan�a componente Siemens - 18/12/2013
'--------------------------------------------------------------------------------------------------------------------
Private Sub MONEXT9000_evtServerConnect()

    Parametros.Ramal8000 = Parametros.PrefixoFone & TxtRamal.Text & Parametros.SufixoFone
    

    MDIForm1.MONEXT9000.fncMonitorStop Parametros.Ramal8000, TIPO_RAMAL
    If MDIForm1.MONEXT9000.fncMonitorStart(Parametros.Ramal8000, TIPO_RAMAL) > 0 Then
        vMonRet = 0
    End If
    
    If vMonRet = 0 Then
        cmdMonitorar.Tag = 0
        cmdMonitorar.Caption = "Desativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(1).Picture
    Else
        cmdMonitorar.Tag = 1
        cmdMonitorar.Caption = "Ativar Monitoramento"
        Set cmdMonitorar.PictureNormal = ImageList1.ListImages(2).Picture
    End If

    If vConectadoHP8000 Then
        
        'Logon
        vMonRet = MDIForm1.MONEXT9000.fncAgentLogon(RegLogonUser.vId, Parametros.Ramal8000, MIDIA_VOZ)
        
        'Dispon�vel
        vMonRet = MDIForm1.MONEXT9000.fncAgentAvailable(RegLogonUser.vId)
    
    End If
    
    TxtRamal.Tag = "8000"
    
    Screen.MousePointer = vbDefault

End Sub

'--------------------------------------------------------------------------------------------------------------------
' MONEXT8000_evtServerDisconnect()
'--------------------------------------------------------------------------------------------------------------------
' Autor: Eduardo Diogo
'  Data: 26-01-2010
'--------------------------------------------------------------------------------------------------------------------
Private Sub MONEXT8000_evtServerDisconnect(ByVal bForced As Boolean)
    vConectadoHP8000 = False
End Sub


'--------------------------------------------------------------------------------------------------------------------
'Mudan�a componente Siemens - 18/12/2013
'--------------------------------------------------------------------------------------------------------------------
Private Sub MONEXT9000_evtServerDisconnect(ByVal bForced As Boolean)
    vConectadoHP8000 = False
End Sub

'--------------------------------------------------------------------------------------------------------------------
' MONEXT8000_TelephonyEvent
'--------------------------------------------------------------------------------------------------------------------
' Autor: Eduardo Diogo
'  Data: 26-01-2010
'--------------------------------------------------------------------------------------------------------------------
Private Sub MONEXT8000_TelephonyEvent(ByVal TelephonyEvent As ConWareSDK.enumMidiaEvents, LineData As ConWareSDK.tyDadosEventoLinha)
    On Error GoTo vda240_Erro

    Dim vEvento As String
    Dim vFoneOrigem As String

    vEvento = NomeEventTel8000(TelephonyEvent)

    'Receptiva
    If Trim(IIf(Len(Trim(LineData.Party2)) > 3, Mid(Trim(LineData.Party2), 2), _
       Trim(LineData.Party2))) = Trim(Parametros.Ramal8000) Or Len(Trim(LineData.Party1)) >= 8 Then
        
        If vEvento = "Delivered" Then
                vFoneOrigem = LineData.Party1
                If Len(vFoneOrigem) < 12 Then
                   If IsNumeric(Trim(vFoneOrigem)) Then
                      Set vNew_Form = New frmRecado
                      vNew_Form.SetFocus
                      Set vForm_Atual = vNew_Form
                      Cliente_Fone Trim(vFoneOrigem)
                      cmdLocalizar_Click
                      Me.vNew_Form.Visible = True
                   End If
                End If
        ElseIf vEvento = "Transfered" Then
              vFoneOrigem = LineData.Party3
              If Len(Trim(vFoneOrigem)) < 12 Then
                 If IsNumeric(Trim(vFoneOrigem)) Then
                    Set vNew_Form = New frmRecado
                    vNew_Form.SetFocus
                    Set vForm_Atual = vNew_Form
                    Cliente_Fone Trim(vFoneOrigem)
                    cmdLocalizar_Click
                 End If
              End If
        End If
    End If

vda240_Erro:
    If Err.Number = 5 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MessageBox 0, "Rotina: MonExt4000_TelephonyEvent" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
        Resume Next
    End If
End Sub

Private Sub MONEXT9000_TelephonyEvent(ByVal TelephonyEvent As CWareSDK2.enumMidiaEvents, LineData As CWareSDK2.tyDadosEventoLinha)
    On Error GoTo vda240_Erro

    Dim vEvento As String
    Dim vFoneOrigem As String

    vEvento = NomeEventTel8000(TelephonyEvent)

    'Receptiva
    If Trim(IIf(Len(Trim(LineData.Party2)) > 3, Mid(Trim(LineData.Party2), 2), _
       Trim(LineData.Party2))) = Trim(Parametros.Ramal8000) Or Len(Trim(LineData.Party1)) >= 8 Then
        
        If vEvento = "Delivered" Then
                vFoneOrigem = LineData.Party1
                If Len(vFoneOrigem) < 12 Then
                   If IsNumeric(Trim(vFoneOrigem)) Then
                      Set vNew_Form = New frmRecado
                      vNew_Form.SetFocus
                      Set vForm_Atual = vNew_Form
                      Cliente_Fone Trim(vFoneOrigem)
                      cmdLocalizar_Click
                      Me.vNew_Form.Visible = True
                   End If
                End If
        ElseIf vEvento = "Transfered" Then
              vFoneOrigem = LineData.Party3
              If Len(Trim(vFoneOrigem)) < 12 Then
                 If IsNumeric(Trim(vFoneOrigem)) Then
                    Set vNew_Form = New frmRecado
                    vNew_Form.SetFocus
                    Set vForm_Atual = vNew_Form
                    Cliente_Fone Trim(vFoneOrigem)
                    cmdLocalizar_Click
                 End If
              End If
        End If
    End If

vda240_Erro:
    If Err.Number = 5 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MessageBox 0, "Rotina: MonExt4000_TelephonyEvent" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
        Resume Next
    End If
End Sub

'------------------------------------------------------------------
'txtID_KeyDown():
'------------------------------------------------------------------
Private Sub txtID_KeyDown(KeyCode As Integer, Shift As Integer)
    If Trim(txtID.Text) <> "" Then
        If KeyCode = 13 Then
            cmdMonitorar_Click
        End If
    End If
End Sub

'------------------------------------------------------------------
'txtID_KeyPress():
'------------------------------------------------------------------
Private Sub txtID_KeyPress(KeyAscii As Integer)
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
        KeyAscii = 0
    End If
End Sub

'------------------------------------------------------------------
'TxtRamal_KeyDown():
'------------------------------------------------------------------
Private Sub TxtRamal_KeyDown(KeyCode As Integer, Shift As Integer)
    If Trim(TxtRamal.Text) <> "" Then
        If KeyCode = 13 Then
            txtID.SetFocus
        End If
    End If
End Sub

'------------------------------------------------------------------
'TxtRamal_KeyPress():
'------------------------------------------------------------------
Private Sub TxtRamal_KeyPress(KeyAscii As Integer)
    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
        KeyAscii = 0
    End If
End Sub
