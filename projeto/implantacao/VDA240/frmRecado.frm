VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmRecado 
   Caption         =   "Recado para Outro Operador"
   ClientHeight    =   4710
   ClientLeft      =   1155
   ClientTop       =   1560
   ClientWidth     =   5100
   Icon            =   "frmRecado.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4710
   ScaleWidth      =   5100
   Begin VB.TextBox txtNomeCliente 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   30
      MaxLength       =   200
      TabIndex        =   2
      Top             =   1500
      Width           =   5025
   End
   Begin VB.TextBox txtCodCliente 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   30
      MaxLength       =   6
      TabIndex        =   0
      Top             =   990
      Width           =   1035
   End
   Begin VB.Frame ssfraAgenda 
      Height          =   2745
      Left            =   30
      TabIndex        =   5
      Top             =   1890
      Width           =   4995
      Begin VB.TextBox txtObs 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   1695
         Left            =   85
         MaxLength       =   90
         MultiLine       =   -1  'True
         TabIndex        =   4
         Top             =   960
         Width           =   4815
      End
      Begin VB.TextBox txtVendedor 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   90
         MaxLength       =   4
         TabIndex        =   3
         Top             =   420
         Width           =   735
      End
      Begin VB.Label lblPseudonimo 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   900
         TabIndex        =   12
         Top             =   480
         Width           =   75
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Recado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   7
         Top             =   750
         Width           =   675
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Vendedor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   6
         Top             =   210
         Width           =   825
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   8
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRecado.frx":058A
      PICN            =   "frmRecado.frx":05A6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   9
      Top             =   720
      Width           =   5040
      _ExtentX        =   8890
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd sscmdGrava_Recado 
      Height          =   675
      Left            =   3510
      TabIndex        =   11
      ToolTipText     =   "Incluir"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRecado.frx":1280
      PICN            =   "frmRecado.frx":129C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand3 
      Height          =   675
      Left            =   4290
      TabIndex        =   10
      ToolTipText     =   "Limpar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRecado.frx":1F76
      PICN            =   "frmRecado.frx":1F92
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   315
      Left            =   1080
      TabIndex        =   1
      ToolTipText     =   "Pesquisar"
      Top             =   960
      Width           =   315
      _ExtentX        =   556
      _ExtentY        =   556
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRecado.frx":20EC
      PICN            =   "frmRecado.frx":2108
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   14
      Top             =   1290
      Width           =   660
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "C�d.Cliente:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   13
      Top             =   780
      Width           =   1050
   End
End
Attribute VB_Name = "frmRecado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vForm_Aberto As Boolean

Private Sub cmdConsultar_Click()
    MDIForm1.cmdLocalizar_Click
End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    Set vForm_Atual = Screen.ActiveForm
End Sub

Private Sub Form_GotFocus()
    Set vForm_Atual = Screen.ActiveForm
End Sub

Private Sub Form_Load()
    Me.Height = 5115
    Me.Width = 5220
End Sub

Private Sub Form_LostFocus()
    Me.Tag = ""
End Sub

Private Sub sscmdGrava_Recado_Click()
    If txtVendedor = "" Then
        MsgBox "Favor escolher o vendedor que dever� receber o recado", vbInformation, "Aten��o"
        Exit Sub
    End If

    If txtObs = "" Then
        MsgBox "Favor incluir o recado", vbInformation, "Aten��o"
        Exit Sub
    End If

    vSql = "Begin producao.pck_vda230.pr_insere_recado(:vend,:cliente,:recado,:vErro);END;"

    vBanco.Parameters.Remove "vend"
    vBanco.Parameters.Add "vend", vForm_Atual.txtVendedor, 1
    vBanco.Parameters.Remove "cliente"
    vBanco.Parameters.Add "cliente", 52306, 1
    vBanco.Parameters.Remove "recado"
    vBanco.Parameters.Add "recado", "VEND.:" & sCOD_VEND & ": " & UCase(vForm_Atual.txtObs), 1
    vBanco.Parameters.Remove "vErro"
    vBanco.Parameters.Add "vErro", 0, 2

    vBanco.ExecuteSQL vSql

    cmdSair_Click
    
End Sub

Private Sub txtCodCliente_DblClick()
    frmClientes.Show
    StayOnTop frmClientes
End Sub


Private Sub txtCodCliente_GotFocus()
    Set vForm_Atual = Screen.ActiveForm
End Sub

Private Sub txtCodCliente_KeyPress(KeyAscii As Integer)
    Me.txtNomeCliente = ""
    Me.txtObs = ""
End Sub

Private Sub txtObs_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub

Private Sub txtVendedor_DblClick()
    txtVendedor.DataChanged = False
    frmVendedor.Show vbModal
End Sub

Private Sub txtVendedor_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Public Sub txtVendedor_LostFocus()
    Dim ss As Object

    If txtVendedor <> "" Then
        vBanco.Parameters.Remove "repres"
        vBanco.Parameters.Add "repres", txtVendedor, 1

        Criar_Cursor
        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA230.Pr_Select_Repres_Tipo(:vCursor, :Repres); END;"
        Set ss = vBanco.Parameters("vCursor").Value
        vBanco.Parameters.Remove ("vCursor")
        
        If ss.EOF And ss.BOF Then
            MsgBox "Vendedor n�o localizado, verifique", vbInformation, "Aten��o"
            Exit Sub
        Else
            lblPseudonimo = ss!pseudonimo
        End If
    End If

End Sub





Public Function Get_DDD_FONE(Fone As String)
    Dim vFone As Double
    Dim vDDD As Double

    If Len(Trim(Fone)) < 5 Then
        OraParameters.Remove "DDD"
        OraParameters.Remove "FONE"
        Exit Function
    End If
    
    If Left(Fone, 1) = "0" Then
        
        If Left(Fone, 2) = "00" Then
            If Left(Fone, 5) = "00800" Or Left(Fone, 5) = "00300" Then
                vDDD = Mid(Fone, 2, 4)
                vFone = Mid(Fone, 5)
            Else
                vDDD = Mid(Fone, 3, 2)
                vFone = Mid(Fone, 5)
            End If
        Else
            If Len(Fone) = 8 Then
                OraParameters.Remove "DDD"
                OraParameters.Add "DDD", 0, 2
                
                OraParameters.Remove "CodLojaDDD"
                OraParameters.Add "CodLojaDDD", lngCD, 1
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                
                vDDD = vBanco.Parameters("DDD").Value
            
                vFone = Mid(Fone, 2)
                
            ElseIf Len(Fone) = 9 Then
                OraParameters.Remove "DDD"
                OraParameters.Add "DDD", 0, 2
                
                OraParameters.Remove "CodLojaDDD"
                OraParameters.Add "CodLojaDDD", lngCD, 1
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                
                vDDD = vBanco.Parameters("DDD").Value
            
                vFone = Mid(Fone, 2)
            
            ElseIf Len(Fone) > 9 Then
                vDDD = Mid(Fone, 2, 2)
                vFone = Mid(Fone, 4)
            End If
        End If
    
    Else
        If Len(Fone) > 10 Then
            vDDD = Mid(Fone, 3, 2)
            vFone = Mid(Fone, 5)
        ElseIf Len(Fone) = 10 Then
            vDDD = Left(Fone, 2)
            vFone = Mid(Fone, 3)

        ElseIf Len(Fone) = 9 Then
            
            OraParameters.Remove "DDD"
            OraParameters.Add "DDD", 0, 2
            
            OraParameters.Remove "CodLojaDDD"
            OraParameters.Add "CodLojaDDD", lngCD, 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
            
            vDDD = vBanco.Parameters("DDD").Value
            
             vFone = Mid(Fone, 2)
        
        ElseIf Len(Fone) = 8 Or Len(Fone) = 7 Then
                        
            vFone = Fone
            
            OraParameters.Remove "DDD"
            OraParameters.Add "DDD", 0, 2
            
            OraParameters.Remove "CodLojaDDD"
            OraParameters.Add "CodLojaDDD", lngCD, 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
            
            vDDD = vBanco.Parameters("DDD").Value
        
        End If
    End If

    OraParameters.Remove "DDD"
    OraParameters.Add "DDD", vDDD, 1
    OraParameters.Remove "FONE"
    OraParameters.Add "FONE", vFone, 1

End Function




Function StayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, HWND_TOPMOST, 0, 0, 0, 0, lFlags)
End Function

Function NonStayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, -2, 0, 0, 0, 0, lFlags)
End Function

