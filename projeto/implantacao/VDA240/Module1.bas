Attribute VB_Name = "Module1"
Option Explicit

Public vSessao As Object                            'SESS�O DO ORACLE
Public vBanco As Object                             'BANCO DO ORACLE
Public vNomeBanco As String                         'NOME DO BANCO DO ARQUIVO .INI
Public vObjOracle As Object                         'OBJETO DO ORACLE
Public vCd As Integer                               'C�DIGO DO CD CONECTADO
Public vTipoCD As String                            'TIPO DO CD CONECTADO (U, M)
Public vLinhaArquivo                                'LEITURA DE LINHA DE ARQUIVO
Public vResposta                                    'GUARDA A ESCOLHA DO USU�RIO DA FUN��O PERGUNTA
Public vUsuarioBanco As String                      'USU�RIO DO BANCO
Public vUsuarioRede As String                       'USU�RIO DA REDE
Public vNomeSistema As String                       'NOME DO SISTEMA
Public vDescricaoSistema As String                  'DESCRI��O DO SISTEMA
Public vAnalistaResponsavel As String               'ANALISTA RESPONS�VEL PELO SISTEMA
Public vAnalistaBackup As String                    'ANALISTA BACKUP
Public vVB_Generica_001 As New clsVB_Generica_001
Public vErro As String
Public vSelect As String
Public vSql As String
Public i As Double
Public j As Double


Public Declare Function GetUserName Lib _
        "advapi32.dll" Alias "GetUserNameA" _
        (ByVal lpbuffer As String, nSize _
        As Long) As Long


Public oradatabase As Object
Public OraParameters As Object
Public VarRec As Object
Public VarRec1 As Object
Public Data_Faturamento As Date
Public txtResposta As String           'RESPOSTA DE UMA LISTA

Public lngCOD_LOJA As Long      'codigo do deposito
Global sCOD_VEND As Long        'CODIGO DE VENDEDOR
Public strPath_Foto As String   'PATH QUE GUARDA AS FOTOS DAS PE�AS
Global strPathUsuario As String 'PATH DO USUARIO

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public strTabela_Banco As String
Public fl_banco As Byte
Public lngCD As Long
Public strTp_banco As String * 1
Public strTp_banco_Destino As String * 1

Public strPath As String
Public lngArq As Long
Public TextLine As String

Public lngCOD_PLANO

'Eduardo Diogo / 26-01-2010
Public vConectadoHP8000 As Boolean

Public Sub Get_CD(strUnidade As String)
    On Error GoTo erro

    strPath = "h:\oracle\dados\32bits\cd.txt"
    
    lngArq = FreeFile

    Open strPath For Input Shared As #lngArq
    Do While Not EOF(lngArq)
        Line Input #lngArq, TextLine
        lngCD = Mid(TextLine, 1, 2)
        strTp_banco = Mid(TextLine, 3, 1)
    Loop
    Close #lngArq

    Exit Sub

erro:

    If Err = 53 Then
        MsgBox "O Arquivo de controle de tipo de Banco n�o foi encontrado !!!", 16, "Controle de Banco"
        End
    Else
        MsgBox Err.Description, 16, "Controle de Banco"
    End If

End Sub


Function BuscaUSERNAME() As String
    'ON ERROR GoTo TrataErro

    ' Chamada da API
    '  Dim VUsuario As String
    '  Dim VSet As String
    '  VUsuario = QueryValue("Network\logon", "username")
    '  BuscaUSERNAME = VUsuario
    '  Exit Function

    'TrataErro:
    '    BuscaUSERNAME = "Houve Erro"
    On Error GoTo TrataErro

    Dim sBuffer As String
    Dim lSize As Long

    sBuffer = Space$(255)
    lSize = Len(sBuffer)
    GetUserName sBuffer, lSize

    If lSize > 0 Then
        BuscaUSERNAME = Left$(sBuffer, lSize - 1)
    End If
    Exit Function

TrataErro:
    BuscaUSERNAME = "Houve Erro"


End Function



Public Sub Criar_Cursor()
    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").serverType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub


