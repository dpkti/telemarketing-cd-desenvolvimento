VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmClientes 
   Caption         =   "Pesquisa de Cliente"
   ClientHeight    =   7020
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7995
   Icon            =   "frmClientes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7020
   ScaleWidth      =   7995
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtNomeCliente 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   30
      TabIndex        =   0
      Top             =   270
      Width           =   4995
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
      Height          =   6315
      Left            =   30
      TabIndex        =   2
      Top             =   660
      Width           =   7965
      _ExtentX        =   14049
      _ExtentY        =   11139
      _Version        =   393216
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   435
      Left            =   5100
      TabIndex        =   1
      ToolTipText     =   "Pesquisar"
      Top             =   180
      Width           =   465
      _ExtentX        =   820
      _ExtentY        =   767
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmClientes.frx":038A
      PICN            =   "frmClientes.frx":03A6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Informe o nome do cliente:"
      Height          =   195
      Left            =   30
      TabIndex        =   3
      Top             =   60
      Width           =   1875
   End
End
Attribute VB_Name = "frmClientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdConsultar_Click()
    If Trim(txtNomeCliente) = "" Then
        MessageBox 0, "Informe o Cliente.", "Pesquisa de Cliente", &H40000
        txtNomeCliente.SetFocus
        Exit Sub
    End If
    Pesquisar
End Sub

Sub Pesquisar()
          On Error GoTo TrataErro

          Dim ss As Object
          Dim i As Integer
          Dim j As Integer

        If txtNomeCliente = "" Then Exit Sub

         Screen.MousePointer = vbHourglass
         
         Criar_Cursor
         
         vBanco.Parameters.Remove "CLIENTE"
         vBanco.Parameters.Add "CLIENTE", txtNomeCliente, 1

         vBanco.Parameters.Remove "vErro"
         vBanco.Parameters.Add "vErro", 0, 2

         vSql = "Begin producao.pck_vda320.pr_cliente1(:vCursor,:cliente,:vErro);END;"

         vBanco.ExecuteSQL vSql
         Set ss = vBanco.Parameters("vCursor").Value
         vBanco.Parameters.Remove ("vCursor")

         If vBanco.Parameters("vErro").Value <> 0 Then
            Screen.MousePointer = vbDefault
            NonStayOnTop Me
            MessageBox 0, "Erro: " & vBanco.Parameters("vErro").Value, "Aten�ao", &H40000 + 0
            StayOnTop Me
        End If

        If ss.EOF And ss.BOF Then
            Screen.MousePointer = vbDefault
            NonStayOnTop Me
            MessageBox 0, "Nenhum cliente encontrado.", "Aten�ao", &H40000 + 0
            StayOnTop Me
            Exit Sub
        End If

        MSHFlexGrid1.Clear

        j = ss.RECORDCOUNT
        
        MSHFlexGrid1.Rows = j + 2
        MSHFlexGrid1.Cols = 5
        
        MSHFlexGrid1.ColWidth(0) = 100
        MSHFlexGrid1.ColWidth(1) = 700
        MSHFlexGrid1.ColWidth(2) = 3500
        MSHFlexGrid1.ColWidth(3) = 2500
        MSHFlexGrid1.ColWidth(4) = 400
        
        For i = 1 To j
            MSHFlexGrid1.TextMatrix(i, 1) = IIf(IsNull(ss.fields(0)), "", ss.fields(0))
            MSHFlexGrid1.TextMatrix(i, 2) = IIf(IsNull(ss.fields(1)), "", ss.fields(1))
            MSHFlexGrid1.TextMatrix(i, 3) = IIf(IsNull(ss.fields(2)), "", ss.fields(2))
            MSHFlexGrid1.TextMatrix(i, 4) = IIf(IsNull(ss.fields(3)), "", ss.fields(3))
            ss.MOVENEXT
        Next
        
        ss.Close
        Set ss = Nothing

        Screen.MousePointer = vbDefault

        Exit Sub

TrataErro:
        If Err.Number <> 0 Then
            MessageBox 0, "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
        End If

End Sub

Private Sub MSHFlexGrid1_DblClick()
    vForm_Atual.txtCodCliente = MSHFlexGrid1.TextMatrix(Me.MSHFlexGrid1.Row, 1)
    Unload frmClientes
    MDIForm1.cmdLocalizar_Click
End Sub

Private Sub txtNomeCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
