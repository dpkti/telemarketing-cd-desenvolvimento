Attribute VB_Name = "modLigAuto"
Option Explicit

Public vIdRamal As Single
Public vIDMonExt As Single

Public vForm_Atual As Form
Public Parametros  As Parametros
Public vMonRet     As Integer  'Armazena o retorno do monitoramento de ramal

Public Declare Function MessageBox Lib "user32" Alias "MessageBoxA" (ByVal hwnd As Long, ByVal lpText As String, ByVal lpCaption As String, ByVal wType As Long) As Long

#If Win32 Then
    Public Const HWND_TOPMOST& = -1
#Else
    Public Const HWND_TOPMOST& = -1
#End If 'WIN32

#If Win32 Then
    Public Const SWP_NOMOVE& = &H2
    Public Const SWP_NOSIZE& = &H1
#Else
    Public Const SWP_NOMOVE& = &H2
    Public Const SWP_NOSIZE& = &H1
#End If 'WIN32

#If Win32 Then
    Declare Function SetWindowPos& Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
#Else
    Declare Sub SetWindowPos Lib "user" (ByVal hwnd As Integer, ByVal hWndInsertAfter As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer)
#End If 'WIN32


Public Type Parametros
    IP4000    As String
    Porta4000 As String
    IP3000    As String
    Porta3000 As String
    
    '----------------------------------
    ' Eduardo Diogo Garcia / 26-01-2010
    '----------------------------------
    IP8000      As String
    Porta8000   As String
    PrefixoFone As String
    Ramal8000   As String
    '----------------------------------
    
    Ativo As String
    SufixoFone As String
    
End Type

'--------------------------------------------------------
' Eduardo Diogo / 10-out-2009
'--------------------------------------------------------
' Registro com informa��es de login do usu�rio HP8000!
'--------------------------------------------------------
Public Type LogonReg
    vId           As String
    vLogon        As Boolean
    vAvailable    As Boolean
    vDoNotDisturb As Boolean
End Type

Public RegLogonUser As LogonReg
'--------------------------------------

'-------------------
'Usado para o HP4000
'-------------------
Function NomeEventTel(ByVal TelephonyEvent As enumTelephonyEvents) As String

Dim lsAux As String

    Select Case TelephonyEvent
        Case EVENT_LINE_DISCONNECTED
            lsAux = "Desligou"
        Case EVENT_LINE_ESTABLISHED
            lsAux = "Liga��o em Curso"
        Case EVENT_LINE_DELIVERED
            lsAux = "Tocar"
        Case EVENT_LINE_FAILED
            lsAux = "Failed"
        Case EVENT_LINE_HELD
            lsAux = "Vai Transferir"
        Case EVENT_LINE_NETWORK
            lsAux = "Ligacao em Curso Para Celular"
        Case EVENT_LINE_RETRIEVED
            lsAux = "Puxou de Volta"
        Case EVENT_LINE_TRANSFERED
            lsAux = "Transferido"
        Case EVENT_LINE_CONFERENCED
            lsAux = "Conferenced"
        Case EVENT_LINE_ORIGINATED
            lsAux = "Originado"
        Case EVENT_LINE_DIVERTED
            lsAux = "Diverted"
        Case EVENT_LINE_QUEUED
            lsAux = "Queued"
        Case EVENT_LINE_SERVICEINITIATED
            lsAux = "Tirou do Gancho"
        Case EVENT_LINE_UNKNOWN
            lsAux = "Unknown"

    End Select

    NomeEventTel = lsAux
    
End Function

'--------------------------------------------------------------------------------
' NomeEventTel8000(TelephonyEvent As ConWareSDK.enumMidiaEvents)
'--------------------------------------------------------------------------------
' Autor: Eduardo Diogo Garcia
'  Data: 26-01-2010
'--------------------------------------------------------------------------------
Function NomeEventTel8000(TelephonyEvent As ConWareSDK.enumMidiaEvents) As String

    Dim lsAux As String

    Select Case TelephonyEvent
        Case EVENT_MIDIA_DISCONNECTED
            lsAux = "Disconnect"
        Case EVENT_MIDIA_ESTABLISHED
            lsAux = "Established"
        Case EVENT_MIDIA_DELIVERED
            lsAux = "Delivered"
        Case EVENT_MIDIA_FAILED
            lsAux = "Failed"
        Case EVENT_MIDIA_HELD
            lsAux = "Held"
        Case EVENT_MIDIA_NETWORK
            lsAux = "Network"
        Case EVENT_MIDIA_RETRIEVED
            lsAux = "Retrieved"
        Case EVENT_MIDIA_TRANSFERED
            lsAux = "Transfered"
        Case EVENT_MIDIA_CONFERENCED
            lsAux = "Conferenced"
        Case EVENT_MIDIA_ORIGINATED
            lsAux = "Originated"
        Case EVENT_MIDIA_DIVERTED
            lsAux = "Diverted"
        Case EVENT_MIDIA_QUEUED
            lsAux = "Queued"
        Case EVENT_MIDIA_SERVICEINITIATED
            lsAux = "Initiated"
        Case EVENT_MIDIA_UNKNOWN
            lsAux = "Unknown"

    End Select

    NomeEventTel8000 = lsAux
    
End Function

Public Sub Pegar_Valores_Parametros()

    Dim vIP4000 As String
    Dim vPorta4000 As String
    Dim vIP3000 As String
    Dim vPorta3000 As String
    
    '----------------------------------------------------------
    'Eduardo Diogo Garcia / 26-01-2010 - Novo ambiente HP8000
    '----------------------------------------------------------
    Dim vIP8000      As String
    Dim vPorta8000   As String
    Dim vPrefixoFone As String
    Dim vSufixoFone  As String
    '----------------------------------------------------------
    
    Dim vLigAuto As String
    Dim vUsuarioConectado As String
    
    vIP4000 = "" & Pegar_VL_Parametro("IP4000")
    vPorta4000 = "" & Pegar_VL_Parametro("PORTA4000")
    vIP3000 = "" & Pegar_VL_Parametro("IP3000")
    vPorta3000 = "" & Pegar_VL_Parametro("PORTA3000")
    vLigAuto = "" & Pegar_VL_Parametro("LIGACAOAUTOMATICA")

    '----------------------------------------------------------
    'Eduardo Diogo Garcia / 26-01-2010 - Novo ambiente HP8000
    '----------------------------------------------------------
    vIP8000 = "" & Pegar_VL_Parametro("IP8000")
    vPorta8000 = "" & Pegar_VL_Parametro("PORTA8000")
    vPrefixoFone = "" & Pegar_VL_Parametro("PREFIXO_FONE")
    vSufixoFone = Pegar_VL_Parametro("SUFIXO_FONE")
    '----------------------------------------------------------

    If vLigAuto = "" Then
        vLigAuto = "" & Pegar_VL_Parametro("LigacaoAutomatica")
    End If
    
    vUsuarioConectado = "" & Pegar_VL_Parametro("USUARIO")
    
    If vLigAuto = "0" Or vUsuarioConectado = "0" Then
        Parametros.Ativo = "0"
    Else
        Parametros.Ativo = "1"
    End If
    
    Parametros.IP4000 = vIP4000
    Parametros.Porta4000 = vPorta4000
    Parametros.IP3000 = vIP3000
    Parametros.Porta3000 = vPorta3000
    
    '----------------------------------------------------------
    'Eduardo Diogo Garcia / 26-01-2010 - Novo ambiente HP8000
    '----------------------------------------------------------
    Parametros.IP8000 = vIP8000
    Parametros.Porta8000 = vPorta8000
    Parametros.PrefixoFone = vPrefixoFone
    Parametros.SufixoFone = vSufixoFone
    '----------------------------------------------------------

End Sub

Public Function Pegar_Codigo_Sistema() As Double
    '-- Pegar o c�digo do sistema na tabela de HELPDESK.SOFTWARE usando o valor da Propriedade TITLE
    vBanco.Parameters.Remove "Nome_Software"
    vBanco.Parameters.Add "Nome_Software", UCase(App.Title), 1
    vBanco.Parameters.Remove "Cod_Software"
    vBanco.Parameters.Add "Cod_Software", 0, 2

    vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.PCK_VDA230.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software)"

    Pegar_Codigo_Sistema = vBanco.Parameters("Cod_Software").Value

    vBanco.Parameters.Remove "Cod_Software"

End Function

Public Function Get_DDD_FONE(Fone As String)
    Dim vFone As Double
    Dim vDDD As Double

    If Len(Trim(Fone)) < 5 Then
        OraParameters.Remove "DDD"
        OraParameters.Remove "FONE"
        Exit Function
    End If
    
    If Left(Fone, 1) = "0" Then
        
        If Left(Fone, 2) = "00" Then
            If Left(Fone, 5) = "00800" Or Left(Fone, 5) = "00300" Then
                vDDD = Mid(Fone, 2, 4)
                vFone = Mid(Fone, 5)
            Else
                vDDD = Mid(Fone, 3, 2)
                vFone = Mid(Fone, 5)
            End If
        Else
            If Len(Fone) = 8 Then
                OraParameters.Remove "DDD"
                OraParameters.Add "DDD", 0, 2
                
                OraParameters.Remove "CodLojaDDD"
                OraParameters.Add "CodLojaDDD", lngCD, 1
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                
                vDDD = vBanco.Parameters("DDD").Value
            
                vFone = Mid(Fone, 2)
                
            ElseIf Len(Fone) = 9 Then
                OraParameters.Remove "DDD"
                OraParameters.Add "DDD", 0, 2
                
                OraParameters.Remove "CodLojaDDD"
                OraParameters.Add "CodLojaDDD", lngCD, 1
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                
                vDDD = vBanco.Parameters("DDD").Value
            
                vFone = Mid(Fone, 2)
            
            ElseIf Len(Fone) > 9 Then
                vDDD = Mid(Fone, 2, 2)
                vFone = Mid(Fone, 4)
            End If
        End If
    
    Else
        If Len(Fone) > 10 Then
            vDDD = Mid(Fone, 3, 2)
            vFone = Mid(Fone, 5)
        ElseIf Len(Fone) = 10 Then
            vDDD = Left(Fone, 2)
            vFone = Mid(Fone, 3)

        ElseIf Len(Fone) = 9 Then
            
            OraParameters.Remove "DDD"
            OraParameters.Add "DDD", 0, 2
            
            OraParameters.Remove "CodLojaDDD"
            OraParameters.Add "CodLojaDDD", lngCD, 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
            
            vDDD = vBanco.Parameters("DDD").Value
            
            'If lngCD = 4 Then
            '    vFone = Mid(Fone, 3)
            'Else
                vFone = Mid(Fone, 2)
            'End If
        
        ElseIf Len(Fone) = 8 Or Len(Fone) = 7 Then
                        
            vFone = Fone
            
            OraParameters.Remove "DDD"
            OraParameters.Add "DDD", 0, 2
            
            OraParameters.Remove "CodLojaDDD"
            OraParameters.Add "CodLojaDDD", lngCD, 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
            
            vDDD = vBanco.Parameters("DDD").Value
        
        End If
    End If

    OraParameters.Remove "DDD"
    OraParameters.Add "DDD", vDDD, 1
    OraParameters.Remove "FONE"
    OraParameters.Add "FONE", vFone, 1

End Function

Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)
    Criar_Cursor
    
    Dim vOwner As String
    
    If strTabela_Banco = "PRODUCAO." Then
        vOwner = "HELPDESK."
    Else
        vOwner = strTabela_Banco
    End If
    
    oradatabase.Parameters.Remove "Cod_Sistema"
    oradatabase.Parameters.Add "Cod_Sistema", 1069, 1

    oradatabase.Parameters.Remove "Nome_Parametro"
    oradatabase.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1

    oradatabase.Parameters.Remove "DEP"
    oradatabase.Parameters.Add "DEP", vOwner, 1

    vErro = vVB_Generica_001.ExecutaPl(oradatabase, "PRODUCAO.PCK_VDA230.PR_SELECT_VL_PARAMETRO2(:vCursor, :Cod_Sistema, :Nome_Parametro, :DEP)")

    If vErro = "" Then
        Set vObjOracle = oradatabase.Parameters("vCursor").Value
        
        'Verificar se o usuario est� cadastrado na tabela HELPDESK.Parametros,
        'se tiver entao nao pode acessar a Ligacao Automatica
        'If UCase(NOME_PARAMETRO) = "USUARIO" Then
        '    For i = 1 To vObjOracle.recordcount
        '        If vObjOracle!vl_parametro = sCOD_VEND Then
        '            Pegar_VL_Parametro = 0
        '            GoTo Sair
        '            'Exit Function
        '        End If
        '        vObjOracle.MOVENEXT
        '    Next
        'End If
        
        Pegar_VL_Parametro = vObjOracle("VL_PARAMETRO")
    End If
    
Sair:
    oradatabase.Parameters.Remove "Cod_Sistema"
    oradatabase.Parameters.Remove "Nome_Parametro"
    oradatabase.Parameters.Remove "DEP"
    
End Function


Public Function Form_Aberto(frm As String) As Boolean
    Dim f As Form
    For Each f In Forms
        If UCase(frm) = UCase(f.Name) Then
            Form_Aberto = True
            Set f = Nothing
            Exit Function
        End If
    Next
    Form_Aberto = False
    Set f = Nothing
End Function

Function StayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, HWND_TOPMOST, 0, 0, 0, 0, lFlags)
End Function

Function NonStayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, -2, 0, 0, 0, 0, lFlags)
End Function
