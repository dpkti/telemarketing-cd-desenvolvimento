VERSION 5.00
Begin VB.Form frmParametros 
   Caption         =   "Par�metros"
   ClientHeight    =   720
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4155
   LinkTopic       =   "Form1"
   ScaleHeight     =   720
   ScaleWidth      =   4155
   StartUpPosition =   2  'CenterScreen
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdMonitorar_Click()
    
    If Me.TxtRamal = "" Then
        MsgBox "Informe o Ramal.", vbInformation, "Aten��o"
        TxtRamal.SetFocus
        Exit Sub
    End If
    
    vRamal = TxtRamal
    
    vMonRet = MDIForm1.monExt4000.fncMonitorStop
    vMonRet = MDIForm1.monExt4000.Disconnect
    
    MDIForm1.monExt4000.EnderecoServidor = "10.33.1.19"
    MDIForm1.monExt4000.PortaServidor = "10000"
    
    vMonRet = MDIForm1.monExt4000.Connect
        
    MDIForm1.monExt4000.Ramal = TxtRamal

    vMonRet = MDIForm1.monExt4000.fncMonitorStart
    
    If Form_Aberto("FRMRECADO") = True Then
        If vMonRet = 1 Then
            Me.lblMonitoramento.Caption = "Ativado"
            Me.lblMonitoramento.ForeColor = vbGreen
        Else
            Me.lblMonitoramento.Caption = "Desativado"
            Me.lblMonitoramento.ForeColor = vbRed
        End If
    End If
        
End Sub
