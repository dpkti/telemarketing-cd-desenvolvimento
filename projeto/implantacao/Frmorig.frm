VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "Grid32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmOrig 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lista de Itens por C�digo Original"
   ClientHeight    =   3345
   ClientLeft      =   1455
   ClientTop       =   2535
   ClientWidth     =   5610
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3345
   ScaleWidth      =   5610
   StartUpPosition =   2  'CenterScreen
   Begin MSGrid.Grid grdOriginal 
      Height          =   2175
      Left            =   30
      TabIndex        =   2
      Top             =   1140
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "Frmorig.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   30
      TabIndex        =   3
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmorig.frx":001C
      PICN            =   "Frmorig.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   4
      Top             =   720
      Width           =   5550
      _ExtentX        =   9790
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "N�mero Original"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   60
      TabIndex        =   1
      Top             =   840
      Width           =   1365
   End
   Begin VB.Label lblOriginal 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   1470
      TabIndex        =   0
      Top             =   810
      Width           =   1575
   End
End
Attribute VB_Name = "frmOrig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()
    lblOriginal = ""
    Unload Me
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmOrig = Nothing
End Sub

Private Sub grdOriginal_DblClick()

    
   
    grdOriginal.Col = 2
    frmVenda.txtCOD_DPK = grdOriginal.Text
    frmVenda.txtCOD_DPK_LostFocus

    frmOrig.Hide

    If frmVenda.txtCOD_DPK <> "" Then
        Call frmVenda.GetItem(frmVenda.txtCOD_DPK, -1, -1)
        frmVenda.fabrica = frmVenda.txtCOD_FABRICA.Text
    End If
End Sub

