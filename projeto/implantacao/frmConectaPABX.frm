VERSION 5.00
Begin VB.Form frmConectaPABX 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conex�o com Pabx"
   ClientHeight    =   1710
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5340
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1710
   ScaleWidth      =   5340
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer2 
      Left            =   30
      Top             =   60
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Conex�o    Ramal"
      Height          =   525
      Left            =   2850
      TabIndex        =   2
      Top             =   1020
      Width           =   1545
      Begin VB.Image ImgRamal 
         Height          =   195
         Left            =   1080
         ToolTipText     =   "Ramal"
         Top             =   240
         Width           =   195
      End
      Begin VB.Image ImgConexao 
         Height          =   195
         Left            =   330
         ToolTipText     =   "Conex�o"
         Top             =   240
         Width           =   195
      End
   End
   Begin VB.Image ImgVermelho 
      Height          =   90
      Left            =   1890
      Picture         =   "frmConectaPABX.frx":0000
      Top             =   1230
      Visible         =   0   'False
      Width           =   120
   End
   Begin VB.Image imgVerde 
      Height          =   105
      Left            =   1890
      Picture         =   "frmConectaPABX.frx":00D2
      Top             =   1080
      Visible         =   0   'False
      Width           =   120
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   2040
      TabIndex        =   4
      Top             =   1020
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label Label2 
      Caption         =   "Label2"
      Height          =   195
      Left            =   2040
      TabIndex        =   3
      Top             =   1230
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Aguarde..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1170
      TabIndex        =   1
      Top             =   90
      Width           =   2895
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "O sistema est� tentando se conectar com o PABX."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   765
      TabIndex        =   0
      Top             =   420
      Width           =   3765
   End
End
Attribute VB_Name = "frmConectaPABX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmConectaPABX
' Author    : c.samuel.oliveira
' Date      : 06/09/18
' Purpose   : TI-6660
'---------------------------------------------------------------------------------------
Option Explicit

Private Sub Form_Load()
    ImgConexao.Picture = ImgVermelho
    ImgRamal.Picture = ImgVermelho

    Timer2.Interval = 1000
End Sub


Private Sub Timer2_Timer()

1         On Error GoTo Trata_Erro

2         Me.Visible = True

          'Conectar o Monitoramento
3         If Label1.Caption <> "1" Then

4             If Parametros.Ativo = "1" Then

5                 If Parametros.IP4000 <> "" Then
6                     'vMonRet = MDIForm1.MonExt4000.Connect 'TI-6660

7                 ElseIf Parametros.IP3000 <> "" Then
                      
8                     'vMonRet = MDIForm1.MonExt3000.Connect(Parametros.IP3000, Parametros.Porta3000, "$.�D$(3_�����ط����4D$�65����4��3��)A3�@�$.7(T$.�Dg��$���ۤ��@")'TI-6660
9                     If vMonRet <> 1 Then vMonRet = 0
                  
10                End If

11                If vMonRet = 0 Then
12                    ImgConexao.Picture = ImgVermelho
13                    Label1.Caption = "0"
14                Else
15                    Label1.Caption = "1"
16                End If
17            Else
18                ImgConexao.Picture = imgVerde
19                Label1.Caption = "1"
20            End If
21        End If

22        If Label2.Caption <> "1" Then
23            Conectar_Ramal
24        End If

25        If Label1.Caption = "1" And Label2.Caption = "1" Then
26            MDIForm1.Conexao_Ramal_OK = True
27            Timer2.Interval = 0
28            Unload Me
29        Else
30            MDIForm1.Conexao_Ramal_OK = False
31            Timer2.Interval = 1000
32            'If Label1.Caption = "0" Then MDIForm1.MonExt4000.Disconnect'TI-6660
33            'If Label2.Caption = "0" Then MDIForm1.MonExt4000.fncMonitorStop 'TI-6660
34            Sleep 1000
35        End If
Trata_Erro:
36            If Err.Number <> 0 Then
37                MessageBox 0, "Sub grdCliente_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
38            End If

End Sub

Sub Conectar_Ramal()

    On Error GoTo Trata_Erro

          Dim vAgent As Byte
          Dim vExt As Byte

1         vMonRet = 0

2         If Parametros.IP4000 <> "" Then
              
3             'vMonRet = MDIForm1.MonExt4000.fncMonitorStart 'TI-6660
          
4         ElseIf Parametros.IP3000 <> "" Then
          
5             vIDRamal = Rnd(9999)
6             vIDMonExt = Rnd(9999)
              
7             'vAgent = MDIForm1.MonExt3000.StartAgentMonitor(vIDRamal, vRamal) 'TI-6660
8             'vExt = MDIForm1.MonExt3000.StartExtMonitor(vIDMonExt, vRamal) 'TI-6660
              
9             If vAgent = 1 And vExt = 1 Then
10                vMonRet = 1
11            End If
          
              'Verificar se a conexao est� OK
12            'Call MDIForm1.MonExt3000_StartExtMonitorResult(vIDRamal, 0) 'TI-6660
          
13        End If

14        If vMonRet = 0 Then
15            ImgRamal.Picture = ImgVermelho
16            Label2 = "0"
17        Else
18            ImgRamal.Picture = imgVerde
19            Label2 = "1"
20        End If
          
Trata_Erro:
        If Err.Number <> 0 Then
            MessageBox 0, "Sub grdCliente_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
        End If
          
End Sub
