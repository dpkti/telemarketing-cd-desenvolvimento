VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmLig_Nao_Atend 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lista de liga��es n�o atendidas"
   ClientHeight    =   5400
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5880
   ControlBox      =   0   'False
   Icon            =   "frmLig_Nao_Atend.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   360
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   392
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   5025
      Width           =   5880
      _ExtentX        =   10372
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10319
            Text            =   "Para fazer liga��o desta tela, d� duplo clique sobre o item da lista"
            TextSave        =   "Para fazer liga��o desta tela, d� duplo clique sobre o item da lista"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox txtDtLigacao 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3780
      Locked          =   -1  'True
      MaxLength       =   8
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   360
      Width           =   1260
   End
   Begin MSComctlLib.ListView lsvLigacoes 
      Height          =   3945
      Left            =   60
      TabIndex        =   2
      Top             =   960
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   6959
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Cliente"
         Object.Width           =   66146
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Data Liga��o"
         Object.Width           =   52917
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "DDD"
         Object.Width           =   13229
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Fone"
         Object.Width           =   38100
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLig_Nao_Atend.frx":23D2
      PICN            =   "frmLig_Nao_Atend.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   5130
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Consultar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLig_Nao_Atend.frx":30C8
      PICN            =   "frmLig_Nao_Atend.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Dt.Liga��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   3780
      TabIndex        =   4
      Top             =   120
      Width           =   915
   End
End
Attribute VB_Name = "frmLig_Nao_Atend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmLig_Nao_Atend
' Author    : c.samuel.oliveira
' Date      : 13/01/16
' Purpose   : TI-3880
'---------------------------------------------------------------------------------------


Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub cmdConsultar_Click()
    If txtDtLigacao = "" Then
       MessageBox 0, "Informe a data para pesquisa.", "Aten��o", &H40000
       Exit Sub
    End If
    Preencher_Lsv
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
'    Preencher_Lsv
End Sub


Private Sub lsvLigacoes_DblClick()
    
1   On Error GoTo Trata_Erro

2   strPreencher_Cliente = False
    Dim DDD As String
          
3   If vTrabalharConectado = True And Left(Trim(Command$), 1) <> "V" Then 'TI-3880 cmd
4       vListaTelfones = True
5       If PEGAR_DDD(UCase(lsvLigacoes.SelectedItem.SubItems(4))) = True Then
6           DDD = ""
7       Else
8           DDD = "0" & lsvTelefones.SelectedItem.SubItems(2)
9       End If
    
10      frmVenda.TxtRamalVozFone = DDD & lsvTelefones.SelectedItem.SubItems(3)
11      frmVenda.cmdDiscar.Tag = "Lista"
12      Unload frmLig_Nao_Atend
13      frmVenda.vAchouCliente = True
14      frmVenda.cmdDiscar_Click
15  End If

Trata_Erro:
16  If Err.Number <> 0 Then
17     MessageBox 0, "Sub lsvLigacoes_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
18  End If
End Sub

Private Sub txtDtLigacao_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtLigacao
End Sub

Sub Preencher_Lsv()
    Dim lItem As ListItem
    Dim vObj As Object
    
    vSql = "Begin PRODUCAO.pck_vda230.PR_SELECT_LIG_NAO_ATEND(:PM_CURSOR, :PM_VEND, :PM_Data);END;"

    'Criar_Cursor
    oradatabase.ExecuteSQL vSql
    Set vObj = oradatabase.Parameters("vCursor").Value
    'oradatabase.Parameters.Remove ("vCursor")
    
    lsvLigacoes.ListItems.Clear
    
    For i = 1 To vObj.RecordCount
        lItem = vObj!cliente
        lItem.SubItems(1) = vObj!Data
        lItem.SubItems(2) = vObj!DDD
        lItem.SubItems(3) = vObj!Fone
        vObj.MoveNext
    Next
    Set vObj = Nothing
    Set lItem = Nothing
End Sub
