VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFoneCliente 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Atribuir o Telefone Novo a um Cliente j� existente"
   ClientHeight    =   4890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7200
   ControlBox      =   0   'False
   Icon            =   "frmFoneCliente.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   326
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   480
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar PbClientes 
      Height          =   1455
      Left            =   6900
      TabIndex        =   21
      Top             =   3330
      Width           =   165
      _ExtentX        =   291
      _ExtentY        =   2566
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Orientation     =   1
      Scrolling       =   1
   End
   Begin MSFlexGridLib.MSFlexGrid FlgClientes 
      Height          =   1665
      Left            =   30
      TabIndex        =   20
      Top             =   1440
      Width           =   7125
      _ExtentX        =   12568
      _ExtentY        =   2937
      _Version        =   393216
      Cols            =   5
      AllowUserResizing=   1
   End
   Begin VB.Frame Frame2 
      Caption         =   "Cliente Selecionado"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   30
      TabIndex        =   11
      Top             =   3150
      Width           =   7125
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "CGC"
         Height          =   195
         Left            =   960
         TabIndex        =   19
         Top             =   210
         Width           =   330
      End
      Begin VB.Label lblCGC 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   960
         TabIndex        =   18
         Top             =   420
         Width           =   1515
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "C�d Cliente"
         Height          =   195
         Left            =   60
         TabIndex        =   17
         Top             =   210
         Width           =   810
      End
      Begin VB.Label lblCODIGO_CLIENTE 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   60
         TabIndex        =   16
         Top             =   420
         Width           =   825
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Cidade"
         Height          =   195
         Left            =   60
         TabIndex        =   15
         Top             =   1170
         Width           =   495
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Cliente"
         Height          =   195
         Left            =   60
         TabIndex        =   14
         Top             =   690
         Width           =   1170
      End
      Begin VB.Label lblNOME_CLIENTE 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   60
         TabIndex        =   13
         Top             =   900
         Width           =   5985
      End
      Begin VB.Label lblNOME_CIDADE 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   60
         TabIndex        =   12
         Top             =   1380
         Width           =   5985
      End
   End
   Begin VB.TextBox txtNomeCliente 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4170
      MaxLength       =   50
      TabIndex        =   3
      Top             =   1110
      Width           =   3015
   End
   Begin VB.TextBox txtCOD_CLIENTE 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   30
      MaxLength       =   6
      TabIndex        =   1
      Top             =   1110
      Width           =   795
   End
   Begin VB.TextBox txtCGC 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   14
      TabIndex        =   2
      Top             =   1110
      Width           =   1905
   End
   Begin VB.Frame Frame1 
      Caption         =   "N�mero Novo"
      Height          =   705
      Left            =   4470
      TabIndex        =   6
      Top             =   60
      Width           =   2655
      Begin VB.Label lblNumFone 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   345
         Left            =   150
         TabIndex        =   7
         Top             =   210
         Width           =   2355
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   7125
      _ExtentX        =   12568
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFoneCliente.frx":23D2
      PICN            =   "frmFoneCliente.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAtribuir 
      Height          =   675
      Left            =   780
      TabIndex        =   4
      ToolTipText     =   "Atribuir o Telefone para o Cliente selecionado"
      Top             =   60
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFoneCliente.frx":30C8
      PICN            =   "frmFoneCliente.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Nome"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   4170
      TabIndex        =   10
      Top             =   900
      Width           =   420
   End
   Begin VB.Label Codigo 
      AutoSize        =   -1  'True
      Caption         =   "C�digo"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   9
      Top             =   900
      Width           =   495
   End
   Begin VB.Label CGC 
      AutoSize        =   -1  'True
      Caption         =   "C.G.C."
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1560
      TabIndex        =   8
      Top             =   900
      Width           =   465
   End
End
Attribute VB_Name = "frmFoneCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdLocalizar_Click()
          
          On Error GoTo Trata_Erro
          
          Dim i As Double
          Dim Regs As Double

1         Me.MousePointer = vbHourglass

2         PbClientes.Value = 0

3         Formatar_Grid

4         OraParameters.Remove "Cod"
5         OraParameters.Add "Cod", IIf(txtCOD_CLIENTE = "", 0, txtCOD_CLIENTE), 1

6         OraParameters.Remove "CGC"
7         OraParameters.Add "CGC", IIf(txtCGC = "", 0, txtCGC), 1

8         OraParameters.Remove "nome"
9         OraParameters.Add "nome", txtNomeCliente, 1

10        vSql = "Begin producao.pck_vda230.Pr_Select_Cliente_Fone(:vCursor,:Cod, :CGC, :nome);END;"

11        'Criar_Cursor
12        oradatabase.ExecuteSQL vSql
13        Set ss = oradatabase.Parameters("vCursor").Value
14        'oradatabase.Parameters.Remove ("vCursor")

15        If ss.EOF = False Then
16            Regs = ss.RecordCount
17            PbClientes.Max = Regs
18        Else
19            NonStayOnTop Me
20            MessageBox 0, "Cliente n�o encontrado. " & vbCrLf & "Verifique se voc� digitou corretamente.", "Aten�ao", &H40000 + 0
21            StayOnTop Me

22            Me.MousePointer = vbNormal
23            Exit Sub
24        End If

25        For i = 1 To Regs
26            If i > 1 Then FlgClientes.AddItem i
27            FlgClientes.TextMatrix(i, 0) = i
28            FlgClientes.TextMatrix(i, 1) = ss("Cod_Cliente")
29            FlgClientes.TextMatrix(i, 2) = ss("CGC")
30            FlgClientes.TextMatrix(i, 3) = ss("Nome_Cliente")
31            FlgClientes.TextMatrix(i, 4) = ss("Nome_Cidade")
32            ss.MoveNext
33            PbClientes.Value = PbClientes.Value + 1
34        Next
35        PbClientes.Visible = False

36        Me.MousePointer = vbNormal

Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub cmdLocalizar_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If

End Sub

Private Sub cmdAtribuir_Click()
1     On Error GoTo TrataErro

2     If lblCODIGO_CLIENTE = "" Then
3       NonStayOnTop Me
4       MessageBox 0, "Selecione um cliente da lista.", "Aten�ao", &H40000 + 0
5       StayOnTop Me
6       Exit Sub
7     End If

8     If Val(txtCOD_CLIENTE) = 52306 Then
9       MessageBox 0, "N�o � possivel adicionar telefones para este cliente.", "Aten�ao", &H40000 + 0
10      Exit Sub
11    End If

12    If Trim(lblNumFone) < 5 Then
13       MessageBox 0, "Este telefone � um ramal e n�o pode ser atribuido.", "Aten��o", &H40000 + 0
14       Exit Sub
15    End If

16    OraParameters.Remove "Cod_Cliente"
17    OraParameters.Remove "Cod_Erro"
18    OraParameters.Remove "Txt_Erro"

19    OraParameters.Add "Cod_Cliente", lblCODIGO_CLIENTE, 1
20    OraParameters.Add "Cod_Erro", 0, 2
21    OraParameters.Add "Txt_Erro", "", 2

22    Get_DDD_FONE Trim(lblNumFone)

23    oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Insert_Fone_Cliente(:COD_CLIENTE, :DDD, :FONE, :COD_ERRO, :TXT_ERRO); END;"

24    If oradatabase.Parameters("Cod_Erro").Value <> 0 Then
25       If InStr(1, UCase(OraParameters("Txt_Erro")), "UNIQUE CONSTRAINT") > 0 Then
26          Unload Me
27       Else
28          MessageBox 0, oradatabase.Parameters("Txt_Erro").Value, "Aten��o", &H40000 + 0
29          Exit Sub
30       End If
31    Else
        'Se n�o tiver nenhum form de resultado aberto ent�o atribuir o codigo do cliente para o Novaligacao
32      If Form_Aberto("FrmLig") = False Then
33          NovaLigacao.CodCliente = lblCODIGO_CLIENTE
34      Else
            Dim QtdFrmLIg As Byte
            Dim frms As Form
35          For Each frms In Forms
36              If UCase(frms.Name) = "FRMVENDA" And Right(frmLig.txtFoneOrigem, 8) = Right(lblNumFone, 8) Then
37                  frmLig.txtCodCliente = lblCODIGO_CLIENTE
38              End If
39          Next
40      End If
        
41      If frmVenda.fraClienteEspera.Visible = True Then
42          frmVenda.cmdClienteEspera.Caption = OraParameters("DDD") & OraParameters("FONE")
43          frmVenda.cmdClienteEspera.Tag = lblCODIGO_CLIENTE
44      Else
45          frmVenda.txtCOD_CLIENTE = ""
46          frmVenda.txtCOD_CLIENTE = lblCODIGO_CLIENTE
47          frmVenda.txtCOD_CLIENTE_LostFocus
48      End If
49      Unload Me
50    End If

TrataErro:
51        If Err.Number <> 0 Then
52              MessageBox 0, "CmdAtribuir_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
53        End If
End Sub

Private Sub cmdVoltar_Click()
    NovaLigacao.CodCliente = 0
    Unload Me
End Sub

Private Sub FlgClientes_Click()
    lblCODIGO_CLIENTE = FlgClientes.TextMatrix(FlgClientes.Row, 1)
    lblCGC = FlgClientes.TextMatrix(FlgClientes.Row, 2)
    lblNOME_CLIENTE = FlgClientes.TextMatrix(FlgClientes.Row, 3)
    lblNOME_CIDADE = FlgClientes.TextMatrix(FlgClientes.Row, 4)
End Sub

Private Sub FlgClientes_DblClick()
    cmdAtribuir_Click
End Sub

Private Sub Form_Load()
    Formatar_Grid
    lblNumFone = vNumFoneNovo
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmFoneCliente = Nothing
End Sub

Private Sub txtCGC_GotFocus()
    txtCOD_CLIENTE = ""
    txtNomeCliente = ""
End Sub

Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCGC_LostFocus()
    If txtCGC <> "" Then
        cmdLocalizar_Click
    End If
End Sub

Private Sub txtCOD_CLIENTE_GotFocus()
    txtCGC = ""
    txtNomeCliente = ""
    lblLimite = ""
End Sub

Private Sub txtCOD_CLIENTE_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cmdLocalizar_Click
    ElseIf KeyAscii < 47 Or KeyAscii > 58 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtCOD_CLIENTE_LostFocus()
    If txtCOD_CLIENTE <> "" Then
        cmdLocalizar_Click
    End If
End Sub

Private Sub txtNomeCliente_GotFocus()
    txtCOD_CLIENTE = ""
    txtCGC = ""
    lblLimite = ""
End Sub

Private Sub txtNomeCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtNomeCliente_LostFocus()
    If txtNomeCliente <> "" Then
        cmdLocalizar_Click
    End If
End Sub

Sub Formatar_Grid()
    FlgClientes.Clear

    FlgClientes.ColWidth(0) = 500
    FlgClientes.ColWidth(1) = 800
    FlgClientes.ColWidth(2) = 1400
    FlgClientes.ColWidth(3) = 2000
    FlgClientes.ColWidth(4) = 1800

    FlgClientes.TextMatrix(0, 1) = "C�digo"
    FlgClientes.TextMatrix(0, 2) = "CGC"
    FlgClientes.TextMatrix(0, 3) = "Nome"
    FlgClientes.TextMatrix(0, 4) = "Cidade"
End Sub

