VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmRamal 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Informe seu ramal"
   ClientHeight    =   1890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3795
   ControlBox      =   0   'False
   Icon            =   "frmRamal.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   126
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   253
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtID 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      IMEMode         =   3  'DISABLE
      Left            =   2790
      MaxLength       =   5
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   1395
      Width           =   945
   End
   Begin VB.TextBox txtRamal 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   2790
      TabIndex        =   0
      Top             =   870
      Width           =   945
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   3
      Top             =   810
      Width           =   3675
      _ExtentX        =   6482
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRamal.frx":000C
      PICN            =   "frmRamal.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Informe o c�digo do agente:"
      Height          =   195
      Left            =   105
      TabIndex        =   5
      Top             =   1425
      Width           =   1995
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Informe o Ramal que voc� ir� utilizar:"
      Height          =   195
      Left            =   105
      TabIndex        =   4
      Top             =   990
      Width           =   2610
   End
End
Attribute VB_Name = "frmRamal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmRamal
' Author    : c.samuel.oliveira
' Date      : 06/09/18
' Purpose   : TI-6660
'---------------------------------------------------------------------------------------

Public Sub cmdVoltar_Click()

1         On Error GoTo Trata_Erro

          'TI-6660
'2         If Len(Trim(txtRamal)) = 0 Then
'3             If MsgBox("Deseja sair ?", vbYesNo + vbInformation, "Aten��o") = vbYes Then
'4                 End
'5             End If
'6             txtRamal.SetFocus
'7             Exit Sub
'8         End If

9         OraParameters.Remove "Cod_Repres"
10        OraParameters.Add "Cod_Repres", sCOD_VEND, 1

          'Criar_Cursor
11        oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Select_Ramal(:vCursor, :Cod_Repres); END;"
12        Set vObjOracle = oradatabase.Parameters("vCursor").Value
          'oradatabase.Parameters.Remove ("vCursor")

'13        If vObjOracle.RecordCount <= 0 Then
'14            MsgBox "Ramal n�o cadastrado. Verifique !", vbInformation, "Aten��o"
'15            txtRamal.SetFocus
'16            Exit Sub
'17        ElseIf IIf(IsNull(vObjOracle("Ramal")), "", vObjOracle("Ramal")) <> txtRamal Then
'18            MsgBox "O Ramal informado � diferente do cadastrado, verifique.", vbInformation, "Aten��o"
'19            txtRamal.SetFocus
'20            Exit Sub
'21        End If

          If vObjOracle.RecordCount <= 0 Then
            MsgBox "Ramal n�o cadastrado. Verifique !", vbInformation, "Aten��o"
            Exit Sub
          End If
          If Not vObjOracle.BOF And Not vObjOracle.EOF Then
            vRamal = vObjOracle("Ramal")
          End If
          
22        'vRamal = txtRamal

          '--------------------------------------------------------
          ' Eduardo Diogo / 20-03-2009
          '--------------------------------------------------------
23        vSufixoFone = Pegar_VL_Parametro("SUFIXO_FONE_CD" & Format(lngCD, "00")) 'DPK-74
24        vPrefixoFone = Pegar_VL_Parametro("PREFIXO_FONE")
25        vRamalTitulo = Mid(vPrefixoFone, 3) & vRamal
26        vRamal = vPrefixoFone & vRamal & vSufixoFone
27        'RegLogonUser.vId = txtID.Text
          vDgTelSenha = Pegar_VL_Parametro("DGTEL_SENHA")
          vRamalTlmk = txtRamal.Text
          '--------------------------------------------------------

'28        If lngCD = 1 Then
'29            vRamalTlmk = IIf(Len(Trim(vRamal)) = 4, Mid(vRamal, 2), vRamal)
'30        Else
'31            vRamalTlmk = vRamal
'32        End If
          'FIM TI-6660

33        Unload Me

34        MDIForm1.Show

35        MDIForm1.SSCommand1_Click

Trata_Erro:
36        If Err.Number <> 0 Then
37            MessageBox 0, "Sub Cmd_Voltar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "LInha:" & Erl, "Aten��o", &H40000 + 0
38        End If

End Sub

Private Sub Form_Activate()
    txtRamal.SetFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmRamal = Nothing
End Sub

Private Sub txtID_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        cmdVoltar_Click
    Else
        If KeyAscii < 48 Or KeyAscii > 57 Then
            If KeyAscii <> 8 Then
                KeyAscii = 0
            End If
        End If
    End If
End Sub

Private Sub txtID_LostFocus()
    If txtID.Text <> "" Then
        cmdVoltar.SetFocus
    End If
End Sub

Private Sub txtRamal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then

        If txtRamal.Text = "" Then
            cmdVoltar_Click
        Else
            txtID.SetFocus
        End If

    Else
        If KeyAscii < 48 Or KeyAscii > 57 Then
            If KeyAscii <> 8 Then
                KeyAscii = 0
            End If
        End If
    End If
End Sub

Private Sub txtRamal_LostFocus()
    If txtRamal <> "" Then
        txtID.SetFocus
    End If
End Sub

