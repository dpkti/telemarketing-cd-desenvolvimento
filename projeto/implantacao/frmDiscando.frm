VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDiscando 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Discando..."
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4425
   ControlBox      =   0   'False
   Icon            =   "frmDiscando.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   92
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   295
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Left            =   1020
      Tag             =   "10000"
      Top             =   90
   End
   Begin VB.Frame Frame1 
      Caption         =   "N�mero sendo chamado"
      Height          =   555
      Left            =   2250
      TabIndex        =   3
      Top             =   90
      Width           =   2145
      Begin VB.Label lblNumFone 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   150
         TabIndex        =   4
         Top             =   210
         Width           =   1875
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDiscando.frx":000C
      PICN            =   "frmDiscando.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Discando... Retire o Fone do Gancho"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   480
      TabIndex        =   2
      Top             =   960
      Width           =   3495
   End
End
Attribute VB_Name = "frmDiscando"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmDiscando
' Author    : c.samuel.oliveira
' Date      : 12/09/18
' Purpose   : TI-6660
'---------------------------------------------------------------------------------------

Private Sub cmdVoltar_Click()
        
    NovaLigacao.RamalCanalFone = False
    NovaLigacao.AtivaSistema = False
    
    'Se o usuario comecou a discar ent�o posso derrubar a linha
    'caso contr�rio n�o.
    If vComecou_Discar = True Then
        'MDIForm1.Desligar TI-6660
    End If
        
    Unload Me
End Sub

Private Sub Form_Load()
    lblNumFone = NovaLigacao.FoneDestino
    If Form_Aberto("frmCVend") = True Then
        NovaLigacao.AtivaSistema = True 'Indica que o usuario clicou em um telefone da lista de ligacoes
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmDiscando = Nothing
End Sub

Private Sub Timer1_Timer()
    Unload Me
End Sub
