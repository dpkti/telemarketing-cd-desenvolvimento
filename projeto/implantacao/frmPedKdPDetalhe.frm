VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmPedKdPDetalhe 
   Caption         =   "Detalhe do Pedido"
   ClientHeight    =   6240
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   9585
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6240
   ScaleWidth      =   9585
   StartUpPosition =   3  'Windows Default
   Begin MSGrid.Grid grdItens 
      Height          =   5175
      Left            =   0
      TabIndex        =   0
      Top             =   900
      Width           =   9465
      _Version        =   65536
      _ExtentX        =   16695
      _ExtentY        =   9128
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmPedKdPDetalhe.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPedKdPDetalhe.frx":001C
      PICN            =   "frmPedKdPDetalhe.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   0
      TabIndex        =   2
      Top             =   765
      Width           =   10170
      _ExtentX        =   17939
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Plano:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3840
      TabIndex        =   12
      Top             =   495
      Width           =   555
   End
   Begin VB.Label lblPlano 
      AutoSize        =   -1  'True
      Caption         =   "999 - WWWWWWWWWWWWWWWWWWWW"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   4440
      TabIndex        =   11
      Top             =   480
      Width           =   3705
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Pedido:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1800
      TabIndex        =   10
      Top             =   495
      Width           =   660
   End
   Begin VB.Label lblPedido 
      AutoSize        =   -1  'True
      Caption         =   "9999999"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   2520
      TabIndex        =   9
      Top             =   480
      Width           =   630
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Loja:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   840
      TabIndex        =   8
      Top             =   495
      Width           =   435
   End
   Begin VB.Label lblLoja 
      AutoSize        =   -1  'True
      Caption         =   "99"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1320
      TabIndex        =   7
      Top             =   480
      Width           =   180
   End
   Begin VB.Label lblCli 
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   840
      TabIndex        =   6
      Top             =   135
      Width           =   660
   End
   Begin VB.Label lblCliente 
      AutoSize        =   -1  'True
      Caption         =   "999999 - WWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1560
      TabIndex        =   5
      Top             =   120
      Width           =   5625
   End
   Begin VB.Label lblCod 
      AutoSize        =   -1  'True
      Caption         =   "CNPJ:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   7320
      TabIndex        =   4
      Top             =   135
      Width           =   540
   End
   Begin VB.Label lblCNPJ 
      AutoSize        =   -1  'True
      Caption         =   "99999999999999"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   7920
      TabIndex        =   3
      Top             =   120
      Width           =   1260
   End
End
Attribute VB_Name = "frmPedKdPDetalhe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmPedKdPDetalhe
' Author    : c.samuel.oliveira
' Date      : 08/12/2017
' Purpose   : KMA-32
'---------------------------------------------------------------------------------------

Option Explicit

Private Sub cmdSair_Click()
    Unload Me
    Set frmPedKdPDetalhe = Nothing
End Sub

Private Sub Form_Load()
    lblCliente.Caption = frmPedidosKdaPeca.lblCliente
    lblCNPJ.Caption = frmPedidosKdaPeca.lblCNPJ
    lblLoja.Caption = vCodLojaKdp
    lblPedido.Caption = vNumPedidoKdp & "-" & vSeqPedidoKdp
    lblPlano.Caption = vDescPlanoKdp
        
    CarregarItens
    
End Sub
Private Sub CarregarItens()

    On Error GoTo TrataErro

    Dim ss As Object
    Dim i As Long
    
    Screen.MousePointer = vbHourglass

    oradatabase.Parameters.Remove "PM_SEQ_PEDIDO"
    oradatabase.Parameters.Add "PM_SEQ_PEDIDO", vSeqPedidoKdp, 1
    oradatabase.Parameters.Remove "PM_COD_LOJA"
    oradatabase.Parameters.Add "PM_COD_LOJA", vCodLojaKdp, 1
    oradatabase.Parameters.Remove "PM_NUM_PEDIDO"
    oradatabase.Parameters.Add "PM_NUM_PEDIDO", vNumPedidoKdp, 1
    
    vSql = "Begin producao.pck_vda230.PR_SEL_DETALHE_KDAPECA(:PM_SEQ_PEDIDO,:PM_COD_LOJA,:PM_NUM_PEDIDO,:vCursor);END;"
      
    'Criar_Cursor
    oradatabase.ExecuteSQL vSql

    Set ss = oradatabase.Parameters("vCursor").Value
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        grdItens.Visible = False
        Exit Sub
    End If
    
    'carrega dados
    With grdItens
        .Cols = 10
        .rows = ss.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 1000
        .ColWidth(2) = 2000
        .ColWidth(3) = 600
        .ColWidth(4) = 600
        .ColWidth(5) = 600
        .ColWidth(6) = 700
        .ColWidth(7) = 750
        .ColWidth(8) = 900
        .ColWidth(9) = 1100
        
        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Fabricante"
        .Col = 2
        .Text = "Descri��o do Item"
        .Col = 3
        .Text = "Desc1"
        .Col = 4
        .Text = "Desc2"
        .Col = 5
        .Text = "Desc3"
        .Col = 6
        .Text = "Qtd Solic"
        .Col = 7
        .Text = "Qtd Atend"
        .Col = 8
        .Text = "Pre�o do Item"
        .Col = 9
        .Text = "Total"
        
        For i = 1 To .rows - 1
            .Row = i
                       
            .Col = 0
            .Text = IIf(IsNull(ss!cod_dpk), "", ss!cod_dpk)
            .Col = 1
            .Text = IIf(IsNull(ss!sigla), "", ss!sigla)
            .Col = 2
            .Text = IIf(IsNull(ss!desc_item), "", ss!desc_item)
            .Col = 3
            .ColAlignment(3) = 1
            .Text = Format(IIf(IsNull(ss!PC_DESC1), "", ss!PC_DESC1), "#0.00")
            .Col = 4
            .ColAlignment(4) = 1
            .Text = Format(IIf(IsNull(ss!PC_DESC2), "", ss!PC_DESC2), "#0.00")
            .Col = 5
            .ColAlignment(5) = 1
            .Text = Format(IIf(IsNull(ss!PC_DESC3), "", ss!PC_DESC3), "#0.00")
            .Col = 6
            .ColAlignment(6) = 1
            .Text = IIf(IsNull(ss!qtd_solicitada), "", ss!qtd_solicitada)
            .Col = 7
            .ColAlignment(7) = 1
            .Text = IIf(IsNull(ss!QTD_ATENDIDA), "", ss!QTD_ATENDIDA)
            .Col = 8
            .ColAlignment(8) = 1
            .Text = Format(IIf(IsNull(ss!PR_LIQUIDO), "", ss!PR_LIQUIDO), "#0.00")
            .Col = 9
            .ColAlignment(9) = 1
            .Text = Format(IIf(IsNull(ss!VL_LIQUIDO), "", ss!VL_LIQUIDO), "#0.00")
            
            ss.MoveNext
        Next
        .Row = 1
    End With

    Screen.MousePointer = vbDefault

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    Else
        MessageBox 0, "Sub CarregarItens" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If

    Screen.MousePointer = vbDefault
    
End Sub

