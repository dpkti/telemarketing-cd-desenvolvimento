VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmOferecaTambem 
   Caption         =   "Ofere�a Tamb�m"
   ClientHeight    =   4965
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7950
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4965
   ScaleWidth      =   7950
   StartUpPosition =   3  'Windows Default
   Begin MSGrid.Grid grdItens 
      Height          =   3975
      Left            =   0
      TabIndex        =   0
      Top             =   900
      Width           =   7905
      _Version        =   65536
      _ExtentX        =   13944
      _ExtentY        =   7011
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmOferecaTambem.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmOferecaTambem.frx":001C
      PICN            =   "frmOferecaTambem.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   0
      TabIndex        =   2
      Top             =   795
      Width           =   9090
      _ExtentX        =   16034
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblMSG 
      AutoSize        =   -1  'True
      Caption         =   "N�O H� PRODUTOS PARA A PESQUISA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   1320
      TabIndex        =   3
      Top             =   360
      Visible         =   0   'False
      Width           =   3525
   End
End
Attribute VB_Name = "frmOferecaTambem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmOferecaTambem
' Author    : c.samuel.oliveira
' Date      : 30/05/2017
' Purpose   : TI-5827
'---------------------------------------------------------------------------------------

Private Sub cmdSair_Click()
    Unload Me
    Set frmOferecaTambem = Nothing
End Sub

Private Sub Form_Load()
    CarregarItens Val(Mid(Trim(frmVenda.cboDeposito.Text), 1, 2)), Val(frmVenda.txtCOD_DPK.Text), lngNUM_PEDIDO
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmOferecaTambem = Nothing
End Sub
Private Sub CarregarItens(pCodLoja As Integer, pCodDPK As Double, pNumPendente As Double)
    On Error GoTo TrataErro

    Dim p As Object
    Dim sUrl As String
    Dim xmlhttp As MSXML2.xmlhttp
    Dim i As Long
    
    grdItens.Visible = True
    lblMSG.Visible = False
    
    Screen.MousePointer = vbHourglass
     
    If Trim(vEnderecoREST) = "" Then CarregarEnderecoREST
        
    Set xmlhttp = CreateObject("MSXML2.xmlhttp")
    
    'Url Rest mandando par�metros
    sUrl = vEnderecoREST & "api/OferecaTambem/" & pCodLoja & "/" & pCodDPK & "/" & pNumPendente

    xmlhttp.Open "GET", sUrl, False
    xmlhttp.setRequestHeader "Content-Type", "application/json"
    xmlhttp.setRequestHeader "Authorization", "basic apisdpk_login:apisdpk_password"
    xmlhttp.send
    
    If xmlhttp.status = "200" Then
        
        Set p = JSON.parse(xmlhttp.responseText)

    End If

    If p.count = 0 Then
        Screen.MousePointer = vbDefault
        grdItens.Visible = False
        lblMSG.Visible = True
        Exit Sub
    End If

    'carrega dados
    With grdItens
        .Cols = 6
        .rows = p.count + 1
        .ColWidth(0) = 500
        .ColWidth(1) = 1000
        .ColWidth(2) = 1200
        .ColWidth(3) = 3100
        .ColWidth(4) = 900
        .ColWidth(5) = 850

        .Row = 0
        .Col = 0
        .Text = "Cod"
        .Col = 1
        .Text = "Forn"
        .Col = 2
        .Text = "F�brica"
        .Col = 3
        .Text = "Descri��o"
        .Col = 4
        .Text = "DPK"
        .Col = 5
        .Text = "Estoque"

        For i = 1 To p.count
            .Row = i
            
            .Col = 0
            .Text = CStr(p.Item(i).Item("CodigoForn"))
            .Col = 1
            .Text = p.Item(i).Item("Sigla")
            .Col = 2
            .Text = p.Item(i).Item("CodigoFabrica")
            .Col = 3
            .Text = p.Item(i).Item("DescricaoItem")
            .Col = 4
            .Text = CStr(p.Item(i).Item("CodigoDPK"))
            .Col = 5
            .Text = CStr(p.Item(i).Item("Estoque"))
            
        Next
        .Row = 1
    End With

    Screen.MousePointer = vbDefault
    
    Set xmlhttp = Nothing
    
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    Else
        MessageBox 0, "Sub CarregarItens" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If

    Screen.MousePointer = vbDefault
End Sub

Private Sub grdItens_DblClick()
On Error GoTo TrataErro
    grdItens.Col = 4
    frmVenda.txtCOD_DPK = grdItens.Text
    frmVenda.txtCOD_DPK_LostFocus
    
    Unload Me
    Set frmOferecaTambem = Nothing
    
    Exit Sub
TrataErro:
    MessageBox 0, "Sub grdItens_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0

End Sub
