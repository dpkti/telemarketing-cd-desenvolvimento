VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmDetalhes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dados do cliente"
   ClientHeight    =   6345
   ClientLeft      =   195
   ClientTop       =   1425
   ClientWidth     =   11595
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6345
   ScaleWidth      =   11595
   Begin VB.Frame Frame2 
      Caption         =   "Solicitar Exclus�o Telefones - Somente a partir da Terceira Linha"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3930
      TabIndex        =   11
      Top             =   2400
      Visible         =   0   'False
      Width           =   6555
      Begin VB.TextBox txtFoneExcluir 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   690
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   14
         Top             =   390
         Width           =   1365
      End
      Begin VB.TextBox txtDDDExcluir 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   13
         Top             =   390
         Width           =   585
      End
      Begin VB.TextBox txtMotivoExcluir 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   510
         Left            =   2070
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   12
         Top             =   390
         Width           =   3435
      End
      Begin Bot�o.cmd cmdExclusao 
         Height          =   285
         Left            =   5550
         TabIndex        =   15
         ToolTipText     =   "Gravar Solicita��o de Exclus�o de Telefone"
         Top             =   300
         Width           =   915
         _ExtentX        =   1614
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDetalhes.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesfazerExcluir 
         Height          =   285
         Left            =   5550
         TabIndex        =   19
         ToolTipText     =   "Desfazer Solicita��o de Exclus�o de Telefone"
         Top             =   600
         Width           =   915
         _ExtentX        =   1614
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "Desfazer"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDetalhes.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Telefone:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   690
         TabIndex        =   18
         Top             =   210
         Width           =   810
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "DDD:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   60
         TabIndex        =   17
         Top             =   210
         Width           =   480
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Motivo da Exclus�o:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2070
         TabIndex        =   16
         Top             =   210
         Width           =   1710
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Alterar Telefone do Cliente - Somente a partir da Terceira linha"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   30
      TabIndex        =   3
      Top             =   5310
      Width           =   5565
      Begin VB.TextBox txtContato 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2190
         MaxLength       =   15
         TabIndex        =   6
         Top             =   420
         Width           =   2445
      End
      Begin VB.TextBox txtDDD 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         MaxLength       =   15
         TabIndex        =   5
         Top             =   420
         Width           =   585
      End
      Begin VB.TextBox txtTelefone 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   690
         MaxLength       =   15
         TabIndex        =   4
         Top             =   420
         Width           =   1455
      End
      Begin Bot�o.cmd cmdSalvar 
         Height          =   675
         Left            =   4740
         TabIndex        =   7
         ToolTipText     =   "Gravar"
         Top             =   210
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   1191
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDetalhes.frx":0038
         PICN            =   "frmDetalhes.frx":0054
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Contato:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2190
         TabIndex        =   10
         Top             =   240
         Width           =   750
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "DDD:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   60
         TabIndex        =   9
         Top             =   240
         Width           =   480
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Telefone:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   690
         TabIndex        =   8
         Top             =   240
         Width           =   810
      End
   End
   Begin MSComctlLib.ListView lsvTelefones 
      Height          =   4485
      Left            =   30
      TabIndex        =   2
      Top             =   780
      Width           =   11565
      _ExtentX        =   20399
      _ExtentY        =   7911
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "DDD"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Fone"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Contato"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Mensagem"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Motivo - Solicitou Exclus�o"
         Object.Width           =   0
      EndProperty
   End
   Begin Bot�o.cmd SSCommand2 
      Height          =   675
      Left            =   30
      TabIndex        =   0
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDetalhes.frx":0D2E
      PICN            =   "frmDetalhes.frx":0D4A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   11550
      _ExtentX        =   20373
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmDetalhes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmDetalhes
' Author    : c.samuel.oliveira
' Date      : 13/01/16
' Purpose   : TI-3880
'---------------------------------------------------------------------------------------


Option Explicit


Private Sub cmdDesfazerExcluir_Click()
    
    If Val(txtDDDExcluir) = 0 Then
        MessageBox 0, "Favor informar o DDD.", "Aten��o", &H40000
        txtDDDExcluir.SetFocus
        Exit Sub
    ElseIf Val(txtFoneExcluir) = 0 Then
        MessageBox 0, "Favor informar o Telefone.", "Aten��o", &H40000
        txtFoneExcluir.SetFocus
        Exit Sub
    ElseIf Trim(Me.txtMotivoExcluir) = "" Then
        MessageBox 0, "Informe o Motivo.", "Aten��o", &H40000
        Me.txtMotivoExcluir.SetFocus
        Exit Sub
    End If
        
    Gravar "EXCLUIR_TELEFONE", 1, frmVenda.txtCOD_CLIENTE & ";" & txtDDDExcluir & txtFoneExcluir & ";" & txtMotivoExcluir & ";" & sCOD_VEND
    Preenche_Lsv
    Marcar_Fones_Excluidos
End Sub

Private Sub cmdExclusao_Click()
    
    If Val(txtDDDExcluir) = 0 Then
        MessageBox 0, "Favor informar o DDD.", "Aten��o", &H40000
        txtDDDExcluir.SetFocus
        Exit Sub
    ElseIf Val(txtFoneExcluir) = 0 Then
        MessageBox 0, "Favor informar o Telefone.", "Aten��o", &H40000
        txtFoneExcluir.SetFocus
        Exit Sub
    ElseIf Trim(Me.txtMotivoExcluir) = "" Then
        MessageBox 0, "Informe o Motivo.", "Aten��o", &H40000
        Me.txtMotivoExcluir.SetFocus
        Exit Sub
    End If
        
    Gravar "EXCLUIR_TELEFONE", 2, frmVenda.txtCOD_CLIENTE & ";" & txtDDDExcluir & txtFoneExcluir & ";" & txtMotivoExcluir & ";" & sCOD_VEND
    Preenche_Lsv
    Marcar_Fones_Excluidos
    
End Sub

Private Sub cmdSalvar_Click()
              
1         On Error GoTo Trata_Erro
              
2         If Val(txtDDD) = 0 Or Val(txtDDD) = 111 Or Val(txtDDD) = 222 Or Val(txtDDD) = 333 Or Val(txtDDD) = 444 _
            Or Val(txtDDD) = 555 Or Val(txtDDD) = 666 Or Val(txtDDD) = 777 Or Val(txtDDD) = 888 Or Val(txtDDD) = 999 Then
3            MsgBox "Informe o DDD.", vbInformation, "Aten��o"
4            Exit Sub
5         End If
6         If Val(txtTelefone) = 0 Or Len(txtTelefone) < 7 Then
7            MsgBox "Informe o Telefone.", vbInformation, "Aten��o"
8            Exit Sub
9         End If
              
10        If Trim(txtContato) = "" And Trim(txtContato.Tag) <> "" Then
11           If MessageBox(0, "Confirma a altera��o do Contato ?", "Aten��o", &H40000 + 36) = 7 Then
12              txtContato.SetFocus
13              Exit Sub
14           End If
15        End If

16        OraParameters.Remove "cliente"
17        OraParameters.Add "cliente", frmVenda.txtCOD_CLIENTE, 1
18        OraParameters.Remove "Contato"
19        OraParameters.Add "COntato", txtContato, 1
20        OraParameters.Remove "DDD"
21        OraParameters.Add "DDD", txtDDD, 1
22        OraParameters.Remove "Fone"
23        OraParameters.Add "Fone", txtTelefone, 1
24        OraParameters.Remove "Linha"
'21/11/2006
25        OraParameters.Add "Linha", lsvTelefones.SelectedItem.index, 1
'25        OraParameters.Add "Linha", IIf(lsvTelefones.SelectedItem.Index <= 2, 1, 2), 1

26        OraParameters.Remove "DDD_ANTIGO"
27        OraParameters.Add "DDD_ANTIGO", txtDDD.Tag, 1
28        OraParameters.Remove "FONE_ANTIGO"
29        OraParameters.Add "FONE_ANTIGO", txtTelefone.Tag, 1

30        vSql = "Producao.pck_vda230.Pr_Update_Contato(:cliente,:Contato,:DDD,:Fone,:Linha, :DDD_ANTIGO, :FONE_ANTIGO)"

31        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
32        If vErro <> "" Then
33            MessageBox 0, vErro, "Aten��o", &H40000 + 0
34        Else
47            If txtTelefone.Enabled = False Then
48               MessageBox 0, "Contato Atualizado com Sucesso !", "Aten��o", &H40000 + 0
49            Else
35               MessageBox 0, "Telefone Atualizado com Sucesso !", "Aten��o", &H40000 + 0
51            End If
36            Preenche_Lsv
37        End If

          txtDDD = ""
          txtTelefone = ""
          txtContato = ""

          Frame1.Enabled = False

Trata_Erro:
38        If Err.Number <> 0 Then
39            MessageBox 0, "Sub cmdSalvar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
40        End If
End Sub

Private Sub Form_Load()
'    Frame1.Enabled = False
'    Frame2.Enabled = False
    Preenche_Lsv
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmDetalhes = Nothing
End Sub

Private Sub lsvTelefones_Click()
'    If lsvTelefones.SelectedItem.Index > 2 Then
'        Frame1.Enabled = False
'        Frame2.Enabled = True
'
'        'Solicitar Exclus�o - 28/11/2005
'        txtDDDExcluir = lsvTelefones.SelectedItem
'        txtFoneExcluir = lsvTelefones.SelectedItem.SubItems(1)
'        txtMotivoExcluir = lsvTelefones.SelectedItem.SubItems(4)
'
'        txtDDD = ""
'        txtTelefone = ""
'        txtContato = ""
'
'        Exit Sub
'    End If

'    txtDDDExcluir = ""
'    txtFoneExcluir = ""
'    txtMotivoExcluir = ""

    Frame1.Enabled = True

'************************************************
'Eduardo relvas - Conforme combinado com a Marici
'Comentado dia 22/11/2006
'************************************************
    Frame1.Caption = "Alterar Telefone do Cliente"
    txtDDD.Enabled = True
    txtTelefone.Enabled = True
    txtContato.Enabled = lsvTelefones.SelectedItem.index = 1
    
    txtContato = lsvTelefones.SelectedItem.SubItems(2)
    txtContato.Tag = lsvTelefones.SelectedItem.SubItems(2)
    txtDDD = lsvTelefones.SelectedItem
    txtDDD.Tag = lsvTelefones.SelectedItem
    txtTelefone = lsvTelefones.SelectedItem.SubItems(1)
    txtTelefone.Tag = lsvTelefones.SelectedItem.SubItems(1)

End Sub

Private Sub lsvTelefones_DblClick()
        On Error GoTo Trata_Erro

1         strPreencher_Cliente = False
          Dim DDD As String
          
2         If vTrabalharConectado = True And Left(Trim(Command$), 1) <> "V" Then 'TI-3880 cmd
3             vListaTelfones = True
              
4             If PEGAR_DDD(UCase(frmVenda.lblNOME_CIDADE)) = True Then
5                 DDD = ""
6             Else
                  DDD = "0" & lsvTelefones.SelectedItem
12            End If

13            frmVenda.TxtRamalVozFone = DDD & lsvTelefones.SelectedItem.SubItems(1)

14            frmVenda.cmdDiscar.Tag = "Lista"

15            Unload frmDetalhes
              
16            frmVenda.vAchouCliente = True
              
17            frmVenda.cmdDiscar_Click
          
18        End If

Trata_Erro:
    If Err.Number <> 0 Then
       MessageBox 0, "Sub lsvTelefones_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If
End Sub

Private Sub SSCommand2_Click()
    Unload Me
End Sub

Sub Preenche_Lsv()
          Dim ss As Object
          Dim lItem As ListItem

1         On Error GoTo Trata_Erro

2         lsvTelefones.ListItems.Clear

3         If frmVenda.txtCOD_CLIENTE = "" Then
4             NonStayOnTop Me
5             MessageBox 0, "Selecione um cliente", "Aten�ao", &H40000 + 0
6             StayOnTop Me

7             Exit Sub
8         Else
9             OraParameters.Remove "cliente"
10            OraParameters.Add "cliente", frmVenda.txtCOD_CLIENTE, 1
11            OraParameters.Remove "vErro"
12            OraParameters.Add "vErro", 0, 2

13            vSql = "Begin producao.pck_vda230.pr_detalhes_cliente(:vCursor,:cliente,:vErro);END;"

14            'Criar_Cursor
15            oradatabase.ExecuteSQL vSql
16            Set ss = oradatabase.Parameters("vCursor").Value
17            'oradatabase.Parameters.Remove ("vCursor")

18            If oradatabase.Parameters("vErro").Value <> 0 Then
19                Call Process_Line_Errors(Sql)
20                Exit Sub
21            End If

22            Set lItem = lsvTelefones.ListItems.Add
23            lItem = ss!ddd1
24            lItem.SubItems(1) = ss!fone1
25            lItem.SubItems(2) = IIf(IsNull(ss!NOME_CONTATO), " ", ss!NOME_CONTATO)
26            lItem.SubItems(3) = IIf(IsNull(ss!desc_mens), " ", ss!desc_mens)

27            If ss!fone2 <> 0 Then
28                Set lItem = lsvTelefones.ListItems.Add
29                lItem = IIf(ss!ddd2 = 0, ss!ddd1, ss!ddd2)
30                lItem.SubItems(1) = ss!fone2
'31                lItem.SubItems(2) = IIf(IsNull(ss!NOME_CONTATO), " ", ss!NOME_CONTATO)
31            End If
32        End If

33        'Criar_Cursor
34        oradatabase.ExecuteSQL "Begin producao.pck_vda230.Pr_Select_Telefones(:vCursor,:cliente);END;"
35        Set ss = oradatabase.Parameters("vCursor").Value
36        'oradatabase.Parameters.Remove ("vCursor")

37        If ss.RecordCount <= 0 Then Exit Sub

38        While ss.EOF = False
39            Set lItem = lsvTelefones.ListItems.Add
40            lItem = ss!DDD
41            lItem.SubItems(1) = ss!Fone
42            ss.MoveNext
43        Wend

44        Set lItem = Nothing

45        Exit Sub

Trata_Erro:
46        MessageBox 0, "Sub Preenche_Lsv" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
End Sub

Sub Marcar_Fones_Excluidos()
          Dim lItem As ListItem
          Dim rst As Object
          Dim vDEP As String
          Dim vParametro
          Dim ii As Integer
          
1         If strTabela_Banco = "PRODUCAO." Then
2             vDEP = "HELPDESK."
3         Else
4             vDEP = strTabela_Banco
5         End If
          
6         vBanco.Parameters.Remove "Cod_Sistema"
7         vBanco.Parameters.Add "Cod_Sistema", Pegar_Cod_Sistema, 1
          
8         vBanco.Parameters.Remove "Nome_Parametro"
9         vBanco.Parameters.Add "Nome_Parametro", "EXCLUIR_TELEFONE", 1
          
10        vBanco.Parameters.Remove "DEP"
11        vBanco.Parameters.Add "DEP", vDEP, 1
          
12        'Criar_Cursor
13        vBanco.ExecuteSQL "Begin producao.pck_vda230.PR_SELECT_VL_PARAMETRO(:vCursor,:Cod_Sistema, :Nome_parametro, :Dep);END;"
14        Set rst = oradatabase.Parameters("vCursor").Value
15        'oradatabase.Parameters.Remove ("vCursor")

16        If rst.RecordCount <= 0 Then Exit Sub
          
17        For i = 1 To lsvTelefones.ListItems.count
18            rst.MoveFirst
19            For ii = 1 To rst.RecordCount
20                If Left(rst!vl_parametro, 5) = frmVenda.txtCOD_CLIENTE Then
21                    vParametro = Split(rst!vl_parametro, ";")
22                    If lsvTelefones.ListItems(i) & lsvTelefones.ListItems(i).SubItems(1) = vParametro(1) Then
23                        lsvTelefones.ListItems(i).SubItems(4) = vParametro(2)
24                    End If
25                End If
26                rst.MoveNext
27            Next ii
28        Next

End Sub
