VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmSeguradora 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informa��es da Seguradora"
   ClientHeight    =   2625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5820
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2625
   ScaleWidth      =   5820
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtApolice 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1770
      MaxLength       =   10
      TabIndex        =   4
      Top             =   2250
      Width           =   1695
   End
   Begin VB.TextBox txtSegurado 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1770
      MaxLength       =   20
      TabIndex        =   3
      Top             =   1890
      Width           =   3975
   End
   Begin VB.TextBox txtPlaca 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1770
      MaxLength       =   7
      TabIndex        =   2
      Top             =   1530
      Width           =   975
   End
   Begin VB.TextBox txtCarro 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1770
      MaxLength       =   15
      TabIndex        =   1
      Top             =   1170
      Width           =   2175
   End
   Begin VB.TextBox txtSinistro 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1770
      MaxLength       =   15
      TabIndex        =   0
      Top             =   810
      Width           =   2175
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   11
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSeguradora.frx":0000
      PICN            =   "frmSeguradora.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   12
      Top             =   720
      Width           =   5760
      _ExtentX        =   10160
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   675
      Left            =   4260
      TabIndex        =   13
      ToolTipText     =   "Incluir"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSeguradora.frx":0CF6
      PICN            =   "frmSeguradora.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   675
      Left            =   5040
      TabIndex        =   10
      ToolTipText     =   "Limpar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSeguradora.frx":19EC
      PICN            =   "frmSeguradora.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "N�mero da Ap�lice"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   9
      Top             =   2310
      Width           =   1620
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Nome do Segurado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   8
      Top             =   1950
      Width           =   1635
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Placa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   570
      TabIndex        =   7
      Top             =   1950
      Width           =   495
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Modelo do Carro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   6
      Top             =   1230
      Width           =   1410
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "N�mero Sinistro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   5
      Top             =   870
      Width           =   1350
   End
End
Attribute VB_Name = "frmSeguradora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdLimpar_Click()
    txtSinistro = ""
    txtCarro = ""
    txtPlaca = ""
    txtSegurado = ""
    txtApolice = ""
End Sub

Private Sub cmdSair_Click()
   cmdSalvar_Click
End Sub

Private Sub cmdSalvar_Click()
    If txtSinistro = "" Or _
            txtCarro = "" Or _
            txtPlaca = "" Or _
            txtSegurado = "" Or _
            txtApolice = "" Then
        NonStayOnTop Me
        MessageBox 0, "Para salvar � necess�rio informar todos os dados", "Aten�ao", &H40000 + 0
        StayOnTop Me
        Exit Sub
    End If

    strSinistro = txtSinistro
    strCarro = txtCarro
    strPlaca = txtPlaca
    strSegurado = txtSegurado
    strApolice = txtApolice
    Unload Me
End Sub

Private Sub Form_Load()
    frmFimPedido.vfrmseguradora_Aberto = True
    txtSinistro = ""
    txtCarro = ""
    txtPlaca = ""
    txtSegurado = ""
    txtApolice = ""
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    '    If strSinistro = "" Then
    '         response = MsgBox("Tem certeza que n�o informar� os dados da seguradora ?", vbYesNo, "Aten��o")
    '         If response = vbNo Then
    '             Cancel = True
    '         End If
    '     End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmSeguradora = Nothing
    frmFimPedido.vfrmseguradora_Aberto = False
End Sub

Private Sub txtApolice_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtCarro_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtPlaca_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtSegurado_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtSinistro_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub
