VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmRecado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recado para Outro Operador"
   ClientHeight    =   3630
   ClientLeft      =   1140
   ClientTop       =   1545
   ClientWidth     =   5085
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   5085
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame ssfraAgenda 
      Height          =   2745
      Left            =   30
      TabIndex        =   0
      Top             =   810
      Width           =   4995
      Begin VB.TextBox txtObs 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   1695
         Left            =   90
         MaxLength       =   100
         MultiLine       =   -1  'True
         TabIndex        =   2
         Top             =   960
         Width           =   4815
      End
      Begin VB.TextBox txtVendedor 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   90
         MaxLength       =   4
         TabIndex        =   1
         Top             =   420
         Width           =   735
      End
      Begin VB.Label lblPseudonimo 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   900
         TabIndex        =   9
         Top             =   480
         Width           =   75
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Recado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   4
         Top             =   750
         Width           =   675
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Vendedor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   3
         Top             =   210
         Width           =   825
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   5
      ToolTipText     =   "Sair sem Salvar Recado"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRecado.frx":0000
      PICN            =   "frmRecado.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   6
      Top             =   720
      Width           =   5010
      _ExtentX        =   8837
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd sscmdGrava_Recado 
      Height          =   675
      Left            =   3540
      TabIndex        =   8
      ToolTipText     =   "Enviar Recado"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRecado.frx":0CF6
      PICN            =   "frmRecado.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand3 
      Height          =   675
      Left            =   4320
      TabIndex        =   7
      ToolTipText     =   "Limpar Recado"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRecado.frx":19EC
      PICN            =   "frmRecado.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmRecado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()
    Unload Me
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmRecado = Nothing
End Sub

Private Sub sscmdGrava_Recado_Click()

    On Error GoTo Trata_Erro

1         If txtVendedor = "" Then
2             NonStayOnTop Me
3             MessageBox 0, "Favor escolher o vendedor que dever� receber o recado", "Aten�ao", &H40000 + 0
4             StayOnTop Me
5             Exit Sub
6         End If

7         If txtObs = "" Then
8             NonStayOnTop Me
9             MessageBox 0, "Favor incluir o recado", "Aten�ao", &H40000 + 0
10            StayOnTop Me
11            Exit Sub
12        End If

13        vSql = "Begin producao.pck_vda230.pr_insere_recado(:vend,:cliente,:recado,:vErro);END;"

14        OraParameters.Remove "vend"
15        OraParameters.Add "vend", txtVendedor, 1
16        OraParameters.Remove "cliente"
17        OraParameters.Add "cliente", frmVenda.txtCOD_CLIENTE, 1
18        OraParameters.Remove "recado"
19        OraParameters.Add "recado", "VEND.:" & sCOD_VEND & ": " & UCase(txtObs), 1
20        OraParameters.Remove "vErro"
21        OraParameters.Add "vErro", 0, 2

22        oradatabase.ExecuteSQL vSql

'23        If Val(OraParameters("vErro").Value) <> 0 Then
'24            Resume Next
'25        End If

26        SSCommand3_Click

27        Unload Me
Trata_Erro:
        If Err.Number <> 0 Then
            MessageBox 0, "Sub ssCmdGravar_Recado_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
        End If
End Sub

Private Sub SSCommand3_Click()
    txtVendedor = ""
    lblPseudonimo = ""
    txtObs = ""
End Sub

Private Sub txtObs_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub

Private Sub txtVendedor_DblClick()
    txtVendedor.DataChanged = False
    StayOnTop frmVendedor
    frmVendedor.Show
End Sub

Private Sub txtVendedor_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Public Sub txtVendedor_LostFocus()
        
        On Error GoTo Trata_Erro

          Dim ss As Object

1         If txtVendedor <> "" Then
2             OraParameters.Remove "repres"
3             OraParameters.Add "repres", txtVendedor, 1

4             'Criar_Cursor
5             oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Select_Repres_Tipo(:vCursor, :Repres); END;"
6             Set ss = oradatabase.Parameters("vCursor").Value
7             'oradatabase.Parameters.Remove ("vCursor")

8             If ss.EOF And ss.BOF Then
9                 NonStayOnTop Me
10                MessageBox 0, "Vendedor n�o localizado, verifique", "Aten�ao", &H40000 + 0
11                StayOnTop Me
12                Exit Sub
13            Else
14                lblPseudonimo = ss!pseudonimo
15            End If
16        End If

Trata_Erro:
        If Err.Number <> 0 Then
            MessageBox 0, "Sub TxtVendedor_LostFocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
        End If

End Sub
