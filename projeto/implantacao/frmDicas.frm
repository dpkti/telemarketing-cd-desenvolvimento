VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDicas 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dicas"
   ClientHeight    =   4770
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7305
   Icon            =   "frmDicas.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   318
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   487
   Begin VB.Frame frm 
      Caption         =   "Dicas Cadastradas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3480
      Left            =   45
      TabIndex        =   6
      Top             =   900
      Width           =   7215
      Begin VB.TextBox txtseq 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   225
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   3015
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.TextBox txtDicas 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2130
         Left            =   180
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   12
         Top             =   765
         Width           =   6855
      End
      Begin VB.TextBox lblData 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4995
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   315
         Width           =   2040
      End
      Begin VB.TextBox lblPseudonimo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1845
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   315
         Width           =   2445
      End
      Begin VB.TextBox lblcod_vend 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   315
         Width           =   690
      End
      Begin Bot�o.cmd cmd4 
         Height          =   420
         Left            =   4905
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Primeiro"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "<<"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":23D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd5 
         Height          =   420
         Left            =   5445
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Anterior"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "<"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd6 
         Height          =   420
         Left            =   6030
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Pr�ximo"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   ">"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":240A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd7 
         Height          =   420
         Left            =   6570
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "�ltimo"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   ">>"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":2426
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Data:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4500
         TabIndex        =   11
         Top             =   405
         Width           =   465
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Vendedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   8
         Top             =   405
         Width           =   870
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4440
      Width           =   7305
      _ExtentX        =   12885
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12832
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":2442
      PICN            =   "frmDicas.frx":245E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   855
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Novo"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":3138
      PICN            =   "frmDicas.frx":3154
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   3015
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":3E2E
      PICN            =   "frmDicas.frx":3E4A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   3735
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Excluir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":4B24
      PICN            =   "frmDicas.frx":4B40
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd8 
      Height          =   690
      Left            =   1575
      TabIndex        =   17
      TabStop         =   0   'False
      ToolTipText     =   "Alterar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":581A
      PICN            =   "frmDicas.frx":5836
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd9 
      Height          =   690
      Left            =   2295
      TabIndex        =   19
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":6510
      PICN            =   "frmDicas.frx":652C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmDicas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ACT As String

Private Sub cmd1_Click()
    On Error GoTo Trata_Erro

1         lblData.Locked = True
2         lblcod_vend.Locked = False
3         lblPseudonimo.Locked = True
4         txtDicas.Locked = False

5         lblcod_vend.Text = sCOD_VEND
6         If lblcod_vend.Locked = False And lblcod_vend.Text <> "" Then
7             Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, lblcod_vend.Text, , , , , , , "M")
              
8             If Not vObjOracle.EOF Then
9                 lblPseudonimo.Text = vObjOracle("PSEUDONIMO")
10            Else
11                lblPseudonimo.Text = ""
12            End If
13        End If
         ' lblPseudonimo.Text = ""
14        lblData.Text = Now()
15        txtDicas.Text = ""
16        txtSeq.Text = ""
          
17        cmd2.Enabled = True
18        cmd9.Enabled = True
          
19        cmd4.Enabled = False
20        cmd5.Enabled = False
21        cmd6.Enabled = False
22        cmd7.Enabled = False
23        cmd8.Enabled = False
24        cmd3.Enabled = False
          
25        ACT = "I"

Trata_Erro:
    If Err.Number <> 0 Then
       MessageBox 0, "Sub cmd1_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If
End Sub

Private Sub cmd2_Click()
    If lblcod_vend = "" Then
        MessageBox 0, "Escolha um Vendedor", "Aten��o", &H40000 + 0
        lblcod_vend.SetFocus
    Else
        ACT_DICA
        Form_Load
    End If
End Sub

Private Sub cmd3_Click()
    strResposta = MsgBox("Confirma a exclus�o desta dica?", vbYesNo, "ATEN��O")
    If strResposta = vbYes Then
        ACT = "D"
        ACT_DICA
        Form_Load
    End If
    
End Sub

Private Sub cmd4_Click()
    If Not vObjOracle.BOF Then
        vObjOracle.MoveFirst
        Preencher_Campos
    End If
End Sub

Private Sub cmd5_Click()
    If Not vObjOracle.BOF Then
        vObjOracle.MovePrevious
        If Not vObjOracle.BOF Then
            Preencher_Campos
        End If
    End If
End Sub

Private Sub cmd6_Click()
   If Not vObjOracle.EOF Then
      vObjOracle.MoveNext
      If Not vObjOracle.EOF Then
         Preencher_Campos
      End If
  End If
End Sub

Private Sub cmd7_Click()

1         On Error GoTo Trata_Erro

2         If Not vObjOracle.EOF Then
3            vObjOracle.MoveLast
4            Preencher_Campos
5         End If
Trata_Erro:
6   If Err.Number <> 0 Then
7       MessageBox 0, "Sub Cmd7_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
8   End If
End Sub

Private Sub cmd8_Click()
    
    lblcod_vend.Locked = False
    txtDicas.Locked = False
    
    cmd2.Enabled = True
    cmd9.Enabled = True
    
    cmd4.Enabled = False
    cmd5.Enabled = False
    cmd6.Enabled = False
    cmd7.Enabled = False
    cmd1.Enabled = False
    cmd3.Enabled = False
    
    ACT = "A"
    
End Sub

Private Sub cmd9_Click()
    Form_Load
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

1         On Error GoTo Trata_Erro
          
'2         Me.Top = 2500
'3         Me.Left = 500
          
4         cmd2.Enabled = False
5         cmd9.Enabled = False
6         lblcod_vend.Locked = True
7         ACT = ""
          
8         vBanco.Parameters.Remove "PM_CODCLI"
9         vBanco.Parameters.Add "PM_CODCLI", frmVenda.txtCOD_CLIENTE.Text, 1
          
10        vBanco.Parameters.Remove "PM_CURSOR1"
11        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
12        vBanco.Parameters("PM_CURSOR1").serverType = 102
13        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
14        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
          
15        vBanco.Parameters.Remove "PM_CODERRO"
16        vBanco.Parameters.Add "PM_CODERRO", 0, 2
17        vBanco.Parameters.Remove "PM_TXTERRO"
18        vBanco.Parameters.Add "PM_TXTERRO", "", 2
          
19        vSql = "PRODUCAO.pck_vda020.pr_CON_DICAS(:PM_CODCLI,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
          
20        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
21        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
          
22        If Not vObjOracle.EOF Then
          
23            cmd1.Enabled = True
24            cmd8.Enabled = True
25            cmd3.Enabled = True
26            cmd4.Enabled = True
27            cmd5.Enabled = True
28            cmd6.Enabled = True
29            cmd7.Enabled = True
              
              Preencher_Campos
              
55        Else
          
56            MessageBox 0, "Nenhuma Dica cadastrada", "Aten��o", &H40000 + 0
57            cmd1.Enabled = True
              
58        End If
          
Trata_Erro:
59        If Err.Number <> 0 Then
60     MessageBox 0, "Sub Form_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
61        End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmDicas = Nothing
End Sub

Private Sub lblcod_vend_Change()
    
    If lblcod_vend.Locked = False And lblcod_vend.Text <> "" Then
        Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, lblcod_vend.Text, , , , , , , "M")
        
        
        If Not vObjOracle.EOF Then
            lblPseudonimo.Text = IIf(IsNull(vObjOracle("PSEUDONIMO")), "", vObjOracle("PSEUDONIMO"))
        Else
            lblPseudonimo.Text = ""
        End If
    End If

End Sub

Sub ACT_DICA()
1         On Error GoTo Trata_Erro

2         vBanco.Parameters.Remove "PM_CODCLI"
3         vBanco.Parameters.Add "PM_CODCLI", frmVenda.txtCOD_CLIENTE.Text, 1
          
4         vBanco.Parameters.Remove "PM_CODDICA"
5         vBanco.Parameters.Add "PM_CODDICA", "0" & txtSeq.Text, 1
          
6         vBanco.Parameters.Remove "PM_CODREPR"
7         vBanco.Parameters.Add "PM_CODREPR", lblcod_vend.Text, 1
          
8         vBanco.Parameters.Remove "PM_DICA"
9         vBanco.Parameters.Add "PM_DICA", UCase(txtDicas.Text), 1
          
10        vBanco.Parameters.Remove "PM_ACT"
11        vBanco.Parameters.Add "PM_ACT", ACT, 1
          
12        vBanco.Parameters.Remove "PM_CODERRO"
13        vBanco.Parameters.Add "PM_CODERRO", 0, 2
14        vBanco.Parameters.Remove "PM_TXTERRO"
15        vBanco.Parameters.Add "PM_TXTERRO", "", 2
          
          'Alterado por Eduardo Relvas em 19/06/2009
          'Isso ir� corrigir o problema de quando o usuario pede para excluir uma
          'dica e o sistema exclui todas da mesma sequencia e cliente.
16        vBanco.Parameters.Remove "PM_DT"
17        vBanco.Parameters.Add "PM_DT", fFormata_Data(lblData), 1
          
18        'vSql = "PRODUCAO.pck_vda020.pr_ALT_DICAS(:PM_CODCLI,:PM_CODDICA,:PM_CODREPR,:PM_DICA,:PM_ACT,:PM_DT,:PM_CODERRO,:PM_TXTERRO)"
19      vSql = "PRODUCAO.pck_vda020.pr_ALT_DICAS(:PM_CODCLI,:PM_CODDICA,:PM_CODREPR,:PM_DICA,:PM_ACT,:PM_DT,:PM_CODERRO,:PM_TXTERRO)"

          
20        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
21        If vBanco.Parameters("PM_CODERRO") <> 0 Then
22            MessageBox 0, "Erro: " & vBanco.Parameters("PM_CODERRO") & " - " & vBanco.Parameters("PM_TXTERRO"), "Aten��o", &H40000 + 0
23        End If

Trata_Erro:
24        If Err.Number <> 0 Then
25     MessageBox 0, "Sub cmdSalvar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
26        End If
End Sub


Sub Preencher_Campos()

    On Error GoTo Trata_Erro

4             If IsNull(vObjOracle("Cod_Vend")) Then
5                lblcod_vend.Text = ""
6             Else
7                lblcod_vend.Text = vObjOracle("cod_vend")
8             End If
9             If IsNull(vObjOracle("pseudonimo")) Then
10               lblPseudonimo.Text = ""
11            Else
12               lblPseudonimo.Text = vObjOracle("pseudonimo")
13            End If
14            If IsNull(vObjOracle("dt_dica")) Then
15               lblData.Text = ""
16            Else
17               lblData.Text = vObjOracle("dt_dica")
18            End If
19            If IsNull(vObjOracle("dica")) Then
20                txtDicas.Text = ""
21            Else
22                txtDicas.Text = vObjOracle("dica")
23            End If
24            If IsNull(vObjOracle("sequencia")) Then
25               txtSeq.Text = ""
26            Else
27               txtSeq.Text = vObjOracle("sequencia")
28            End If
Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub Preencher_Campos" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If
End Sub

Function fFormata_Data(pData As String) As String

    Dim vDia As Byte
    Dim vMes As Byte
    Dim vAno As Integer
    Dim vMesExtenso As String
    Dim vHora As String
    
    vDia = Day(CDate(pData))
    vMes = Month(CDate(pData))
    vAno = Year(CDate(pData))
    vHora = Trim(Right(pData, 8))

    vMesExtenso = Choose(vMes, "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
    
    fFormata_Data = Format(vDia, "00") & "-" & vMesExtenso & "-" & Format(vAno, "0000") & " " & vHora

End Function
