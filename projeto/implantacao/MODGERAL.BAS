Attribute VB_Name = "Module1"
'---------------------------------------------------------------------------------------
' Module    : Module1
' Author    : c.samuel.oliveira
' Date      : 27/03/2019
' Purpose   : DPK-74
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : Module1
' Author    : c.samuel.oliveira
' Date      : 08/01/2019
' Purpose   : TI-6870
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : Module1
' Author    : c.samuel.oliveira
' Date      : 11/01/16
' Purpose   : TI-3880 - retirar vendas.mdb
'---------------------------------------------------------------------------------------

Option Explicit

Public Declare Function MessageBox Lib "user32" Alias "MessageBoxA" (ByVal hwnd As Long, ByVal lpText As String, ByVal lpCaption As String, ByVal wType As Long) As Long



#If Win32 Then
    Public Const HWND_TOPMOST& = -1
#Else
    Public Const HWND_TOPMOST& = -1
#End If 'WIN32

#If Win32 Then
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#Else
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#End If 'WIN32

#If Win32 Then
    Declare Function SetWindowPos& Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
#Else
    Declare Sub SetWindowPos Lib "user" (ByVal hwnd As Integer, ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer)
#End If 'WIN32

'*****************************************************
Public Type Ligacao
    AtivaSistema    As Boolean     'Informar se o usuario clicou na lista de telefones ou fez uma ligacao sem clicar na lista
    FoneOrigem      As String * 15 'Fone que est� discando
    INICIO          As String * 20 'DAta e hora do inicio da ligacao
    Fim             As String * 20 'Data e hora do fim da ligacao
    Duracao         As String * 5  'Tempo em segundos entre o inicio e o fim da ligacao
    FoneDestino     As String * 15 'Fone para onde foi feita a ligacao
    Desligou        As Boolean     'usado pois quando desliga ele executa 2 vezes esse evento
    TipoLigacao     As Byte        '1-Ativa  2-Receptiva
    Conversou       As Boolean     'Se estabeleceu conex�o entre os dois numeros
    Particular      As Boolean     'Se � particular
    Interna         As Boolean     'Se � uma liga��o interna
    Transferencia   As Boolean     'Se � uma transferencia
    RamalCanalFone  As Boolean     'Se o usuario est� ligando pela tela de Vendas
    CodCliente      As Double      'Codigo do Cliente para que estamos ligando
    Transferido     As Boolean
End Type

Public Type Parametros
    IP4000    As String
    Porta4000 As String
    IP3000    As String
    Porta3000 As String
    IP8000    As String
    Porta8000 As String
    Ativo As String
    Prefixo_2x As Byte
End Type

Public vExternaCliente      As Boolean 'Se True indica que o usu�rio est� ligando para um cliente cujo c�digo ele n�o sabe e o numero do telefone para o qual ele
'est� discando n�o est� cadastrado.
Public vConectadoHP8000     As Boolean
Public vListaTelfones       As Boolean 'Indica se eu estou discando pela lista de Detalhes/Telefones Atribuo o valor na tela de detalhes e mudo no init cliente
Public vTrabalharConectado  As Boolean
Public NovaLigacao          As Ligacao
Public Parametros           As Parametros
Public vMonRet              As Integer  'Armazena o retorno do monitoramento de ramal
Public vRamal               As Double
Public vRamalTitulo         As Double
Public vPrefixoFone         As String 'Eduardo Diogo /11-10-2009
Public vSufixoFone          As String 'Eduardo Diogo /21-09-2010
Public vMonFlag             As Boolean
Public vNumFoneNovo         As String 'Armazenar o Numero do Telefone NOVO
Public vDgTelSenha          As String 'TI-6660

'SDS2589 - Ricardo Gomes
Public vChave               As String 'Define se a consulta de liga��o ser� feita com join
                                      'na tabela r_inteligencia_mercado (S = faz consulta com join
                                      'N = faz consulta se o join
'**************************************************

'Public LookUpCode As String 'Variavel usada para abrir o form Child como Modal

Public oradatabase As Object
Public OraParameters As Object
Public VarRec As Object
Public VarRec1 As Object
Public Data_Faturamento As Date
Public txtResposta As String           'RESPOSTA DE UMA LISTA

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

'--------------------------------------------------------
' Eduardo Diogo / 10-out-2009
'--------------------------------------------------------
' Registro com informa��es de login do usu�rio HP8000!
'--------------------------------------------------------
Public Type LogonReg
    vId As String
    vLogon As Boolean
    vAvailable As Boolean
    vDoNotDisturb As Boolean
End Type

Public RegLogonUser As LogonReg
'--------------------------------------

Sub Process_Line_Errors(ByRef Sql)
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
        Resume
    Else

        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & vbCrLf & "Linha: " & Erl & vbCrLf & ". Ligue para o departamento de sistemas"

        If fl_banco = 1 Then
            'dbAccess.Close TI-3880
        End If
        Screen.MousePointer = vbDefault
        End
    End If
    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer

    Temp = Trim(Valor)

    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function

Function iRetornaDigito(ByVal iOrigem As Long) As Integer

    Dim iSoma As Integer
    Dim gResto As Single
    Dim iTam As Integer
    Dim iFator As Integer
    Dim iContador As Integer
    Dim sOrigem As String


    iTam = Len(Trim$(str$(iOrigem)))
    iFator = 2

    sOrigem = Trim$(str$(iOrigem))


    For iContador = iTam To 1 Step -1
        iSoma = iSoma + Val(Mid$(sOrigem, iContador, 1)) * iFator
        iFator = iFator + 1
    Next iContador


    gResto = iSoma Mod 11

    If gResto = 0 Then
        iRetornaDigito = 1
    ElseIf gResto = 1 Then
        iRetornaDigito = 0
    Else
        iRetornaDigito = 11 - gResto
    End If

End Function

Public Sub Criar_Cursor()
    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").serverType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Function InformacoesSistema( _
            pBanco As Object, _
            pSistema As String) As Object

    'EXCLUINDO BINDS
    pBanco.Parameters.Remove "p_Cursor"
    pBanco.Parameters.Remove "p_Nome_Sistema"
    pBanco.Parameters.Remove "p_Cod_Erro"

    'ADICIONANDO BINDS
    pBanco.Parameters.Add "p_Nome_Sistema", pSistema, 1
    pBanco.Parameters.Add "p_Cod_Erro", 0, 2

    pBanco.Parameters.Add "p_Cursor", 0, 3
    pBanco.Parameters("p_Cursor").serverType = 102
    pBanco.Parameters("p_Cursor").DynasetOption = &H2&
    pBanco.Parameters("p_Cursor").DynasetCacheParams 256, 16, 20, 2000, 0

    'EXECUTANDO PACKAGE
    vSql = "BEGIN Producao.pck_Generica.pr_Programa(" & _
            ":p_Cursor, " & _
            ":p_Nome_Sistema, " & _
            ":p_Cod_Erro); END;"

    pBanco.ExecuteSQL vSql
    Set InformacoesSistema = pBanco.Parameters("p_Cursor").Value

    vNomeSistema = UCase(App.title)
    vDescricaoSistema = InformacoesSistema.Fields(0)
    vAnalistaResponsavel = InformacoesSistema.Fields(1)
    vAnalistaBackup = InformacoesSistema.Fields(2)

    frmSobre.lblNomeSistema = vNomeSistema & " - " & vDescricaoSistema
    frmSobre.lblResponsavel = UCase(vAnalistaResponsavel)
    frmSobre.lblBackup = UCase(vAnalistaBackup)

End Function

Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)
    'Criar_Cursor
    
    Dim vOwner As String
    
    If strTabela_Banco = "PRODUCAO." Then
        vOwner = "HELPDESK."
    Else
        vOwner = strTabela_Banco
    End If
    
    oradatabase.Parameters.Remove "Cod_Sistema"
    oradatabase.Parameters.Add "Cod_Sistema", Pegar_Cod_Sistema, 1

    oradatabase.Parameters.Remove "Nome_Parametro"
    oradatabase.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1

    oradatabase.Parameters.Remove "DEP"
    oradatabase.Parameters.Add "DEP", vOwner, 1

    vErro = vVB_Generica_001.ExecutaPl(oradatabase, "PRODUCAO.pck_vda230.PR_SELECT_VL_PARAMETRO2(:vCursor, :Cod_Sistema, :Nome_Parametro, :DEP)")

    If vErro = "" Then
        Set vObjOracle = oradatabase.Parameters("vCursor").Value
        
        'Verificar se o usuario est� cadastrado na tabela HELPDESK.Parametros,
        'se tiver entao nao pode acessar a Ligacao Automatica
        If UCase(NOME_PARAMETRO) = "USUARIO" Then
            For i = 1 To vObjOracle.RecordCount
                If vObjOracle!vl_parametro = sCOD_VEND Then
                    Pegar_VL_Parametro = 0
                    GoTo Sair
                    'Exit Function
                End If
                vObjOracle.MoveNext
            Next
            Pegar_VL_Parametro = IIf(IsNull(vObjOracle("VL_PARAMETRO")), 0, vObjOracle("VL_PARAMETRO"))
        ElseIf UCase(NOME_PARAMETRO) = "LOG3000" Then
        
            For i = 1 To vObjOracle.RecordCount
                If vObjOracle!vl_parametro = sCOD_VEND Then
                    Pegar_VL_Parametro = sCOD_VEND
                    GoTo Sair
                    'Exit Function
                End If
                vObjOracle.MoveNext
            Next
            Pegar_VL_Parametro = IIf(IsNull(vObjOracle("VL_PARAMETRO")), 0, vObjOracle("VL_PARAMETRO"))
        End If
        
        Pegar_VL_Parametro = IIf(IsNull(vObjOracle("VL_PARAMETRO")), "", vObjOracle("VL_PARAMETRO"))
    End If
    
Sair:
    oradatabase.Parameters.Remove "Cod_Sistema"
    oradatabase.Parameters.Remove "Nome_Parametro"
    oradatabase.Parameters.Remove "DEP"
    
End Function

Public Function Pegar_Cod_Sistema()

    oradatabase.Parameters.Remove "Nome_Software"
    oradatabase.Parameters.Add "Nome_Software", UCase(App.title), 1

    oradatabase.Parameters.Remove "Cod_Software"
    oradatabase.Parameters.Add "Cod_Software", 0, 2

    vVB_Generica_001.ExecutaPl oradatabase, "PRODUCAO.pck_vda230.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software)"

    Pegar_Cod_Sistema = oradatabase.Parameters("Cod_Software")

    oradatabase.Parameters.Remove "Cod_Software"

End Function

Function StayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, HWND_TOPMOST, 0, 0, 0, 0, lFlags)
End Function

Function NonStayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, -2, 0, 0, 0, 0, lFlags)
End Function


Public Function Form_Aberto(frm As String) As Boolean
    Dim f As Form
    For Each f In Forms
        If UCase(frm) = UCase(f.Name) Then
            Form_Aberto = True
            Set f = Nothing
            Exit Function
        End If
    Next
    Form_Aberto = False
    Set f = Nothing
End Function

Public Function Get_DDD_FONE(Fone As String)
          Dim vFone As Double
          Dim vDDD As Double

1         If Len(Trim(Fone)) < 5 Then
2             OraParameters.Remove "DDD"
3             OraParameters.Remove "FONE"
              OraParameters.Add "DDD", 0, 1
              OraParameters.Add "FONE", 0, 1
4             Exit Function
5         End If
          
6         If Left(Fone, 1) = "0" Then
              
7             If Left(Fone, 2) = "00" Then
8                 If Left(Fone, 5) = "00800" Or Left(Fone, 5) = "00300" Then
9                     vDDD = Mid(Fone, 2, 4)
10                    vFone = Mid(Fone, 5)
11                Else
12                    vDDD = Mid(Fone, 3, 2)
13                    vFone = Mid(Fone, 5)
14                End If
15            Else
16                If Len(Fone) = 8 Then
17                    OraParameters.Remove "DDD"
18                    OraParameters.Add "DDD", 0, 2
                      
19                    OraParameters.Remove "CodLojaDDD"
20                    OraParameters.Add "CodLojaDDD", lngCD, 1
                      
21                    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.pck_vda230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                      
22                    vDDD = vBanco.Parameters("DDD").Value
                  
23                    vFone = Mid(Fone, 2)
                      
24                ElseIf Len(Fone) = 9 Then
25                    OraParameters.Remove "DDD"
26                    OraParameters.Add "DDD", 0, 2
                      
27                    OraParameters.Remove "CodLojaDDD"
28                    OraParameters.Add "CodLojaDDD", lngCD, 1
                      
29                    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.pck_vda230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                      
30                    vDDD = vBanco.Parameters("DDD").Value
                  
31                    vFone = Mid(Fone, 2)
                  
32                ElseIf Len(Fone) > 9 Then
33                    vDDD = Mid(Fone, 2, 2)
34                    vFone = Mid(Fone, 4)
35                End If
36            End If
          
37        Else
38            If Len(Fone) > 10 Then
39                vDDD = Mid(Fone, 3, 2)
40                vFone = Mid(Fone, 5)

41            ElseIf Len(Fone) = 10 Then
42                vDDD = Left(Fone, 2)
43                vFone = Mid(Fone, 3)

44            ElseIf Len(Fone) = 9 Then
                  
45                OraParameters.Remove "DDD"
46                OraParameters.Add "DDD", 0, 2
                  
47                OraParameters.Remove "CodLojaDDD"
48                OraParameters.Add "CodLojaDDD", lngCD, 1
                  
49                vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.pck_vda230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                  
50                vDDD = vBanco.Parameters("DDD").Value
                  
                  'If lngCD = 4 Then
                  '    vFone = Mid(Fone, 3)
                  'Else
51                    vFone = Mid(Fone, 2)
                  'End If
              
52            ElseIf Len(Fone) = 8 Or Len(Fone) = 7 Then
                              
53                vFone = Fone
                  
54                OraParameters.Remove "DDD"
55                OraParameters.Add "DDD", 0, 2
                  
56                OraParameters.Remove "CodLojaDDD"
57                OraParameters.Add "CodLojaDDD", lngCD, 1
                  
58                vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.pck_vda230.Pr_Select_DDD_Loja(:DDD, :CodLojaDDD)")
                  
59                vDDD = vBanco.Parameters("DDD").Value
              
60            End If
61        End If

62        OraParameters.Remove "DDD"
63        OraParameters.Add "DDD", vDDD, 1
64        OraParameters.Remove "FONE"
65        OraParameters.Add "FONE", vFone, 1

Trata_Erro:
66        If Err.Number <> 0 Then
67            MessageBox 0, "Sub Get_DDD_Fone" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
68        End If
End Function
Sub Posicionar_Forms(frmRepresAberto As Boolean)
    Dim frm As Form
    For Each frm In Forms
        'Fernando Nascimento - 16/11/2012 - CH_4202495
        'If UCase(frm.Name) <> "FRMREPRES" And UCase(frm.Name) <> "MDIFORM1" And UCase(frm.Name) <> "FRMVENDA" Then
        If UCase(frm.Name) <> "FRMREPRES" And UCase(frm.Name) <> "MDIFORM1" And UCase(frm.Name) <> "FRMVENDA" And UCase(frm.Name) <> "FRMCA" Then
            If Form_Aberto(frm.Name) = True Then
                If frm.Visible = True Then
                    If frmRepresAberto = True Then
                        frm.Top = frmRepres.Top + frmRepres.Height / 2
                    Else
                        frm.Top = (Screen.Height - frm.Height) / 2
                        frm.Left = (Screen.Width - frm.Width) / 2
                    End If
                End If
            End If
        End If
    Next
End Sub

Public Sub Gravar(pNomeParam As String, pAcao As Byte, pValorParam As String)

        Dim vDEP As String
        
1       If strTabela_Banco = "PRODUCAO." Then
2           vDEP = "HELPDESK."
3       Else
4           vDEP = strTabela_Banco
5       End If

6         vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Codigo_Sistema, 1
7         vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
8         vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
9         vBanco.Parameters.Remove "DEP": vBanco.Parameters.Add "DEP", vDEP, 1
10        vBanco.Parameters.Remove "ACAO": vBanco.Parameters.Add "ACAO", pAcao, 1
11        vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

12        vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.pck_vda230.Pr_Salvar_Fones_Excluidos(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :ACAO, :Txt_Erro)"

13        If vBanco.Parameters("Txt_Erro") <> "" Then
14            MessageBox 0, "Sub Gravar" & vbCrLf & "Descri��o:" & vBanco.Parameters("Txt_Erro").Value, "Aten�ao", &H40000 + 0
15            Exit Sub
16        Else
17            MessageBox 0, "Dados Gravados com Sucesso!", "Aten��o", &H40000
18        End If
End Sub

Public Function Pegar_Codigo_Sistema() As Double
    '-- Pegar o c�digo do sistema na tabela de HELPDESK.SOFTWARE usando o valor da Propriedade TITLE
    vBanco.Parameters.Remove "Nome_Software"
    vBanco.Parameters.Add "Nome_Software", UCase(App.title), 1
    vBanco.Parameters.Remove "Cod_Software"
    vBanco.Parameters.Add "Cod_Software", 0, 2

    vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.pck_vda230.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software)"

    Pegar_Codigo_Sistema = vBanco.Parameters("Cod_Software").Value

    vBanco.Parameters.Remove "Cod_Software"

End Function

Public Sub Pegar_Valores_Parametros()

    Dim vIP8000 As String
    Dim vPorta8000 As String
    Dim vIP4000 As String
    Dim vPorta4000 As String
    Dim vIP3000 As String
    Dim vPorta3000 As String
    Dim vLigAuto As String
    Dim vUsuarioConectado As String
    Dim vUsuarioLog3000 As String
    Dim vUsuarioLog4000 As String
    Dim vUsuarioLog8000 As String
    Dim vPrefixo_2x As Byte
    
    vIP8000 = "" & Pegar_VL_Parametro("IP8000")
    vPorta8000 = "" & Pegar_VL_Parametro("PORTA8000")
    
    vIP4000 = "" & Pegar_VL_Parametro("IP4000")
    'vIP4000 = "10.10.31.11" 'IP da maquina da Dpaschoal.
    
    vPorta4000 = "" & Pegar_VL_Parametro("PORTA4000")
    vIP3000 = "" & Pegar_VL_Parametro("IP3000")
    vPorta3000 = "" & Pegar_VL_Parametro("PORTA3000")
    vLigAuto = "" & Pegar_VL_Parametro("LIGACAOAUTOMATICA")
    strTp_Pedido10 = "" & Pegar_VL_Parametro("TP_PEDIDO10")
    
    If intSegmento_Vend = 1 Then
      dblMargem_Parametro = "" & CDbl(Pegar_VL_Parametro("MARGEMP"))
    ElseIf intSegmento_Vend = 2 Then
      dblMargem_Parametro = "" & CDbl(Pegar_VL_Parametro("MARGEMF"))
    Else
      dblMargem_Parametro = "" & CDbl(Pegar_VL_Parametro("MARGEMA"))
    End If
    
    vUsuarioLog3000 = "" & Pegar_VL_Parametro("LOG3000")
    vUsuarioLog4000 = "" & Pegar_VL_Parametro("LOG4000")
    vUsuarioLog8000 = "" & Pegar_VL_Parametro("LOG8000")
    
    If Val(vUsuarioLog3000) = sCOD_VEND Then
        vGerarLog3000 = True
    Else
        vGerarLog3000 = False
    End If
    
    If Val(vUsuarioLog4000) = sCOD_VEND Then
        vGerarLog4000 = True
    Else
        vGerarLog4000 = False
    End If
    
    If Val(vUsuarioLog8000) = sCOD_VEND Then
        vGerarLog8000 = True
    Else
        vGerarLog8000 = False
    End If
    
    If vLigAuto = "" Then
        vLigAuto = "" & Pegar_VL_Parametro("LigacaoAutomatica")
    End If
    
    vUsuarioConectado = "" & Pegar_VL_Parametro("USUARIO")
    
    If vLigAuto = "0" Or vUsuarioConectado <> "" Then
        Parametros.Ativo = "0"
    Else
        Parametros.Ativo = "1"
    End If
    
    vPrefixo_2x = Val(0 & Pegar_VL_Parametro("PREFIXO_2X"))
    
    DPKLUB = "" & Pegar_VL_Parametro("DPKLUB_SITE")
    DPKLUB_USUARIO = "" & Pegar_VL_Parametro("DPKLUB_USUARIO") 'TI-6486
    DPKLUB_SENHA = "" & Pegar_VL_Parametro("DPKLUB_SENHA") 'TI-6486
    vRedeAutoZ = "" & Pegar_VL_Parametro("REDE_AUTOZ") 'TI-6870
    vRedeAutozPgto = "" & Pegar_VL_Parametro("REDE_AUTOZ_PAGTO") 'TI-6870
    vDeposito = "" & Pegar_VL_Parametro("DEP" & Format(lngCD, "00") & ".") 'DPK-74
    
    Parametros.IP8000 = vIP8000
    Parametros.Porta8000 = vPorta8000
    Parametros.IP4000 = vIP4000
    Parametros.Porta4000 = vPorta4000
    Parametros.IP3000 = vIP3000
    Parametros.Porta3000 = vPorta3000
    Parametros.Prefixo_2x = vPrefixo_2x
    
End Sub

Public Function fConsulta_Cliente(pFone As String) As Long

1         On Error GoTo TrataErro

2         If Len(Trim(pFone)) < 7 Then Exit Function

3         Get_DDD_FONE Val(Trim(pFone))

4         oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Select_Cod_Cliente(:vCursor, :DDD, :FONE); END;"
5         Set vObjOracle = oradatabase.Parameters("vCursor").Value

6         If vObjOracle.EOF = True Then
7            fConsulta_Cliente = 0
8         Else
9            fConsulta_Cliente = vObjOracle!COD_CLIENTE
10        End If
11        Set vObjOracle = Nothing

TrataErro:
12        If Err.Number <> 0 Then
13            MessageBox 0, "fConsulta_Cliente" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H4000 + 0
14        End If
    
End Function

Sub Exibir_Mensagem(pMensagem As String)
    frmVenda.sstInfoGeral.Tab = 1
    frmVenda.lblMensagemSistema.Caption = pMensagem
    frmVenda.TimerMensagem.Enabled = True
End Sub

'------------------------------------------------------------------------------------------
' Alexsandro de Macedo - 13/10/2011
' # Se cliente � Bosch Diesel(194) e o pedido contem produtos Bosch Diesel
'   e prazo medio de faturamento maior que 49 dias.
'
' # incluir parametro na tabela helpdesk.Parametros para
'   prazo m�dio, codigo do fornecedor, grupo e subgrupo...
'
Public Sub Valida_PMF_Bosch_Diesel(lngCod_Loja As Long, _
                                   lngNUM_PEDIDO As Long, _
                                   lngSEQ_PEDIDO As Long, _
                                   strDeposito As String)

          Dim strDEP As String
          Dim rstBoschDiesel As Object

1         On Error GoTo TrataErro

2         strDEP = ""

3         If Trim(deposito_default) <> Trim(strDeposito) And Mid(strDeposito, 1, 2) <> vRedeAutoZ Then  'TI-6870
4           If Len(vDeposito) = 0 Then 'DPK-74
                strDEP = "DEP" & Mid(strDeposito, 1, 2) & "."
            End If
5         End If

6         Sql = ""
          
          'WILLIAM LEITE - TUNNING
7         'Sql = Sql & "Select Count(Distinct Iv.Cod_Dpk) qtdBoschDiesel"
          Sql = Sql & "Select /*+DRIVING_SITE(Pv)*/ Count(Distinct Iv.Cod_Dpk) qtdBoschDiesel"
8         Sql = Sql & "  From " & strDEP & "Pednota_Venda   Pv,"
          'FERNANDO NASCIMENTO - CORRE��O OWNER - 06/11/2012
'8         Sql = Sql & "       " & strDEP & "Plano_Pgto   Pp,"
9         Sql = Sql & "               PRODUCAO.Plano_Pgto   Pp,"
10        Sql = Sql & "       " & strDEP & "Itpednota_Venda Iv,"
11        Sql = Sql & "       " & strDEP & "Item_Cadastro   Ic"
12        Sql = Sql & " Where Pv.Cod_Plano = Pp.Cod_Plano"
13        Sql = Sql & "   And Pv.Cod_Loja = Iv.Cod_Loja"
14        Sql = Sql & "   And Pv.Num_Pedido = Iv.Num_Pedido"
15        Sql = Sql & "   And Pv.Seq_Pedido = Iv.Seq_Pedido"
16        Sql = Sql & "   And Iv.Cod_Dpk = Iv.Cod_Dpk"
17        Sql = Sql & "   And Pp.Prazo_Medio > 49"
18        Sql = Sql & "   And Pv.Cod_Loja = :cod_loja"        '1
19        Sql = Sql & "   And Pv.Num_Pedido = :num_pedido"    '823304
20        Sql = Sql & "   And Pv.Seq_Pedido = :seq_pedido"    '0
21        Sql = Sql & "   And Pv.Bloq_Vlfatmin = 'S'"
22        Sql = Sql & "   And Iv.Situacao = 0"
23        Sql = Sql & "   And Iv.Qtd_Atendida > 0"
24        Sql = Sql & "   And (Ic.Cod_Fornecedor, Ic.Cod_Grupo, Ic.Cod_Subgrupo) In"
25        Sql = Sql & "       (Select v.Cod_Fornecedor,"
26        Sql = Sql & "               v.Cod_Grupo,"
27        Sql = Sql & "               v.Cod_Subgrupo"
28        Sql = Sql & "          From Vdr.Controle_Vdr v"
29        Sql = Sql & "         Where (v.Cod_Fornecedor = 194 And (v.Cod_Grupo = 11 And v.Cod_Subgrupo <> 56))"
30        Sql = Sql & "         Group By v.Cod_Fornecedor,"
31        Sql = Sql & "                  v.Cod_Grupo,"
32        Sql = Sql & "                  v.Cod_Subgrupo)"

33        oradatabase.Parameters.Remove "cod_loja"
34        oradatabase.Parameters.Remove "num_pedido"
35        oradatabase.Parameters.Remove "seq_pedido"

36        oradatabase.Parameters.Add "cod_loja", lngCod_Loja, 1
37        oradatabase.Parameters.Add "num_pedido", lngNUM_PEDIDO, 1
38        oradatabase.Parameters.Add "seq_pedido", lngSEQ_PEDIDO, 1

39        Set rstBoschDiesel = oradatabase.dbcreatedynaset(Sql, 8&)

40        If Not rstBoschDiesel.EOF Then
41            If rstBoschDiesel!qtdBoschDiesel > 0 Then
42                Call MsgBox("Pedido possui itens Bosch Diesel" & vbCrLf & _
                              "com PMF > 49 dias, entrar� na" & vbCrLf & _
                              "Pol�tica Comercial", vbExclamation Or vbOKOnly, "ATEN��O!")
43            End If
44        End If

45        rstBoschDiesel.Close

46        Set rstBoschDiesel = Nothing

47        Exit Sub
TrataErro:

48        Call MsgBox("Line: " & Erl & vbCrLf & _
                      "Code: " & Err.Number & vbCrLf & _
                      "Description: " & Err.Description, _
                      vbExclamation Or vbOKOnly, _
                      "Error in Sub Valida_PMF_Bosch_Diesel")
49        Err.Clear
50        End

End Sub
'
'------------------------------------------------------------------------------------------

