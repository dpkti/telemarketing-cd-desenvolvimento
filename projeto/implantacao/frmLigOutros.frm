VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmLigOutros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Resultado do Contato"
   ClientHeight    =   4275
   ClientLeft      =   900
   ClientTop       =   2805
   ClientWidth     =   6840
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4275
   ScaleWidth      =   6840
   Begin VB.Frame frCliente 
      Caption         =   "CLIENTE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   645
      Index           =   1
      Left            =   120
      TabIndex        =   24
      Top             =   840
      Width           =   6585
      Begin VB.Label lblNOME_CLIENTE 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   6255
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "FORMAS CONTATO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   645
      Left            =   4200
      TabIndex        =   23
      Top             =   2820
      Width           =   2505
      Begin VB.ComboBox cboFormaContato 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame fraMotivo 
      Caption         =   "MOTIVO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   645
      Left            =   60
      TabIndex        =   22
      Top             =   3510
      Visible         =   0   'False
      Width           =   4065
      Begin VB.ComboBox cboMotivo 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   240
         Width           =   3855
      End
   End
   Begin VB.Timer Timer1 
      Left            =   3000
      Top             =   1680
   End
   Begin VB.TextBox txtFoneOrigem 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   2520
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.TextBox txtFoneDestino 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3120
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   2520
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.TextBox txtDuracao 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   2160
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Frame fraDadosLigacao 
      Caption         =   "C�digo do Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   4260
      TabIndex        =   18
      Top             =   1560
      Width           =   2445
      Begin VB.TextBox txtCodCliente 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   600
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtCGC 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   600
         MaxLength       =   14
         TabIndex        =   2
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label lbCod 
         Caption         =   "Cod.:"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   270
         Width           =   375
      End
      Begin VB.Label CGC 
         Caption         =   "CGC:"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   630
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "LIGA��O"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   555
      Left            =   120
      TabIndex        =   17
      Top             =   1560
      Width           =   2745
      Begin VB.OptionButton Option2 
         Caption         =   "RECEPTIVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1290
         TabIndex        =   4
         Top             =   270
         Width           =   1365
      End
      Begin VB.OptionButton Option1 
         Caption         =   "ATIVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   3
         Top             =   270
         Width           =   885
      End
   End
   Begin Bot�o.cmd cmdRecado 
      Height          =   675
      Left            =   3750
      TabIndex        =   11
      Top             =   0
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   "Recado para Outro Operador"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLigOutros.frx":0000
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Frame Frame2 
      Caption         =   "RESULTADO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   645
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   2820
      Width           =   4065
      Begin VB.ComboBox cboresultado 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   240
         Width           =   3855
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   30
      TabIndex        =   13
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLigOutros.frx":001C
      PICN            =   "frmLigOutros.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   12
      Top             =   690
      Width           =   6510
      _ExtentX        =   11483
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdMENSAGEM 
      Height          =   435
      Left            =   840
      TabIndex        =   10
      Top             =   120
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   767
      BTYPE           =   3
      TX              =   "Informe o Resultado"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   -1  'True
      BCOL            =   16711680
      BCOLO           =   16711680
      FCOL            =   16777215
      FCOLO           =   16777215
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLigOutros.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   555
      Left            =   5760
      TabIndex        =   8
      ToolTipText     =   "Voltar"
      Top             =   3600
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   979
      BTYPE           =   3
      TX              =   "Cancelar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLigOutros.frx":0D2E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblFoneOrigem 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Fone Origem:"
      Height          =   195
      Left            =   120
      TabIndex        =   21
      Top             =   2520
      Visible         =   0   'False
      Width           =   945
   End
   Begin VB.Label lblFoneDestino 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Fone Destino:"
      Height          =   195
      Left            =   2160
      TabIndex        =   20
      Top             =   2520
      Visible         =   0   'False
      Width           =   990
   End
   Begin VB.Label lblDuracao 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Dura��o:"
      Height          =   195
      Left            =   360
      TabIndex        =   19
      Top             =   2280
      Visible         =   0   'False
      Width           =   660
   End
End
Attribute VB_Name = "frmLigOutros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmLigOutros
' Author    : c.samuel.oliveira
' Date      : 06/09/18
' Purpose   : TI-6660
'---------------------------------------------------------------------------------------
Option Explicit

Public vFecharfrmLigOutros As Boolean
Dim NovaLigacao2 As Ligacao

Private Sub cboresultado_Click()
    Timer1.Interval = 0
    cmdMENSAGEM.Visible = False
    If Val(Mid(cboresultado, 1, 2)) = 8 Then
      cboMotivo.Visible = True
      fraMotivo.Visible = True
    Else
      cboMotivo.Visible = False
      fraMotivo.Visible = False
    End If
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdRecado_Click()
    frmRecado.Show
    StayOnTop frmRecado
End Sub

Private Sub cmdSair_Click()
          
1         On Error GoTo Trata_Erro

2         If cboresultado = "" And Parametros.Ativo = "1" Then
3             cmdMENSAGEM.Caption = "Informe o Resultado."
4             cmdMENSAGEM.Visible = True
5             Timer1.Interval = 1000
6             cboresultado.SetFocus
7             Exit Sub
          ElseIf Val(Mid(cboresultado, 1, 2)) = 8 And cboMotivo = "" Then 'TI-6660
             cmdMENSAGEM.Caption = "Informe o Motivo."
             cmdMENSAGEM.Visible = True
             Timer1.Interval = 1000
             cboMotivo.SetFocus
             Exit Sub
8         ElseIf cboresultado = "" And Parametros.Ativo <> "1" Then
              Unload Me
          End If
            
          If cboFormaContato = "" And Parametros.Ativo = "1" Then
               cmdMENSAGEM.Caption = "Informe a Forma de Contato."
              cmdMENSAGEM.Visible = True
              Timer1.Interval = 1000
              cboFormaContato.SetFocus
              Exit Sub
          End If
             
9         If txtCodCliente = "" And Parametros.Ativo = "1" Then
10            cmdMENSAGEM.Caption = "Informe o Codigo do Cliente."
11            cmdMENSAGEM.Visible = True
12            Timer1.Interval = 1000
              txtCodCliente.Enabled = True
              txtCodCliente.Locked = False
             Exit Sub
          ElseIf txtCodCliente = "0" And Parametros.Ativo = "1" Then
             cmdMENSAGEM.Caption = "Informe o Codigo do Cliente."
             cmdMENSAGEM.Visible = True
             Timer1.Interval = 1000
              txtCodCliente.Enabled = True
              txtCodCliente.Locked = False
             Exit Sub
         End If

16        frmVenda.lblTime.Caption = "00:00"
17        frmVenda.lblTime.Tag = 0
          NovaLigacao2.CodCliente = txtCodCliente.Text
                   
          'For�ar o usuario a informar o codigo do cliente
'          If Parametros.Ativo = "1" Then
'18           If Val(NovaLigacao2.CodCliente) = 0 Then
'19              If NovaLigacao2.Interna = True And NovaLigacao2.Transferido = True Then
'20                 NonStayOnTop Me
'21                 If MessageBox(0, "Informe o cliente." & vbCrLf & "Caso seja uma liga��o Externa, clique no Bot�o SIM.", "Aten�ao", &H40000 + 36) = 6 Then
'                      NovaLigacao2.CodCliente = 52306
'22                    frmVenda.txtCOD_CLIENTE = ""
'23                    frmVenda.txtCOD_CLIENTE = 52306
'24                    frmVenda.txtCOD_CLIENTE_LostFocus
'25                 Else
'26                    StayOnTop Me
'27                    Exit Sub
'28                 End If
'29                 StayOnTop Me
'30              ElseIf NovaLigacao2.Interna = False And NovaLigacao2.Transferido = False And Val(NovaLigacao2.Duracao) > 0 Then
'31                 NonStayOnTop Me
'32                 If MessageBox(0, "Informe o cliente." & vbCrLf & "Caso seja uma liga��o Externa, clique no Bot�o SIM.", "Aten�ao", &H40000 + 36) = 6 Then
'                      NovaLigacao2.CodCliente = 52306
'33                    frmVenda.txtCOD_CLIENTE = ""
'34                    frmVenda.txtCOD_CLIENTE = 52306
'35                    frmVenda.txtCOD_CLIENTE_LostFocus
'36                 Else
'37                    Exit Sub
'38                    StayOnTop Me
'39                 End If
'40                 StayOnTop Me
'41              End If
'42           End If
'          End If
43        SSCommand1_Click
44        frmVenda.vAchouCliente = False
          
45        Unload Me

Trata_Erro:
46        If Err.Number <> 0 Then
47           MessageBox 0, "Sub: cmdSair_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
48        End If

End Sub


Private Sub Form_Load()
1     On Error GoTo Trata_Erro
          
2     Unload frmFoneCliente

3     If Form_Aberto("frmLigOutros") = True Then
4       Me.Top = ((Screen.Height - Me.Height) / 2) + fContarForms * 200
5       Me.Left = ((Screen.Width - Me.Width) / 2)
6     Else
7       Me.Top = (Screen.Height - Me.Height) / 2
8       Me.Left = (Screen.Width - Me.Width) / 2
9     End If

15    MDIForm1.vFalando = False
16    MDIForm1.vVaiTransferir = False

17    If Not vConectadoHP8000 Then
18        If NovaLigacao.TipoLigacao = 1 Then
19          Option1.Value = True
20        Else
21          Option2.Value = True
22        End If
23    Else
24        If NovaLigacao.TipoLigacao = 1 And (Len(Trim(NovaLigacao.FoneOrigem)) = "13" And Left(Trim(NovaLigacao.FoneOrigem), 8) = vPrefixoFone) Then
25            Option1.Value = True
26        Else
27            Option2.Value = True
28        End If

29    End If

30    Preencher_Combo_Resultados

31    Me.Visible = True
          
37    If NovaLigacao2.CodCliente = 0 Then
38      If frmVenda.fraClienteEspera.Visible = False Then
39         If Val(frmVenda.txtCOD_CLIENTE) = 0 Then
40      NovaLigacao2.CodCliente = 0
41         Else
42      NovaLigacao2.CodCliente = Val(frmVenda.txtCOD_CLIENTE)
43         End If
44      Else
45         If Val(frmVenda.cmdClienteEspera.Tag) = 0 Then
46      NovaLigacao2.CodCliente = 0
47         Else
48      NovaLigacao2.CodCliente = Val(frmVenda.cmdClienteEspera.Tag)
49         End If
50      End If
51    End If

61    txtCodCliente = NovaLigacao2.CodCliente
      txtCGC = 0
        
Trata_Erro:
114    If Err.Number <> 0 Then
115       MessageBox 0, "Sub: Form_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
116    End If
End Sub

Private Sub SSCommand1_Click()

1         On Error GoTo Trata_Erro

2         If Parametros.Ativo = "0" And Trim(cboresultado) = "" Then Exit Sub

3         If Trim(cboresultado) = "" Then
4             NonStayOnTop Me
5             MessageBox 0, "Selecione um resultado para gravar", "Aten�ao", &H40000 + 0
6             StayOnTop Me
7             Exit Sub
8         End If

          'eliminar da lista de liga��es quando o
          'resultado for diferente de (sem contato, agendamento e vda para outro operador, N�o Atendida)
9         If Mid(Trim(cboresultado), 1, 2) <> 3 And _
                  Mid(Trim(cboresultado), 1, 2) <> 4 And _
                  Mid(Trim(cboresultado), 1, 2) <> 7 And _
                  Mid(Trim(cboresultado), 1, 2) <> 19 And _
                  Mid(Trim(cboresultado), 1, 2) <> 20 And _
                  Mid(Trim(cboresultado), 1, 2) <> 23 Then

10            OraParameters.Remove "cod"
11            OraParameters.Add "cod", NovaLigacao2.CodCliente, 1
12            OraParameters.Remove "cod_errora"
13            OraParameters.Add "cod_errora", 0, 2
14            OraParameters.Remove "txt_errora"
15            OraParameters.Add "txt_errora", "", 2

16            oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Update_Situacao(:Cod, :Cod_errora, :Txt_Errora); End;"

17            vErro = IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora")))

18            If vErro <> 0 Then
19                 MessageBox 0, "Sub: SSCommand1_Click" & vbCrLf & "Descri��o:" & oradatabase.Parameters("txt_errora") & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
20            End If
21        End If

22        OraParameters.Remove "DDD"
23        OraParameters.Add "DDD", 0, 1
24        OraParameters.Remove "FONE"
25        OraParameters.Add "FONE", 0, 1

26        OraParameters.Remove "Data_Real"
27        OraParameters.Add "Data_Real", Format$(DateValue(Date), "DD/MM/YY"), 1

28        OraParameters.Remove "cod"
                    
29        If NovaLigacao2.Interna = True Or NovaLigacao2.CodCliente = 0 Or NovaLigacao2.Particular = True Then
30            OraParameters.Add "Cod", 52306, 1
31        Else
32            OraParameters.Add "Cod", NovaLigacao2.CodCliente, 1
33        End If

34        OraParameters.Remove "vend"
35        OraParameters.Add "vend", CLng(sCOD_VEND), 1

          'TI-1395 - C.FELIPE.CASSIANI - Chamada sempre ser� ativa devido ao meio de comunica��o ser muito din�mico
36        OraParameters.Remove "tp"
37        OraParameters.Add "tp", IIf(Option1.Value = True, 1, 2), 1

38        OraParameters.Remove "res"
39        OraParameters.Add "res", Mid(cboresultado, 1, 2), 1

40        OraParameters.Remove "Tempo"
          OraParameters.Add "Tempo", 0, 1
'41        If Val(NovaLigacao2.Duracao) < 0 Then
'42           OraParameters.Add "Tempo", 0, 1
'43        Else
'44           OraParameters.Add "Tempo", Val(NovaLigacao2.Duracao), 1
'45        End If
          OraParameters.Remove "DDD"
          OraParameters.Add "DDD", 0, 1
          OraParameters.Remove "FONE"
          OraParameters.Add "FONE", 0, 1
'06/10/2006
'46          If Option1.Value = True Then
'47             If Len(Trim(NovaLigacao2.FoneDestino)) < 8 Then
'48                  OraParameters.Remove "DDD"
'49                  OraParameters.Add "DDD", 0, 1
'50                  OraParameters.Remove "FONE"
'51                  If NovaLigacao2.FoneDestino = vRamal Then
'                      'Neste ponto estou pegando o FoneOrigem porque ele recebeu uma
'                      'transferencia e transferiu para outro ramal.
'52                     If IsNumeric(NovaLigacao2.FoneOrigem) Then
'53                        OraParameters.Add "FONE", Val(NovaLigacao2.FoneOrigem), 1
'54                     Else
'55                        OraParameters.Add "FONE", 0, 1
'56                     End If
'57                  Else
'58                      If IsNumeric(NovaLigacao2.FoneDestino) Then
'59                         OraParameters.Add "FONE", Val(NovaLigacao2.FoneDestino), 1
'60                      Else
'61                         OraParameters.Add "FONE", 0, 1
'62                      End If
'63                  End If
'64             Else
'65                 If IsNumeric(NovaLigacao2.FoneDestino) Then
'66                    Get_DDD_FONE Trim(CStr(NovaLigacao2.FoneDestino))
'67                 Else
'68                    OraParameters.Remove "DDD"
'69                    OraParameters.Add "DDD", 0, 1
'70                    OraParameters.Remove "FONE"
'71                    OraParameters.Add "FONE", 0, 1
'72                 End If
'73             End If
'74          Else
'75             If Len(Trim(NovaLigacao2.FoneOrigem)) < 8 Then
'76                  OraParameters.Remove "DDD"
'77                  OraParameters.Add "DDD", 0, 1
'78                  OraParameters.Remove "FONE"
'
'79                  If Val(NovaLigacao2.FoneOrigem) = Val(vRamal) Then
'                      'Neste ponto estou pegando o FoneDestino porque ele recebeu uma
'                      'transferencia e transferiu para outro ramal.
'80                     If IsNumeric(NovaLigacao2.FoneDestino) Then
''77                        OraParameters.Add "FONE", Val(NovaLigacao2.FoneDestino), 1
'81                        Get_DDD_FONE Trim(CStr(NovaLigacao2.FoneDestino))
'82                     Else
'83                        OraParameters.Add "FONE", 0, 1
'84                     End If
'85                  Else
'86                      If IsNumeric(NovaLigacao2.FoneOrigem) Then
''83                         OraParameters.Add "FONE", Val(NovaLigacao2.FoneOrigem), 1
'87                         Get_DDD_FONE Trim(CStr(NovaLigacao2.FoneOrigem))
'88                      Else
'89                         OraParameters.Add "FONE", 0, 1
'90                      End If
'91                  End If
'92             Else
'93                 If IsNumeric(NovaLigacao2.FoneOrigem) Then
'94                    Get_DDD_FONE Trim(CStr(NovaLigacao2.FoneOrigem))
'95                 Else
'96                    OraParameters.Remove "DDD"
'97                    OraParameters.Add "DDD", 0, 1
'98                    OraParameters.Remove "FONE"
'99                    OraParameters.Add "FONE", 0, 1
'100                End If
'101            End If
'102         End If
'06/10/2006

103       OraParameters.Remove "cod_errora"
104       OraParameters.Add "cod_errora", 0, 2
105       OraParameters.Remove "txt_errora"
106       OraParameters.Add "txt_errora", "", 2
107       OraParameters.Remove "motivo"
108       If fraMotivo.Visible Then
109         OraParameters.Add "motivo", IIf(Trim(cboMotivo) = "", 0, Mid(Trim(cboMotivo), 1, 2)), 1
110       Else
111         OraParameters.Add "motivo", 0, 1
112       End If

          'FELIPE CASSIANI TI-1395
          OraParameters.Remove "tipo_contato"
          OraParameters.Add "tipo_contato", IIf(Trim(cboFormaContato) = "", 0, Mid(Trim(cboFormaContato), 1, 2)), 1

113       oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Insert_Contato(:Cod, :Data_Real, :Vend, :Tp, :Res, :Tempo, :DDD, :FONE, :motivo, :tipo_contato,:Cod_Errora, :Txt_Errora); End;"

114       vErro = IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora")))

115       If vErro <> 0 Then
116            MessageBox 0, "Sub: SSCommand1_Click" & vbCrLf & "Descri��o:" & oradatabase.Parameters("txt_errora") & vbCrLf & "Linha:" & Erl & vbCrLf & "Erro Oracle: " & vErro & vbCrLf & "C�d.Cliente:" & OraParameters("cod").Value & vbCrLf & "Data Real:" & OraParameters("Data_Real").Value & vbCrLf & "Vend:" & OraParameters("Vend").Value & vbCrLf & "TP:" & OraParameters("TP").Value & vbCrLf & "REs:" & OraParameters("Res").Value & vbCrLf & "Tempo:" & OraParameters("Tempo").Value & vbCrLf & "DDD:" & OraParameters("DDD").Value & vbCrLf & "Fone:" & OraParameters("Fone").Value, "Aten�ao", &H40000 + 0
117           Exit Sub
118       End If

          'O CLiente ligou e o atendente falou com o cliente, por�m n�o clicou no bot�o para colocar o codigo
          'do cliente na txtCod_Cli, com isso ele falou e desligou sem abrir o cliente.
          'Sendo assim, gerar uma log com a liga��o e n�o dar update no banco.
119       If frmVenda.fraClienteEspera.Visible = True And frmVenda.cmdClienteEspera.Tag = frmVenda.txtCOD_CLIENTE Then
120          Limpar_Cliente_Espera
121       End If

Trata_Erro:
122       If Err.Number <> 0 Then
123             MessageBox 0, "Sub: SSCommand1_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Binds:" & vbCrLf & Pegar_Binds, "Aten�ao", &H40000 + 0
124       End If


End Sub


Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next

    If Form_Aberto("frmFabrica") = True Then
        If frmFabrica.vFrmFabricaAberto = True Then
            frmFabrica.Visible = True
        End If
    End If

    If InStr(1, frmVenda.Caption, "::") > 0 Then
        frmVenda.Caption = Left(frmVenda.Caption, InStr(1, frmVenda.Caption, "::") - 2)
    End If
    frmVenda.cmdAtribuir.Visible = False

    Set frmLigOutros = Nothing
    'Set MDIForm1.frmLigOutros = Nothing

End Sub

Sub Atribuir_Dados_Ligacao()
    NovaLigacao2.AtivaSistema = NovaLigacao.AtivaSistema
    NovaLigacao2.FoneOrigem = NovaLigacao.FoneOrigem
    NovaLigacao2.INICIO = NovaLigacao.INICIO
    NovaLigacao2.Fim = NovaLigacao.Fim
    NovaLigacao2.Duracao = NovaLigacao.Duracao
    NovaLigacao2.FoneDestino = NovaLigacao.FoneDestino
    NovaLigacao2.Desligou = NovaLigacao.Desligou
    NovaLigacao2.TipoLigacao = NovaLigacao.TipoLigacao
    NovaLigacao2.Conversou = NovaLigacao.Conversou
    NovaLigacao2.Particular = NovaLigacao.Particular
    NovaLigacao2.Interna = NovaLigacao.Interna
    NovaLigacao2.Transferencia = NovaLigacao.Transferencia
    NovaLigacao2.RamalCanalFone = NovaLigacao.RamalCanalFone
    NovaLigacao2.CodCliente = NovaLigacao.CodCliente
    NovaLigacao2.Transferido = NovaLigacao.Transferido
End Sub
Public Sub Limpar_NovaLigacao2()
    
    If vGerarLog3000 = True Then
        'MDIForm1.GravarNoArquivo "LimparNovaLigacao2" 'TI-6660
    End If
    
    NovaLigacao2.AtivaSistema = False
    NovaLigacao2.Desligou = False
    NovaLigacao2.Duracao = ""
    NovaLigacao2.Fim = ""
    NovaLigacao2.FoneDestino = ""
    NovaLigacao2.FoneOrigem = ""
    NovaLigacao2.INICIO = ""
    NovaLigacao2.TipoLigacao = 0
    NovaLigacao2.Conversou = False
    NovaLigacao2.Particular = False
    NovaLigacao2.Interna = False
    NovaLigacao2.Transferencia = False
    NovaLigacao2.RamalCanalFone = False
    NovaLigacao2.CodCliente = 0
    NovaLigacao2.Transferido = False
End Sub

Private Sub Timer1_Timer()
    If cmdMENSAGEM.BackColor = vbWhite Then
        cmdMENSAGEM.BackColor = vbBlue
        cmdMENSAGEM.ForeColor = vbWhite
    Else
        cmdMENSAGEM.BackColor = vbWhite
        cmdMENSAGEM.ForeColor = vbBlue
    End If
End Sub



Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCGC_LostFocus()
    If txtCGC = "" Then
        cmdMENSAGEM.Visible = True
        Timer1.Interval = 1000
        cmdMENSAGEM.Caption = "Informe o Codigo do Cliente."
        txtCGC.SetFocus
        Exit Sub
    Else
        If txtCGC = "0" Then
            lblNOME_CLIENTE.Caption = ""
            Exit Sub
        Else
            If GetCliente(txtCodCliente.Text, txtCGC.Text) Then
                cmdMENSAGEM.Visible = False
                Timer1.Interval = 0
            End If
        End If
    End If
End Sub

Private Sub txtCodCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCodCliente_LostFocus()
    If txtCodCliente = "" Then
        cmdMENSAGEM.Visible = True
        Timer1.Interval = 1000
        cmdMENSAGEM.Caption = "Informe o Codigo do Cliente."
        txtCodCliente.SetFocus
        Exit Sub
    Else
        If txtCodCliente = "0" Then
            lblNOME_CLIENTE.Caption = ""
            Exit Sub
        Else
            If GetCliente(txtCodCliente.Text, txtCGC.Text) Then
                cmdMENSAGEM.Visible = False
                Timer1.Interval = 0
            End If
        End If
    End If

    'NovaLigacao2.CodCliente = txtCodCliente
End Sub

Public Function GetCliente(lngCod_cliente As Long, strCGC As String) As Boolean

    Dim ss As Object

    On Error GoTo TrataErro

    'inicializa retorno da funcao
    GetCliente = False

    'Me.lblTime = "00:00"
    'lblTime.Tag = 0

    'monta SQL
    OraParameters.Remove "cod"
    OraParameters.Remove "cgc"
    OraParameters.Remove "vErro"
    If lngCod_cliente <> 0 Then
        OraParameters.Add "cod", lngCod_cliente, 1
        OraParameters.Add "cgc", 0, 1
    Else
        OraParameters.Add "cod", 0, 1
        OraParameters.Add "cgc", strCGC, 1
    End If
    OraParameters.Add "vErro", 0, 2

    vSql = "Begin producao.pck_vda320.pr_cliente(:vCursor,:cod,:cgc,:vErro);END;"
    'Criar_Cursor
    oradatabase.ExecuteSQL vSql
    Set ss = oradatabase.Parameters("vCursor").Value
    'oradatabase.Parameters.Remove ("vCursor")

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(Sql)
        Set ss = Nothing
        Exit Function
    End If

    If ss.EOF() And ss.BOF Then
        MessageBox 0, "Cliente n�o Cadastrado", "Aten��o", &H40000 + 0
        frmLigOutros.txtCodCliente = ""
        frmLigOutros.txtCodCliente.SetFocus
        Set ss = Nothing
        Exit Function

    ElseIf ss!SITUACAO = 9 Then
        MessageBox 0, "Cliente Desativado", "Aten��o", &H40000 + 0
        frmLigOutros.txtCodCliente = ""
        frmLigOutros.txtCodCliente.SetFocus
        Set ss = Nothing
        Exit Function

    ElseIf ss!SITUACAO = 0 Then
        txtCodCliente.Text = ss!COD_CLIENTE
        txtCGC.Text = ss!CGC
        lblNOME_CLIENTE.Caption = ss!NOME_CLIENTE
        'valida funcao
        GetCliente = True
    End If
    
    Exit Function

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 400 Or Err = 5 Or Err = 30009 Then
        Resume Next
    Else
        MessageBox 0, "GetCliente" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl, "Aten��o", &H40000 + 0
    End If

End Function

Sub Preencher_Combo_Resultados()
          
1         On Error GoTo Trata_Erro
          
          Dim ss As Object
          Dim i As Long
          
2         oradatabase.Parameters.Remove "vErro"
3         oradatabase.Parameters.Add "vErro", 0, 2
          'Criar_Cursor
4         vSql = "Begin producao.pck_vda610.pr_resultados(:vCursor,:vErro);END;"
5         oradatabase.ExecuteSQL vSql
          
6         Set ss = oradatabase.Parameters("vCursor").Value
          'oradatabase.Parameters.Remove "vCursor"

7         If oradatabase.Parameters("vErro") <> 0 Then
8               MessageBox 0, "Sub: Form_Load" & vbCrLf & "Descri��o:" & oradatabase.Parameters("vErro") & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
9         End If

10        If ss.BOF And ss.EOF Then
11            NonStayOnTop Me
12            MessageBox 0, "N�o h� resultados cadastrados", "Aten�ao", &H40000 + 0
13            StayOnTop Me
14            Exit Sub
15        End If

16        For i = 1 To ss.RecordCount
17            cboresultado.AddItem Format(CStr(ss!cod_resultado), "00") & " - " & CStr(ss!desc_resultado)
18            If ss!cod_resultado = 8 Then
19              cboresultado.AddItem ""
20            End If
21            ss.MoveNext
22        Next
23        ss.Close
24        Set ss = Nothing
          
          
          'Criar_Cursor
25        vSql = "Begin producao.pck_vda610.pr_MOTIVOS(:vCursor,:vErro);END;"
26        oradatabase.ExecuteSQL vSql

27        Set ss = oradatabase.Parameters("vCursor").Value
          'oradatabase.Parameters.Remove "vCursor"

28        If oradatabase.Parameters("vErro") <> 0 Then
29              MessageBox 0, "Sub: Form_Load" & vbCrLf & "Descri��o:" & oradatabase.Parameters("vErro") & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
30        End If

31        If ss.BOF And ss.EOF Then
32            NonStayOnTop Me
33            MessageBox 0, "N�o h� motivos cadastrados", "Aten�ao", &H40000 + 0
34            StayOnTop Me
35            Exit Sub
36        End If

37        For i = 1 To ss.RecordCount
38            cboMotivo.AddItem Format(CStr(ss!cod_motivo), "00") & " - " & CStr(ss!motivo)
39            ss.MoveNext
40        Next
41        ss.Close
42        Set ss = Nothing

          'FELIPE CASSIANI TI-1395 - POPULA COMBO FORMA DE CONTATO
           'Criar_Cursor
         vSql = "Begin producao.pck_vda610.pr_MOTIVOS_OUTROS(:vCursor,:vErro);END;"
         oradatabase.ExecuteSQL vSql
          
         Set ss = oradatabase.Parameters("vCursor").Value
          'oradatabase.Parameters.Remove "vCursor"

         If oradatabase.Parameters("vErro") <> 0 Then
               MessageBox 0, "Sub: Form_Load" & vbCrLf & "Descri��o:" & oradatabase.Parameters("vErro") & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
         End If

         If ss.BOF And ss.EOF Then
             NonStayOnTop Me
             MessageBox 0, "N�o h� motivos cadastrados", "Aten�ao", &H40000 + 0
             StayOnTop Me
             Exit Sub
         End If

         For i = 1 To ss.RecordCount
             cboFormaContato.AddItem Format(CStr(ss!cod_motivo), "00") & " - " & CStr(ss!motivo)
             ss.MoveNext
         Next
         ss.Close
         Set ss = Nothing
        
          
Trata_Erro:
43        If Err.Number <> 0 Then
44              MessageBox 0, "Sub: Preenche_Combo_Resultados" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Binds:" & vbCrLf & Pegar_Binds, "Aten�ao", &H40000 + 0
45        End If

End Sub

Function Pegar_Binds() As String

1         On Error Resume Next

          Dim vBind As String
          Dim vNome As String
          Dim vIndice

2         For vIndice = vBanco.Parameters.count To 0 Step -1
3             vNome = vBanco.Parameters(vIndice).Name
4             If InStr(1, UCase(vNome), "CURSOR") = 0 Then
5                 If vBind = "" Then
6                     vBind = vBanco.Parameters(vIndice).Name & " = " & vBanco.Parameters(vIndice).Value
7                 Else
8                     vBind = vBind & "," & vBanco.Parameters(vIndice).Name & " = " & vBanco.Parameters(vIndice).Value
9                 End If
10            End If
11        Next
12        Pegar_Binds = vBind
          
End Function

