VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmRepres 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SELECIONE O RESPONS�VEL PELO CLIENTE "
   ClientHeight    =   2955
   ClientLeft      =   1560
   ClientTop       =   2250
   ClientWidth     =   4905
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2955
   ScaleWidth      =   4905
   Begin MSGrid.Grid grdrep 
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   390
      Width           =   4575
      _Version        =   65536
      _ExtentX        =   8070
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "Frmrep.frx":0000
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "SELECIONE O RESPONS�VEL PELO CLIENTE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   240
      TabIndex        =   2
      Top             =   90
      Width           =   4035
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "D� UM DUPLO CLICK SOBRE O REPRES. ESCOLHIDO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   30
      TabIndex        =   1
      Top             =   2640
      Width           =   4770
   End
End
Attribute VB_Name = "frmRepres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Posicionar_Forms True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmRepres = Nothing
    Posicionar_Forms False
End Sub

Private Sub grdrep_DblClick()

    lngCod_vend = 0
    grdrep.Col = 2
    frmVenda.cboFilial = grdrep.Text
    grdrep.Col = 3

    grdrep.Col = 0
    lngCod_repres = Format(grdrep.Text, "0000")

    grdrep.Col = 1
    frmVenda.lblRepres = IIf(lngCod_repres = 0, Format(lngCod_vend, "0000"), Format(lngCod_repres, "0000")) & "-" & grdrep.Text

    Unload Me
End Sub

