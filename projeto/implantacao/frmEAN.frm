VERSION 5.00
Begin VB.Form frmEAN 
   Caption         =   "Consulta DPK / EAN"
   ClientHeight    =   1500
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3885
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1500
   ScaleWidth      =   3885
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtEAN 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   75
      MaxLength       =   13
      TabIndex        =   0
      Top             =   150
      Width           =   3690
   End
   Begin VB.Label lblRetorno 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   90
      TabIndex        =   1
      Top             =   945
      Width           =   3645
   End
End
Attribute VB_Name = "frmEAN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    txtEAN.Text = ""
End Sub

Private Sub txtEAN_Change()
    
    If Len(txtEAN.Text) > 12 Then
        SendKeys ("{TAB}")
        OraParameters.Remove "vEAN"
        OraParameters.Add "vEAN", txtEAN.Text, 1
        OraParameters.Remove "vErro"
        OraParameters.Add "vErro", 0, 2
       
        oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.PR_CONV_EAN(:vCursor, :vEAN, :vErro); END;"
        Set ss = oradatabase.Parameters("vCursor").Value
        
        If IsNull(oradatabase.Parameters("vErro").Value) Then
            If IsNull(ss!cod_dpk) = False Then
                lblRetorno.ForeColor = &HC000&
                lblRetorno.Caption = "ITEM OK"
                frmVenda.txtCOD_DPK.Text = ss!cod_dpk
                i = 0
                Do While i < 100000
                    i = i + 1
                    DoEvents
                Loop
                'frmVenda.txtCOD_DPK.SetFocus
                Set ss = Nothing
                Unload Me
            Else
                lblRetorno.ForeColor = &HFF&
                lblRetorno.Caption = "EAN N�O CADASTRADO"
                i = 0
                Do While i < 2000000
                    i = i + 1
                    DoEvents
                Loop
                'frmVenda.txtCOD_DPK.SetFocus
                Set ss = Nothing
                Unload Me
            End If

        Else
            MsgBox "Erro na convers�o do EAN, verifique o cadastro do item!", vbCritical, "Erro"
            frmVenda.txtCOD_DPK.SetFocus
            Set ss = Nothing
            Unload Me
        End If
        
    End If
End Sub

Private Sub txtEAN_KeyPress(KeyAscii As Integer)
    If KeyAscii = 8 Then
        Exit Sub
    End If
    If (KeyAscii <= 47) Or (KeyAscii >= 58) Then
        KeyAscii = 2
    End If
End Sub
