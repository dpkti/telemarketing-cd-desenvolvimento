VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmInventario 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "LISTA DE ITENS DO INVENT�RIO"
   ClientHeight    =   5250
   ClientLeft      =   855
   ClientTop       =   2340
   ClientWidth     =   7830
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5250
   ScaleWidth      =   7830
   StartUpPosition =   2  'CenterScreen
   Begin MSGrid.Grid grdInventario 
      Height          =   4335
      Left            =   30
      TabIndex        =   0
      Top             =   780
      Visible         =   0   'False
      Width           =   7755
      _Version        =   65536
      _ExtentX        =   13679
      _ExtentY        =   7646
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   30
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmInventario.frx":0000
      PICN            =   "frmInventario.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   2
      Top             =   720
      Width           =   6930
      _ExtentX        =   12224
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblDeposito 
      AutoSize        =   -1  'True
      Caption         =   "Dep�sito: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   1200
      TabIndex        =   3
      Top             =   240
      Width           =   1080
   End
End
Attribute VB_Name = "frmInventario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'psergio CI&T - 09/09/15 - TI-3198 Projeto Invent�rio Ciclico
Option Explicit

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_Load()

    DoEvents
    
    lblDeposito.Caption = "Dep�sito: " & Format(lngCD, "00")
    
    Lista_Itens_Inventario
    grdInventario.Visible = True
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmInventario = Nothing
End Sub

'psergio CI&T - 09/09/15 - TI-3198 Projeto Invent�rio Ciclico
Private Sub Lista_Itens_Inventario()
    On Error GoTo Trata_Erro

    Dim rsINV As Object
    
     OraParameters.Remove "loja"
     OraParameters.Add "loja", lngCD, 1

     OraParameters.Remove "vErro"
     OraParameters.Add "vErro", 0, 2

     vSql = "Begin producao.pck_vda230.PR_LISTA_INVENTARIO(:vCursor,:loja,:vErro);END;"

    'Criar_Cursor
    oradatabase.ExecuteSQL vSql
    
    
    If oradatabase.Parameters("vErro").Value = 0 Then
    
        Set rsINV = oradatabase.Parameters("vCursor").Value
        
              'carrega dados
            With grdInventario
     
                .Cols = 2
     
                .Rows = rsINV.RecordCount + 1
     
                .ColWidth(0) = 800
                .ColWidth(1) = 6880
    
                .Row = 0
     
                .Col = 0: .Text = "COD DPK"
                .Col = 1: .Text = "DESCRI��O"
     
                For i = 1 To .Rows - 1
     
                    .Row = i
                    .Col = 0: .Text = rsINV!cod_dpk
                    .Col = 1: .Text = rsINV!desc_item
     
                    rsINV.MoveNext
     
                Next
     
                If .Rows > 1 Then
                    .Row = 1
                End If
     
            End With
     
              
            rsINV.Close
              
            Set rsINV = Nothing
            
    End If
        
        Exit Sub

Trata_Erro:

   MessageBox 0, "Lista_Itens_Inventario" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten�ao", &H40000 + 0

End Sub



