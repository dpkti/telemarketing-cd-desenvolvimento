VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConecta8000 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Conex�o com HP8000"
   ClientHeight    =   1140
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4275
   Icon            =   "frmConecta8000.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1140
   ScaleWidth      =   4275
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer2 
      Interval        =   1000
      Left            =   1155
      Top             =   210
   End
   Begin MSComctlLib.ProgressBar pbPABX 
      Height          =   135
      Left            =   150
      TabIndex        =   0
      Top             =   825
      Width           =   4035
      _ExtentX        =   7117
      _ExtentY        =   238
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Max             =   10
      Scrolling       =   1
   End
   Begin VB.Label Label12 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "O sistema est� conectando com HP8000"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   405
      Left            =   1050
      TabIndex        =   2
      Top             =   345
      Width           =   2925
   End
   Begin VB.Label Label10 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Aguarde..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1170
      TabIndex        =   1
      Top             =   105
      Width           =   2895
   End
End
Attribute VB_Name = "frmConecta8000"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmConecta8000
' Author    : c.samuel.oliveira
' Date      : 17/08/18
' Purpose   : TI-6660
'---------------------------------------------------------------------------------------

Option Explicit

'------------------------------------------------------------------
'A cada 1 segundo o sistema tenta conectar com o HP8000
'Caso ela nao consiga conexao, ele entra em loop por 10 vezes
'Na 11 vez, o sistema exibir� uma mensagem que n�o foi poss�vel
'efetuar conex�o com HP8000
'------------------------------------------------------------------
Private Sub Timer2_Timer()

    On Error GoTo Trata_Erro

    If pbPABX.Value >= 10 Then
        MsgBox "N�o foi poss�vel conectar no HP8000." & vbCrLf & "Por favor, avise o Helpdesk.", vbCritical, "Aten��o"
        End
    End If
    
    pbPABX.Value = pbPABX.Value + 1

    If Val(Parametros.Ativo) = 1 Then
        If Parametros.IP8000 <> "" Then
            'Mudan�a componente Siemens - 18/12/2013
            'MDIForm1.MONEXT8000.EnderecoServidor = Parametros.IP8000
            'MDIForm1.MONEXT8000.PortaServidor = Parametros.Porta8000
            'MDIForm1.MONEXT8000.TipoAplicacao = 80
            'MDIForm1.MONEXT8000.Connect
'            MDIForm1.MONEXT9000.CTIServerIP = Parametros.IP8000
'            MDIForm1.MONEXT9000.CTIServerPort = Parametros.Porta8000
'            MDIForm1.MONEXT9000.AppTipo = 80
'            MDIForm1.MONEXT9000.Connect
            'TI-6660
            'Set DgTelRetorno = objDGTel.Login("teste.teste", Trim(frmRamal.txtID.Text), Trim(vRamal))
            Set DgTelRetorno = objDGTel.Login(CarregarCPF(sCOD_VEND), vDgTelSenha, Trim(Mid(vRamal, 9, 4)))
            DgTelRetorno.Get lngErro, strMessage, lngAgenteId, strAgenteNome, lngAgenteRamal
            If (lngAgenteId = 0) Then
                MsgBox "Houve um erro ao efetuar o login. Erro: " & strMessage
            Else
                Set DgTelRetorno = objDGTel.PauseAgent("VDA230")

                Timer2.Interval = 0
                vConectadoHP8000 = True
                RegLogonUser.vLogon = True
                Unload Me
            End If
            'FIM TI-6660
        ElseIf Parametros.IP3000 <> "" Then
            'vMonRet = MDIForm1.MonExt3000.Connect(Parametros.IP3000, Parametros.Porta3000, "$.�D$(3_�����ط����4D$�65����4��3��)A3�@�$.7(T$.�Dg��$���ۤ��@") 'TI-6660
        Else
            MsgBox "Para utilizar Liga��o Autom�tica, o par�metro IP3000 ou IP8000 devem estar preenchidos.", vbInformation, "Aten��o"
            Exit Sub
        End If
    
    End If

'TI-6660
'    If vConectadoHP8000 = True Then
'        Conectar_Ramal
'    End If
'FIM TI-6660
    
Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub grdCliente_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H4000 + 0
    End If

End Sub

Sub Conectar_Ramal()

    On Error GoTo Trata_Erro

    Dim vAgent As Byte
    Dim vExt As Byte
    Dim vRetorno As String

    vMonRet = False
 
    vRetorno = ""

    If Val(Parametros.IP8000) <> 0 Then
        'Mudan�a componente Siemens - 18/12/2013
        'MDIForm1.MONEXT8000.fncMonitorStop vRamal, TIPO_RAMAL
        'If MDIForm1.MONEXT8000.fncMonitorStart(vRamal, TIPO_RAMAL) > 0 Then
        ''TI-6660
'        MDIForm1.MONEXT9000.fncMonitorStop vRamal, TIPO_RAMAL
'        If MDIForm1.MONEXT9000.fncMonitorStart(vRamal, TIPO_RAMAL) > 0 Then
'            vMonRet = True
'            MDIForm1.Conexao_Ramal_OK = True
'            Timer2.Interval = 0
'            Unload Me
'        End If
        'FIM TI-6660
        
    ElseIf Val(Parametros.IP3000) <> 0 Then

        vIDRamal = Rnd(9999)
        vIDMonExt = Rnd(9999)

        'vAgent = MDIForm1.MonExt3000.StartAgentMonitor(vIDRamal, vRamal)'TI-6660
        'vExt = MDIForm1.MonExt3000.StartExtMonitor(vIDMonExt, vRamal)'TI-6660

        If vAgent = 1 And vExt = 1 Then
            vMonRet = True
        End If

        'Call MDIForm1.MonExt3000_StartExtMonitorResult(vIDRamal, 0)'TI-6660

    End If

    If vMonRet = True Then
        MDIForm1.Conexao_Ramal_OK = True
        Timer2.Interval = 0
        Unload Me
    Else
        MDIForm1.Conexao_Ramal_OK = False
        'Mudan�a componente Siemens - 18/12/2013
        'MDIForm1.MONEXT8000.Disconnect
        'MDIForm1.MONEXT8000.fncMonitorStop vRamal, TIPO_RAMAL
        'MDIForm1.MONEXT9000.Disconnect 'TI-6660
        'MDIForm1.MONEXT9000.fncMonitorStop vRamal, TIPO_RAMAL 'TI-6660
        Sleep 1000
    End If

Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub Conectar_Ramal" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H4000 + 0
    End If

End Sub
