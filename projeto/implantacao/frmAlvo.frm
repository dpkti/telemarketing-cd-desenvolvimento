VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmAlvo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CLIENTES FIEIS SEM COMPRA NO M�S"
   ClientHeight    =   4740
   ClientLeft      =   150
   ClientTop       =   1515
   ClientWidth     =   9390
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4740
   ScaleWidth      =   9390
   StartUpPosition =   2  'CenterScreen
   Begin MSGrid.Grid grdFiel 
      Height          =   3855
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Visible         =   0   'False
      Width           =   9375
      _Version        =   65536
      _ExtentX        =   16536
      _ExtentY        =   6800
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "frmAlvo.frx":0000
   End
   Begin Bot�o.cmd SSCommand2 
      Height          =   675
      Left            =   30
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAlvo.frx":001C
      PICN            =   "frmAlvo.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   2
      Top             =   720
      Width           =   9360
      _ExtentX        =   16510
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmAlvo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmAlvo
' Author    : c.samuel.oliveira
' Date      : 08/09/2017
' Purpose   : TI-6092
'---------------------------------------------------------------------------------------

Option Explicit

Private Sub Form_Unload(Cancel As Integer)
    Set frmAlvo = Nothing
End Sub

Private Sub grdfiel_Click()
    'Grava o codigo do cliente em arquivo
    'texto para ser lido pelo programa tlmkt
    
    grdFiel.Col = 0

    frmVenda.txtCOD_CLIENTE.Text = ""
    frmVenda.txtCOD_CLIENTE.Text = CStr(grdFiel.Text)
    frmVenda.txtCOD_CLIENTE.DataChanged = True
    frmVenda.txtCOD_CLIENTE.SetFocus
    frmVenda.txtCOD_CLIENTE_LostFocus 'TI-6092
    Unload Me


End Sub

Private Sub SSCommand2_Click()
    Unload Me
End Sub
