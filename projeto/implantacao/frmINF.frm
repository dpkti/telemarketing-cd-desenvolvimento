VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmINF 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "INFORMA��ES SOBRE AS COMPRAS DOS �LTIMOS 3 MESES"
   ClientHeight    =   3660
   ClientLeft      =   855
   ClientTop       =   2340
   ClientWidth     =   9420
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3660
   ScaleWidth      =   9420
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox cheMostrarPreco 
      Caption         =   "Mostrar pre�o"
      Height          =   240
      Left            =   45
      TabIndex        =   3
      Top             =   3420
      Visible         =   0   'False
      Width           =   1455
   End
   Begin MSGrid.Grid grdINF2 
      Height          =   2535
      Left            =   30
      TabIndex        =   0
      Top             =   780
      Visible         =   0   'False
      Width           =   9315
      _Version        =   65536
      _ExtentX        =   16431
      _ExtentY        =   4471
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   30
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmINF.frx":0000
      PICN            =   "frmINF.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   2
      Top             =   720
      Width           =   6930
      _ExtentX        =   12224
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmINF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Adeilson - 01/02/12
Private Sub cheMostrarPreco_Click()
    If cheMostrarPreco.Value = 1 Then
        bMostrarPrecoConversao = True
    Else
        bMostrarPrecoConversao = False
    End If
    Call OrdenaGridINF
End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmINF = Nothing
End Sub

Private Sub grdINF2_DblClick()
    grdINF2.Col = 4
    frmVenda.txtCOD_DPK = grdINF2.Text

    If frmVenda.txtCOD_DPK <> "" Then
        frmVenda.txtCOD_DPK_LostFocus
    End If

    Unload Me
End Sub

Private Sub grdINF2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    ' Jo�o Marcos Sakalauska - 24/11/2011
    ' Devido a limita��o do componente Grid nativo do VB6 foi implementado o m�todo que verifica
    ' a �rea correspondente ao t�tulo "Pre�o" da Grid, sendo assim, qualquer altera��o no tamanho
    ' ou quantidade dos campos afetar� esse evento sendo necess�rio o ajuste na verifica��o do
    ' tamanho nas dimens�es X e Y abaixo.
    If ((x >= 6480 And x <= 7275) And (y > 0 And y <= 225)) Then
        If (grdINF2.rows > 1) Then
            Call OrdenaGridINF
        End If
    End If
End Sub

Private Sub OrdenaGridINF()
    'Verificar se a �ltima coluna foi clicada para realizar a ordena��o
    'Lembrar que a ordena��o est� baseada na �ltima coluna que � o PRE�O B
    If (grdINF2.Col = (grdINF2.Cols - 1)) Then
    
        ' Verifica se j� foi ordenado, invertendo a ordem a cada click na �ltima coluna (PRE�O B)
        Select Case strOrdenar
            Case "C"
                strOrdenar = "D"
            Case "D"
                strOrdenar = "C"
            Case ""
                strOrdenar = "D"
        End Select

        frmINF.Hide
        Call frmVenda.CarregaGrdINF2(strOrdenar)
        grdINF2.Refresh
    End If
End Sub


