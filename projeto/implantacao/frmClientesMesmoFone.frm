VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmClientesMesmoFone 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Clientes com o mesmo Fone"
   ClientHeight    =   2655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5175
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2655
   ScaleWidth      =   5175
   StartUpPosition =   3  'Windows Default
   Begin MSFlexGridLib.MSFlexGrid mfgClientes 
      Height          =   1800
      Left            =   30
      TabIndex        =   0
      Top             =   810
      Width           =   5100
      _ExtentX        =   8996
      _ExtentY        =   3175
      _Version        =   393216
      BackColorBkg    =   -2147483633
      HighLight       =   0
      ScrollBars      =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd SSCommand1 
      Height          =   675
      Left            =   30
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmClientesMesmoFone.frx":0000
      PICN            =   "frmClientesMesmoFone.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   2
      Top             =   720
      Width           =   5160
      _ExtentX        =   9102
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmClientesMesmoFone"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub Form_Load()
    Me.Top = (Screen.Width - Me.Width) / 2
    Me.Left = (Screen.Height - Me.Height) / 2

    Preencher_Clientes_Mesmo_Fone vObjOracle
End Sub

Public Sub Preencher_Clientes_Mesmo_Fone(rst As Object)
          
    On Error GoTo Trata_Erro
          
          Dim i As Double

1         mfgClientes.Rows = rst.RecordCount + 1
2         mfgClientes.Cols = 3
3         mfgClientes.ColWidth(0) = 200
4         mfgClientes.ColWidth(1) = 1000
5         mfgClientes.ColWidth(2) = 3500

6         mfgClientes.TextMatrix(0, 1) = "C�digo"
7         mfgClientes.TextMatrix(0, 2) = "Nome"
8         rst.MoveFirst

9         For i = 1 To rst.RecordCount
10            mfgClientes.TextMatrix(i, 1) = rst.Fields(0)
11            mfgClientes.TextMatrix(i, 2) = rst.Fields(1)
12            rst.MoveNext
13        Next

Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub Preencher_Clientes_mesmo_fone" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
    End If


End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmClientesMesmoFone = Nothing
End Sub

Private Sub mfgClientes_DblClick()

1         On Error GoTo Trata_Erro

2         strPreencher_Cliente = False
3         NovaLigacao.CodCliente = mfgClientes.TextMatrix(mfgClientes.Row, 1)
4         frmVenda.txtCod_cliente = ""
5         frmVenda.txtCod_cliente = mfgClientes.TextMatrix(mfgClientes.Row, 1)
6         frmVenda.cmdClienteEspera.Tag = mfgClientes.TextMatrix(mfgClientes.Row, 1)
7         frmVenda.txtCOD_CLIENTE_LostFocus
8         vNumFoneNovo = ""
9         Unload Me

Trata_Erro:
10        If Err.Number <> 0 Then
11            MessageBox 0, "Sub grdCliente_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
12        End If


End Sub

Private Sub SSCommand1_Click()
    Unload Me
End Sub
