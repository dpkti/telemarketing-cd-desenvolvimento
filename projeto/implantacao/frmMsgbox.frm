VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMsgbox 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aten��o"
   ClientHeight    =   1080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5685
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1080
   ScaleWidth      =   5685
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Left            =   5250
      Top             =   0
   End
   Begin MSComctlLib.ImageList imlMsg 
      Left            =   4830
      Top             =   450
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMsgbox.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMsgbox.frx":5D62
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&Ok"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2340
      TabIndex        =   3
      Top             =   630
      Width           =   1335
   End
   Begin VB.CommandButton cmdNao 
      Caption         =   "&N�o"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   1
      Top             =   630
      Width           =   1335
   End
   Begin VB.CommandButton cmdSim 
      Caption         =   "&Sim"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1410
      TabIndex        =   0
      Top             =   630
      Width           =   1335
   End
   Begin VB.Image Image1 
      Height          =   465
      Left            =   90
      Top             =   90
      Width           =   405
   End
   Begin VB.Label lblMsgbox 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   690
      TabIndex        =   2
      Top             =   90
      Width           =   4905
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmMsgbox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Comentado por Eduardo - 25/11/2005
'
'Public vRetorno As Byte '1=OK  2=SIM  3=NAO
'Public vPosicao As String
'
'Private Sub cmdNao_Click()
'    vRetorno = 3
'
''    If vPosicao = "Finaliza Pedido 4" Then
''        Exit Sub
'
''Comentado por Eduardo 25/11/05
''   ElseIf vPosicao = "Adicionar Cliente" Then
''        NonStayOnTop Me
''        MessageBox 0, "Selecione o Cliente.", "Aten�ao", &H40000 + 0
''        StayOnTop Me
''
''        frmVenda.txtCOD_CLIENTE.SetFocus
''
''        If Timer1.Interval > 0 Then
''            Timer1.Interval = 0
''            MDIForm1.Desligar
''        End If
'
'
''Neste ponto ele colocou um telefone que nao existe para um cliente e foi perguntado
''se confirmava a ligacao para este cliente e foi respondido nao
''Comentado por Eduardo - 25/11/05
''    ElseIf vPosicao = "Adicionar Fone" Then
''        frmVenda.vAchouCliente = False
''        frmTipoLigacao.Show
''        StayOnTop frmTipoLigacao
''    End If
'
'    Unload Me
'End Sub
'
'Private Sub cmdOk_Click()
'    vRetorno = 1
'
'    Select Case vPosicao
'        Case "Finalizar Pedido"
'            frmFimPedido.Finalizar_Pedido
'            Exit Sub
'        Case "Finalizar Pedido 2"
'            frmFimPedido.Finalizar_Pedido_2
'            Exit Sub
'        'Case "Finalizar Pedido 3"
'        '   frmFimPedido.Finalizar_Pedido_3
'        '    Exit Sub
''Comentado 25/11/05 - Eduardo
'        'Case "Finalizar Pedido 4"
'        '    frmFimPedido.Finalizar_Pedido_4
'        '    Exit Sub
'    End Select
'
'    Timer1.Interval = 0
'    Unload Me
'End Sub
'
'Private Sub cmdSim_Click()
'
'    vRetorno = 2
'    Timer1.Interval = 0
'
'    Me.Visible = False
'
'    Select Case vPosicao
'
''Comentado por Eduardo - 25/11/05
'
''        Case "Adicionar Cliente"
''            If Val(OraParameters("DDD")) = "0" Then
''                NonStayOnTop Me
''                MessageBox 0, "Telefone n�o foi adicionado. Verifique.", "Aten�ao", &H40000 + 0
''                StayOnTop Me
''
''            End If
''            MDIForm1.Gravar_Cliente_Banco OraParameters("DDD"), OraParameters("FONE")
'
'        'Comentado por Eduardo - 05/07/05
'            'frmDiscando.Show
'            'StayOnTop frmDiscando
'
''        Case "Adicionar Fone"
''            frmVenda.txtCOD_CLIENTE = ""
''            frmVenda.txtCOD_CLIENTE_LostFocus
''
''            If Form_Aberto("frmfonecliente") = False Then
''                frmFoneCliente.Show
''                StayOnTop frmFoneCliente
''            End If
''            Limpar_Cliente_Espera
'
''Comentado por Eduardo - 25/11/05
''        Case "Adicionar Fone"
''             frmVenda.cmdAtribuir_Click
''             frmVenda.vAchouCliente = True
''        Case "Finalizar Pedido 4"
''            frmFimPedido.Finalizar_Pedido_4
'
'    End Select
'
'    Me.Visible = True
'    Unload Me
'End Sub
'
'Private Sub Form_Activate()
'    If cmdOK.Visible = True Then
'        Image1.Picture = imlMsg.ListImages(1).Picture
'    ElseIf cmdOK.Visible = False Then
'        Image1.Picture = imlMsg.ListImages(2).Picture
'    End If
'End Sub
'
'Private Sub Form_Load()
'    frmFimPedido.vfrmmsgbox_Aberto = True
'    vRetorno = 0
'End Sub
'
'
'Private Sub Form_Unload(Cancel As Integer)
'    frmFimPedido.vfrmmsgbox_Aberto = False
'    Set frmMsgbox = Nothing
'End Sub
'
'Private Sub Timer1_Timer()
'    If Timer1.Interval > 0 Then
'        MDIForm1.Desligar
'        Timer1.Interval = 0
'    End If
'    Unload Me
'End Sub
'
