VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmConsulta_Recados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recados"
   ClientHeight    =   4890
   ClientLeft      =   1140
   ClientTop       =   1545
   ClientWidth     =   12315
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4890
   ScaleWidth      =   12315
   StartUpPosition =   2  'CenterScreen
   Begin MSGrid.Grid grdRecados 
      Height          =   4095
      Left            =   30
      TabIndex        =   0
      ToolTipText     =   "Para excluir um recado: ""duplo click"""
      Top             =   750
      Visible         =   0   'False
      Width           =   12255
      _Version        =   65536
      _ExtentX        =   21616
      _ExtentY        =   7223
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmConsulta_Recados.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   30
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta_Recados.frx":001C
      PICN            =   "frmConsulta_Recados.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   2
      Top             =   690
      Width           =   12300
      _ExtentX        =   21696
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmConsulta_Recados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmConsulta_Recados = Nothing
End Sub

Private Sub grdRecados_DblClick()

1         On Error GoTo Trata_Erro

2         Sql = "Begin producao.pck_vda230.pr_deleta_recado(:vend,:dt_recado,:vErro);END;"

3         OraParameters.Remove "vend"
4         OraParameters.Add "vend", sCOD_VEND, 1
5         grdRecados.Col = 0
6         OraParameters.Remove "dt_recado"
7         OraParameters.Add "dt_recado", CDate(grdRecados.Text), 1
8         OraParameters.Remove "vErro"
9         OraParameters.Add "vErro", 0, 2


10        oradatabase.ExecuteSQL Sql

11        If oradatabase.Parameters("vErro").Value <> 0 Then
12            Resume Next
13        End If
14        Unload Me
Trata_Erro:
15            If Err.Number <> 0 Then
16                MessageBox 0, "Sub grdRecados_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
17            End If
          
End Sub

