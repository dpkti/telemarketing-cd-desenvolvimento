VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmQuadrante 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "A��es - Cliente por Quadrante"
   ClientHeight    =   3135
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11640
   Icon            =   "frmQuadrante.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "frmQuadrante.frx":5C12
   ScaleHeight     =   3135
   ScaleWidth      =   11640
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList imgList 
      Left            =   3960
      Top             =   2880
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmQuadrante.frx":B824
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lv 
      Height          =   2775
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   4895
      View            =   3
      MultiSelect     =   -1  'True
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      SmallIcons      =   "imgList"
      ForeColor       =   8388608
      BackColor       =   14737632
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "A��O"
         Object.Width           =   21167
      EndProperty
   End
End
Attribute VB_Name = "frmQuadrante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    On Error GoTo TrataErro

     Dim ss As Object
     Dim i As Integer
     Dim itmX As ListItem
     Dim clmX As ColumnHeader


         Screen.MousePointer = vbHourglass
         
         
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2

     frmQuadrante.Caption = "A��es para Cliente " & frmVenda.cmdQuadrante.Caption

         OraParameters.Remove "vErro"
         OraParameters.Add "vErro", 0, 2
         OraParameters.Remove "loja"
         OraParameters.Add "loja", Mid(Trim(deposito_default), 1, 2), 1
         OraParameters.Remove "quadr"
         OraParameters.Add "quadr", frmVenda.cmdQuadrante.Caption, 1
         OraParameters.Remove "tipo"
         OraParameters.Add "tipo", 1, 1



         vSql = "Begin producao.pck_vda020.pr_acao_quadrante(:vCursor,:loja,:quadr,:tipo,:vErro);END;"


         oradatabase.ExecuteSQL vSql
         Set ss = oradatabase.Parameters("vCursor").Value



        If ss.EOF And ss.BOF Then
            Screen.MousePointer = vbDefault
            NonStayOnTop Me
            MessageBox 0, "N�o ha acoes cadastradas", "Aten�ao", &H40000 + 0
            StayOnTop Me
            Unload Me
            Exit Sub
        End If
        
         lv.ColumnHeaders.Clear
         lv.ListItems.Clear
         
         lv.ColumnHeaders.Add , , "A��O", 12500
        For i = 1 To ss.RecordCount
          Set itmX = lv.ListItems.Add()
          itmX.SmallIcon = 1
          itmX.Text = ss!desc_acao
           
           ss.MoveNext
    
         Next i

   
        Screen.MousePointer = vbDefault

     

        Exit Sub

TrataErro:
        If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
            Resume
        ElseIf Err = 30009 Then
            Resume Next
        Else
            MessageBox 0, "Sub Form_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
        End If
End Sub


Private Sub imgVerde_Click()

End Sub

Private Sub Image1_Click()

End Sub
