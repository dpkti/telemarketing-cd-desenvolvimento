VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmPrecoConcorrencia 
   Caption         =   "Pre�o da Concorr�ncia"
   ClientHeight    =   3150
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   4560
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtPrecoConcorrencia 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3280
      MaxLength       =   10
      TabIndex        =   0
      Top             =   1680
      Width           =   1095
   End
   Begin VB.TextBox txtDPK 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3280
      Locked          =   -1  'True
      MaxLength       =   18
      TabIndex        =   5
      Top             =   1380
      Width           =   1095
   End
   Begin VB.TextBox txtCliente 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3280
      Locked          =   -1  'True
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1080
      Width           =   1095
   End
   Begin VB.TextBox txtDeposito 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3280
      Locked          =   -1  'True
      MaxLength       =   2
      TabIndex        =   3
      Top             =   780
      Width           =   1095
   End
   Begin VB.TextBox txtObs 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   30
      MaxLength       =   200
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   2350
      Width           =   4360
   End
   Begin Bot�o.cmd cmdOk 
      Default         =   -1  'True
      Height          =   675
      Left            =   3810
      TabIndex        =   2
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   "&OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrecoConcorrencia.frx":0000
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   6
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrecoConcorrencia.frx":001C
      PICN            =   "frmPrecoConcorrencia.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   0
      TabIndex        =   9
      Top             =   720
      Width           =   4710
      _ExtentX        =   8308
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblPrecoConcorrencia 
      AutoSize        =   -1  'True
      Caption         =   "Pre�o da Concorr�ncia:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   12
      Top             =   1740
      Width           =   2010
   End
   Begin VB.Label lblDPK 
      AutoSize        =   -1  'True
      Caption         =   "DPK:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   11
      Top             =   1440
      Width           =   450
   End
   Begin VB.Label lblCliente 
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   10
      Top             =   1140
      Width           =   660
   End
   Begin VB.Label lblDeposito 
      AutoSize        =   -1  'True
      Caption         =   "Dep�sito:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   8
      Top             =   840
      Width           =   810
   End
   Begin VB.Label lblObs 
      AutoSize        =   -1  'True
      Caption         =   "Observa��es:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   7
      Top             =   2100
      Width           =   1170
   End
End
Attribute VB_Name = "frmPrecoConcorrencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmPrecoConcorrencia
' Author    : c.samuel.oliveira
' Date      : 15/02/2017
' Purpose   : TI-5584
'---------------------------------------------------------------------------------------

Private Sub cmdOk_Click()

  On Error GoTo TrataErro
  
    'verifica pre�o zero
    If Val(Trim(txtPrecoConcorrencia.Text)) = 0 Then
        MessageBox 0, "O Pre�o da Concorr�ncia deve ser preenchido e com valor maior que zero!", "Aten�ao", &H40000 + 0
        txtPrecoConcorrencia.SetFocus
        Exit Sub
    End If

    GravarPrecoConcorrencia CInt(txtDeposito.Text), Val(txtCliente), sCOD_VEND, Val(txtDPK.Text), txtPrecoConcorrencia.Text, Trim(txtObs.Text)
    
    MessageBox 0, "Pre�o da Concorr�ncia gravado com sucesso!", "Aten��o", &H40000 + 0
    
    LimparCampos
    
    Exit Sub

TrataErro:
    MessageBox 0, "Sub cmdOk_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0

End Sub

Private Sub cmdSair_Click()
    LimparCampos
End Sub

Private Sub Form_Activate()

txtDeposito.Text = Left(Trim(frmVenda.cboDeposito), 2)
txtCliente.Text = frmVenda.txtCOD_CLIENTE.Text
txtDPK.Text = frmVenda.txtCOD_DPK.Text

End Sub

Private Sub txtObs_LostFocus()
    txtObs.Text = Replace(txtObs.Text, "'", "")
End Sub

Private Sub txtPrecoConcorrencia_KeyPress(KeyAscii As Integer)
    If Asc(",") = KeyAscii Then
        KeyAscii = Asc(".")
    End If
KeyAscii = Valor(KeyAscii, txtPrecoConcorrencia.Text)
End Sub

Private Sub txtPrecoConcorrencia_LostFocus()
If txtPrecoConcorrencia.Text = "" Then
    txtPrecoConcorrencia.Text = 0
End If
txtPrecoConcorrencia.Text = Format(txtPrecoConcorrencia.Text, "0.00")
End Sub

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
  If KeyAscii = 8 Then    'BACKSPACE
    Valor = KeyAscii
    Exit Function
  End If
    
  If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
    If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
      KeyAscii = 0
      Beep
    End If
  Else
    If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
      KeyAscii = 0
      Beep
    End If
  End If
    
  Valor = KeyAscii
End Function
Private Sub LimparCampos()

txtDeposito.Text = ""
txtCliente.Text = ""
txtDPK.Text = ""
txtPrecoConcorrencia.Text = "0.00"
txtObs.Text = ""
frmPrecoConcorrencia.Hide

End Sub

Function GravarPrecoConcorrencia(pCodLoja As Integer, pCodCliente As Long, pCodVend As Long, pCodDPK As Long, pPrecoConcorrencia As Double, pOBS As String)
    
    OraParameters.Remove "PM_COD_LOJA"
    OraParameters.Add "PM_COD_LOJA", pCodLoja, 1
    OraParameters.Remove "PM_COD_CLIENTE"
    OraParameters.Add "PM_COD_CLIENTE", pCodCliente, 1
    OraParameters.Remove "PM_COD_VEND"
    OraParameters.Add "PM_COD_VEND", pCodVend, 1
    OraParameters.Remove "PM_COD_DPK"
    OraParameters.Add "PM_COD_DPK", pCodDPK, 1
    OraParameters.Remove "PM_PRECO"
    OraParameters.Add "PM_PRECO", pPrecoConcorrencia, 1
    OraParameters.Remove "PM_OBS"
    OraParameters.Add "PM_OBS", pOBS, 1
    
    vSql = "Begin producao.pck_vda230.PR_INS_PRECO_CONCORRENCIA(:PM_COD_LOJA,:PM_COD_CLIENTE,:PM_COD_VEND,:PM_COD_DPK,:PM_PRECO,:PM_OBS);END;"
    
    oradatabase.ExecuteSQL vSql
    
    Exit Function
    
End Function
