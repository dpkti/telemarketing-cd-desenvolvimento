VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmNaoAtend 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Liga��es n�o atendidas no dia"
   ClientHeight    =   4560
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8535
   Icon            =   "frmNaoAtend.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4560
   ScaleWidth      =   8535
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvNaoAtend 
      Height          =   3585
      Left            =   90
      TabIndex        =   2
      Top             =   900
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   6324
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�d Cliente"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome Cliente"
         Object.Width           =   5644
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Data/Hora"
         Object.Width           =   2999
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "DDD"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Telefone"
         Object.Width           =   1940
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   8475
      _ExtentX        =   14949
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmNaoAtend.frx":23D2
      PICN            =   "frmNaoAtend.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmNaoAtend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me
    
End Sub

Private Sub Form_Load()
    Dim vObjNaoAtend As Object
    Dim lItem As ListItem

    oradatabase.Parameters.Remove "PM_VEND"
    'oradatabase.Parameters.Add "PM_VEND", 588, 1
    oradatabase.Parameters.Add "PM_VEND", sCOD_VEND, 1

    vSql = "Begin PRODUCAO.pck_vda230.PR_SELECT_LIG_NAO_ATENDIDAS(:vCursor, :PM_VEND);END;"

    'Criar_Cursor
    oradatabase.ExecuteSQL vSql
    Set vObjNaoAtend = oradatabase.Parameters("vCursor").Value
    'oradatabase.Parameters.Remove ("vCursor")
    
    For i = 1 To vObjNaoAtend.RecordCount
        Set lItem = lsvNaoAtend.ListItems.Add
        lItem = vObjNaoAtend("Cod_Cliente")
        lItem.SubItems(1) = vObjNaoAtend("Nome_Cliente")
        lItem.SubItems(2) = vObjNaoAtend("Dt_Contato")
        lItem.SubItems(3) = vObjNaoAtend("DDD")
        lItem.SubItems(4) = vObjNaoAtend("Telefone")
        vObjNaoAtend.MoveNext
    Next
    Set vObjNaoAtend = Nothing
    Set lItem = Nothing
End Sub


Private Sub lsvNaoAtend_DblClick()
    
    frmVenda.txtCOD_CLIENTE = ""
    frmVenda.txtCOD_CLIENTE = lsvNaoAtend.SelectedItem
    frmVenda.txtCOD_CLIENTE.DataChanged = True
    frmVenda.txtCOD_CLIENTE_LostFocus
    
    Unload Me
End Sub
