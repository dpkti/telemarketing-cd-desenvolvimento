VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmPedidosKdaPeca 
   Caption         =   "Vis�o da Venda KdaPe�a dos Clientes de sua Carteira"
   ClientHeight    =   6720
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   13620
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6720
   ScaleWidth      =   13620
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton optData 
      Appearance      =   0  'Flat
      Caption         =   "Data"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5520
      TabIndex        =   21
      Top             =   840
      Width           =   1695
   End
   Begin VB.OptionButton optValor 
      Appearance      =   0  'Flat
      Caption         =   "Valor"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4440
      TabIndex        =   19
      Top             =   840
      Width           =   975
   End
   Begin VB.OptionButton optPedido 
      Appearance      =   0  'Flat
      Caption         =   "Pedido"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3120
      TabIndex        =   18
      Top             =   840
      Width           =   1215
   End
   Begin VB.OptionButton optCliente 
      Appearance      =   0  'Flat
      Caption         =   "Cliente"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1920
      TabIndex        =   17
      Top             =   840
      Width           =   1215
   End
   Begin VB.Frame ssfraPesq 
      Caption         =   "Pesquisa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   615
      Left            =   0
      TabIndex        =   13
      Top             =   6120
      Width           =   8115
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   180
         TabIndex        =   14
         Top             =   240
         Width           =   3255
      End
      Begin Bot�o.cmd cmdBuscaCliente 
         Height          =   345
         Left            =   3660
         TabIndex        =   15
         ToolTipText     =   "Limpar"
         Top             =   210
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Cliente"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPedidosKdaPeca.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdBuscaPedido 
         Height          =   345
         Left            =   5160
         TabIndex        =   16
         ToolTipText     =   "Pesquisa Descri��o do Item"
         Top             =   210
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Pedido"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPedidosKdaPeca.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimpar 
         Height          =   345
         Left            =   6720
         TabIndex        =   23
         ToolTipText     =   "Limpar Pesquisa"
         Top             =   210
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Limpar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPedidosKdaPeca.frx":0038
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame frmTotal 
      Caption         =   "Carteira KdaPe�a Cliente:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1095
      Left            =   10560
      TabIndex        =   3
      Top             =   0
      Width           =   3015
      Begin VB.Label lblTotalMes 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1710
         TabIndex        =   7
         Top             =   705
         Width           =   1230
      End
      Begin VB.Label lblMes 
         AutoSize        =   -1  'True
         Caption         =   "Totalizador do m�s:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   50
         TabIndex        =   6
         Top             =   735
         Width           =   1665
      End
      Begin VB.Label lblTotalDia 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1710
         TabIndex        =   5
         Top             =   360
         Width           =   1230
      End
      Begin VB.Label lblDia 
         AutoSize        =   -1  'True
         Caption         =   "Totalizador do dia:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   50
         TabIndex        =   4
         Top             =   420
         Width           =   1605
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPedidosKdaPeca.frx":0054
      PICN            =   "frmPedidosKdaPeca.frx":0070
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   1125
      Width           =   14370
      _ExtentX        =   25347
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSGrid.Grid grdPedidosKdapeca 
      Height          =   4815
      Left            =   45
      TabIndex        =   22
      Top             =   1200
      Width           =   13545
      _Version        =   65536
      _ExtentX        =   23892
      _ExtentY        =   8493
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmPedidosKdaPeca.frx":0D4A
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Ordenar:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   840
      TabIndex        =   20
      Top             =   840
      Width           =   750
   End
   Begin VB.Label lblTelefone 
      AutoSize        =   -1  'True
      Caption         =   "19999999999"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3720
      TabIndex        =   12
      Top             =   510
      Width           =   990
   End
   Begin VB.Label lblTel 
      AutoSize        =   -1  'True
      Caption         =   "Telefone:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   2880
      TabIndex        =   11
      Top             =   525
      Width           =   825
   End
   Begin VB.Label lblCNPJ 
      AutoSize        =   -1  'True
      Caption         =   "99999999999999"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1440
      TabIndex        =   10
      Top             =   510
      Width           =   1260
   End
   Begin VB.Label lblCod 
      AutoSize        =   -1  'True
      Caption         =   "CNPJ:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   840
      TabIndex        =   9
      Top             =   525
      Width           =   540
   End
   Begin VB.Label lblCliente 
      AutoSize        =   -1  'True
      Caption         =   "999999 - WWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1560
      TabIndex        =   8
      Top             =   150
      Width           =   5625
   End
   Begin VB.Label lblCli 
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   840
      TabIndex        =   2
      Top             =   165
      Width           =   660
   End
End
Attribute VB_Name = "frmPedidosKdaPeca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmPedidosKdaPeca
' Author    : c.samuel.oliveira
' Date      : 08/12/2017
' Purpose   : KMA-32
'---------------------------------------------------------------------------------------
Option Explicit
Dim vNomCliente As String
Dim vNumPedido As Long

Private Sub cmdSair_Click()
    Unload Me
    Set frmPedidosKdaPeca = Nothing
End Sub

Private Sub Form_Load()
    
    lblCliente.Caption = ""
    lblCNPJ.Caption = ""
    lblTelefone.Caption = ""
    
    CarregarPedidos "0", 0, 0
End Sub

Private Sub CarregarPedidos(pNomCliente As String, pNumPedido As Long, pOrdem As Integer)

    On Error GoTo TrataErro

    Dim ss As Object
    Dim i As Long
    Dim vCliente As String, vPedido As String
    
    Screen.MousePointer = vbHourglass
    
    oradatabase.Parameters.Remove "PM_COD_VEND"
    oradatabase.Parameters.Add "PM_COD_VEND", sCOD_VEND, 1
    oradatabase.Parameters.Remove "PM_NOM_CLIENTE"
    oradatabase.Parameters.Add "PM_NOM_CLIENTE", pNomCliente, 1
    oradatabase.Parameters.Remove "PM_NUM_PEDIDO"
    oradatabase.Parameters.Add "PM_NUM_PEDIDO", pNumPedido, 1
    oradatabase.Parameters.Remove "PM_ORDEM"
    oradatabase.Parameters.Add "PM_ORDEM", pOrdem, 1
    oradatabase.Parameters.Remove "PM_TOTAL_DIA"
    oradatabase.Parameters.Add "PM_TOTAL_DIA", 0, 2
    oradatabase.Parameters.Remove "PM_TOTAL_MES"
    oradatabase.Parameters.Add "PM_TOTAL_MES", 0, 2
    
    vSql = "Begin producao.pck_vda230.PR_SEL_PEDIDOS_KDAPECA(:PM_COD_VEND,:PM_NOM_CLIENTE,:PM_NUM_PEDIDO,:PM_ORDEM,:vCursor,:PM_TOTAL_DIA,:PM_TOTAL_MES);END;"
      
    'Criar_Cursor
    oradatabase.ExecuteSQL vSql
    lblTotalDia.Caption = Format(oradatabase.Parameters("PM_TOTAL_DIA").Value, "#0.00")
    lblTotalMes.Caption = Format(oradatabase.Parameters("PM_TOTAL_MES").Value, "#0.00")
    Set ss = oradatabase.Parameters("vCursor").Value
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        grdPedidosKdapeca.Visible = False
        Exit Sub
    End If
    
    'carrega dados
    With grdPedidosKdapeca
        .Cols = 19
        .rows = ss.RecordCount + 1
        .ColWidth(0) = 2380
        .ColWidth(1) = 930
        .ColWidth(2) = 1000
        .ColWidth(3) = 900
        .ColWidth(4) = 700
        .ColWidth(5) = 950
        .ColWidth(6) = 900
        .ColWidth(7) = 800
        .ColWidth(8) = 800
        .ColWidth(9) = 800
        .ColWidth(10) = 800
        .ColWidth(11) = 2100
        .ColWidth(12) = 1
        .ColWidth(13) = 1
        .ColWidth(14) = 1
        .ColWidth(15) = 1
        .ColWidth(16) = 1
        .ColWidth(17) = 1
        .ColWidth(18) = 1
        
        .Row = 0
        .Col = 0
        .Text = "Cliente"
        .Col = 1
        .Text = "Dt Pedido"
        .Col = 2
        .Text = "Pedido"
        .Col = 3
        .Text = "Situa��o"
        .Col = 4
        .Text = "Nota"
        .Col = 5
        .Text = "Dt Nota"
        .Col = 6
        .Text = "Vl. Pedido"
        .Col = 7
        .Text = "Bloq Cr�d"
        .Col = 8
        .Text = "Bloq Frete"
        .Col = 9
        .Text = "Bloq Com"
        .Col = 10
        .Text = "Bloq OP"
        .Col = 11
        .Text = "Status Pedido"
        
        For i = 1 To .rows - 1
            .Row = i
            
            vCliente = ss!COD_CLIENTE & " - " & ss!NOME_CLIENTE
            vPedido = Right("00" & ss!Cod_Loja, 2) & "-" & ss!Num_Pedido & "-" & ss!Seq_Pedido
  
            .Col = 0
            .Text = IIf(IsNull(vCliente), "", vCliente)
            .Col = 1
            .Text = IIf(IsNull(ss!dt_pedido), "", ss!dt_pedido)
            .Col = 2
            .ColAlignment(2) = 1
            .Text = IIf(IsNull(vPedido), "", vPedido)
            .Col = 3
            .Text = IIf(IsNull(ss!SITUACAO), "", ss!SITUACAO)
            .Col = 4
            .ColAlignment(4) = 1
            .Text = IIf(IsNull(ss!NUM_NOTA), "", ss!NUM_NOTA)
            .Col = 5
            .Text = IIf(IsNull(ss!DT_EMISSAO_NOTA), "", ss!DT_EMISSAO_NOTA)
            .Col = 6
            .ColAlignment(6) = 1
            .Text = Format(IIf(IsNull(ss!VL_CONTABIL), "", ss!VL_CONTABIL), "#0.00")
            .Col = 7
            .Text = IIf(IsNull(ss!BLOQ_CRED), "", ss!BLOQ_CRED)
            .Col = 8
            .Text = IIf(IsNull(ss!bloq_frete), "", ss!bloq_frete)
            .Col = 9
            .Text = IIf(IsNull(ss!BLOQ_COM), "", ss!BLOQ_COM)
            .Col = 10
            .Text = IIf(IsNull(ss!BLOQ_OP), "", ss!BLOQ_OP)
            .Col = 11
            .Text = IIf(IsNull(ss!status), "", ss!status)
            
            .Col = 12
            .Text = IIf(IsNull(ss!Cod_Loja), "", ss!Cod_Loja)
            .Col = 13
            .Text = IIf(IsNull(ss!Num_Pedido), "", ss!Num_Pedido)
            .Col = 14
            .Text = IIf(IsNull(ss!Seq_Pedido), "", ss!Seq_Pedido)
            .Col = 15
            .Text = IIf(IsNull(ss!ddd1), "", ss!ddd1) & "-" & IIf(IsNull(ss!fone1), "", ss!fone1)
            .Col = 16
            .Text = IIf(IsNull(ss!CGC), "", ss!CGC)
            .Col = 17
            .Text = IIf(IsNull(ss!plano), "", ss!plano)
            .Col = 18
            .Text = IIf(IsNull(ss!COD_CLIENTE), "", ss!COD_CLIENTE)
            
            ss.MoveNext
        Next
        .Row = 1
    End With

    Screen.MousePointer = vbDefault

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    Else
        MessageBox 0, "Sub CarregarPedidos" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If

    Screen.MousePointer = vbDefault
    
End Sub

Private Sub grdPedidosKdapeca_Click()
    
    grdPedidosKdapeca.Col = 0
    lblCliente.Caption = grdPedidosKdapeca.Text
    grdPedidosKdapeca.Col = 16
    lblCNPJ.Caption = grdPedidosKdapeca.Text
    grdPedidosKdapeca.Col = 15
    lblTelefone.Caption = grdPedidosKdapeca.Text

End Sub

Private Sub cmdBuscaCliente_Click()

    vNomCliente = Trim(UCase(Text1.Text))
    
    CarregarPedidos vNomCliente, 0, 0

End Sub

Private Sub cmdBuscaPedido_Click()

    vNumPedido = Trim(UCase(Text1.Text))
    
    CarregarPedidos "0", vNumPedido, 0
    
End Sub

Private Sub grdPedidosKdapeca_DblClick()
Dim vDtPedido As String, vCodCli As Long

    grdPedidosKdapeca.Col = 12
    vCodLojaKdp = Val(grdPedidosKdapeca.Text)
    grdPedidosKdapeca.Col = 13
    vNumPedidoKdp = Val(grdPedidosKdapeca.Text)
    grdPedidosKdapeca.Col = 14
    vSeqPedidoKdp = Val(grdPedidosKdapeca.Text)
    grdPedidosKdapeca.Col = 17
    vDescPlanoKdp = grdPedidosKdapeca.Text
    grdPedidosKdapeca.Col = 1
    vDtPedido = grdPedidosKdapeca.Text
    grdPedidosKdapeca.Col = 18
    vCodCli = Val(grdPedidosKdapeca.Text)
    
    GravarLogKdaPeca vCodLojaKdp, vNumPedidoKdp, vSeqPedidoKdp, sCOD_VEND, vDtPedido, vCodCli, 2
    
    frmPedKdPDetalhe.Show
    StayOnTop frmPedKdPDetalhe

End Sub

Private Sub optCliente_Click()
    CarregarPedidos vNomCliente, vNumPedido, 1
End Sub

Private Sub optData_Click()
    CarregarPedidos vNomCliente, vNumPedido, 2
End Sub

Private Sub optPedido_Click()
    CarregarPedidos vNomCliente, vNumPedido, 3
End Sub

Private Sub optValor_Click()
    CarregarPedidos vNomCliente, vNumPedido, 4
End Sub
Private Sub cmdLimpar_Click()
Text1.Text = ""
vNomCliente = "0"
vNumPedido = 0
CarregarPedidos "0", 0, 0

End Sub
