VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEstatisticaTlmk 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Estat�sticas - Telemarketing"
   ClientHeight    =   6900
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11640
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   11640
   StartUpPosition =   3  'Windows Default
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   10860
      TabIndex        =   49
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   6090
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLogWeb.frx":0000
      PICN            =   "frmLogWeb.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6885
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   12144
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "LOG"
      TabPicture(0)   =   "frmLogWeb.frx":0CF6
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lsvLog"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame4"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "FraPesquisar"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Estat�sticas"
      TabPicture(1)   =   "frmLogWeb.frx":0D12
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label7"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cmdAtualizarGrafico"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame1"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Grafico"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "cmdImprimir"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "lstTotais"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "fraPeriodoGrafico"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cboAtendente"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).ControlCount=   8
      Begin VB.ComboBox cboAtendente 
         Height          =   315
         Left            =   90
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   1890
         Width           =   2265
      End
      Begin VB.Frame FraPesquisar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   -74910
         TabIndex        =   40
         Top             =   390
         Width           =   11475
         Begin VB.ComboBox cboSituacao 
            Height          =   315
            ItemData        =   "frmLogWeb.frx":0D2E
            Left            =   60
            List            =   "frmLogWeb.frx":0D30
            TabIndex        =   2
            Top             =   840
            Width           =   1725
         End
         Begin VB.Frame fraPeriodo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            Caption         =   "Per�odo"
            ForeColor       =   &H80000008&
            Height          =   765
            Left            =   2100
            TabIndex        =   44
            Top             =   120
            Width           =   2265
            Begin VB.TextBox txtDe 
               Height          =   315
               Left            =   90
               TabIndex        =   3
               Top             =   390
               Width           =   1005
            End
            Begin VB.TextBox txtAte 
               Height          =   315
               Left            =   1170
               TabIndex        =   4
               Top             =   390
               Width           =   1005
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "De"
               Height          =   195
               Left            =   90
               TabIndex        =   46
               Top             =   210
               Width           =   210
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "At�"
               Height          =   195
               Left            =   1170
               TabIndex        =   45
               Top             =   210
               Width           =   240
            End
         End
         Begin VB.Frame fraUsuarios 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            Caption         =   "Usu�rio"
            ForeColor       =   &H80000008&
            Height          =   765
            Left            =   4800
            TabIndex        =   41
            Top             =   120
            Width           =   5835
            Begin VB.ComboBox cboUsuarioWeb 
               Height          =   315
               Left            =   90
               Style           =   2  'Dropdown List
               TabIndex        =   5
               Top             =   390
               Width           =   2835
            End
            Begin VB.ComboBox cboUsuarioCredito 
               Height          =   315
               Left            =   2970
               Style           =   2  'Dropdown List
               TabIndex        =   6
               Top             =   390
               Width           =   2805
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Web"
               Height          =   195
               Left            =   90
               TabIndex        =   43
               Top             =   180
               Width           =   345
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Cr�dito"
               Height          =   195
               Left            =   2970
               TabIndex        =   42
               Top             =   180
               Width           =   495
            End
         End
         Begin VB.ComboBox cboTipo 
            Height          =   315
            ItemData        =   "frmLogWeb.frx":0D32
            Left            =   60
            List            =   "frmLogWeb.frx":0D42
            TabIndex        =   1
            Top             =   330
            Width           =   1725
         End
         Begin Bot�o.cmd cmdConsultar 
            Height          =   690
            Left            =   10740
            TabIndex        =   7
            TabStop         =   0   'False
            ToolTipText     =   "Consultar"
            Top             =   180
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   1217
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmLogWeb.frx":0D81
            PICN            =   "frmLogWeb.frx":0D9D
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Situa��o"
            Height          =   195
            Left            =   60
            TabIndex        =   48
            Top             =   630
            Width           =   630
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Tipo Solicita��o:"
            Height          =   195
            Left            =   60
            TabIndex        =   47
            Top             =   120
            Width           =   1185
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Total dos Itens Listados"
         Height          =   585
         Left            =   -74850
         TabIndex        =   37
         Top             =   5730
         Width           =   2265
         Begin VB.TextBox txtTotalnoPeriodo 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1320
            TabIndex        =   38
            Top             =   240
            Width           =   885
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Total no Per�odo:"
            Height          =   195
            Left            =   60
            TabIndex        =   39
            Top             =   270
            Width           =   1245
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Total por Situa��o"
         Height          =   855
         Left            =   -70140
         TabIndex        =   28
         Top             =   5730
         Width           =   4035
         Begin VB.TextBox txtPendente 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1260
            TabIndex        =   32
            Top             =   270
            Width           =   885
         End
         Begin VB.TextBox txtEmAnalise 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1260
            TabIndex        =   31
            Top             =   540
            Width           =   885
         End
         Begin VB.TextBox txtAceito 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3090
            TabIndex        =   30
            Top             =   240
            Width           =   885
         End
         Begin VB.TextBox txtRecusado 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   3090
            TabIndex        =   29
            Top             =   510
            Width           =   885
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "N�o Analisados:"
            Height          =   195
            Left            =   60
            TabIndex        =   36
            Top             =   300
            Width           =   1155
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "Em An�lise:"
            Height          =   195
            Left            =   60
            TabIndex        =   35
            Top             =   570
            Width           =   825
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Aceitos:"
            Height          =   195
            Left            =   2250
            TabIndex        =   34
            Top             =   300
            Width           =   570
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            Caption         =   "Recusados:"
            Height          =   195
            Left            =   2220
            TabIndex        =   33
            Top             =   570
            Width           =   855
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Total Por Tipo de Solicita��o"
         Height          =   1065
         Left            =   -72540
         TabIndex        =   21
         Top             =   5730
         Width           =   2295
         Begin VB.TextBox txtPreCadastro 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1230
            TabIndex        =   24
            Top             =   210
            Width           =   885
         End
         Begin VB.TextBox txtPreAlteracao 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1230
            TabIndex        =   23
            Top             =   480
            Width           =   885
         End
         Begin VB.TextBox txtPreRecadastro 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1230
            TabIndex        =   22
            Top             =   750
            Width           =   885
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Pr� Cadastro:"
            Height          =   195
            Left            =   60
            TabIndex        =   27
            Top             =   270
            Width           =   960
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Pr� Altera��o:"
            Height          =   195
            Left            =   60
            TabIndex        =   26
            Top             =   540
            Width           =   1005
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Pr� Recadastro:"
            Height          =   195
            Left            =   60
            TabIndex        =   25
            Top             =   810
            Width           =   1155
         End
      End
      Begin VB.Frame fraPeriodoGrafico 
         Caption         =   "Per�odo"
         Height          =   735
         Left            =   60
         TabIndex        =   15
         Top             =   2520
         Width           =   2355
         Begin VB.TextBox txtDeLst 
            Height          =   315
            Left            =   60
            TabIndex        =   16
            Top             =   360
            Width           =   1005
         End
         Begin VB.TextBox txtAteLst 
            Height          =   315
            Left            =   1110
            TabIndex        =   18
            Top             =   360
            Width           =   1155
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "De"
            Height          =   195
            Left            =   60
            TabIndex        =   20
            Top             =   180
            Width           =   210
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "At�"
            Height          =   195
            Left            =   1110
            TabIndex        =   17
            Top             =   180
            Width           =   240
         End
      End
      Begin VB.ListBox lstTotais 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1110
         ItemData        =   "frmLogWeb.frx":1A77
         Left            =   60
         List            =   "frmLogWeb.frx":1A81
         TabIndex        =   11
         Top             =   600
         Width           =   2385
      End
      Begin Bot�o.cmd cmdImprimir 
         Height          =   690
         Left            =   960
         TabIndex        =   10
         ToolTipText     =   "Imprimir Gr�fico"
         Top             =   3270
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmLogWeb.frx":1ABC
         PICN            =   "frmLogWeb.frx":1AD8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin GraphLib.Graph Grafico 
         Height          =   5685
         Left            =   2460
         TabIndex        =   9
         Top             =   360
         Width           =   9105
         _Version        =   65536
         _ExtentX        =   16060
         _ExtentY        =   10028
         _StockProps     =   96
         BorderStyle     =   1
         GraphTitle      =   "Telemarketing"
         GraphType       =   4
         IndexStyle      =   1
         LineStats       =   8
         NumPoints       =   4
         Palette         =   1
         PrintStyle      =   1
         RandomData      =   0
         ColorData       =   0
         ExtraData       =   0
         ExtraData[]     =   0
         FontFamily      =   4
         FontSize        =   4
         FontSize[0]     =   200
         FontSize[1]     =   150
         FontSize[2]     =   100
         FontSize[3]     =   100
         FontStyle       =   4
         GraphData       =   1
         GraphData[]     =   4
         GraphData[0,0]  =   0
         GraphData[0,1]  =   0
         GraphData[0,2]  =   0
         GraphData[0,3]  =   0
         LabelText       =   0
         LegendText      =   0
         PatternData     =   0
         SymbolData      =   0
         XPosData        =   0
         XPosData[]      =   0
      End
      Begin MSComctlLib.ListView lsvLog 
         Height          =   4065
         Left            =   -74940
         TabIndex        =   8
         Top             =   1620
         Width           =   11475
         _ExtentX        =   20241
         _ExtentY        =   7170
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FlatScrollBar   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "CGC"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nome Cliente"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tipo Solicita��o"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Data Solicita��o"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Situa��o"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Data An�lise"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Usu�rio Web"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Usu�rio Cr�dito"
            Object.Width           =   4410
         EndProperty
      End
      Begin VB.Frame Frame1 
         Caption         =   "Total Geral"
         Height          =   675
         Left            =   2490
         TabIndex        =   13
         Top             =   570
         Width           =   1365
         Begin VB.TextBox txtTotalGeral 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   120
            TabIndex        =   14
            Top             =   240
            Width           =   1155
         End
      End
      Begin Bot�o.cmd cmdAtualizarGrafico 
         Height          =   690
         Left            =   1710
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Consultar"
         Top             =   3270
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmLogWeb.frx":27B2
         PICN            =   "frmLogWeb.frx":27CE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Escolha um item da lista:"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   1740
      End
   End
End
Attribute VB_Name = "frmEstatisticaTlmk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim UsuarioWeb(500, 1) As Integer
'Dim UsuarioCre(500, 1) As Integer

Private Sub cboSituacao_Click()
    If cboSituacao = "Pendente" Then
        txtDe = ""
        txtAte = ""
        fraPeriodo.Enabled = False
    Else
        fraPeriodo.Enabled = True
    End If
End Sub

Private Sub cboUsuarioCredito_Click()
    If cboUsuarioCredito = "" Then Exit Sub
    cboUsuarioWeb.ListIndex = -1
End Sub

Private Sub cboUsuarioWeb_Click()
    If cboUsuarioWeb = "" Then Exit Sub
    cboUsuarioCredito.ListIndex = -1
End Sub

'Private Sub chkUsuarioCre_Click()
'    For i = 0 To UBound(UsuarioCre)
'        If UsuarioCre(i, 0) = 0 Then Exit For
'    Next
'    Grafico.NumPoints = i + 1
'
'    Grafico.AutoInc = 1
'
'    For i = 0 To UBound(UsuarioCre)
'        If UsuarioCre(i, 0) = 0 Then Exit For
'        Grafico.GraphData = UsuarioCre(i, 1)
'    Next
'    Grafico.DrawMode = gphDraw
'End Sub

'Private Sub chkUsuarioWeb_Click()
'    For i = 0 To UBound(UsuarioWeb)
'        If UsuarioWeb(i, 0) = 0 Then Exit For
'    Next
'    Grafico.NumPoints = i + 1
'
'    Grafico.AutoInc = 1
'
'    For i = 0 To UBound(UsuarioWeb)
'        If UsuarioWeb(i, 0) = 0 Then Exit For
'        Grafico.GraphData = UsuarioWeb(i, 1)
'    Next
'    Grafico.DrawMode = gphDraw
'
'End Sub

Private Sub cmdAtualizarGrafico_Click()
    Preenche_Grafico
End Sub

Private Sub cmdConsultar_Click()
    Screen.MousePointer = vbHourglass
    Me.Refresh
    
    Set OraParameters = db_INT.Parameters
    
    If cboTipo = "" Then
        MsgBox "Campo Tipo de Solicita��o � Obrigat�rio.", vbInformation, "Aten��o"
        cboTipo.SetFocus
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    OraParameters.Remove "pm_Tipo": OraParameters.Add "pm_Tipo", Left(cboTipo, 1), 1
    
    If cboSituacao = "" Then
       OraParameters.Remove "pm_Situacao": OraParameters.Add "pm_Situacao", 99, 1
    Else
       OraParameters.Remove "pm_Situacao": OraParameters.Add "pm_Situacao", cboSituacao.ItemData(cboSituacao.ListIndex), 1
    End If
    
    If cboUsuarioWeb = "" Then
        OraParameters.Remove "pm_CodUsuarioWeb": OraParameters.Add "pm_CodUsuarioWeb", 0, 1
    Else
        OraParameters.Remove "pm_CodUsuarioWeb": OraParameters.Add "pm_CodUsuarioWeb", cboUsuarioWeb.ItemData(cboUsuarioWeb.ListIndex), 1
    End If
    
    If cboUsuarioCredito = "" Then
       OraParameters.Remove "pm_CodUsuarioCre": OraParameters.Add "pm_CodUsuarioCre", 0, 1
    Else
       OraParameters.Remove "pm_CodUsuarioCre": OraParameters.Add "pm_CodUsuarioCre", cboUsuarioCredito.ItemData(cboUsuarioCredito.ListIndex), 1
    End If
    
    OraParameters.Remove "pm_DTINI": OraParameters.Add "pm_DTINI", Replace(txtDe, "/", "-"), 1
    OraParameters.Remove "pm_DTFIM": OraParameters.Add "pm_DTFIM", Replace(txtAte, "/", "-"), 1
    OraParameters.Remove "pm_CodErro": OraParameters.Add "pm_CodErro", "", 2
    OraParameters.Remove "pm_TxtErro": OraParameters.Add "pm_TxtErro", "", 2

    Criar_Cursor OraParameters, "PM_CURSOR1"

    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_Select_Log(:PM_CURSOR1, :pm_Tipo, :pm_Situacao, :pm_CodUsuarioWeb, :pm_CodUsuarioCre," & _
    ":pm_DTINI, :pm_DTFIM, :pm_CodErro, :pm_TxtErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    
    Preenche_lsvLog OraParameters("PM_CURSOR1").Value
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Atualizar_Grafico(Colunas As Byte, Val1 As Double, val2 As Double, val3 As Double, Optional val4 As Double)
    
    Grafico.AutoInc = 1
    
    Grafico.NumPoints = Colunas
    
    Grafico.GraphData = Val1
    Grafico.GraphData = val2
    Grafico.GraphData = val3
    
    If Colunas = 4 Then
        Grafico.GraphData = val4
    End If
    
    Grafico.DrawMode = gphDraw
    
End Sub

Private Sub cmdImprimir_Click()
    Grafico.DrawMode = gphPrint
End Sub

Private Sub DtAte_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'txtAte = Format(DtAte.Value, "DD/MM/YY")
End Sub

Private Sub DtDe_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'txtDe = Format(DtDe.Value, "DD/MM/YY")
End Sub

Private Sub Form_Load()
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False
    
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    
    Preenche_Combos_Usuarios

    SSTab1.Tab = 0
End Sub

Sub Preenche_Combos_Usuarios()
    Dim rst2 As Object
    
    Criar_Cursor db_INT.Parameters, "PM_CURSOR1"
    Criar_Cursor db_INT.Parameters, "PM_CURSOR2"
    
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.Pr_Select_Usuarios(:PM_CURSOR1, :PM_CURSOR2)")
    If vErro = "" Then
        Set rst = db_INT.Parameters("PM_CURSOR1").Value
        Set rst2 = db_INT.Parameters("PM_CURSOR2").Value
    Else
        Exit Sub
    End If
    cboUsuarioCredito.Clear
    cboUsuarioWeb.Clear
    
    cboUsuarioWeb.AddItem ""
    cboUsuarioWeb.ItemData(cboUsuarioWeb.NewIndex) = 0
    
    For i = 1 To rst.RecordCount
        cboUsuarioWeb.AddItem rst("NOME_USUARIO")
        cboUsuarioWeb.ItemData(cboUsuarioWeb.NewIndex) = rst("Cod_Usuario")
        rst.MoveNext
    Next
    
    cboUsuarioCredito.AddItem ""
    cboUsuarioCredito.ItemData(cboUsuarioCredito.NewIndex) = 0
        
    For i = 1 To rst2.RecordCount
        cboUsuarioCredito.AddItem rst2("NOME_USUARIO")
        cboUsuarioCredito.ItemData(cboUsuarioCredito.NewIndex) = rst2("COD_USUARIO_CREDITO")
        rst2.MoveNext
    Next
    
    Set rst = Nothing
    Set rst2 = Nothing
    
    cboSituacao.AddItem ""
    cboSituacao.ItemData(cboSituacao.NewIndex) = 0
    cboSituacao.AddItem "Pendente"
    cboSituacao.ItemData(cboSituacao.NewIndex) = 1
    cboSituacao.AddItem "Em An�lise"
    cboSituacao.ItemData(cboSituacao.NewIndex) = 2
    cboSituacao.AddItem "Aceito"
    cboSituacao.ItemData(cboSituacao.NewIndex) = 3
    cboSituacao.AddItem "Recusado"
    cboSituacao.ItemData(cboSituacao.NewIndex) = 4
End Sub

Sub Preenche_lsvLog(rst As Object)
    Dim TotalPeriodo As Double
    Dim TotalUsuarioCred As Double
    Dim TotalUsuarioWeb As Double
    Dim TotalPendente As Double
    Dim TotalEmAnalise As Double
    Dim TotalRecusado As Double
    Dim TotalAceito As Double
    Dim TotalPreCadastro As Double
    Dim TotalPreAlteracao As Double
    Dim TotalPreRecadastro As Double
    Dim litem As ListItem
    Dim i As Long
    
    lsvLog.ListItems.Clear
    
    If rst.RecordCount = 0 Then
        MsgBox "Nenhum registro encontrado.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    For i = 1 To rst.RecordCount
        Set litem = Me.lsvLog.ListItems.Add
        litem = rst("CGC")
        litem.SubItems(1) = rst("Nome_CLiente")
        litem.SubItems(2) = rst("Tp_Solicitacao")
        litem.SubItems(3) = Format(rst("Dt_solicitacao"), "DD/MM/YY")
        litem.SubItems(4) = rst("Situacao")
        litem.SubItems(5) = IIf(IsNull(rst("Dt_Analise")), "", Format(rst("Dt_Analise"), "DD/MM/YY"))
        litem.SubItems(6) = rst("Nome_usuario_Web") & " - " & rst("Cod_Usuario_web")
        litem.SubItems(7) = rst("Nome_usuario") & " - " & rst("Cod_Usuario_Credito")
        
'        Somar_usuario_web rst("Cod_usuario_web")
'        If Not IsNull(rst("Cod_usuario_Credito")) Then
'            Somar_usuario_cre rst("Cod_usuario_Credito")
'        End If
        
        TotalPendente = TotalPendente + IIf(rst("Situacao") = "Pendente", 1, 0)
        TotalEmAnalise = TotalEmAnalise + IIf(rst("Situacao") = "Em An�lise", 1, 0)
        TotalRecusado = TotalRecusado + IIf(rst("Situacao") = "Recusado", 1, 0)
        TotalAceito = TotalAceito + IIf(rst("Situacao") = "Aceito", 1, 0)
        
        TotalPreCadastro = TotalPreCadastro + IIf(rst("Tp_Solicitacao") = "Pr�-Cadastro", 1, 0)
        TotalPreAlteracao = TotalPreAlteracao + IIf(rst("Tp_Solicitacao") = "Pr�-Altera��o", 1, 0)
        TotalPreRecadastro = TotalPreRecadastro + IIf(rst("Tp_Solicitacao") = "Pr�-Recadastro", 1, 0)
        
        rst.MoveNext
    Next
    
    TotalPeriodo = lsvLog.ListItems.Count
    
    Set rst = Nothing
    
    If Not IsNumeric(TotalPeriodo) Then
        txtTotalnoPeriodo = 0
    Else
        txtTotalnoPeriodo = TotalPeriodo
    End If
    
    txtPendente = TotalPendente
    txtEmAnalise = TotalEmAnalise
    txtRecusado = TotalRecusado
    txtAceito = TotalAceito
        
    txtPreCadastro = TotalPreCadastro
    txtPreAlteracao = TotalPreAlteracao
    txtPreRecadastro = TotalPreRecadastro
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    mdiCAD060.SSPanel1.Visible = True
End Sub

Private Sub lstTotais_Click()
    Grafico.DataReset = gphAllData
    Grafico.DataReset = gphLabelText
    Grafico.GraphTitle = lstTotais
    
    Grafico.DrawMode = gphDraw
    
    txtDeLst = ""
    txtAteLst = ""
    cmdAtualizarGrafico.Enabled = False
    
    Preenche_Grafico
End Sub

Private Sub lsvLog_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    lsvLog.Sorted = True
    lsvLog.SortKey = ColumnHeader.Index - 1
    lsvLog.SortOrder = lvwAscending
End Sub

'Sub Somar_usuario_cre(CodUsr As Double)
'    For i = 0 To 500
'        If UsuarioCre(i, 0) = 0 Then Exit For
'        If UsuarioCre(i, 0) = CodUsr Then
'            UsuarioCre(i, 1) = UsuarioCre(i, 1) + 1
'            Exit Sub
'        End If
'    Next
'    UsuarioCre(i, 0) = CodUsr
'    UsuarioCre(i, 1) = 1
'End Sub

Sub Preenche_Grafico()
    Dim Contador As Integer
    
    db_INT.Parameters.Remove "Opcao"
    db_INT.Parameters.Add "Opcao", lstTotais.ListIndex + 1, 1
    If txtDeLst = "" Then
        db_INT.Parameters.Remove "pm_DTINI": db_INT.Parameters.Add "pm_DTINI", "", 1
        db_INT.Parameters.Remove "pm_DTFIM": db_INT.Parameters.Add "pm_DTFIM", "", 1
    Else
        db_INT.Parameters.Remove "pm_DTINI": db_INT.Parameters.Add "pm_DTINI", Replace(txtDeLst, "/", "-"), 1
        db_INT.Parameters.Remove "pm_DTFIM": db_INT.Parameters.Add "pm_DTFIM", Replace(txtAteLst, "/", "-"), 1
    End If

    Criar_Cursor
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA230.Pr_Estatisticas(:vCursor, :Opcao,:pm_DTINI, :pm_DTFIM, :CodTlmk)")
    
    If vErro = "" Then
        Set rst = db_INT("vCursor").Value
    Else
        Exit Sub
    End If
    
    cmdImprimir.Enabled = True
    
    If rst.RecordCount <= 1 Then
        Grafico.NumPoints = 2
    Else
        Grafico.NumPoints = rst.RecordCount
    End If
    
    Grafico.AutoInc = 0
    Grafico.ThisPoint = 1
    Grafico.IndexStyle = gphStandard
    Grafico.ThisPoint = 1
    Grafico.DataReset = gphAllData
    Grafico.DataReset = gphLabelText
    
    Grafico.FontUse = gphLabels
    Grafico.FontFamily = gphSwiss
    Grafico.FontSize = 120

    Grafico.FontUse = gphLegend
    Grafico.FontFamily = gphSwiss
    Grafico.FontSize = 90
    
    While rst.EOF = False
        Grafico.GraphData = rst.Fields(0)
        Grafico.LegendText = CStr(rst.Fields(1).Value)
        Grafico.LabelText = "          " & rst.Fields(0)
        rst.MoveNext
        If Not rst.EOF Then
            Grafico.ThisPoint = Grafico.ThisPoint + 1
        End If
    Wend
    Grafico.DrawMode = gphDraw
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    If SSTab1.Tab = 1 Then
        FraPesquisar.Enabled = False
    Else
        FraPesquisar.Enabled = True
    End If
End Sub

Private Sub txtAte_KeyPress(KeyAscii As Integer)
   vVB_Generica_001.Data KeyAscii, txtAte
End Sub

Private Sub txtAteLst_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtAteLst
End Sub

Private Sub txtDe_KeyPress(KeyAscii As Integer)
   vVB_Generica_001.Data KeyAscii, txtDe
End Sub

Private Sub txtDeLst_KeyPress(KeyAscii As Integer)
   vVB_Generica_001.Data KeyAscii, txtDeLst
End Sub
