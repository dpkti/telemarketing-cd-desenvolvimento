Attribute VB_Name = "modCalculos"
'---------------------------------------------------------------------------------------
' Module    : modCalculos
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 25/10/16
' Purpose   : TI-5398
'---------------------------------------------------------------------------------------


Option Explicit

'ALEXSANDRO DE MACEDO - 12/11/2011

Const intDif_ICMS As Integer = 0
Const intDesc_Suframa As Integer = 1

Const intDesconto_Adicional = 0
Const intAcrescimo_Financeiro = 1

Public Function Calcular_Descontos(ByVal strCod_Deposito As String, _
                                   ByVal strCod_Filial As String, _
                                   ByVal strCod_DPK As String, _
                                   ByVal strCod_Fornecedor As String, _
                                   ByVal strCod_Fabrica As String, _
                                   ByVal strUf_Origem As String, _
                                   ByVal strUf_Destino As String, _
                                   ByVal strCodigo_Cliente As String, _
                                   ByVal strCNPJ As String, _
                                   ByVal strCod_Plano As String, _
                                   ByVal dblPc_Acrescimo As Double, _
                                   ByVal dblPc_Desconto As Double, _
                                   ByVal strTipo_Menor_Preco As String, _
                                   Optional ByVal dblPc_Desc1 As Double, _
                                   Optional ByVal dblPc_Desc2 As Double, _
                                   Optional ByRef dblPc_DescUF As Double, _
                                   Optional ByRef dblPc_Desc3 As Double, _
                                   Optional ByRef dblPc_DescICM As Double) As Double

    Dim rstCliente As Object
    Dim rstItem As Object
    Dim strTipo_Cliente As String
    Dim strTipo_Empresa As String
    'Dim strCNPJ As String
    Dim strClassificacao_Fiscal As String
    Dim strCaracteristica As String
    Dim strCod_Trib_Item As String
    Dim strIE As String
    Dim strIS As String
    Dim strGrupo As String
    Dim strSubGrupo As String

    '
    Dim intCategoria As Integer

    '
    Dim lngTare As Long

    '
    Dim dblRetorno As Double
    Dim dblPERC_DESC_UF As Double
    Dim dblDESCONTO_ADICIONAL As Double
    Dim arrDblICMS_SUFRAMA() As Double
    Dim arrDblDESC_ADIC_DPK() As Double
    Dim dblDIF_ICMS As Double
    Dim dblDESCONTO_SUFRAMA As Double
    Dim dblACRESCIMO_FINANCEIRO_DPK As Double
    Dim dblDESCONTO_FINANCEIRO_DPK As Double
    Dim dblDESCONTO_PERIODO As Double
    Dim dblPreco_Venda_Ant As Double
    Dim dblPreco_Venda As Double
    Dim dblPreco_OF_Ant As Double
    Dim dblPreco_OF As Double
    Dim dblPreco_SP_Ant As Double
    Dim dblPreco_SP As Double

    '
    strTipo_Empresa = "M"

    If strCodigo_Cliente <> "" And strCNPJ <> "" Then

        'DADOS DO CLIENTE------------------------------------------------------------------------------------------
        OraParameters.Remove "cod"
        OraParameters.Remove "cgc"
        OraParameters.Remove "vErro"

        If strCodigo_Cliente <> "0" Then
            OraParameters.Add "cod", strCodigo_Cliente, 1
            OraParameters.Add "cgc", 0, 1
        Else
            OraParameters.Add "cod", 0, 1
            OraParameters.Add "cgc", strCNPJ, 1
        End If
        OraParameters.Add "vErro", 0, 2

        oradatabase.ExecuteSQL "Begin producao.pck_vda320.pr_cliente(:vCursor, :cod, :cgc, :vErro); END;"

        Set rstCliente = oradatabase.Parameters("vCursor").Value

        If oradatabase.Parameters("vErro").Value <> 0 Then
            Call MsgBox("Erro ao buscar informa��es do cliente!", vbCritical Or vbOKOnly, "ATEN��O!")
            End
        End If

        'Define tipo do cliente...
        If rstCliente!FL_CONS_FINAL = "S" Then
            strTipo_Cliente = "CONS. FINAL"
        Else
            If Trim(rstCliente!INSCR_ESTADUAL) = "ISENTO" Then
                strTipo_Cliente = "ISENTO"
            Else
                strTipo_Cliente = "REVENDEDOR"
            End If
        End If

        'Define tipo da empresa...
        strTipo_Empresa = IIf(IsNull(Trim(rstCliente!tp_empresa)), "M", Trim(rstCliente!tp_empresa))

        'Inscri��o Estadual...
        strIE = Trim$(rstCliente!INSCR_ESTADUAL)

        'Inscri��o Suframa...
        strIS = Trim$(rstCliente!INSCR_SUFRAMA)

        'Codigo TARE... Copiei essa merda abaixo: eu nunca ia declarar uma variavel LONG e passar um valor STRING...
        lngTare = CStr(rstCliente!COD_TARE)

        'CNPJ... j� vem de parametro...
        'strCNPJ = rstCliente!CGC

        'Classifica��o cliente...
        'strClassificacao_Fiscal = rstCliente!CLASSIFICACAO

        'Caracteristica do cliente...
        strCaracteristica = rstCliente!caracteristica

        rstCliente.Close

        Set rstCliente = Nothing
        '----------------------------------------------------------------------------------------------------------
        
    End If

    'DADOS ITEM------------------------------------------------------------------------------------------------
    OraParameters.Remove "loja"
    OraParameters.Remove "dpk"
    OraParameters.Remove "forn"
    OraParameters.Remove "fabrica"
    OraParameters.Remove "owner"
    OraParameters.Remove "vErro"
    
    

    OraParameters.Add "loja", strCod_Deposito, 1
    OraParameters.Add "dpk", strCod_DPK, 1
    OraParameters.Add "forn", strCod_Fornecedor, 1
    OraParameters.Add "fabrica", strCod_Fabrica, 1
    
    'Felipe Cassiani - TI-1395 - Inclus�o de parametro Owner
    If strTp_banco = "U" Then
       vOwner = "HELPDESK."
       OraParameters.Add "owner", vOwner, 1
    Else
       vOwner = "DEP" & Format(lngCD, "00") & "."
       OraParameters.Add "owner", vOwner, 1
    End If
    
    OraParameters.Add "vErro", 0, 2

    oradatabase.ExecuteSQL "Begin producao.pck_vda230.pr_getitem(:vCursor, :loja, :dpk, :forn, :fabrica, :owner, :vErro); END;"

    Set rstItem = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call MsgBox("Erro ao buscar informa��es do Item!", vbCritical Or vbOKOnly, "ATEN��O!")
        End
    End If

    'Codigo de tributa��o...
    strCod_Trib_Item = CStr(rstItem!cod_tributacao)

    'Grupo do item...
    strGrupo = rstItem!cod_grupo

    'Sub Grupo do item...
    strSubGrupo = rstItem!cod_subgrupo

    'Categoria do item...
    intCategoria = rstItem!categoria

    'Classifica��o fiscal item...
    strClassificacao_Fiscal = rstItem!class_fiscal

    '
    dblPreco_Venda_Ant = rstItem!Preco_Venda_Ant

    '
    dblPreco_Venda = rstItem!Preco_Venda

    '
    dblPreco_OF_Ant = rstItem!Preco_Of_Ant

    '
    dblPreco_OF = rstItem!Preco_Of

    '
    dblPreco_SP_Ant = rstItem!Preco_Sp_Ant

    '
    dblPreco_SP = rstItem!Preco_Sp
    '----------------------------------------------------------------------------------------------------------

    'PERCENTUAL DE DESCONTO UF:
    dblPERC_DESC_UF = Retorna_Desconto_UF(strCod_DPK, strUf_Origem, strUf_Destino, strTipo_Cliente)


    'BUSCA DIFEREN�A DE ICMS e DESCONTO DE SUFRAMA:
    ReDim arrDblICMS_SUFRAMA(1)

    arrDblICMS_SUFRAMA = Retorna_Dif_ICMS_Desc_Suframa(strClassificacao_Fiscal, _
                                                       strUf_Origem, _
                                                       strUf_Destino, _
                                                       strCod_Fornecedor, _
                                                       strCod_Trib_Item, _
                                                       strTipo_Cliente, _
                                                       strCod_DPK, _
                                                       strGrupo, _
                                                       strSubGrupo, _
                                                       strTipo_Empresa, _
                                                       strIE, _
                                                       strIS, _
                                                       lngTare)

    dblDIF_ICMS = arrDblICMS_SUFRAMA(intDif_ICMS)
    dblDESCONTO_SUFRAMA = arrDblICMS_SUFRAMA(intDesc_Suframa)


    'DESCONTO ADICIONAL:
    dblDESCONTO_ADICIONAL = Retorna_Desconto_Adicional(strCaracteristica, _
                                                       intCategoria, _
                                                       strCod_Fornecedor, _
                                                       strGrupo, _
                                                       strSubGrupo, _
                                                       strCod_DPK, _
                                                       strUf_Destino)


    'ACRESCIMO FINANCEIRO DPK:
    ReDim arrDblDESC_ADIC_DPK(1)

    arrDblDESC_ADIC_DPK = Retorna_Desc_Adic_Fin_DPK(strCod_Plano, _
                                                    dblPc_Acrescimo, _
                                                    dblPc_Desconto)

    dblDESCONTO_FINANCEIRO_DPK = arrDblDESC_ADIC_DPK(intDesconto_Adicional)
    dblACRESCIMO_FINANCEIRO_DPK = arrDblDESC_ADIC_DPK(intAcrescimo_Financeiro)


    'DESCONTOS DE PERIODO:
    dblDESCONTO_PERIODO = Retorna_Desconto_Periodo(strCod_DPK, _
                                                   strCod_Fornecedor, _
                                                   strCod_Filial, _
                                                   strGrupo, _
                                                   strSubGrupo, _
                                                   strTipo_Menor_Preco, _
                                                   dblPreco_Venda_Ant, _
                                                   dblPreco_Venda, _
                                                   dblPreco_OF_Ant, _
                                                   dblPreco_OF, _
                                                   dblPreco_SP_Ant, _
                                                   dblPreco_SP)


    '-----------------------------------------------------------------------
    'RETORNA O FATOR PARA CALCULO COM O PRE�O DE VENDA:
    '
    'TI-5398
    If dblPc_Desc1 > 0 Or dblPc_Desc2 > 0 Then
        dblRetorno = (1 - dblPc_Desc1 / 100) * _
                     (1 - dblDESCONTO_FINANCEIRO_DPK / 100) * _
                     (1 + dblACRESCIMO_FINANCEIRO_DPK / 100) * _
                     (1 - dblDESCONTO_SUFRAMA / 100) * _
                     (1 - dblDIF_ICMS / 100) * _
                     (1 - dblDESCONTO_ADICIONAL / 100) * _
                     (1 - (dblPERC_DESC_UF + dblPc_Desc2) / 100)
                     
        dblPc_DescUF = (dblPERC_DESC_UF + dblPc_Desc2)
        dblPc_Desc3 = dblDESCONTO_ADICIONAL
        dblPc_DescICM = dblDIF_ICMS
    Else
        dblRetorno = (1 - dblDESCONTO_PERIODO / 100) * _
                     (1 - dblDESCONTO_FINANCEIRO_DPK / 100) * _
                     (1 + dblACRESCIMO_FINANCEIRO_DPK / 100) * _
                     (1 - dblDESCONTO_SUFRAMA / 100) * _
                     (1 - dblDIF_ICMS / 100) * _
                     (1 - dblDESCONTO_ADICIONAL / 100) * _
                     (1 - dblPERC_DESC_UF / 100)
    End If
    'FIM TI-5398
    '
    '-----------------------------------------------------------------------

    Calcular_Descontos = dblRetorno

End Function

Private Function Retorna_Desc_Adic_Fin_DPK(ByVal strCod_Plano As String, _
                                           ByVal dblPc_Acrescimo As Double, _
                                           ByVal dblPc_Desconto As Double) As Double()

    Dim rstPlano As Object
    Dim arrDblRetorno(1) As Double

    OraParameters.Remove "plano"
    OraParameters.Remove "vErro"

    OraParameters.Add "plano", strCod_Plano, 1
    OraParameters.Add "vErro", 0, 2

    oradatabase.ExecuteSQL "Begin producao.pck_vda320.pr_plano2(:vCursor, :plano, :vErro); END;"

    Set rstPlano = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        MessageBox 0, "Erro ao buscar o desconto adicional e acrescimo financeiro DPK", "ATEN��O!", &H40000 + 0
        Set rstPlano = Nothing
        End
    End If

    If dblPc_Acrescimo = -1 And dblPc_Desconto = -1 Then

        If rstPlano!PC_ACRES_FIN_DPK > 0 Then

            arrDblRetorno(intAcrescimo_Financeiro) = CDbl(rstPlano!PC_ACRES_FIN_DPK)

            arrDblRetorno(intDesconto_Adicional) = 0

        Else

            arrDblRetorno(intAcrescimo_Financeiro) = 0

            arrDblRetorno(intDesconto_Adicional) = CDbl(rstPlano!PC_DESC_FIN_DPK)

        End If

    Else

        If dblPc_Acrescimo > 0 Then

            arrDblRetorno(intAcrescimo_Financeiro) = dblPc_Acrescimo

            arrDblRetorno(intDesconto_Adicional) = 0

        Else

            arrDblRetorno(intAcrescimo_Financeiro) = 0

            arrDblRetorno(intDesconto_Adicional) = dblPc_Desconto

        End If

    End If

    Retorna_Desc_Adic_Fin_DPK = arrDblRetorno

End Function

Private Function Retorna_Desconto_Adicional(ByVal strCaracteristica As String, _
                                            ByVal intCategoria As Integer, _
                                            ByVal strCod_Fornecedor As String, _
                                            ByVal strGrupo As String, _
                                            ByVal strSubGrupo As String, _
                                            ByVal strCod_DPK As String, _
                                            ByVal strUf_Destino As String) As Double

    Dim rstDescAuto As Object
    Dim rstUfCateg As Object
    Dim rstICMS_FornEsp As Object
    Dim strDesc_auto As String
    Dim strFlg_Dif_ICMS As String
    Dim dblRetorno As Double

    '--

    dblRetorno = 0

    '--

    OraParameters.Remove "carac"

    OraParameters.Add "carac", strCaracteristica, 1

    oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_Caracteristica(:vCursor, :Carac); END;"

    Set rstDescAuto = oradatabase.Parameters("vCursor").Value

    If rstDescAuto.EOF And rstDescAuto.BOF Then
        strDesc_auto = "N"
    Else
        strDesc_auto = rstDescAuto!fl_desc_automatico
    End If

    rstDescAuto.Close

    Set rstDescAuto = Nothing

    '--

    OraParameters.Remove "forn"
    OraParameters.Remove "grupo"
    OraParameters.Remove "subgrupo"
    OraParameters.Remove "dpk"

    OraParameters.Add "forn", strCod_Fornecedor, 1
    OraParameters.Add "grupo", strGrupo, 1
    OraParameters.Add "subgrupo", strSubGrupo, 1
    OraParameters.Add "dpk", strCod_DPK, 1

    oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_ICM_Forn_Especifico(:vCursor, :Dpk, :SubGrupo, :Grupo, :Forn); END;"

    Set rstICMS_FornEsp = oradatabase.Parameters("vCursor").Value

    If rstICMS_FornEsp.EOF Or rstICMS_FornEsp.BOF Then

        strFlg_Dif_ICMS = "S"

    Else

        strFlg_Dif_ICMS = rstICMS_FornEsp!fl_dif_icms

    End If

    rstICMS_FornEsp.Close

    Set rstICMS_FornEsp = Nothing

    '--

    If strDesc_auto = "S" Then

        OraParameters.Remove "uf"
        OraParameters.Remove "categ"
        OraParameters.Remove "carac"
        OraParameters.Remove "Cod_Erro"

        OraParameters.Add "uf", strUf_Destino, 1
        OraParameters.Add "categ", intCategoria, 1
        OraParameters.Add "carac", strCaracteristica, 1
        OraParameters.Add "Cod_Erro", 0, 2

        oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA320.PR_UF_CATEG(:vCursor, :UF, :Categ, :Carac, :Cod_Erro); END;"

        Set rstUfCateg = oradatabase.Parameters("vCursor").Value

        If Not rstUfCateg.EOF And Not rstUfCateg.BOF Then

            If CDbl(rstUfCateg!pc_desc_adic) > 0 And strFlg_Dif_ICMS = "N" Then
                dblRetorno = 0
            Else
                dblRetorno = rstUfCateg!pc_desc_adic
            End If

        Else

            dblRetorno = 0

        End If

        rstUfCateg.Close

        Set rstUfCateg = Nothing

    End If

    Retorna_Desconto_Adicional = dblRetorno

End Function

Private Function Retorna_Desconto_Periodo(ByVal strCod_DPK As String, _
                                          ByVal strCod_Fornecedor As String, _
                                          ByVal strCod_Filial As String, _
                                          ByVal strGrupo As String, _
                                          ByVal strSubGrupo As String, _
                                          ByVal strTipo_Menor_Preco As String, _
                                          ByVal dblPreco_Venda_Ant As Double, _
                                          ByVal dblPreco_Venda As Double, _
                                          ByVal dblPreco_OF_Ant As Double, _
                                          ByVal dblPreco_OF As Double, _
                                          ByVal dblPreco_SP_Ant As Double, _
                                          ByVal dblPreco_SP As Double) As Double

    Dim rstDescontos As Object

    '
    Dim dblRetorno As Double
    Dim dblPRECO_NORMAL As Double
    Dim dblPRECO_OFERTA As Double
    Dim dblPRECO_EXCELENCIA As Double

    Dim dblPC_DESC_PERIODO1_NORMAL As Double
    Dim dblPC_DESC_PERIODO2_NORMAL As Double
    Dim dblPC_DESC_PERIODO3_NORMAL As Double

    Dim dblPC_DESC_PERIODO1_OFERTA As Double
    Dim dblPC_DESC_PERIODO2_OFERTA As Double
    Dim dblPC_DESC_PERIODO3_OFERTA As Double

    Dim dblPC_DESC_PERIODO1_SP As Double
    Dim dblPC_DESC_PERIODO2_SP As Double
    Dim dblPC_DESC_PERIODO3_SP As Double

    '
    Dim strSEQUENCIA0 As String * 1
    Dim strSEQUENCIA1 As String * 1
    Dim strSEQUENCIA2 As String * 1

    '
    dblPC_DESC_PERIODO1_NORMAL = 0
    dblPC_DESC_PERIODO2_NORMAL = 0
    dblPC_DESC_PERIODO3_NORMAL = 0

    dblPC_DESC_PERIODO1_OFERTA = 0
    dblPC_DESC_PERIODO2_OFERTA = 0
    dblPC_DESC_PERIODO3_OFERTA = 0

    dblPC_DESC_PERIODO1_SP = 0
    dblPC_DESC_PERIODO2_SP = 0
    dblPC_DESC_PERIODO3_SP = 0

    '
    OraParameters.Remove "dpk"
    OraParameters.Remove "forn"
    OraParameters.Remove "filial"
    OraParameters.Remove "grupo"
    OraParameters.Remove "subgrupo"

    OraParameters.Remove "ocorr_preco0"
    OraParameters.Remove "ocorr_preco1"
    OraParameters.Remove "ocorr_preco2"

    OraParameters.Remove "seq0"
    OraParameters.Remove "seq1"
    OraParameters.Remove "seq2"

    OraParameters.Add "dpk", strCod_DPK, 1
    OraParameters.Add "forn", strCod_Fornecedor, 1
    OraParameters.Add "filial", strCod_Filial, 1
    OraParameters.Add "grupo", strGrupo, 1
    OraParameters.Add "subgrupo", strSubGrupo, 1

    If frmVenda.chkSemanaPassada.Value = vbUnchecked Then

        OraParameters.Add "ocorr_preco0", ocor_atu(0), 1
        OraParameters.Add "ocorr_preco1", ocor_atu(1), 1
        OraParameters.Add "ocorr_preco2", ocor_atu(2), 1

        OraParameters.Add "seq0", seq_atu(0), 1
        OraParameters.Add "seq1", seq_atu(1), 1
        OraParameters.Add "seq2", seq_atu(2), 1

        strSEQUENCIA0 = seq_atu(0)
        strSEQUENCIA1 = seq_atu(1)
        strSEQUENCIA2 = seq_atu(2)

    Else

        OraParameters.Add "ocorr_preco0", ocor_ant(0), 1
        OraParameters.Add "ocorr_preco1", ocor_ant(1), 1
        OraParameters.Add "ocorr_preco2", ocor_ant(2), 1

        OraParameters.Add "seq0", seq_ant(0), 1
        OraParameters.Add "seq1", seq_ant(1), 1
        OraParameters.Add "seq2", seq_ant(2), 1

        strSEQUENCIA0 = seq_ant(0)
        strSEQUENCIA1 = seq_ant(1)
        strSEQUENCIA2 = seq_ant(2)

    End If

    'Criar_Cursor
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_Consulta_Desconto(" & _
                           ":vCursor, " & _
                           ":Seq0, " & _
                           ":Ocorr_Preco0, " & _
                           ":Forn, " & _
                           ":Grupo, " & _
                           ":SubGrupo, " & _
                           ":Dpk, " & _
                           ":Filial, " & _
                           ":Seq1, " & _
                           ":Ocorr_Preco1, " & _
                           ":Seq2, " & _
                           ":Ocorr_Preco2); " & _
                           "END;"

    Set rstDescontos = oradatabase.Parameters("vCursor").Value

    If Not rstDescontos.EOF And Not rstDescontos.BOF Then

        If frmVenda.chkSemanaPassada.Value = vbChecked Then

            If ocor_ant(0) = 2 Then
                dblPRECO_NORMAL = dblPreco_Venda_Ant
            Else
                dblPRECO_NORMAL = dblPreco_Venda
            End If

        Else

            If ocor_atu(0) = 2 Then
                dblPRECO_NORMAL = dblPreco_Venda_Ant
            Else
                dblPRECO_NORMAL = dblPreco_Venda
            End If

        End If

        If frmVenda.chkSemanaPassada.Value = vbChecked Then

            If ocor_ant(1) = 2 Then
                dblPRECO_OFERTA = dblPreco_OF_Ant
            Else
                dblPRECO_OFERTA = dblPreco_OF
            End If

        Else

            If ocor_atu(1) = 2 Then
                dblPRECO_OFERTA = dblPreco_OF_Ant
            Else
                dblPRECO_OFERTA = dblPreco_OF
            End If

        End If

        If frmVenda.chkSemanaPassada.Value = vbChecked Then

            If ocor_ant(2) = 2 Then
                dblPRECO_EXCELENCIA = dblPreco_SP_Ant
            Else
                dblPRECO_EXCELENCIA = dblPreco_SP
            End If

        Else

            If ocor_atu(2) = 2 Then
                dblPRECO_EXCELENCIA = dblPreco_SP_Ant
            Else
                dblPRECO_EXCELENCIA = dblPreco_SP
            End If

        End If

        rstDescontos.MoveFirst

        Do

            If dblPRECO_NORMAL > 0 And _
               rstDescontos!tp_tabela <> "2" And _
               rstDescontos!tp_tabela <> "1" And _
               dblPC_DESC_PERIODO1_NORMAL = 0 And _
               dblPC_DESC_PERIODO2_NORMAL = 0 And _
               dblPC_DESC_PERIODO3_NORMAL = 0 And _
               rstDescontos!Sequencia = strSEQUENCIA0 Then

                If CSng(rstDescontos!PC_DESC_PERIODO1) > 0 Then
                    dblPC_DESC_PERIODO1_NORMAL = CDbl(rstDescontos!PC_DESC_PERIODO1)
                End If

                If CSng(rstDescontos!PC_DESC_PERIODO2) > 0 Then
                    dblPC_DESC_PERIODO2_NORMAL = CDbl(rstDescontos!PC_DESC_PERIODO2)
                End If

                If Not IsNull(CSng(rstDescontos!PC_DESC_PERIODO3)) Then
                    dblPC_DESC_PERIODO3_NORMAL = CDbl(rstDescontos!PC_DESC_PERIODO3)
                End If

            End If

            'localizar desconto da tabela de oferta
            If dblPRECO_OFERTA > 0 And _
               rstDescontos!tp_tabela = "1" And _
               dblPC_DESC_PERIODO1_OFERTA = 0 And _
               dblPC_DESC_PERIODO2_OFERTA = 0 And _
               dblPC_DESC_PERIODO3_OFERTA = 0 And _
               rstDescontos!Sequencia = strSEQUENCIA1 Then

                If CSng(rstDescontos!PC_DESC_PERIODO1) > 0 Then
                    dblPC_DESC_PERIODO1_OFERTA = CDbl(rstDescontos!PC_DESC_PERIODO1)
                End If

                If CSng(rstDescontos!PC_DESC_PERIODO2) > 0 Then
                    dblPC_DESC_PERIODO2_OFERTA = CDbl(rstDescontos!PC_DESC_PERIODO2)
                End If

                If Not IsNull(CSng(rstDescontos!PC_DESC_PERIODO3)) Then
                    dblPC_DESC_PERIODO3_OFERTA = CDbl(rstDescontos!PC_DESC_PERIODO3)
                End If

            End If

            'localizar desconto da tabela de SP
            If dblPRECO_EXCELENCIA > 0 And _
               rstDescontos!tp_tabela = "2" And _
               dblPC_DESC_PERIODO1_SP = 0 And _
               dblPC_DESC_PERIODO2_SP = 0 And _
               dblPC_DESC_PERIODO3_SP = 0 And _
               rstDescontos!Sequencia = strSEQUENCIA2 Then

                If CSng(rstDescontos!PC_DESC_PERIODO1) > 0 Then
                    dblPC_DESC_PERIODO1_SP = CDbl(rstDescontos!PC_DESC_PERIODO1)
                End If

                If CSng(rstDescontos!PC_DESC_PERIODO2) > 0 Then
                    dblPC_DESC_PERIODO2_SP = CDbl(rstDescontos!PC_DESC_PERIODO2)
                End If

                If Not IsNull(CSng(rstDescontos!PC_DESC_PERIODO3)) Then
                    dblPC_DESC_PERIODO3_SP = CDbl(rstDescontos!PC_DESC_PERIODO3)
                End If

            End If

            rstDescontos.MoveNext

        Loop Until rstDescontos.EOF

    End If

    rstDescontos.Close

    Set rstDescontos = Nothing

    'DUVIDA COMO OBTER O PERIODO????
    
    Select Case UCase(strTipo_Menor_Preco)

        Case "NORMAL"
            'dblRetorno = IIf(dblPC_DESC_PERIODO1_NORMAL = 0, IIf(dblPC_DESC_PERIODO2_NORMAL = 0, dblPC_DESC_PERIODO3_NORMAL, dblPC_DESC_PERIODO2_NORMAL), dblPC_DESC_PERIODO1_NORMAL)
            dblRetorno = dblPC_DESC_PERIODO2_NORMAL

        Case "OFERTA"
            'dblRetorno = IIf(dblPC_DESC_PERIODO1_OFERTA = 0, IIf(dblPC_DESC_PERIODO2_OFERTA = 0, dblPC_DESC_PERIODO3_OFERTA, dblPC_DESC_PERIODO2_NORMAL), dblPC_DESC_PERIODO1_OFERTA)
            dblRetorno = dblPC_DESC_PERIODO2_OFERTA

        Case "EXCELENCIA"
            'dblRetorno = IIf(dblPC_DESC_PERIODO1_SP = 0, IIf(dblPC_DESC_PERIODO2_SP = 0, dblPC_DESC_PERIODO3_SP, dblPC_DESC_PERIODO2_SP), dblPC_DESC_PERIODO1_SP)
            dblRetorno = dblPC_DESC_PERIODO2_SP

    End Select

    Retorna_Desconto_Periodo = dblRetorno

End Function

Private Function Retorna_Desconto_UF(ByVal strCod_DPK As String, _
                                     ByVal strUf_Origem As String, _
                                     ByVal strUf_Destino As String, _
                                     ByVal strTipo_Cliente As String) As Double

    Dim rstUF As Object
    Dim dblRetorno As Double

    dblRetorno = 0

    OraParameters.Remove "dpk"
    OraParameters.Remove "origem"
    OraParameters.Remove "destino"
    OraParameters.Remove "vErro"

    OraParameters.Add "dpk", Val(strCod_DPK), 1
    OraParameters.Add "origem", strUf_Origem, 1
    OraParameters.Add "destino", strUf_Destino, 1
    OraParameters.Add "vErro", 0, 2

    vSql = "Begin producao.pck_vda320.pr_uf_dpk(:vCursor, :dpk, :origem, :destino, :vErro); END;"

    oradatabase.ExecuteSQL vSql

    Set rstUF = oradatabase.Parameters("vCursor").Value

    If rstUF.EOF And rstUF.BOF Then

        dblRetorno = 0

    Else

        If frmVenda.lblMsgTributacao.Visible = True And strTipo_Cliente = "ISENTO" Then

            dblRetorno = 0

        Else

            dblRetorno = CSng(rstUF!pc_desc)

        End If

    End If

    rstUF.Close

    Set rstUF = Nothing

    Retorna_Desconto_UF = dblRetorno

End Function

Private Function Retorna_Dif_ICMS_Desc_Suframa(ByVal strClass_Fiscal As String, _
                                               ByVal strUf_Origem As String, _
                                               ByVal strUf_Destino As String, _
                                               ByRef strCod_Fornecedor As String, _
                                               ByRef strCod_Trib_Item As String, _
                                               ByVal strTipo_Cliente As String, _
                                               ByVal strCod_DPK As String, _
                                               ByVal strGrupo As String, _
                                               ByVal strSubGrupo As String, _
                                               ByVal strTipo_Empresa As String, _
                                               ByVal strIE As String, _
                                               ByVal strIS As String, _
                                               ByVal lngTare As Long) As Double()

    Dim rstTrib As Object
    Dim rstCodTrib As Object
    Dim rstICMS As Object
    Dim rstBase_Red As Object
    Dim rstDescAdic As Object
    Dim rstICMS_FornEsp As Object
    Dim rstUf_Origem_Destino As Object

    '
    Dim arrDblRetorno(1) As Double
    Dim dblPC_ICM As Double

    '
    Dim strCod_Trib As String
    Dim strBase_Red As String
    Dim strFl_Dif_ICMS As String
    Dim strFl_Adicional As String

    '
    Dim bdif_ICMS As Byte

    '
    arrDblRetorno(intDif_ICMS) = 0
    arrDblRetorno(intDesc_Suframa) = 0

    OraParameters.Remove "forn"
    OraParameters.Remove "grupo"
    OraParameters.Remove "subgrupo"
    OraParameters.Remove "dpk"

    OraParameters.Add "forn", strCod_Fornecedor, 1
    OraParameters.Add "grupo", strGrupo, 1
    OraParameters.Add "subgrupo", strSubGrupo, 1
    OraParameters.Add "dpk", strCod_DPK, 1

    oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_ICM_Forn_Especifico(:vCursor, :Dpk, :SubGrupo, :Grupo, :Forn); END;"

    Set rstICMS_FornEsp = oradatabase.Parameters("vCursor").Value

    If rstICMS_FornEsp.EOF Or rstICMS_FornEsp.BOF Then

        strFl_Dif_ICMS = "S"
        strFl_Adicional = "S"
        bdif_ICMS = 1

    Else

        strFl_Dif_ICMS = rstICMS_FornEsp!fl_dif_icms
        strFl_Adicional = rstICMS_FornEsp!fl_adicional
        bdif_ICMS = IIf(IsNull(rstICMS_FornEsp!TP_DIF_ICMS), 1, rstICMS_FornEsp!TP_DIF_ICMS)

    End If

    rstICMS_FornEsp.Close

    Set rstICMS_FornEsp = Nothing

    'CODIGO TRIBUTACAO----------------------------------------------------------------------------------------
    OraParameters.Remove "uf_origem"
    OraParameters.Remove "uf_destino"
    OraParameters.Remove "trib"
    OraParameters.Remove "tp_cli"

    OraParameters.Add "uf_origem", strUf_Origem, 1
    
    If strIS <> "" And strUf_Origem <> strUf_Destino Then
        OraParameters.Add "uf_destino", "SU", 1
    Else
        OraParameters.Add "uf_destino", strUf_Destino, 1
    End If
    
    OraParameters.Add "trib", strCod_Trib_Item, 1
    
    If strTipo_Cliente = "REVENDEDOR" Then
        OraParameters.Add "tp_cli", "R", 1
    ElseIf strTipo_Cliente = "CONS. FINAL" Then
        OraParameters.Add "tp_cli", "C", 1
    ElseIf strTipo_Cliente = "ISENTO" Then
        OraParameters.Add "tp_cli", "I", 1
    ElseIf strTipo_Cliente = "" Then
        OraParameters.Add "tp_cli", "R", 1
    End If

    oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_Tributacao(:vCursor, :Trib, :Tp_Cli, :UF_Origem, :UF_Destino); END;"

    Set rstTrib = oradatabase.Parameters("vCursor").Value

    If rstTrib.EOF And rstTrib.BOF Then
        strCod_Trib = strCod_Trib_Item
    Else
        strCod_Trib = CStr(rstTrib!cod_tributacao_final)
    End If

    rstTrib.Close

    Set rstTrib = Nothing
    '---------------------------------------------------------------------------------------------------------

    OraParameters.Remove "uf_origem"
    OraParameters.Remove "uf_destino"

    OraParameters.Add "uf_origem", strUf_Origem, 1
    OraParameters.Add "uf_destino", strUf_Destino, 1

    oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_ICM(:vCursor, :UF_Origem, :UF_Destino); END;"

    Set rstICMS = oradatabase.Parameters("vCursor").Value

    If strIS <> "" And strTipo_Cliente = "REVENDEDOR" Then
        arrDblRetorno(intDesc_Suframa) = CDbl(rstICMS!pc_icm)
    Else
        arrDblRetorno(intDesc_Suframa) = 0
    End If

    If strIE <> "ISENTO" And IIf(strCod_Trib = "8" And rstICMS!pc_icm <> "7", False, True) And strCod_Trib <> "1" Then

        arrDblRetorno(intDif_ICMS) = rstICMS!PC_DIFICM

    ElseIf strIE = "ISENTO" And strCod_Trib <> "1" Then

        arrDblRetorno(intDif_ICMS) = 0

    ElseIf strIE <> "ISENTO" And IIf(strCod_Trib = "8" And rstICMS!pc_icm = "12", True, False) And strCod_Trib <> "1" Then

        arrDblRetorno(intDif_ICMS) = 0

        arrDblRetorno(intDif_ICMS) = rstICMS!PC_DIFICM

    Else

        OraParameters.Remove "class"
        OraParameters.Remove "uf_origem"
        OraParameters.Remove "uf_destino"

        OraParameters.Add "class", strClass_Fiscal, 1
        OraParameters.Add "uf_origem", strUf_Origem, 1
        If strIS <> "" And strUf_Origem <> strUf_Destino Then
            OraParameters.Add "uf_destino", "SU", 1
        Else
            OraParameters.Add "uf_destino", strUf_Destino, 1
        End If

        oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_Cod_Trib(:vCursor, :Class, :UF_Origem, :UF_Destino); END;"

        Set rstCodTrib = oradatabase.Parameters("vCursor").Value

        If rstCodTrib.EOF Then

            arrDblRetorno(intDif_ICMS) = 0

        ElseIf strTipo_Cliente = "REVENDEDOR" Or strTipo_Cliente = "" Then

            If lngTare = 0 Then

                strCod_Trib = rstCodTrib!COD_TRIB_REVENDEDOR

                If rstCodTrib!COD_TRIB_REVENDEDOR <> "0" Then
                    arrDblRetorno(intDif_ICMS) = 0
                Else
                    arrDblRetorno(intDif_ICMS) = rstICMS!PC_DIFICM
                End If

            Else

                strCod_Trib = rstCodTrib!COD_TRIB_TARE

                If rstCodTrib!COD_TRIB_TARE <> "0" Then
                    arrDblRetorno(intDif_ICMS) = 0
                Else
                    arrDblRetorno(intDif_ICMS) = rstICMS!PC_DIFICM
                End If

            End If

        ElseIf strTipo_Cliente = "CONS. FINAL" Then

            strCod_Trib = rstCodTrib!COD_TRIB_INSCRITO

            If rstCodTrib!COD_TRIB_INSCRITO <> "0" Then
                arrDblRetorno(intDif_ICMS) = 0
            Else
                arrDblRetorno(intDif_ICMS) = rstICMS!PC_DIFICM
            End If

        ElseIf strTipo_Cliente = "ISENTO" Then

            strCod_Trib = rstCodTrib!COD_TRIB_ISENTO

            If rstCodTrib!COD_TRIB_ISENTO <> "0" Then
                arrDblRetorno(intDif_ICMS) = 0
            Else
                arrDblRetorno(intDif_ICMS) = rstICMS!PC_DIFICM
            End If

        End If

    End If

    If strTipo_Empresa = "E" Then

        OraParameters.Remove "class"
        OraParameters.Remove "uf_origem"
        OraParameters.Remove "uf_destino"

        OraParameters.Add "class", strClass_Fiscal, 1
        OraParameters.Add "uf_origem", strUf_Origem, 1
        If strIS <> "" And strUf_Origem <> strUf_Destino Then
            OraParameters.Add "uf_destino", "SU", 1
        Else
            OraParameters.Add "uf_destino", strUf_Destino, 1
        End If

        oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_BASE_RED(:vCursor, :Class, :UF_Origem, :UF_Destino); END;"

        Set rstBase_Red = oradatabase.Parameters("vCursor").Value

        If (Not rstBase_Red.EOF And Not rstBase_Red.BOF) And rstBase_Red!total > 0 Then

            strBase_Red = "S"

            OraParameters.Remove "uf_origem"
            OraParameters.Remove "trib"
            OraParameters.Remove "uf_destino"
            OraParameters.Remove "tp_cli"

            OraParameters.Add "uf_origem", strUf_Origem, 1
            OraParameters.Add "trib", "8", 1
            If strIS <> "" And strUf_Origem <> strUf_Destino Then
                OraParameters.Add "uf_destino", "SU", 1
            Else
                OraParameters.Add "uf_destino", strUf_Destino, 1
            End If
            If tipo_cliente = "REVENDEDOR" Then
                OraParameters.Add "tp_cli", "R", 1
            ElseIf tipo_cliente = "CONS. FINAL" Then
                OraParameters.Add "tp_cli", "C", 1
            ElseIf tipo_cliente = "ISENTO" Then
                OraParameters.Add "tp_cli", "I", 1
            End If

            oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA230.Pr_Desconto_Adicional(:vCursor, :Trib, :Tp_cli, :UF_Destino, :UF_Origem); END;"

            Set rstDescAdic = oradatabase.Parameters("vCursor").Value

            If rstDescAdic.EOF And rstDescAdic.BOF Then

                dblPC_ICM = 0

            Else

                If bdif_ICMS = 2 Then
                    dblPC_ICM = rstDescAdic!pc_desc_diesel
                Else
                    dblPC_ICM = rstDescAdic!pc_desc
                End If

                arrDblRetorno(intDif_ICMS) = 100 - (100 * (1 - CDbl(arrDblRetorno(intDif_ICMS)) / 100) * (1 - CDbl(dblPC_ICM) / 100))

            End If

            rstDescAdic.Close

            Set rstDescAdic = Nothing

        Else

            strBase_Red = "N"

        End If

        rstBase_Red.Close

        Set rstBase_Red = Nothing


    End If

    If strCod_DPK <> "" And _
       (UCase(strUf_Origem) <> UCase(strUf_Destino) And strFl_Dif_ICMS = "S") And _
       strBase_Red <> "S" Then

        OraParameters.Remove "uf_origem"
        OraParameters.Remove "trib"
        OraParameters.Remove "uf_destino"
        OraParameters.Remove "tp_cli"

        OraParameters.Add "uf_origem", strUf_Origem, 1
        OraParameters.Add "trib", strCod_Trib, 1
        If strIS <> "" And strUf_Origem <> strUf_Destino Then
            OraParameters.Add "uf_destino", "SU", 1
        Else
            OraParameters.Add "uf_destino", strUf_Destino, 1
        End If
        If tipo_cliente = "REVENDEDOR" Then
            OraParameters.Add "tp_cli", "R", 1
        ElseIf tipo_cliente = "CONS. FINAL" Then
            OraParameters.Add "tp_cli", "C", 1
        ElseIf tipo_cliente = "ISENTO" Then
            OraParameters.Add "tp_cli", "I", 1
        ElseIf tipo_cliente = "" Then
            OraParameters.Add "tp_cli", "R", 1
        End If

        oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA230.Pr_Desconto_Adicional(:vCursor, :Trib, :Tp_cli, :UF_Destino, :UF_Origem); END;"

        Set rstDescAdic = oradatabase.Parameters("vCursor").Value

        If rstDescAdic.EOF And rstDescAdic.BOF Then

            dblPC_ICM = 0

        Else

            If bdif_ICMS = 2 Then
                dblPC_ICM = rstDescAdic!pc_desc_diesel
            Else
                dblPC_ICM = rstDescAdic!pc_desc
            End If

            arrDblRetorno(intDif_ICMS) = 100 - (100 * (1 - CDbl(arrDblRetorno(intDif_ICMS)) / 100) * (1 - CDbl(dblPC_ICM) / 100))

        End If

        rstDescAdic.Close

        Set rstDescAdic = Nothing

    End If

    If strFl_Dif_ICMS = "N" Then

        arrDblRetorno(intDif_ICMS) = 0

        OraParameters.Remove "uf_origem"
        OraParameters.Remove "uf_destino"
        OraParameters.Remove "Cod_Erro"

        OraParameters.Add "uf_origem", strUf_Origem, 1
        OraParameters.Add "uf_destino", strUf_Destino, 1
        OraParameters.Add "Cod_Erro", 0, 2

        oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA320.PR_UF_ORIGEM_DESTINO(:vCursor, :UF_Origem, :UF_Destino, :Cod_Erro); END;"

        Set rstUf_Origem_Destino = oradatabase.Parameters("vCursor").Value

        If strIS <> "" And strTipo_Cliente = "REVENDEDOR" Then
            arrDblRetorno(intDesc_Suframa) = CDbl(rstUf_Origem_Destino!pc_icm)
        Else
            arrDblRetorno(intDesc_Suframa) = 0
        End If

        rstUf_Origem_Destino.Close

        Set rstUf_Origem_Destino = Nothing

    End If

    Retorna_Dif_ICMS_Desc_Suframa = arrDblRetorno

End Function

Public Function Retornar_Uf_Origem(ByVal strCod_Deposito As String) As String
    
    Dim rstUf_Origem As Object
    Dim strRetorno As String
    
    strRetorno = ""
    
    OraParameters.Remove "loja"
    
    OraParameters.Add "loja", strCod_Deposito, 1
    
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_Select_UF_Cidade(:vCursor, :Loja); END;"
    
    Set rstUf_Origem = oradatabase.Parameters("vCursor").Value
    
    If Not rstUf_Origem.EOF Then
    
        strRetorno = rstUf_Origem!cod_uf
        
    Else
    
        MessageBox 0, "Erro ao buscar Uf de Origem!", "ATEN��O!", &H40000 + 0
        
        End
    
    End If
    
    rstUf_Origem.Close
    
    Set rstUf_Origem = Nothing

    Retornar_Uf_Origem = strRetorno
    
End Function

