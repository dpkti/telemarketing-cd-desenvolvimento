Attribute VB_Name = "modRegistroOCX"
Option Explicit

Private Const HKEY_LOCAL_MACHINE  As Long = &H80000000
Private Const REG_SZ As Long = 1
Private Const ERROR_MORE_DATA = 234
Private Const ERROR_SUCCESS As Long = 0
Private Const KEY_QUERY_VALUE As Long = &H1
Private Const KEY_ENUMERATE_SUB_KEYS As Long = &H8
Private Const KEY_NOTIFY As Long = &H10
Private Const STANDARD_RIGHTS_READ As Long = &H20000
Private Const SYNCHRONIZE As Long = &H100000
Private Const KEY_READ As Long = ((STANDARD_RIGHTS_READ Or _
                                   KEY_QUERY_VALUE Or _
                                   KEY_ENUMERATE_SUB_KEYS Or _
                                   KEY_NOTIFY) And _
                                   (Not SYNCHRONIZE))
                                   
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function lstrlenW Lib "kernel32" (ByVal lpString As Long) As Long
  
Public Function GetRegistryInfo(wri As String) As String

   Dim hKey As Long
   Dim regPath As String
   
   regPath = "CLSID\{F8D04BE5-5C5D-4305-8CAF-1350915C524E}\TypeLib"
     
  'Before reading a value from the reg
  'the key to read must first be opened.
  'hKey contains the handle used in
  'subsequent calls.
   hKey = OpenRegKey(HKEY_LOCAL_MACHINE, regPath)

   If hKey <> 0 Then
      GetRegistryInfo = GetRegValue(hKey, "")
  
     'the opened key must be closed
      Call RegCloseKey(hKey)
  End If

End Function


Private Function GetRegValue(hSubKey As Long, sKeyName As String) As String

   Dim lpValue As String   'value retrieved
   Dim lpcbData As Long    'length of retrieved string

  'if valid
   If hSubKey <> 0 Then
   
     'Pass an zero-length string to
     'obtain the required buffer size
     'required to return the result.
     'If the key passed exists, the call
     'will return error 234 (more data)
     'and lpcbData will indicate the
     'required buffer size (including
     'the terminating null).
      lpValue = ""
      lpcbData = 0
      If RegQueryValueEx(hSubKey, _
                         sKeyName, _
                         0&, _
                         0&, _
                         ByVal lpValue, _
                         lpcbData) = ERROR_MORE_DATA Then

         lpValue = Space$(lpcbData)
      
        'retrieve the desired value
         If RegQueryValueEx(hSubKey, _
                            sKeyName, _
                            0&, _
                            0&, _
                            ByVal lpValue, _
                            lpcbData) = ERROR_SUCCESS Then
                        
            GetRegValue = TrimNull(lpValue)
         
         End If  'If RegQueryValueEx (second call)
      End If  'If RegQueryValueEx (first call)
   End If  'If hSubKey

End Function


Private Function OpenRegKey(ByVal hKey As Long, _
                            ByVal lpSubKey As String) As Long

  Dim hSubKey As Long

  If RegOpenKeyEx(hKey, _
                  lpSubKey, _
                  0, _
                  KEY_READ, _
                  hSubKey) = ERROR_SUCCESS Then

      OpenRegKey = hSubKey

  End If
End Function


Private Function TrimNull(startstr As String) As String
   TrimNull = Left$(startstr, lstrlenW(StrPtr(startstr)))
End Function





