﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aplicacao.Class;

namespace Aplicacao
{
    public partial class RestricaoBt : System.Web.UI.Page
    {
        public MaxxDataSet _vDataSet= new MaxxDataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            int pLoja;
            int pForn;
            int pMontadora;
            int pGrupo;
            int pSubGrupo;
            int pVeiculo;
            int pModelo;
            int pMotor;
            int pAno;
            string pDesc = "";
            pLoja = Convert.ToInt32(this.Request.QueryString["loja"]);
            pForn = Convert.ToInt32(this.Request.QueryString["forn"]);
            pMontadora = Convert.ToInt32(this.Request.QueryString["montadora"]);
            pGrupo = Convert.ToInt32(this.Request.QueryString["grupo"]);
            pSubGrupo = Convert.ToInt32(this.Request.QueryString["subgrupo"]);
            pVeiculo = Convert.ToInt32(this.Request.QueryString["veiculo"]);
            pModelo = Convert.ToInt32(this.Request.QueryString["modelo"]);
            pMotor = Convert.ToInt32(this.Request.QueryString["motor"]);
            pAno = Convert.ToInt32(this.Request.QueryString["ano"]);
            pDesc = this.Request.QueryString["item"];
            this.grvBt.Attributes.Add("bordercolor", "black");
            this.grvBt.DataSource = this._vDataSet.fRestricaoBt(pLoja, pForn, pMontadora, pGrupo, pSubGrupo, pVeiculo, pModelo, pMotor, pAno, pDesc);
            this.grvBt.DataBind();
            if (this.grvBt.Rows.Count == 0)
            {
                this.grvBt.BorderWidth = 0;
            }

        }
    }
}