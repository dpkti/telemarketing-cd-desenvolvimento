﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RestricaoBt.aspx.cs" Inherits="Aplicacao.RestricaoBt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .th
        {
            position: relative;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function F(a, b) {
            //alert(a);
            //document.location.reload(true);
        }

    </script>
</head>
<body scroll="no" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
    <hr style="color: blue; height: 3px" />
    <div style="height: 370px; overflow: auto; width: 100%;">
        <asp:GridView ID="grvBt" runat="server" AutoGenerateColumns="False" BackColor="#FFFFCD"
            BorderColor="Black" BorderWidth="1px" Font-Names="Arial" Font-Size="XX-Small" EmptyDataRowStyle-BorderWidth="0"
            CellPadding="1" EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-Font-Size="Large">
            <AlternatingRowStyle BackColor="#F0F0F0" BorderColor="Black" BorderWidth="1px" />
            <Columns>
                <asp:TemplateField HeaderText="Codigo de F&#225;brica">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("COD_FABRICA")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="14%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descri&#231;&#227;o do Item">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("DESC_ITEM")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="14%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Aplica&#231;&#227;o">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("DESC_APLICACAO")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="29%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sigla">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("sigla")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="6%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Restri&#231;&#227;o por Ve&#237;c.">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("RESTRICAO_2")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="13%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Restri&#231;&#227;o por Prod.">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%# Eval("RESTRICAO")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="13%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Qtde. Disp.">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("QTD_DISP")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="8%" HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CDs">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("ESTOQ")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="3%" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                A Consulta Não Retornou Dados
            </EmptyDataTemplate>
            <HeaderStyle BackColor="#0099CC" ForeColor="White" CssClass="th" />
        </asp:GridView>
    </div>
    </form>
</body>
</html>
