﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OracleClient;
using System.Collections;

namespace Aplicacao.Class
{
    public class MaxxDataSet
    {
        // Fields
        private conexao _vAcessaDados = new conexao();

        // Methods
        public DataTable fRestricao(int pLoja, int pForn, string pFabr, string pDesc)
        {
            ArrayList pParametros = new ArrayList();
            DataTable table = new DataTable();
            Parametro parametro = new Parametro("P_CURSOR", ParameterDirection.Output, DBNull.Value, OracleType.Cursor, 0);
            Parametro parametro6 = new Parametro("P_COD_LOJA", ParameterDirection.Input, pLoja, OracleType.Int16, 0);
            Parametro parametro5 = new Parametro("P_COD_FORN", ParameterDirection.Input, pForn, OracleType.Int16, 0);
            Parametro parametro4 = new Parametro("P_COD_FABRICA", ParameterDirection.Input, pFabr.Replace("@", "%"), OracleType.VarChar, 0);
            Parametro parametro2 = new Parametro("P_DESC_ITEM", ParameterDirection.Input, pDesc.Replace("@", "%"), OracleType.VarChar, 0);
            Parametro parametro3 = new Parametro("P_ERRO", ParameterDirection.Output, 0, OracleType.Int16, 0);
            pParametros.Add(parametro);
            pParametros.Add(parametro6);
            pParametros.Add(parametro5);
            pParametros.Add(parametro4);
            pParametros.Add(parametro2);
            pParametros.Add(parametro3);
            return this._vAcessaDados.RetornarDataTable("producao.pck_vda320_novo.pr_restricao_temp", ref pParametros);
        }

        public DataTable fRestricaoBt(int pLoja, int pForn, int pMontadora, int pGrupo, int pSubGrupo, int pVeiculo, int pModelo, int pMotor, int pAno, string pDesc)
        {
            ArrayList pParametros = new ArrayList();
            DataTable table = new DataTable();
            Parametro parametro2 = new Parametro("P_CURSOR", ParameterDirection.Output, DBNull.Value, OracleType.Cursor, 0);
            Parametro parametro6 = new Parametro("P_COD_LOJA", ParameterDirection.Input, pLoja, OracleType.Int16, 0);
            Parametro parametro4 = new Parametro("P_COD_FORN", ParameterDirection.Input, pForn, OracleType.Int16, 0);
            Parametro parametro8 = new Parametro("P_MONTADORA", ParameterDirection.Input, pMontadora, OracleType.Int16, 0);
            Parametro parametro5 = new Parametro("P_GRUPO", ParameterDirection.Input, pGrupo, OracleType.Int16, 0);
            Parametro parametro10 = new Parametro("P_SUBGRUPO", ParameterDirection.Input, pSubGrupo, OracleType.Int16, 0);
            Parametro parametro11 = new Parametro("P_VEICULO", ParameterDirection.Input, pVeiculo, OracleType.Int16, 0);
            Parametro parametro7 = new Parametro("P_MODELO", ParameterDirection.Input, pModelo, OracleType.Int16, 0);
            Parametro parametro9 = new Parametro("P_MOTOR", ParameterDirection.Input, pMotor, OracleType.Int16, 0);
            Parametro parametro = new Parametro("P_ANO", ParameterDirection.Input, pAno, OracleType.Int16, 0);
            Parametro parametro3 = new Parametro("P_DESC_ITEM", ParameterDirection.Input, pDesc.Replace("@", "%"), OracleType.VarChar, 0);
            pParametros.Add(parametro2);
            pParametros.Add(parametro6);
            pParametros.Add(parametro4);
            pParametros.Add(parametro8);
            pParametros.Add(parametro5);
            pParametros.Add(parametro10);
            pParametros.Add(parametro11);
            pParametros.Add(parametro7);
            pParametros.Add(parametro9);
            pParametros.Add(parametro);
            pParametros.Add(parametro3);
            return this._vAcessaDados.RetornarDataTable("producao.pck_vda320_novo.pr_restricao_temp_3", ref pParametros);
        }
    }

 

}