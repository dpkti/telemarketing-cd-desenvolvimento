﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OracleClient;
using System.Runtime.CompilerServices;
using System.Collections;
using System.Configuration;

namespace Aplicacao.Class
{
public class conexao
{
    // Fields
    private OracleConnection conn;
    private IEnumerator enumerator;
    private OracleDataAdapter adapter;

    // Methods
    public void MontaParametro(ref OracleDataAdapter da, ArrayList pParametros)
    {        
        try
        {
            enumerator = pParametros.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Parametro current = (Parametro) enumerator.Current;
                OracleParameter parameter = new OracleParameter {
                    ParameterName = current.Nome,
                    Value = RuntimeHelpers.GetObjectValue(current.Valor),
                    OracleType = current.DbType,
                    Direction = current.Direcao,
                    Size = current.Size
                };
                da.SelectCommand.Parameters.Add(parameter);
            }
        }
        finally
        {
            if (enumerator is IDisposable)
            {
                (enumerator as IDisposable).Dispose();
            }
        }
    }

    public DataTable RetornarDataTable(string pComandoSql, ref ArrayList pParametros)
    {        
        string connectionString = ConfigurationManager.ConnectionStrings["CONN"].ConnectionString;
        this.conn = new OracleConnection(connectionString);
        DataTable dataTable = new DataTable();
        try
        {
            this.conn.Open();
            adapter = new OracleDataAdapter(pComandoSql, this.conn) {
                SelectCommand = { CommandType = CommandType.StoredProcedure }
            };
            this.MontaParametro(ref adapter, pParametros);
            adapter.Fill(dataTable);
        }
        catch (Exception exception1)
        {
            Exception exception = exception1;
            throw exception;
        }
        finally
        {
            this.conn.Close();
            this.conn.Dispose();
            adapter.Dispose();
        }
        return dataTable;
    }
}


}