﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OracleClient;
using System.Runtime.CompilerServices;

namespace Aplicacao.Class
{
    public class Parametro
    {
        // Fields
        public OracleType DbType;
        public ParameterDirection Direcao;
        public string Nome;
        public int Size;
        public object Valor;

        // Methods
        public Parametro(string pnomeParam, ParameterDirection pDirecao, object pvalor, OracleType pDbType = 0, int pSize = 0)
        {
            this.Nome = pnomeParam;
            this.Direcao = pDirecao;
            this.Valor = RuntimeHelpers.GetObjectValue(pvalor);
            this.DbType = pDbType;
            this.Size = pSize;
        }
    }


}