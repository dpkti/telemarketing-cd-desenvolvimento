﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Restricao.aspx.cs" Inherits="Aplicacao.Restricao" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .th
        {
            position: relative;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function F(a, b) {
            //alert(a);
            //document.location.reload(true);
        }

    </script>
</head>
<body scroll="no" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
    <hr style="color: blue; height: 3px" />
    <div style="height: 370px; overflow: auto; width: 100%;">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="#FFFFCD"
            BorderColor="Black" BorderWidth="1px" Font-Names="Arial" Font-Size="XX-Small" 
            CellPadding="1" EnableTheming="True" EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-Font-Size="Large">
            <AlternatingRowStyle BackColor="#F0F0F0" BorderColor="Black" BorderWidth="1px" />
            <Columns>
                <asp:TemplateField HeaderText="C&oacute;digo de F&#225;brica">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("COD_FABRICA")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="15%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descri&#231;&#227;o do Item">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("DESC_ITEM")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="20%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Aplica&#231;&#227;o">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("DESC_APLICACAO")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="44%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sigla">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("sigla")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="10%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Qtde. Disp.">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("QTD_DISP")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="8%" HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CDs">
                    <ItemTemplate>
                        <a href="javascript:F('<%# Eval("COD_FABRICA") %>',<%# Eval("COD_FORNECEDOR") %>);"
                            style="text-decoration: none; color: #000;">
                            <%#Eval("ESTOQ")%></a>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" Width="3%" />
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#0099CC" ForeColor="White" CssClass="th" />
            <EmptyDataTemplate>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                A Consulta Não Retornou Dados
            </EmptyDataTemplate>
            <EmptyDataRowStyle BackColor="White" Font-Size="Large" />
        </asp:GridView>
    </div>
    </form>
</body>
</html>
