﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aplicacao.Class;

namespace Aplicacao
{
    public partial class Restricao : System.Web.UI.Page
    {
        public MaxxDataSet _vDataSet = new MaxxDataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            int pLoja;
            int pForn;
            string pFabr = "";
            string pDesc = "";
            pLoja = Convert.ToInt32(this.Request.QueryString["loja"]);
            pForn = Convert.ToInt32(this.Request.QueryString["forn"]);
            pFabr = this.Request.QueryString["fabr"];
            pDesc = this.Request.QueryString["item"];
            this.GridView1.Attributes.Add("bordercolor", "black");
            this.GridView1.DataSource = this._vDataSet.fRestricao(pLoja, pForn, pFabr, pDesc);
            this.GridView1.DataBind();
            if (this.GridView1.Rows.Count == 0)
            {
                this.GridView1.BorderWidth = 0;
            }

        }
    }
}