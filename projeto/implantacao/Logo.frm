VERSION 5.00
Object = "{10814058-0E52-11D4-AC0C-00E07D76E465}#4.1#0"; "IrregForm.ocx"
Begin VB.Form frmLogo 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   0  'None
   ClientHeight    =   3090
   ClientLeft      =   2400
   ClientTop       =   1620
   ClientWidth     =   3750
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   Picture         =   "Logo.frx":0000
   ScaleHeight     =   3090
   ScaleWidth      =   3750
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ConneticaIrregForm.IrregFormCtl IrregFormCtl1 
      Height          =   480
      Left            =   540
      TabIndex        =   0
      Top             =   45
      Width           =   480
      _ExtentX        =   847
      _ExtentY        =   847
      AllowFormDrag   =   0   'False
      TransparentColor=   16777215
   End
   Begin VB.Timer Timer1 
      Interval        =   4000
      Left            =   60
      Top             =   60
   End
End
Attribute VB_Name = "frmLogo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmLogo
' Author    : c.samuel.oliveira
' Date      : 11/01/16
' Purpose   : TI-3880 - retirar vendas.mdb
'---------------------------------------------------------------------------------------

Option Explicit

Private Sub Form_Load()

          Dim ss As Object
          Dim vend As Object

1         On Error GoTo Trata_Erro

'          If UCase(App.Path) <> "H:\ORACLE\SISTEMAS\VB\32BITS" Then
'            MsgBox "O Telemarketing n�o deve ser executado atrav�s deste local." & vbCrLf & _
'            "Entre em contato com ServiceDesk. O sistema ser� encerrado"
'            End
'          End If
2         vCarregando = True

3         'Me.Visible = True

4         Me.Refresh

5         If App.PrevInstance Then
6             MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
7             End
8         End If
          
          'TI-3880
9         'Call Get_CD(Mid(App.Path, 1, 1))
          lngCD = Mid(Command$, InStr(1, Command$, "CD") + 2, 2)
          strTp_banco = Mid(Command$, InStr(1, Command$, "CD") + 4)
          'FIM TI-3880
10        If strTp_banco = "U" Then
11            strTabela_Banco = "PRODUCAO."
12        Else
13            strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
14        End If

          'strTp_banco = "M"
          'strTabela_Banco = "DEP" & Format(15, "00") & "."

15        fl_banco = 0
          
16        If Left(Trim(Command$), 1) = "V" Then
            Dim vCmd As Variant
17          vCmd = Split(Command$, "=")
            If UBound(vCmd) >= 1 Then
18             sCOD_VEND = CInt(vCmd(1))
            Else
19             sCOD_VEND = 599 'TI-4046
            End If
          Else
20           sCOD_VEND = CInt(BuscaUSERNAME)
          End If

          'Producao
21        strPath = "H:\ORACLE\DADOS\32BITS\"
22        strPathUsuario = "G:\USR\" & sCOD_VEND & "\"
23        strPath_Foto = "H:\oracle\dados\fotos\"

          '********************************************************************
          'Antes de gerar o Executavel verifique se vGERARLOG est� como FALSE e
          'o sCOD_VEND est� comentado
          '********************************************************************

24        Set vSessao = CreateObject("oracleinprocserver.xorasession")

          'Producao
25        Set oradatabase = vSessao.OpenDatabase("CD" & Format(lngCD, "00"), "tlm_" & sCOD_VEND & "/PROD", 0&)
          'Set oradatabase = vSessao.OpenDatabase("CD04", "vda020/PROD", 0&)

          'Desenvolvimento
          'Set oradatabase = vSessao.OpenDatabase("SDPKT", "tlm_" & sCOD_VEND & "/PROD", 0&)
          'Set oradatabase = vSessao.OpenDatabase("CDDPKT", "vda020/PROD", 0&)
          
26        Set vBanco = oradatabase
27        Set OraParameters = oradatabase.Parameters

          'psergio - 20/03/12
          'RTI-5 - 50. Eliminar o erro de PK no cadastro de NF de Fornecedor
          'Valida o Vendedor

          Criar_Cursor
          OraParameters.Remove "repres"
          OraParameters.Add "repres", sCOD_VEND, 1
    
          'Criar_Cursor
          oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_Select_Representante(:vCursor, :Repres); END;"
          Set vend = oradatabase.Parameters("vCursor").Value
    
          'Vendedor tem permiss�o para acessar o sistema VDA230
          If vend.EOF Or vend.BOF Then
              MsgBox "Usu�rio n�o tem permiss�o para acessar o sistema.", vbInformation, "VDA230"
              End
          End If

          'FIM valida��o Vendedor

28        Criar_Cursor

29        OraParameters.Remove "cod_vend"
30        OraParameters.Add "cod_vend", Val(sCOD_VEND), 1

31        oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.Pr_segmento_repres(:vCursor, :cod_vend); END;"
32        Set ss = oradatabase.Parameters("vCursor").Value

33        If ss.EOF And ss.BOF Then
34            intSegmento_Vend = 1
35        Else
36            intSegmento_Vend = IIf(IsNull(ss!cod_tipo_segmento), 1, ss!cod_tipo_segmento)
37        End If

          'SDS2589  Ricardo Gomes
          'Busca a chave para que define se a consulta de liga��o
          'ser� feita com join com a tabela r_inteligencia_mercado
          vChave = Module1.Pegar_VL_Parametro("BUSCA_META")
          
          'EDUARDO - 15/05/2007
38        Criar_Cursor

39        Pegar_Valores_Parametros

40        'Set dbAccess = OpenDatabase("c:\" & "VENDAS.MDB") TI-3880
42        'Set dbAccess = OpenDatabase("C:\Work\SDS2589\VENDAS.MDB") 'edudiogo voltar

41        fl_banco = 1

        vTrabalharConectado = IIf(Val(Parametros.Ativo) = 1, True, False)

43        If vTrabalharConectado = True And Left(Trim(Command$), 1) <> "V" And Val(vRedeAutoZ) <> lngCD Then 'TI-3880 cmd TI-6870
44            Unload Me
45            frmRamal.cmdVoltar_Click 'TI-6660
46            vCarregando = False
47        Else
48            vCarregando = False
49            Unload Me
50            MDIForm1.Show
51        End If

Trata_Erro:
52        If Err = 13 Then
53            MessageBox 0, "PARA EXECUTAR ESTE PROGRAMA � PRECISO ESTAR LOGADO" & vbCrLf & "NA REDE COMO UM TLMKT (C�DIGO DE VENDA).", "ATEN��O", &H40000 + 0
54            End
55        ElseIf Err = 440 Then
56            If Err.Description Like "*ORA-02391*" Then
57                MessageBox 0, "ESTE USU�RIO J� EST� LOGADO NO BANCO DE DADOS, " & vbCrLf & " LIGUE NO CPD E SOLICITE PARA MATAR O USU�RIO: " & vbCrLf & "TLM_" & sCOD_VEND, "ATEN��O", &H40000 + 0
58                End
59            Else
60                MessageBox 0, "Sub LOGO_LOAD" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "DEscricao:" & Err.Description & vbCrLf & "Linha: " & Erl, "Aten��o", &H40000 + 0
61                Resume Next
62            End If
63        ElseIf Err.Number <> 0 Then
64            MessageBox 0, "Sub LOGO_LOAD" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "DEscricao:" & Err.Description & vbCrLf & "Linha: " & Erl, "Aten��o", &H40000 + 0
65        End If

End Sub

Private Sub Timer1_Timer()
    Unload Me
End Sub
