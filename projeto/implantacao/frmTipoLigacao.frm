VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmTipoLigacao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tipo de Liga��o"
   ClientHeight    =   1425
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   4920
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1425
   ScaleWidth      =   4920
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer_Restante 
      Interval        =   1000
      Left            =   180
      Top             =   840
   End
   Begin VB.Timer Timer1 
      Left            =   660
      Top             =   840
   End
   Begin Bot�o.cmd cmdTrabalho 
      Height          =   825
      Left            =   30
      TabIndex        =   1
      Top             =   30
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "&Trabalho"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTipoLigacao.frx":0000
      PICN            =   "frmTipoLigacao.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdParticular 
      Height          =   825
      Left            =   1170
      TabIndex        =   2
      Top             =   30
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "&Externa"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTipoLigacao.frx":08F6
      PICN            =   "frmTipoLigacao.frx":0912
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   825
      Left            =   3750
      TabIndex        =   3
      Top             =   30
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "&Cancelar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTipoLigacao.frx":11EC
      PICN            =   "frmTipoLigacao.frx":1208
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   5
      Top             =   930
      Width           =   4800
      _ExtentX        =   8467
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdExternaCliente 
      Height          =   825
      Left            =   2310
      TabIndex        =   6
      Top             =   30
      Width           =   1395
      _ExtentX        =   2461
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "E&xterna Cliente"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTipoLigacao.frx":1AE2
      PICN            =   "frmTipoLigacao.frx":1AFE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblTempoRestante 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   4410
      TabIndex        =   4
      Top             =   1050
      Width           =   435
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Escolha o Tipo de Liga��o em:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1440
      TabIndex        =   0
      Top             =   1080
      Width           =   2940
   End
End
Attribute VB_Name = "frmTipoLigacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmTipoLigacao
' Author    : c.samuel.oliveira
' Date      : 12/09/18
' Purpose   : TI-6660
'---------------------------------------------------------------------------------------
Option Explicit

Dim Tempo_Restante As Double

Private Sub cmdCancelar_Click()
    
    Timer1.Interval = 0
    'MDIForm1.Desligar TI-6660
    Unload Me
    
End Sub


Private Sub cmdExternaCliente_Click()
    
    frmVenda.cmdAtribuir.Visible = True

    Timer1.Interval = 0

    NovaLigacao.Particular = False

    frmVenda.cmdDiscar.Tag = ""
    
    If vConectadoHP8000 Then
        MDIForm1.Discar
    End If
    
    frmDiscando.lblNumFone = frmVenda.TxtRamalVozFone
    frmDiscando.Show
    StayOnTop frmDiscando

    Unload Me
    
End Sub

Private Sub cmdParticular_Click()
    Timer1.Interval = 0
    NovaLigacao.Particular = True

    If vConectadoHP8000 Then
        MDIForm1.Discar
    End If

    frmVenda.cmdDiscar.Tag = ""
    frmDiscando.lblNumFone = frmVenda.TxtRamalVozFone
    frmDiscando.Show
    StayOnTop frmDiscando

    Unload Me
    
End Sub

Private Sub cmdTrabalho_Click()
1        On Error GoTo Erro_VDA230

         Dim vResposta As String

2         Timer1.Interval = 0
3         NovaLigacao.Particular = False

4         If Form_Aberto("frmtipoLigacao") = True Then Unload frmTipoLigacao

5         If frmVenda.txtCOD_CLIENTE = "" Then
6             'MDIForm1.Desligar TI-6660
7             NonStayOnTop Me
8             MessageBox 0, "Selecione um cliente.", "Aten�ao", &H40000 + 0
9             StayOnTop Me

10            frmVenda.txtCOD_CLIENTE.SetFocus
11            Exit Sub
12        Else

13           MDIForm1.Abrir_Tela_Adicionar_Fone

14        End If

15        Unload Me
Erro_VDA230:
16        If Err.Number = 5 Then
17            Resume Next
18        ElseIf Err.Number <> 0 Then
19            MessageBox 0, "cmdTrabalho_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & "Linha:" & Erl, "Aten�ao", &H40000 + 0
20        End If

End Sub

Private Sub Form_Load()

    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2

    Tempo_Restante = 8000
    Timer1.Interval = 8000
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmTipoLigacao = Nothing
 '   frmVenda.Refresh_Venda
End Sub

Private Sub Timer_Restante_Timer()
    Tempo_Restante = CDbl(Tempo_Restante) - CDbl(Timer_Restante.Interval)
    lblTempoRestante = Tempo_Restante / 1000
End Sub

Private Sub Timer1_Timer()
    'MDIForm1.Desligar TI-6660
    Unload Me
End Sub


