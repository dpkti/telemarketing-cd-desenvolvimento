VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmMIXItem 
   Caption         =   "MIX Item"
   ClientHeight    =   6090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13065
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6090
   ScaleWidth      =   13065
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtQtdCombo 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   12490
      MaxLength       =   2
      TabIndex        =   8
      Top             =   950
      Width           =   495
   End
   Begin VB.CheckBox chkMostrarPreco 
      Caption         =   "Mostrar pre�o"
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   3600
      TabIndex        =   0
      Top             =   960
      Width           =   1455
   End
   Begin MSGrid.Grid grdItem 
      Height          =   4455
      Left            =   0
      TabIndex        =   1
      Top             =   1260
      Width           =   13005
      _Version        =   65536
      _ExtentX        =   22939
      _ExtentY        =   7858
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmMIXItem.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   2
      ToolTipText     =   "Voltar"
      Top             =   10
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMIXItem.frx":001C
      PICN            =   "frmMIXItem.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   0
      TabIndex        =   3
      Top             =   840
      Width           =   13050
      _ExtentX        =   23019
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdGerarPedido 
      Height          =   675
      Left            =   12280
      TabIndex        =   13
      ToolTipText     =   "Gerar Pedido"
      Top             =   10
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMIXItem.frx":0D12
      PICN            =   "frmMIXItem.frx":0D2E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Total ICM Retido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3720
      TabIndex        =   12
      Top             =   5790
      Width           =   1455
   End
   Begin VB.Label lblTotalRetidoCombo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   5340
      TabIndex        =   11
      Top             =   5760
      Width           =   1935
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Valor do Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   10
      Top             =   5790
      Width           =   1350
   End
   Begin VB.Label lblTotalCombo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1500
      TabIndex        =   9
      Top             =   5760
      Width           =   1935
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Quantidade de Combos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   10380
      TabIndex        =   7
      Top             =   990
      Width           =   1980
   End
   Begin VB.Label lblMSG 
      AutoSize        =   -1  'True
      Caption         =   "N�O H� ITENS PARA O MIX DE PRODUTOS ESCOLHIDO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   3480
      TabIndex        =   6
      Top             =   360
      Width           =   5010
   End
   Begin VB.Label lblNumMix 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1530
      TabIndex        =   5
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "N�mero de MIX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   4
      Top             =   990
      Width           =   1320
   End
End
Attribute VB_Name = "frmMIXItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmMIXItem
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 25/10/16
' Purpose   : TI-5398
'---------------------------------------------------------------------------------------
' Module    : frmMIXItem
' Author    : c.samuel.oliveira
' Date      : 27/01/16
' Purpose   : TI-3951
'---------------------------------------------------------------------------------------

Private Sub chkMostrarPreco_Click()
           
    chkMostrarPreco.Enabled = False
    grdItem.Enabled = False
    cmdSair.Enabled = False
    
    If chkMostrarPreco.Value = 1 Then
        flgCalculaMix = 1

    Else
        flgCalculaMix = 0
    End If
    
    'TI-5398
    If flgCalculaMix = 1 And strTipoMix = "COMBO" Then
        cmdGerarPedido.Enabled = True
        txtQtdCombo.Enabled = True
    ElseIf flgCalculaMix = 0 And strTipoMix = "COMBO" Then
        cmdGerarPedido.Enabled = False
        txtQtdCombo.Enabled = False
    End If
    'FIM TI-5398
    
    CarregarItem flgCalculaMix
    cmdSair.Enabled = True
    grdItem.Enabled = True
    chkMostrarPreco.Enabled = True
End Sub

Private Sub cmdSair_Click()
    lngNUM_MIX = 0
    flgCalculaMix = 0
    strTipoMix = "" 'TI-5398
    
    Unload Me
    Set frmMIXItem = Nothing
End Sub

Private Sub Form_Load()
    
    'TI-5398
    If strTipoMix = "COMBO" Then
        Label1.Visible = True
        Label4.Visible = True
        Label6.Visible = True
        lblTotalCombo.Visible = True
        lblTotalRetidoCombo.Visible = True
        txtQtdCombo.Visible = True
        txtQtdCombo.Enabled = False
        cmdGerarPedido.Visible = True
        cmdGerarPedido.Enabled = False
    Else
        Label1.Visible = False
        Label4.Visible = False
        Label6.Visible = False
        lblTotalCombo.Visible = False
        lblTotalRetidoCombo.Visible = False
        txtQtdCombo.Visible = False
        cmdGerarPedido.Visible = False
    End If
    'FIM TI-5398
    
    If flgCalculaMix = 1 Then
        chkMostrarPreco.Value = 1
    Else
        chkMostrarPreco.Value = 0
    End If
    
    CarregarItem flgCalculaMix

End Sub

Private Sub CarregarItem(pMostrarPreco As Byte)
    On Error GoTo TrataErro

    Dim ss As Object
    Dim i As Long
    Dim strCod_Deposito As String
    Dim strCod_Filial As String
    Dim strCod_DPK As String
    Dim strCod_Fornecedor As String
    Dim strCod_Fabrica As String
    Dim strUf_Origem As String
    Dim strUf_Destino As String
    Dim strCodigo_Cliente As String
    Dim strCNPJ As String
    Dim strCod_Plano As String
    Dim dblPc_Acrescimo As Double
    Dim dblPc_Desconto As Double
    Dim strTipo_Menor_Preco As String
    'TI-5398
    Dim dblPrecoUnitario As Double
    Dim dblPrecoTotal As Double
    Dim dblIcmRetido As Double
    Dim dblTotalIcmRetido As Double
    Dim vCodTributacao As Byte
    Dim strTpCliente As String
    Dim dblIcmRetidoPE As String
    Dim dblPc_DescUF As Double
    Dim dblPc_Desc3 As Double
    Dim dblPc_DescICM As Double
    'FIM TI-5398
    
    lblMSG.Visible = False
    
    Screen.MousePointer = vbHourglass

    oradatabase.Parameters.Remove "PM_NUM_MIX"
    oradatabase.Parameters.Add "PM_NUM_MIX", lngNUM_MIX, 1
    
    vSql = "Begin producao.pck_vda230.PR_SEL_MIX_ITEM(:PM_NUM_MIX,:vCursor);END;"

    'Criar_Cursor
    oradatabase.ExecuteSQL vSql
    Set ss = oradatabase.Parameters("vCursor").Value

    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        lblMSG.Visible = True
        Exit Sub
    End If
    
    lblNumMix.Caption = CStr(lngNUM_MIX)
    
    'carrega dados
    With grdItem
        .Cols = 20 'TI-5398
        .rows = ss.RecordCount + 1
        .ColWidth(0) = 1500
        .ColWidth(1) = 1000
        .ColWidth(2) = 800
        .ColWidth(3) = 1200
        .ColWidth(4) = 2000
        .ColWidth(5) = 3070
        .ColWidth(6) = 1000
        .ColWidth(7) = 1000
        .ColWidth(8) = 1000
        'TI-5398
        .ColWidth(9) = 800
        .ColWidth(10) = 800
        .ColWidth(11) = 800
        .ColWidth(12) = 800
        .ColWidth(13) = 800
        .ColWidth(14) = 800
        .ColWidth(15) = 800
        .ColWidth(16) = 800
        .ColWidth(17) = 800
        .ColWidth(18) = 800
        .ColWidth(19) = 800
        'FIM TI-5398
        
        .Row = 0
        .Col = 0
        .Text = "Loja"
        .Col = 1
        .Text = "C�d. DPK"
        .Col = 2
        .Text = "C�d. Forn."
        .Col = 3
        .Text = "Forn."
        .Col = 4
        .Text = "C�d. F�brica"
        .Col = 5
        .Text = "Desc. Item"
        .Col = 6
        .Text = "Qtde. Disp."
        .Col = 7
        .Text = "Qtde. MIX"
        .Col = 8
        .Text = "Pre�o Venda"
        'TI-5398
        .Col = 9
        .Text = "Retido"
        .Col = 10
        .Text = "Cod. Trib."
        .Col = 11
        .Text = "Cod. Trib. IPI"
        .Col = 12
        .Text = "Tabela"
        .Col = 13
        .Text = "Pre�o Venda"
        .Col = 14
        .Text = "Desc1"
        .Col = 15
        .Text = "Desc2"
        .Col = 16
        .Text = "Desc3"
        .Col = 17
        .Text = "DescICM"
        .Col = 18
        .Text = "Pc. IPI"
        .Col = 19
        .Text = "Peso"
        'FIM TI-5398
        
        For i = 1 To .rows - 1
            .Row = i
              
            .Col = 0
            .Text = ss("NOMELOJA")
            .Col = 1
            .Text = ss("CODDPK")
            .Col = 2
            .Text = ss("CODFORN")
            .Col = 3
            .Text = ss("FORN")
            .Col = 4
            .Text = ss("CODFABRICA")
            .Col = 5
            .Text = ss("DESCITEM")
            .Col = 6
            .ColAlignment(6) = 1
            .Text = ss("QTD_DISP")
            .Col = 7
            .ColAlignment(7) = 1
            .Text = ss("QTDE")

            If pMostrarPreco = 1 Then
            
                'busca fator de descontos para calculo...
                strCod_Deposito = Mid(Trim(frmVenda.cboDeposito.Text), 1, 2)
                strCod_Filial = Mid(Trim(frmVenda.cboFilial), 1, 4)
                strCod_DPK = ss!CODDPK
                strCod_Fornecedor = ss!CODFORN
                strCod_Fabrica = ss!CODFABRICA
                strUf_Origem = Retornar_Uf_Origem(strCod_Deposito)
                strUf_Destino = frmVenda.cboUf.Text
                strCodigo_Cliente = frmVenda.txtCOD_CLIENTE.Text
                strCNPJ = frmVenda.txtCGC.Text
                strCod_Plano = frmVenda.txtCOD_PLANO.Text
                dblPc_Acrescimo = CDbl(frmVenda.txtPC_ACRES_FIN_DPK.Text)
                dblPc_Desconto = CDbl(frmVenda.txtPC_DESC_FIN_DPK.Text)
                'TI-5398
                strTpCliente = Left(frmVenda.lblTipoCliente.Caption, 1)
                
                dblCalculo = Calcular_Descontos(strCod_Deposito, _
                                                strCod_Filial, _
                                                strCod_DPK, _
                                                strCod_Fornecedor, _
                                                strCod_Fabrica, _
                                                strUf_Origem, _
                                                strUf_Destino, _
                                                strCodigo_Cliente, _
                                                strCNPJ, _
                                                strCod_Plano, _
                                                dblPc_Acrescimo, _
                                                dblPc_Desconto, _
                                                "NORMAL", _
                                                ss("PC_DESC1"), _
                                                ss("PC_DESC2"), _
                                                dblPc_DescUF, _
                                                dblPc_Desc3, _
                                                dblPc_DescICM)
                                                 
                dblPrecoUnitario = Round(CDbl(ss("PRECO_VENDA") * dblCalculo), 2)
                If strTipoMix = "COMBO" Then
                    vCodTributacao = CarregarTributacao(Val(strCod_Deposito), Val(strCodigo_Cliente), ss("COD_TRIBUTACAO"), _
                                     strTpCliente, CStr(strInSuf), lngTare, CStr(ss("CLASS_FISCAL")))
                    
                    '---------------------------------------------------------------------------------
                    oradatabase.Parameters.Remove "loja"
                    oradatabase.Parameters.Add "loja", Val(strCod_Deposito), 1
                    oradatabase.Parameters.Remove "cliente"
                    oradatabase.Parameters.Add "cliente", Val(strCodigo_Cliente), 1
                    oradatabase.Parameters.Remove "dpk"
                    oradatabase.Parameters.Add "dpk", ss("CODDPK"), 1
                    oradatabase.Parameters.Remove "trib"
                    oradatabase.Parameters.Add "trib", vCodTributacao, 1
                    oradatabase.Parameters.Remove "preco"
                    oradatabase.Parameters.Add "preco", FmtBR(dblPrecoUnitario), 1
                    oradatabase.Parameters.Remove "retido"
                    oradatabase.Parameters.Add "retido", 0, 2
                    oradatabase.Parameters.Remove "vErro"
                    oradatabase.Parameters.Add "vErro", 0, 2
                    
                    vSql = "Begin producao.pck_vda230.pr_icms_retido(:loja,:cliente,:dpk,:trib,:preco,:retido,:vErro);END;"
                    
                    oradatabase.ExecuteSQL vSql
                    dblIcmRetido = oradatabase.Parameters("retido").Value
                    '---------------------------------------------------------------------------------
                    
                    If strUf_Origem = "PE" And strUf_Destino = "PE" And ss("REGIME") = 1 Then
                        dblPrecoUnitario = dblPrecoUnitario + dblIcmRetido
                    End If
                    
                    dblPrecoTotal = dblPrecoTotal + (dblPrecoUnitario * ss("QTDE"))
                    dblTotalIcmRetido = dblTotalIcmRetido + (dblIcmRetido * ss("QTDE"))
                End If
                .Col = 8
                .ColAlignment(8) = 1
                .Text = Format$(dblPrecoUnitario, "Standard")
                .Col = 9
                .ColAlignment(9) = 1
                .Text = Format$(dblIcmRetido, "Standard")
                .Col = 10
                .Text = ss("COD_TRIBUTACAO")
                .Col = 11
                .Text = ss("COD_TRIBUTACAO_IPI")
                .Col = 12
                .Text = ss("TABELA")
                .Col = 13
                .Text = Format$(ss("PRECO_VENDA"), "Standard")
                .Col = 14
                .Text = Format$(ss("PC_DESC1"), "Standard")
                .Col = 15
                .Text = Format$(dblPc_DescUF, "Standard")
                .Col = 16
                .Text = Format$(dblPc_Desc3, "Standard")
                .Col = 17
                .Text = Format$(dblPc_DescICM, "Standard")
                .Col = 18
                .Text = Format$(ss("PC_IPI"), "Standard")
                .Col = 19
                .Text = ss("PESO")
                'FIM TI-5398
                
            Else
            
                .Col = 8
                .Text = "-"
                
            End If
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
    'TI-5398
    lblTotalCombo.Caption = Format(dblPrecoTotal, "0.00")
    lblTotalRetidoCombo.Caption = Format(dblTotalIcmRetido, "0.00")
    'FIM TI-5398
    
    Screen.MousePointer = vbDefault

    Exit Sub

TrataErro:
    MessageBox 0, "Sub Form_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0

    Screen.MousePointer = vbDefault
End Sub


Private Sub grdItem_DblClick()

On Error GoTo TrataErro
    grdItem.Col = 1
    frmVenda.txtCOD_DPK = grdItem.Text
    frmVenda.txtCOD_DPK_LostFocus
    
    frmMIXItem.Hide
    Unload frmMIXProduto
    Set frmMIXProduto = Nothing
    
    Exit Sub
TrataErro:
    MessageBox 0, "Sub grdItem_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
End Sub
'TI-5398
Private Sub txtQtdCombo_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub
Private Sub txtQtdCombo_LostFocus()
    If Len(Trim(txtQtdCombo.Text)) = 0 Or Val(txtQtdCombo.Text) = 0 Then
        MessageBox 0, "Informe a Quantidade de Combos!", "Aten�ao", &H40000 + 0
        'If txtQtdCombo.Enabled Then txtQtdCombo.SetFocus
        Exit Sub
    End If
    Val (txtQtdCombo.Text)
    lblTotalCombo.Caption = Format(CDbl(lblTotalCombo.Caption) * Val(txtQtdCombo.Text), "0.00")
    lblTotalRetidoCombo.Caption = Format(CDbl(lblTotalRetidoCombo.Caption) * Val(txtQtdCombo.Text), "0.00")
    
End Sub
Private Sub cmdGerarPedido_Click()

On Error GoTo TrataErro
Dim dblNumPendente As Double
Dim cont As Long
Dim lngQuantidade As Long

    '---------------------------------------------------------------------------------
    'VALIDA��O
    If frmVenda.txtCOD_CLIENTE.Text = "" Or frmVenda.txtCOD_CLIENTE.Text = "0" Then
         MessageBox 0, "Selecione um cliente para fazer o pedido!", "Aten�ao", &H40000 + 0
         Exit Sub
    End If
    If Len(Trim(txtQtdCombo.Text)) = 0 Or Val(txtQtdCombo.Text) = 0 Then
         MessageBox 0, "Informe a Quantidade de Combos!", "Aten�ao", &H40000 + 0
         If txtQtdCombo.Enabled Then txtQtdCombo.SetFocus
         Exit Sub
    End If
    If MessageBox(0, "Confirma a gera��o do Pedido?", "Aten��o", &H40000 + 36) = vbNo Then
         Exit Sub
    End If

    '---------------------------------------------------------------------------------
    'INSERE CABE�ALHO DO PEDIDO NA VENDAS.PEDIDO
    oradatabase.Parameters.Remove "PM_COD_FILIAL"
    oradatabase.Parameters.Add "PM_COD_FILIAL", Mid(Trim(frmVenda.cboFilial), 1, 4), 1
    oradatabase.Parameters.Remove "PM_COD_LOJA"
    oradatabase.Parameters.Add "PM_COD_LOJA", Val(Mid(Trim(frmVenda.cboDeposito.Text), 1, 2)), 1
    oradatabase.Parameters.Remove "PM_FINALIZADO"
    oradatabase.Parameters.Add "PM_FINALIZADO", "N", 1
    oradatabase.Parameters.Remove "PM_DT_DIGITACAO"
    oradatabase.Parameters.Add "PM_DT_DIGITACAO", fFormataDataOracle(Date), 1
    oradatabase.Parameters.Remove "PM_COD_CLIENTE"
    oradatabase.Parameters.Add "PM_COD_CLIENTE", Val(frmVenda.txtCOD_CLIENTE.Text), 1
    oradatabase.Parameters.Remove "PM_COD_UF"
    oradatabase.Parameters.Add "PM_COD_UF", frmVenda.cboUf.Text, 1
    oradatabase.Parameters.Remove "PM_COD_REPRES"
    oradatabase.Parameters.Add "PM_COD_REPRES", lngCod_repres, 1
    oradatabase.Parameters.Remove "PM_COD_VEND"
    oradatabase.Parameters.Add "PM_COD_VEND", sCOD_VEND, 1
    oradatabase.Parameters.Remove "PM_PC_DESC_SUFRAMA"
    oradatabase.Parameters.Add "PM_PC_DESC_SUFRAMA", FmtBR(frmVenda.lblDescSuframa.Caption), 1
    oradatabase.Parameters.Remove "PM_COD_PLANO"
    oradatabase.Parameters.Add "PM_COD_PLANO", Val(frmVenda.txtCOD_PLANO.Text), 1
    oradatabase.Parameters.Remove "PM_PC_DESCONTO"
    oradatabase.Parameters.Add "PM_PC_DESCONTO", FmtBR(frmVenda.txtPC_DESC_FIN_DPK.Text), 1
    oradatabase.Parameters.Remove "PM_PC_ACRESCIMO"
    oradatabase.Parameters.Add "PM_PC_ACRESCIMO", FmtBR(frmVenda.txtPC_ACRES_FIN_DPK.Text), 1
    oradatabase.Parameters.Remove "PM_FL_DIF_ICM"
    oradatabase.Parameters.Add "PM_FL_DIF_ICM", IIf(frmVenda.chkDifIcm.Value = vbChecked, "S", "N"), 1
    oradatabase.Parameters.Remove "PM_NUM_PENDENTE"
    oradatabase.Parameters.Add "PM_NUM_PENDENTE", 0, 2
    oradatabase.Parameters.Remove "PM_COD_ERRO"
    oradatabase.Parameters.Add "PM_COD_ERRO", 0, 2
    oradatabase.Parameters.Remove "PM_TXT_ERRO"
    oradatabase.Parameters.Add "PM_TXT_ERRO", 0, 2

    oradatabase.ExecuteSQL "Begin producao.pck_vda230.PR_INS_VENDAS_PEDIDO(:PM_COD_FILIAL,:PM_COD_LOJA,:PM_FINALIZADO,:PM_DT_DIGITACAO," & _
                           ":PM_COD_CLIENTE,:PM_COD_UF,:PM_COD_REPRES,:PM_COD_VEND,:PM_PC_DESC_SUFRAMA,:PM_COD_PLANO, " & _
                           ":PM_PC_DESCONTO,:PM_PC_ACRESCIMO,:PM_FL_DIF_ICM,:PM_NUM_PENDENTE,:PM_COD_ERRO,:PM_TXT_ERRO);END;"
                           
    vErro = oradatabase.Parameters("PM_COD_ERRO").Value

    If vErro <> 0 Then
        MessageBox 0, "PR_INS_VENDAS_PEDIDO " & vbCrLf & oradatabase.Parameters("PM_TXT_ERRO") & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
        Exit Sub
    End If
    dblNumPendente = oradatabase.Parameters("PM_NUM_PENDENTE").Value
    '---------------------------------------------------------------------------------
    '---------------------------------------------------------------------------------
    'INSERE ITENS DO PEDIDO NA VENDAS.ITEM_PEDIDO
    With grdItem
        For cont = 1 To (.rows - 1)
                
            .Row = cont
            .Col = 7
            lngQuantidade = Val(.Text) * Val(txtQtdCombo.Text)
            If lngQuantidade > 9999 Then lngQuantidade = 9999
            
            oradatabase.Parameters.Remove "PM_NUM_PENDENTE"
            oradatabase.Parameters.Add "PM_NUM_PENDENTE", dblNumPendente, 1
            oradatabase.Parameters.Remove "PM_COD_LOJA"
            oradatabase.Parameters.Add "PM_COD_LOJA", Val(Mid(Trim(frmVenda.cboDeposito.Text), 1, 2)), 1
            oradatabase.Parameters.Remove "PM_NUM_ITEM"
            oradatabase.Parameters.Add "PM_NUM_ITEM", cont, 1
            .Col = 1
            oradatabase.Parameters.Remove "PM_COD_DPK"
            oradatabase.Parameters.Add "PM_COD_DPK", CDbl(.Text), 1
            .Col = 2
            oradatabase.Parameters.Remove "PM_COD_FORN"
            oradatabase.Parameters.Add "PM_COD_FORN", Val(.Text), 1
            .Col = 4
            oradatabase.Parameters.Remove "PM_COD_FABRICA"
            oradatabase.Parameters.Add "PM_COD_FABRICA", Trim(.Text), 1
            .Col = 10
            oradatabase.Parameters.Remove "PM_COD_TRIB"
            oradatabase.Parameters.Add "PM_COD_TRIB", Val(.Text), 1
            .Col = 11
            oradatabase.Parameters.Remove "PM_COD_TRIB_IPI"
            oradatabase.Parameters.Add "PM_COD_TRIB_IPI", Val(.Text), 1
            oradatabase.Parameters.Remove "PM_QTDE"
            oradatabase.Parameters.Add "PM_QTDE", lngQuantidade, 1
            .Col = 12
            oradatabase.Parameters.Remove "PM_TABELA"
            oradatabase.Parameters.Add "PM_TABELA", .Text, 1
            .Col = 13
            oradatabase.Parameters.Remove "PM_PRECO_UNITARIO"
            oradatabase.Parameters.Add "PM_PRECO_UNITARIO", FmtBR(.Text), 1
            .Col = 8
            oradatabase.Parameters.Remove "PM_PRECO_LIQUIDO"
            oradatabase.Parameters.Add "PM_PRECO_LIQUIDO", FmtBR(.Text), 1
            .Col = 14
            oradatabase.Parameters.Remove "PM_DESC1"
            oradatabase.Parameters.Add "PM_DESC1", FmtBR(.Text), 1
            .Col = 15
            oradatabase.Parameters.Remove "PM_DESC2"
            oradatabase.Parameters.Add "PM_DESC2", FmtBR(.Text), 1
            .Col = 16
            oradatabase.Parameters.Remove "PM_DESC3"
            oradatabase.Parameters.Add "PM_DESC3", FmtBR(.Text), 1
            .Col = 17
            oradatabase.Parameters.Remove "PM_DESCICM"
            oradatabase.Parameters.Add "PM_DESCICM", FmtBR(.Text), 1
            .Col = 18
            oradatabase.Parameters.Remove "PM_PC_IPI"
            oradatabase.Parameters.Add "PM_PC_IPI", FmtBR(.Text), 1
            .Col = 19
            oradatabase.Parameters.Remove "PM_PESO"
            oradatabase.Parameters.Add "PM_PESO", Format(.Text, "00000.000"), 1
            .Col = 9
            oradatabase.Parameters.Remove "PM_VL_ICM_RETIDO"
            oradatabase.Parameters.Add "PM_VL_ICM_RETIDO", FmtBR(.Text), 1
            oradatabase.Parameters.Remove "PM_COD_ERRO"
            oradatabase.Parameters.Add "PM_COD_ERRO", 0, 2
            oradatabase.Parameters.Remove "PM_TXT_ERRO"
            oradatabase.Parameters.Add "PM_TXT_ERRO", 0, 2
        
            oradatabase.ExecuteSQL "Begin producao.pck_vda230.PR_INS_VENDAS_ITEM_PEDIDO(:PM_NUM_PENDENTE,:PM_COD_LOJA,:PM_NUM_ITEM, " & _
                                   ":PM_COD_DPK,:PM_COD_FORN,:PM_COD_FABRICA,:PM_COD_TRIB,:PM_COD_TRIB_IPI,:PM_QTDE,:PM_TABELA, " & _
                                   ":PM_PRECO_UNITARIO,:PM_PRECO_LIQUIDO,:PM_DESC1,:PM_DESC2,:PM_DESC3,:PM_DESCICM, " & _
                                   ":PM_PC_IPI,:PM_PESO,:PM_VL_ICM_RETIDO,:PM_COD_ERRO,:PM_TXT_ERRO);END;"
                                   
            vErro = oradatabase.Parameters("PM_COD_ERRO").Value
        
            If vErro <> 0 Then
                MessageBox 0, "PR_INS_VENDAS_ITEM_PEDIDO ITEM: " & cont & vbCrLf & oradatabase.Parameters("PM_TXT_ERRO") & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
                Exit Sub
            End If
            
        Next cont
    End With
    '---------------------------------------------------------------------------------
    
    lngNUM_PEDIDO = dblNumPendente
    lngCod_Loja = Val(Mid(Trim(frmVenda.cboDeposito.Text), 1, 2))
    
    frmFimPedido.Show
    StayOnTop frmFimPedido
    frmFimPedido.frmeCliente.Refresh
    frmFimPedido.Carregar_Dados
    frmFimPedido.txtMsgPedido = txtQtdCombo.Text & " COMBO(S): " & lblNumMix.Caption & ". "
        
    Unload frmMIXProduto
    Set frmMIXProduto = Nothing
    cmdSair_Click
    
    Exit Sub
TrataErro:
    MessageBox 0, "Sub cmdGerarPedido_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0

End Sub
'FIM TI-5398

