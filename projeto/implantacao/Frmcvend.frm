VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCVend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   5355
   ClientLeft      =   810
   ClientTop       =   1110
   ClientWidth     =   11355
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   Picture         =   "Frmcvend.frx":0000
   ScaleHeight     =   5355
   ScaleWidth      =   11355
   Begin MSFlexGridLib.MSFlexGrid grdLista 
      Height          =   2895
      Left            =   120
      TabIndex        =   15
      Top             =   1560
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   5106
      _Version        =   393216
      Rows            =   1
      FixedCols       =   0
      BackColorSel    =   16404530
      Enabled         =   -1  'True
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
   End
   Begin VB.Timer Timer1 
      Left            =   6600
      Top             =   960
   End
   Begin Bot�o.cmd SSCommand3 
      Height          =   555
      Left            =   9840
      TabIndex        =   13
      Top             =   4650
      Visible         =   0   'False
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   979
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmcvend.frx":0C42
      PICN            =   "Frmcvend.frx":0C5E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand1 
      Height          =   675
      Left            =   9720
      TabIndex        =   8
      ToolTipText     =   "Refazer Lista"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmcvend.frx":1938
      PICN            =   "Frmcvend.frx":1954
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1020
      TabIndex        =   0
      Top             =   1140
      Width           =   3855
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   60
      Picture         =   "Frmcvend.frx":1C6E
      ScaleHeight     =   255
      ScaleWidth      =   615
      TabIndex        =   4
      Top             =   4650
      Visible         =   0   'False
      Width           =   615
   End
   Begin Bot�o.cmd cmdLigar 
      Height          =   675
      Left            =   8040
      TabIndex        =   9
      ToolTipText     =   "Ligar para Cliente Selecionado"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmcvend.frx":1F78
      PICN            =   "Frmcvend.frx":1F94
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command1 
      Height          =   675
      Left            =   10560
      TabIndex        =   10
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   "Inicio"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmcvend.frx":286E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand2 
      Height          =   675
      Left            =   30
      TabIndex        =   11
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmcvend.frx":288A
      PICN            =   "Frmcvend.frx":28A6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand4 
      Height          =   675
      Left            =   10440
      TabIndex        =   12
      ToolTipText     =   "Contatos OK"
      Top             =   4560
      Visible         =   0   'False
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmcvend.frx":3580
      PICN            =   "Frmcvend.frx":359C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   14
      Top             =   720
      Width           =   10980
      _ExtentX        =   19368
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdAgend 
      Height          =   675
      Left            =   8880
      TabIndex        =   18
      ToolTipText     =   "Agendamento"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmcvend.frx":4276
      PICN            =   "Frmcvend.frx":4292
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOTAL DE LIGA��ES EXCEDENTES"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   5160
      TabIndex        =   17
      Top             =   4740
      Width           =   3195
   End
   Begin VB.Label lbl_qtde_lig_Excedentes 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   8460
      TabIndex        =   16
      Top             =   4680
      Width           =   615
   End
   Begin VB.Label lbl_qtde_lig_Executar 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   4080
      TabIndex        =   7
      Top             =   4680
      Width           =   615
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TOTAL DE LIGA��ES A EXECUTAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   900
      TabIndex        =   6
      Top             =   4740
      Width           =   3135
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "PESQUISA"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   180
      TabIndex        =   5
      Top             =   1230
      Width           =   810
   End
   Begin VB.Label lblPseudonimo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1740
      TabIndex        =   3
      Top             =   840
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCod_vend 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1020
      TabIndex        =   2
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "VENDEDOR"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   1
      Top             =   900
      Width           =   915
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "Ordenar"
      Visible         =   0   'False
      Begin VB.Menu mnuSort 
         Caption         =   "Dt. Lig."
         Index           =   0
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Motivo"
         Index           =   1
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Status"
         Index           =   2
      End
      Begin VB.Menu mnuSort 
         Caption         =   "C�d. Cliente"
         Index           =   3
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Cliente"
         Index           =   4
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Meta"
         Index           =   5
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Venda"
         Index           =   6
      End
      Begin VB.Menu mnuSort 
         Caption         =   "% Atingido"
         Index           =   7
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Dt. Ult. Compra"
         Index           =   8
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Contato"
         Index           =   9
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Cidade"
         Index           =   10
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Dt. Ult. Contato"
         Index           =   13
      End
   End
End
Attribute VB_Name = "frmCVend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmCVend
' Author    : c.samuel.oliveira
' Date      : 13/01/16
' Purpose   : TI-3880
'---------------------------------------------------------------------------------------

Option Explicit

'VARIAVEIS UTILIZADAS PARA PESQUISA
Dim CONTINUAPESQUISA As Integer
Dim OCORRENCIA As Integer
Dim CONTADOROCORRENCIA As Long
Dim TEXTOPARAPROCURA As String
Dim INICIOPESQUISA As Long
Dim INILINHA As Long

Dim vTempoClick As Byte
Public vFechar As Boolean
Public vCodCli As Double
Dim vLinha As Integer

Private Sub cmdAgend_Click()
    frmAgenda.Show
    StayOnTop frmAgenda
End Sub

'SDS2589 - Ricardo Gomes
'Trocado o nome do componente de
'GRID1 para grdLista
Private Sub cmdLigar_Click()

1         On Error GoTo Trata_Erro

          'Bot�o especifico para vChave = 'N'
          If vChave = "S" Then Exit Sub

2         grdLista.Col = 2
3         If vTrabalharConectado = True And Left(Trim(Command$), 1) <> "V" Then 'TI-3880 cmd
4             grdLista.Col = 1
5             vCodCli = grdLista.Text
6         End If

          Dim DDD As String
7         grdLista.Col = 8

8         If PEGAR_DDD(grdLista.Text) = True Then
9             DDD = ""
10        Else
11            grdLista.Col = 4
12            DDD = "0" & grdLista.Text
13        End If

14        grdLista.Col = 5

15        frmVenda.TxtRamalVozFone = DDD & grdLista.Text

16        frmVenda.cmdDiscar_Click

17        frmVenda.txtCOD_CLIENTE.SetFocus
18        frmVenda.txtCOD_CLIENTE = ""
19        frmVenda.txtCOD_CLIENTE = frmCVend.vCodCli
20        frmVenda.txtCOD_CLIENTE_LostFocus

21        Unload Me

Trata_Erro:
22        If Err.Number <> 0 Then
23            MessageBox 0, "Sub CmdLigar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
24        End If
End Sub

Private Sub Command1_Click()
    Call Text1_LostFocus
End Sub

Private Sub Form_Load()

    On Error GoTo Trata_Erro

    frmVenda.vFrmcVend = True

    vFechar = False
    Me.Top = ((MDIForm1.Height - Me.Height) / 2) - (MDIForm1.SSCommand1.Height)
    Me.Left = (MDIForm1.Width - Me.Width) / 2

    Dim ss As Object
    Dim SS1 As Object
    Dim Sql As String
    Dim i As Integer
    'William Leite
    'Dim vSqlDia As String
    'Dim VarRecDia As Object
    
    ''TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
    Dim lig_executar As Long
    Dim lig_excedente As Long
    Dim l As Long
    Dim motivo As String

    lig_executar = 0
    lig_excedente = 0
    
    cmdLigar.Visible = (vTrabalharConectado = True And Left(Trim(Command$), 1) <> "V") 'TI-3880 cmd

    Screen.MousePointer = 11
    frmCVend.lblcod_vend = sCOD_VEND

    frmCVend.Caption = "LISTA DE LIGA��ES A EXECUTAR"
    '10        Set OraParameters = oradatabase.Parameters
    OraParameters.Remove "vend"
    OraParameters.Add "vend", sCOD_VEND, 1

    OraParameters.Remove "chave"
    OraParameters.Add "chave", vChave, 1

    OraParameters.Remove "vErro"
    OraParameters.Add "vErro", 0, 2

    vSql = "Begin " & strTabela_Banco & "pck_vda630.pr_ligacao(:vCursor,:vend,:vErro,:chave);END;"

    'Criar_Cursor

    oradatabase.ExecuteSQL vSql
    Set VarRec = oradatabase.Parameters("vCursor").Value
    'oradatabase.Parameters.Remove ("vCursor")

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(Sql)
        Exit Sub
    End If
    
    If VarRec.EOF And VarRec.BOF Then
        NonStayOnTop Me
        MessageBox 0, "N�o h� liga��es para executar", "Aten�ao", &H40000 + 0
        StayOnTop Me

        Screen.MousePointer = 0
        Me.Visible = False
        vFechar = True
        Exit Sub
    Else
        'Eduardo - 22/11/2006
        Me.Visible = True
        Me.Refresh

        tot_grid = VarRec.RecordCount
        'Label4 = tot_grid
        
        With grdLista
            'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
            If vChave = "S" Then
                'TI-3848 - AMELO - MELHORIA ROTEIRIZACAO
                .Cols = 14
            Else
                .Cols = 12
            End If
            
            If vChave = "N" Then
                .TextMatrix(0, 0) = "MOTIVO CONTATO"
                .ColWidth(0) = 2000
                .TextMatrix(0, 1) = "COD"
                .ColWidth(1) = 700
                .TextMatrix(0, 2) = "CLIENTE"
                .ColWidth(2) = 3000
                .TextMatrix(0, 3) = "CONTATO"
                .ColWidth(3) = 1000
                .TextMatrix(0, 4) = "DDD1"
                .ColWidth(4) = 600
                .TextMatrix(0, 5) = "FONE1"
                .ColWidth(5) = 1000
                .TextMatrix(0, 6) = "DT.LIG."
                .ColWidth(6) = 950
                .TextMatrix(0, 7) = "RESP."
                .ColWidth(7) = 600
                .TextMatrix(0, 8) = "CIDADE"
                .ColWidth(8) = 1000
                .TextMatrix(0, 9) = "UF"
                .ColWidth(9) = 350
                .TextMatrix(0, 10) = "DDD2"
                .ColWidth(10) = 600
                .TextMatrix(0, 11) = "FONE2"
                .ColWidth(11) = 1000
            End If
            
            'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
            If vChave = "S" Then
                .TextMatrix(0, 0) = "DT. LIG."
                .ColWidth(0) = 900
                .ColAlignment(0) = 4
                
                .TextMatrix(0, 1) = "MOT. CONTATO"
                .ColWidth(1) = 2700
                
                .TextMatrix(0, 2) = "STATUS"
                .ColWidth(2) = 800
                
                .TextMatrix(0, 3) = "CD CLIENTE"
                .ColWidth(3) = 1000
                .ColAlignment(3) = 4
                
                .TextMatrix(0, 4) = "CLIENTE"
                .ColWidth(4) = 3600
                
                .TextMatrix(0, 5) = "META"
                .ColWidth(5) = 1000
                .ColAlignment(5) = 7
                
                .TextMatrix(0, 6) = "VENDAS"
                .ColWidth(6) = 1000
                .ColAlignment(6) = 7
                
                .TextMatrix(0, 7) = "%ATINGIDO"
                .ColWidth(7) = 1000
                .ColAlignment(7) = 7
                
                .TextMatrix(0, 8) = "DT.ULT.COMPRA"
                .ColWidth(8) = 1500
                .ColAlignment(8) = 4
                
                
                .TextMatrix(0, 9) = "CONTATO"
                .ColWidth(9) = 1700
                
                .TextMatrix(0, 10) = "CIDADE"
                .ColWidth(10) = 3000
                
                .TextMatrix(0, 11) = "FLCONTATO"
                .ColWidth(11) = 0
                
                .TextMatrix(0, 12) = "ITEMMOTIVO"
                .ColWidth(12) = 0
                'TI-3848 - AMELO - MELHORIA ROTEIRIZACAO
                .TextMatrix(0, 13) = "DT.ULT.CONTATO"
                .ColWidth(13) = 1500
                
            End If
            
            VarRec.MoveFirst
            
            
            l = 1
            Do While Not VarRec.EOF
            
            If vChave = "N" Then
                .AddItem IIf(IsNull(VarRec!MOTIVO_CONTATO), 0, VarRec!MOTIVO_CONTATO) & vbTab & _
                    VarRec!COD_CLIENTE & vbTab & _
                    VarRec!NOME_CLIENTE & vbTab & _
                    IIf(IsNull(VarRec!NOME_CONTATO), "", VarRec!NOME_CONTATO) & vbTab & _
                    IIf(IsNull(VarRec!ddd1), 0, VarRec!ddd1) & vbTab & _
                    IIf(IsNull(VarRec!fone1), 0, VarRec!fone1) & vbTab & _
                    vbTab & _
                    VarRec!cod_repres & vbTab & _
                    VarRec!Nome_Cidade & vbTab & _
                    VarRec!cod_uf & vbTab & _
                    IIf(IsNull(VarRec!ddd2), 0, VarRec!ddd2) & vbTab & _
                    IIf(IsNull(VarRec!fone2), 0, VarRec!fone2)
                    
                    .TextMatrix(.rows - 1, 6) = VarRec!dt_ligacao
            End If
            
            'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
            If vChave = "S" Then
                 
'                motivo = ""
'                If Not IsNull(VarRec!motivo) Then
'                    motivo = Formata_Motivo(VarRec!motivo)
'                End If
                
                .AddItem VarRec!data_roteiro & vbTab & _
                         VarRec!motivo & vbTab & _
                         VarRec!status_cliente & vbTab & _
                         VarRec!COD_CLIENTE & vbTab & _
                         VarRec!cliente & vbTab & _
                         VarRec!meta & vbTab & _
                         VarRec!venda & vbTab & _
                         vbTab & _
                         VarRec!dtultcompra & vbTab & _
                         VarRec!NOME_CONTATO & vbTab & _
                         VarRec!Nome_Cidade & vbTab & _
                         VarRec!Flcontato & vbTab & _
                         VarRec!itemmotivo & vbTab & _
                         VarRec!DTULTCONTATO

'                        VarRec!COD_CLIENTE & vbTab & _
'                        IIf(IsNull(VarRec!STATUS_CLIENTE), "", VarRec!STATUS_CLIENTE) & vbTab & _
'                        VarRec!NOME_CLIENTE & vbTab & _
'                        VarRec!vl_meta & vbTab & _
'                        IIf(IsNull(VarRec!vl_totalizado), 0, VarRec!vl_totalizado) & vbTab & _
'                        vbTab & _
'                        IIf(IsNull(VarRec!Contatos), "N�O", VarRec!Contatos) & vbTab & _
'                        IIf(IsNull(VarRec!CONTATOS_ATIVOS_MES), 0, VarRec!CONTATOS_ATIVOS_MES) & vbTab & _
'                        IIf(IsNull(VarRec!CONTATOS_MES), 0, VarRec!CONTATOS_MES) & vbTab & _
'                        IIf(IsNull(VarRec!ULTIMO_CONTATO), 0, VarRec!ULTIMO_CONTATO) & vbTab & _
'                        vbTab & _
'                        VarRec!cod_repres & vbTab & _
'                        VarRec!Nome_Cidade & vbTab & _
'                        IIf(IsNull(VarRec!NOME_CONTATO), "", VarRec!NOME_CONTATO) & vbTab & _
'                        vbTab & _
'                        vbTab & _
'                        vbTab & _
'                        vbTab & _
'                        vbTab & _
'                        vbTab & _
'                        vbTab & _
'                        VarRec!MOTIVO_CONTATO
                
                        If IsNull(VarRec!meta) Then
                            .TextMatrix(.rows - 1, 7) = 0
                        Else

                            If VarRec!venda / VarRec!meta * 100 >= 100 Then
                                .Col = 7
                                .Row = .rows - 1
                                grdLista.CellPictureAlignment = flexAlignCenterCenter
                                Set grdLista.CellPicture = LoadPicture(App.Path & "\meta.gif")
                            Else
                                .TextMatrix(.rows - 1, 7) = Format(VarRec!venda / VarRec!meta * 100, "0.00")
                            End If
                        End If
                
'                If Not IsNull(VarRec!Contatos) Then
'                    .TextMatrix(.Rows - 1, 7) = VarRec!Contatos
'                Else
'                    .TextMatrix(.Rows - 1, 7) = "N�O"
'                End If
                
'                If Not IsNull(VarRec!CONTATOS_ATIVOS_MES) Then
'                    .TextMatrix(.Rows - 1, 8) = VarRec!CONTATOS_ATIVOS_MES
'                Else
'                    .TextMatrix(.Rows - 1, 8) = 0
'                End If
                
'                If Not IsNull(VarRec!CONTATOS_MES) Then
'                    .TextMatrix(.Rows - 1, 9) = VarRec!CONTATOS_MES
'                Else
'                    .TextMatrix(.Rows - 1, 9) = 0
'                End If
                
'                If Not IsNull(VarRec!ULTIMO_CONTATO) Then
'                    .TextMatrix(.Rows - 1, 10) = VarRec!ULTIMO_CONTATO
'                Else
'                    .TextMatrix(.Rows - 1, 10) = ""
'                End If
                
'                If VarRec!nota <> 0 Then
'                    .TextMatrix(.rows - 1, 11) = Format(VarRec!resultado / VarRec!nota, "0.00")
'                Else
'                    .TextMatrix(.rows - 1, 11) = 0
'                End If
                
'                .TextMatrix(.Rows - 1, 12) = VarRec!cod_repres
'
'                .TextMatrix(.Rows - 1, 13) = VarRec!Nome_Cidade
'
'                .TextMatrix(.Rows - 1, 14) = IIf(IsNull(VarRec!NOME_CONTATO), "", VarRec!NOME_CONTATO)
                
            End If
        
                'Calcula Totais Liga��es a Executar e Excedente
                If VarRec!Flcontato = 2 Then
                    lig_executar = lig_executar + 1
                ElseIf VarRec!Flcontato = 3 Then
                    lig_excedente = lig_excedente + 1
                End If
                    
                CorLinha VarRec!Flcontato, l
                
                
                l = l + 1
                VarRec.MoveNext
            Loop
            
            .Row = 1
            
            'Call MarcaLinha(True, .Row)
        End With

    End If
    
    'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
    'Totais Liga��es
    lbl_qtde_lig_Executar.Caption = lig_executar
    lbl_qtde_lig_Excedentes.Caption = lig_excedente

    
    Screen.MousePointer = 0
      
    Set ss = Nothing
    Set SS1 = Nothing

    Exit Sub

Trata_Erro:
    If Err = 364 Then
        Set ss = Nothing
        Set SS1 = Nothing
        Exit Sub
    Else
        MessageBox 0, "Sub Form_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten�ao", &H40000 + 0
        Resume
    End If

    On Error GoTo 0
    
    Set ss = Nothing
    Set SS1 = Nothing

    Exit Sub

End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmCVend = Nothing
    frmVenda.vFrmcVend = False
End Sub

'SDS2589 - Ricardoo Gomes
'Substitui a rotina GRID1_Click
'devido a troca de componente
Private Sub grdLista_Click()

    On Error GoTo Trata_Erro
    
    
    
    strPreencher_Cliente = False
    'SendKeys "{LEFT}+{END}"
    If vTempoClick = 0 Then
        Timer1.Interval = 1000
        vLinha = grdLista.Row
        vTempoClick = 1
        'SendKeys "{LEFT}+{END}"
        
        'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
        'Recupera o c�digo do Cliente
        If vChave = "S" Then
            grdLista.Col = 3
        Else
            grdLista.Col = 1
        End If
        
        If vTrabalharConectado = True And Left(Trim(Command$), 1) <> "V" Then 'TI-3880 cmd
            NovaLigacao.CodCliente = grdLista.Text
        End If
    Else
        Timer1.Interval = 0
        If vTempoClick <> 0 Then vTempoClick = 0
        If vLinha = grdLista.Row Then
            grdLista_DblClick
            Exit Sub
        End If
    End If
    
    Call MarcaLinha(True, grdLista.Row)
    
    'Exibe o Nr. cota��o DPKNet
    grdLista.Col = 1
    If InStr(UCase(grdLista.Text), "DPKNET") Then
        grdLista.Col = 12
        grdLista.ToolTipText = "Cota��o DPKNET " & grdLista.Text
    Else
        grdLista.ToolTipText = ""
    End If
    
    
Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub grdLista_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If
End Sub
'SDS2589 - Ricardoo Gomes
'Substitui a rotina GRID1_Click
'devido a troca de componente
Private Sub grdLista_DblClick()
1         On Error GoTo Trata_Erro

          '**************************************
          'O codigo abaixo estava no evento Click
          '**************************************
          'Grava o codigo do cliente em arquivo
          'texto para ser lido pelo programa tlmkt
          
          
        'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
        'Recupera o c�digo do Cliente
        If vChave = "S" Then
            grdLista.Col = 3
        Else
            grdLista.Col = 1
        End If

3         If Not IsNumeric(grdLista.Text) Or grdLista.Row = 0 Then Exit Sub

4         Me.Visible = False

5         frmVenda.txtCOD_CLIENTE = ""
6         frmVenda.txtCOD_CLIENTE.Text = CStr(grdLista.Text)
7         frmVenda.txtCOD_CLIENTE.DataChanged = True
8         frmVenda.txtCOD_CLIENTE_LostFocus

9         Unload frmCVend

Trata_Erro:
10        If Err.Number <> 0 Then
11      MessageBox 0, "Sub grdLista_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
12        End If

End Sub


'SDS2589 - Ricardo Gomes
'Classifica o grid com base na coluna selecionada
'Private Sub ClassificaGrid(col As Integer)
'    If Button = 2 Then
'    With grdLista
'        If .MouseRow = 0 Then
'            .col = col
'            .Sort = flexSortGenericAscending
'        End If
'    End With
'End If
'End Sub

Private Sub grdLista_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        mnuMenu.Visible = True
        PopupMenu mnuMenu
'ti-3403 - projeto roteirizacao - amelo
'        If vChave = "S" Then
'            mnuSort.Item(12).Visible = True
'            mnuSort.Item(13).Visible = True
'            mnuSort.Item(14).Visible = True
'            mnuSort.Item(15).Visible = True
'            mnuSort.Item(16).Visible = True
'        Else
'            mnuSort.Item(12).Visible = False
'            mnuSort.Item(13).Visible = False
'            mnuSort.Item(14).Visible = False
'            mnuSort.Item(15).Visible = False
'        End If
    End If
    
End Sub

Private Sub mnuSort_Click(index As Integer)
    grdLista.Col = index
    grdLista.Sort = flexSortGenericAscending
End Sub

'SDS2589 - Ricardo Gomes
'Rotina desabilitada devido a troca de compomente
'Private Sub Grid1_Click()
'
'    On Error GoTo Trata_Erro
'
'1         strPreencher_Cliente = False
'2         SendKeys "{LEFT}+{END}"
'3         If vTempoClick = 0 Then
'4             Timer1.Interval = 1000
'5             vLinha = Grid1.Row
'6             vTempoClick = 1
'7             SendKeys "{LEFT}+{END}"
'8             Grid1.Col = 2
'9             If vTrabalharConectado = True And Command$ = "" Then
'10                Grid1.Col = 1
'11                NovaLigacao.CodCliente = Grid1.Text
'12            End If
'13        Else
'14            Timer1.Interval = 0
'15            If vTempoClick <> 0 Then vTempoClick = 0
'16            If vLinha = Grid1.Row Then Grid1_DblClick
'17        End If
'
'Trata_Erro:
'    If Err.Number <> 0 Then
'        MessageBox 0, "Sub Grid1_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
'    End If
'
'End Sub
'
'
'SDS2589 - Ricardo Gomes
'Rotina desabilitada devido a troca de compomente
'Private Sub Grid1_DblClick()
'
'1         On Error GoTo Trata_Erro
'
'          '**************************************
'          'O codigo abaixo estava no evento Click
'          '**************************************
'          'Grava o codigo do cliente em arquivo
'          'texto para ser lido pelo programa tlmkt
'
'2         Grid1.Col = 1
'
'3         If Not IsNumeric(Grid1.Text) Then Exit Sub
'
'4         Me.Visible = False
'
'5         frmVenda.txtCOD_CLIENTE = ""
'6         frmVenda.txtCOD_CLIENTE.Text = CStr(Grid1.Text)
'7         frmVenda.txtCOD_CLIENTE.DataChanged = True
'8         frmVenda.txtCOD_CLIENTE_LostFocus
'
'9         Unload frmCVend
'
'Trata_Erro:
'10        If Err.Number <> 0 Then
'11      MessageBox 0, "Sub Grid1_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
'12        End If
'
'End Sub


Private Sub SSCommand1_Click()
    Unload Me
    frmCVend.Caption = "LISTA DE LIGA��ES A EXECUTAR"
    Call SSCommand3_Click
End Sub


Private Sub SSCommand2_Click()
    NovaLigacao.CodCliente = 0
    Unload Me
End Sub

Private Sub SSCommand3_Click()
    If frmCVend.Visible = False Then
        frmCVend.Show
    End If
End Sub

'TI-3403 psergio 18/11/15 Projeto Roteiriza��o
'Private Sub SSCommand4_Click()
'    Unload Me
'    frmCVend.Caption = "LISTA DE CONTATOS J� EFETUADOS NESTA DATA"
'End Sub

Private Sub Text1_Change()
    If tot_grid = 0 Then
        Exit Sub
    End If
    'reset nas posi�oes do grid
    'Grid1.HighLight = False
    
    grdLista.TopRow = 1
    grdLista.Row = 1
    grdLista.Col = 1

    'Variaveis que definem inicio ou continuacao da pesquisa
    CONTINUAPESQUISA = 0
    OCORRENCIA = 0
    CONTADOROCORRENCIA = 0

    Command1.Caption = "In�cio"

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub

'SDS2589 - Ricardo Gomes
'Troca no nome do componente de
'GRID1 para grdLista
Private Sub Text1_LostFocus()
    Dim linha As Integer
    Dim coluna As Integer
    If tot_grid = 0 Then
        Exit Sub
    End If
    
    
    'N�mero da coluna onde a pesquisa deve agir
    grdLista.Col = 2
    
    If Text1.Text <> "" Then
        Call MarcaLinha(False)
        On Error GoTo TopRowError

        'texto que dever� ser encontrado
        TEXTOPARAPROCURA = Trim(Text1.Text)

        'desabilita o highlight
        'Grid1.HighLight = False

        'posiciona no in�cio do grid
        grdLista.Row = 1
        grdLista.TopRow = 1
        'William Leite - Corre��o de busca por c�digo de cliente.
        'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
        'Recupera o c�digo do Cliente
        If vChave = "S" Then
            grdLista.Col = 3
        Else
            grdLista.Col = 1
        End If

        For INICIOPESQUISA = CONTINUAPESQUISA + 1 To tot_grid
            grdLista.Row = INICIOPESQUISA
            
            'verifica se a string digitada est� contida no texto da aplica�ao
            If InStr(grdLista.Text, TEXTOPARAPROCURA) <> 0 Then
                'define nova ocorr�ncia
                OCORRENCIA = OCORRENCIA + 1
            End If

            If InStr(grdLista.Text, TEXTOPARAPROCURA) <> 0 And OCORRENCIA > CONTADOROCORRENCIA Then
                'vari�vel que controla quantas vezes determinado string j� foi localizado
                CONTADOROCORRENCIA = CONTADOROCORRENCIA + 1
                
                'SDS2589 = Ricardo Gomes
                'Removida as propriedades que o flexgrid
                'n�o suporta
                
                'liga o highlight
                'Grid1.HighLight = True
                'Grid1.SelStartRow = INICIOPESQUISA
                'Grid1.SelEndRow = INICIOPESQUISA
                'Grid1.SelStartCol = 0
                'Grid1.SelEndCol = 2
                
                'SDS2589 - Ricardo Gomes
                'Marca a linha que contem o valor procurado
                Call MarcaLinha(True, grdLista.Row)
'                For coluna = 0 To grdLista.Cols - 1
'                    grdLista.Col = coluna
'                    grdLista.CellBackColor = RGB(50, 80, 250)
'                Next
            
                
                'se encontrou o texto posiciona a linha do grid
                If grdLista.RowSel Then
                    If INICIOPESQUISA > 1 Then
                        For INILINHA = 1 To INICIOPESQUISA - 1
                            'o m�ximo valor que toprow pode ter deve ser igual ao numero de
                            'linhas total do grid menos o numero de linhas visiveis do grid
                            If grdLista.TopRow = grdLista.Row - tot_grid Then
                                grdLista.TopRow = grdLista.rows - tot_grid
                            Else
                                grdLista.TopRow = grdLista.TopRow + 1
                            End If
                        Next
                    End If
                End If

                'a proxima pesquisa continuara a partir desta variavel
                CONTINUAPESQUISA = INICIOPESQUISA
                Command1.Caption = "Pr�xima"
                Exit Sub
            End If
        Next

        NonStayOnTop Me
        MessageBox 0, "Fim de Pesquisa", "Aten�ao", &H40000 + 0
        StayOnTop Me

        'reset nas posi�oes do grid
        'Grid1.HighLight = False
        grdLista.TopRow = 1
        grdLista.Row = 1
        grdLista.Col = 2

        'Variaveis que definem inicio ou continuacao da pesquisa
        CONTINUAPESQUISA = 0
        OCORRENCIA = 0
        CONTADOROCORRENCIA = 0

        Command1.Caption = "In�cio"
        Exit Sub

TopRowError:
        grdLista.TopRow = 1
        Resume Next
    End If

End Sub


Private Sub Timer1_Timer()
    vTempoClick = 0
End Sub


Private Sub MarcaLinha(cor As Boolean, Optional linhaSel As Integer)
    Dim linha As Integer
    Dim coluna As Integer
    
    'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
    For linha = 1 To grdLista.rows - 1
        grdLista.Row = linha
        For coluna = 0 To grdLista.Cols - 1
            grdLista.Col = coluna
            grdLista.CellBackColor = RGB(255, 255, 255)
            
            grdLista.Col = 11
            
            If grdLista.Text = "2" Then
                grdLista.Col = coluna
                grdLista.CellForeColor = RGB(50, 80, 250)
            ElseIf grdLista.Text = "3" Then
                grdLista.Col = coluna
                grdLista.CellForeColor = RGB(200, 20, 20)
            End If
            
            
        Next
    Next
    
    If cor = True Then
        grdLista.Row = linhaSel
        For coluna = 0 To grdLista.Cols - 1
            grdLista.Col = coluna
            grdLista.CellBackColor = RGB(50, 80, 250)
            grdLista.CellForeColor = RGB(255, 255, 255)
            
        Next
    End If
    
End Sub

'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
Private Sub CorLinha(cor As Integer, linhaSel As Long)
Dim coluna As Integer
    
    grdLista.Row = linhaSel
    For coluna = 0 To grdLista.Cols - 1
        grdLista.Col = coluna
        If cor = 2 Then
            grdLista.CellForeColor = &HFF0000 'AZUL
        ElseIf cor = 3 Then
            grdLista.CellForeColor = &HFF& 'VERMELHO
        End If
    Next
    
End Sub

'TI-3403 - PSERGIO - 23/11/15 - PROJETO ROTEIRIZACAO
Private Function Formata_Motivo(Texto As String) As String

Dim texto_novo As String

texto_novo = ""

    If InStr(Texto, "Cheguei") <> 0 Then
        texto_novo = "Cheguei"
    End If
    
    If InStr(Texto, "Exce��o") <> 0 Then
        If texto_novo = "" Then
            texto_novo = "Exce��o"
        Else
            texto_novo = texto_novo & "/Exce��o"
        End If
    End If
    
    If InStr(Texto, "Agendamento") <> 0 Then
        If texto_novo = "" Then
            texto_novo = "Agendamento"
        Else
            texto_novo = texto_novo & "/Agendamento"
        End If
    End If
    
    If InStr(Texto, "DPKNet") <> 0 Then
        If texto_novo = "" Then
            texto_novo = "DPKNet"
        Else
            texto_novo = texto_novo & "/DPKNet"
        End If
    End If
    
    If InStr(Texto, "DPKlub") <> 0 Then
        If texto_novo = "" Then
            texto_novo = "DPKlub"
        Else
            texto_novo = texto_novo & "/DPKlub"
        End If
    End If

Formata_Motivo = texto_novo

End Function

