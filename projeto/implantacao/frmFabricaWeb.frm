VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmFabricaWeb 
   Caption         =   "Lista de C�digo de F�brica do Fornecedor"
   ClientHeight    =   8295
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   11655
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8295
   ScaleWidth      =   11655
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picAguardarParametros 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1005
      Left            =   5280
      Picture         =   "frmFabricaWeb.frx":0000
      ScaleHeight     =   1005
      ScaleWidth      =   1095
      TabIndex        =   19
      Top             =   3645
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.ComboBox cboMotor 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   4800
      TabIndex        =   18
      Top             =   1200
      Width           =   2205
   End
   Begin VB.ComboBox cboSubgrupo 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   8670
      TabIndex        =   6
      Top             =   1200
      Width           =   2775
   End
   Begin VB.ComboBox cboGrupo 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   8670
      TabIndex        =   5
      Top             =   840
      Width           =   2775
   End
   Begin VB.ComboBox cboMontadora 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   1140
      TabIndex        =   4
      Top             =   840
      Width           =   2190
   End
   Begin VB.ComboBox cboVeiculo 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   1140
      TabIndex        =   3
      Top             =   1200
      Width           =   2190
   End
   Begin VB.ComboBox cboModelo 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   4800
      TabIndex        =   2
      Top             =   840
      Width           =   2190
   End
   Begin VB.TextBox txtAno 
      ForeColor       =   &H00FF0000&
      Height          =   285
      Left            =   8670
      MaxLength       =   4
      TabIndex        =   1
      Top             =   1590
      Width           =   615
   End
   Begin SHDocVwCtl.WebBrowser WebFabrica 
      Height          =   5925
      Left            =   30
      TabIndex        =   0
      Top             =   2340
      Visible         =   0   'False
      Width           =   11565
      ExtentX         =   20399
      ExtentY         =   10451
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   7
      Top             =   720
      Width           =   11580
      _ExtentX        =   20426
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   690
      Left            =   0
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFabricaWeb.frx":1AC3
      PICN            =   "frmFabricaWeb.frx":1ADF
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand3 
      Height          =   645
      Left            =   10830
      TabIndex        =   16
      ToolTipText     =   "Limpar"
      Top             =   30
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1138
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFabricaWeb.frx":27B9
      PICN            =   "frmFabricaWeb.frx":27D5
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd sscmdPesquisa 
      Height          =   690
      Left            =   10770
      TabIndex        =   17
      TabStop         =   0   'False
      ToolTipText     =   "Consultar"
      Top             =   1560
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFabricaWeb.frx":292F
      PICN            =   "frmFabricaWeb.frx":294B
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdKdApeca 
      Height          =   645
      Left            =   3960
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Encontrou erro? Clique aqui."
      Top             =   30
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1138
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFabricaWeb.frx":3625
      PICN            =   "frmFabricaWeb.frx":3641
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblKdApeca 
      Alignment       =   2  'Center
      Caption         =   "Encontrou erro? Clique aqui ->"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   840
      TabIndex        =   21
      Top             =   360
      Width           =   3045
   End
   Begin VB.Label lblSubgrupo 
      Caption         =   "Subgrupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7320
      TabIndex        =   15
      Top             =   1200
      Width           =   855
   End
   Begin VB.Label lblGrupo 
      Caption         =   "Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7320
      TabIndex        =   14
      Top             =   840
      Width           =   1815
   End
   Begin VB.Label lblMontadora 
      Caption         =   "Montadora"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Top             =   840
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Ve�culo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   1200
      Width           =   1125
   End
   Begin VB.Label Label2 
      Caption         =   "Modelo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3600
      TabIndex        =   11
      Top             =   840
      Width           =   1185
   End
   Begin VB.Label Label3 
      Caption         =   "Motor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3600
      TabIndex        =   10
      Top             =   1200
      Width           =   1185
   End
   Begin VB.Label Label4 
      Caption         =   "Ano"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7320
      TabIndex        =   9
      Top             =   1590
      Width           =   495
   End
End
Attribute VB_Name = "frmFabricaWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmFabricaWeb
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 01/09/16
' Purpose   : TI-5291
'---------------------------------------------------------------------------------------

Option Explicit
Dim strPesquisa As String
Public vFrmFabricaAberto As Boolean

'VARIAVEIS UTILIZADAS PARA PESQUISA
Dim CONTINUAPESQUISA As Integer
Dim OCORRENCIA As Integer
Dim CONTADOROCORRENCIA As Long
Dim TEXTOPARAPROCURA As String
Dim INICIOPESQUISA As Long
Dim INILINHA As Long

' vetores
Dim arrmontadora() As Integer
Dim arrgrupo() As Integer
Dim arrsubgrupo() As Integer

Dim arrveiculo() As Integer
Dim arrmodelo() As Integer
Dim arrmotor() As Integer

Dim Veiculo As Integer
Dim Modelo As Integer
Dim Motor As Integer

Dim montadora As Integer
Dim grupo As Integer
Dim subgrupo As Integer
'para destravar montadora e veiculo
'Private Sub cboVeiculo_LostFocus()
'
'On Error GoTo Trata_Erro
'
'    If Veiculo <> 0 Then
'
'        vSql = "Begin producao.PCK_VDA320_NOVO.pr_BUSCA_montadora(:veiculo,:montadora);END;"
'
'        oradatabase.Parameters.Remove "veiculo"
'        oradatabase.Parameters.Add "veiculo", Trim(cboVeiculo.Text), 1
'
'        oradatabase.Parameters.Remove "montadora"
'        oradatabase.Parameters.Add "montadora", montadora, 2
'
'        oradatabase.ExecuteSQL vSql
'
'        montadora = oradatabase.Parameters("montadora").Value
'
'    End If
'
'Trata_Erro:
'  If Err <> 0 Then
'    MsgBox "Erro no carregamento da Montadora : " & Err.Description, vbInformation, "Aten��o"
'    Exit Sub
'  End If
'
'End Sub

Private Sub Form_Activate()

On Error GoTo Trata_Erro
    
    If InStr(1, frmVenda.txtCOD_FABRICA.Text, pesq) > 0 And _
        InStr(1, frmVenda.txtDescricao.Text, pesq) = 0 Then

        WebFabrica.Navigate "about:blank"
        DoEvents
    
        Me.picAguardarParametros.Visible = True
        Me.Refresh
    
        SSCommand3_Click
        WebFabrica.Navigate ("http://" & strDominioDpkNetWeb & "/dirTelemarketing/restricao.aspx?loja=" & Mid(Trim(frmVenda.cboDeposito.Text), 1, 2) & "&forn=" & IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR) & "&fabr=" & Replace(frmVenda.txtCOD_FABRICA.Text, "%", "@") & "&item=0")
        'DESENV
        'WebFabrica.Navigate ("http://DPKWDES/dirTelemarketing/restricao.aspx?loja=" & Mid(Trim(frmVenda.cboDeposito.Text), 1, 2) & "&forn=" & IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, Trim(frmVenda.txtCOD_FORNECEDOR)) & "&fabr=" & Trim(Replace(frmVenda.txtCOD_FABRICA.Text, "%", "@")) & "&item=0")
        WebFabrica.Visible = True
        
        If IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR) = 0 Then
            
            strDesc = "S"
        
        Else
        
            strDesc = "N"
            
        End If
        
        Exit Sub
    ElseIf InStr(1, frmVenda.txtDescricao, pesq) > 0 And _
        InStr(1, frmVenda.txtCOD_FABRICA, pesq) = 0 Then
        
        WebFabrica.Navigate "about:blank"
        DoEvents
        
        Me.picAguardarParametros.Visible = True
        Me.Refresh
        
        strDesc = "N"
        SSCommand3_Click
        WebFabrica.Navigate ("http://" & strDominioDpkNetWeb & "/dirTelemarketing/restricao.aspx?loja=" & Mid(Trim(frmVenda.cboDeposito.Text), 1, 2) & "&forn=" & IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR) & "&fabr=0&item=" & Replace(frmVenda.txtDescricao.Text, "%", "@"))
        'DESENV
        'WebFabrica.Navigate ("http://DPKWDES/dirTelemarketing/restricao.aspx?loja=" & Mid(Trim(frmVenda.cboDeposito.Text), 1, 2) & "&forn=" & IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR) & "&fabr=0&item=" & Replace(frmVenda.txtDescricao.Text, "%", "@"))
        WebFabrica.Visible = True
        
        Exit Sub
        
    Else
        Exit Sub
    End If
    
Trata_Erro:
    
    Me.picAguardarParametros.Visible = False
    NonStayOnTop Me
    StayOnTop Me

    Unload Me
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    SSCommand3_Click
End Sub

Private Sub sscmdPesquisa_Click()
     
    'Screen.MousePointer = vbHourglass
    Dim vDescItem As String
    
    On Error GoTo Trata_Erro

   'Seleciona um grupo, guarda o c�digo para posterior consulta
   If cboSubgrupo.ListIndex = -1 Then
        subgrupo = 0
   Else
        subgrupo = arrsubgrupo(cboSubgrupo.ListIndex + 1)
   End If
   
   If grupo = 0 And subgrupo = 0 And montadora = 0 And _
     Veiculo = 0 And Modelo = 0 And Motor = 0 Then
     MsgBox "Fa�a uma sele��o para efetuar a pesquisa.", vbCritical, "Aten��o"
     Exit Sub
   End If

'   If grupo = 0 Or subgrupo = 0 Or montadora = 0 Then
'     MsgBox "Escolha Montadora,Grupo e SubGrupo para efetuar a pesquisa", vbCritical, "Aten��o"
'     Exit Sub
'   End If
   
    WebFabrica.Navigate "about:blank"
    DoEvents
    
    If InStr(1, frmVenda.txtDescricao, pesq) > 0 Then
        vDescItem = Replace(frmVenda.txtDescricao.Text, "%", "@")
    Else
        vDescItem = "0"
    End If
       
    If Me.picAguardarParametros.Visible = False Then
        Me.picAguardarParametros.Visible = True
        Me.Refresh
    End If
    
    WebFabrica.Navigate ("http://" & strDominioDpkNetWeb & "/dirTelemarketing/restricaoBt.aspx?loja=" & Mid(Trim(frmVenda.cboDeposito.Text), 1, 2) & "&forn=" & IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR) & "&montadora=" & montadora & "&grupo=" & grupo & "&subgrupo=" & subgrupo & "&veiculo=" & Veiculo & "&modelo=" & Modelo & "&motor=" & Motor & "&ano=" & Val(txtAno.Text) & "&item=" & vDescItem)
    'DESENV
    'WebFabrica.Navigate ("http://DPKWDES/dirTelemarketing/restricaoBt.aspx?loja=" & Mid(Trim(frmVenda.cboDeposito.Text), 1, 2) & "&forn=" & IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR) & "&montadora=" & montadora & "&grupo=" & grupo & "&subgrupo=" & subgrupo & "&veiculo=" & Veiculo & "&modelo=" & Modelo & "&motor=" & Motor & "&ano=" & Val(txtAno.Text) & "&item=" & vDescItem)
    WebFabrica.Visible = True

    If frmVenda.txtCOD_FORNECEDOR.Text = "" Then
    
        strDesc = "S"
    
    End If
    
Trata_Erro:
  If Err = 9 Then
    MsgBox "Clique sobre um subgrupo", vbInformation, "Aten��o"
    If Me.picAguardarParametros.Visible = True Then Me.picAguardarParametros.Visible = False
    Exit Sub
  ElseIf Err <> 0 Then
    MsgBox Err.Description
    If Me.picAguardarParametros.Visible = True Then Me.picAguardarParametros.Visible = False
    'Resume
  End If

End Sub


Private Sub SSCommand3_Click()

  Erase arrmontadora
  Erase arrgrupo
  Erase arrsubgrupo
  
  Erase arrveiculo
  Erase arrmodelo
  Erase arrmotor
  
  
  
  montadora = 0
  grupo = 0
  subgrupo = 0
  
  Veiculo = 0
  Modelo = 0
  Motor = 0
  txtAno = ""
  
  
  cboMontadora.Clear
  cboGrupo.Clear
  cboSubgrupo.Clear
  
  cboVeiculo.Clear
  cboModelo.Clear
  cboMotor.Clear
 

End Sub


Private Sub cboGrupo_Click()
  On Error GoTo Trata_Erro
 'Seleciona um grupo, guarda o c�digo para posterior consulta
  grupo = arrgrupo(cboGrupo.ListIndex + 1)
  
  'Limpa o array que guarda os subgrupos
  Erase arrsubgrupo
  
  'Limpa o combo e vari�vel do subgrupo
  frmFabricaWeb.cboSubgrupo.Clear
  frmFabricaWeb.cboSubgrupo.Text = ""
  subgrupo = 0
  Exit Sub
  
Trata_Erro:
   If Err = 9 Then
     MsgBox "Clique sobre um Grupo", vbInformation, "Aten��o"
     Exit Sub
   End If
End Sub

Private Sub cboGrupo_DropDown()

Dim ss As Object
Dim cont As Long

' Verifica se foi escolhido um fornecedor, caso contr�rio n�o permite sele��o
' Se escolheu fornecedor executa o select que busca os grupos
' N�o permite executar o select se o combo estiver cheio

'If frmVenda.TXTCOD_FORNECEDOR.Text = "" Then
'  MsgBox "Escolha um fornecedor", 0, "ATEN��O"
'  Exit Sub
'Else
  If cboGrupo.ListCount = 0 Then
    Screen.MousePointer = 11
    'Select utilizado para preencher o combo de grupo
    OraParameters.Remove "forn"
    OraParameters.Add "forn", IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR), 1
         
  
     vSql = "Begin producao.PCK_VDA320_NOVO.pr_grupo(:vCursor,:forn,:vErro);END;"
    
     Criar_Cursor
     oradatabase.ExecuteSQL vSql
  
     Set ss = oradatabase.Parameters("vCursor").Value
  
    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(Sql)
        Exit Sub
    End If

    
      If ss.EOF Then
         MsgBox "N�o exite Grupo cadastrado para o fornecedor " & frmVenda.txtCOD_FORNECEDOR, 0, "ATEN��O"
         Screen.MousePointer = 0
         Exit Sub
      Else
        'Enche o combo
        For cont = 1 To ss.RecordCount
         'Redefine array para guardar os c�digos de grupos
          ReDim Preserve arrgrupo(1 To ss.RecordCount)
          cboGrupo.AddItem ss.Fields("desc_grupo").Value
          arrgrupo(cont) = ss.Fields("cod_grupo").Value
          ss.MoveNext
        Next
        Screen.MousePointer = 0
      End If
  End If
'End If

End Sub

Private Sub cboGrupo_KeyPress(KeyAscii As Integer)

  'n�o permite alterar a informa��o do combo
  
KeyAscii = Maiusculo(KeyAscii)

End Sub

Private Sub cboModelo_Change()
   If cboModelo.ListCount = 0 Then
     Call cboModelo_DropDown
 End If
 If cboModelo = "" Then
   Modelo = 0
 End If
 
End Sub

Private Sub cboModelo_Click()
 On Error GoTo Trata_Erro
 
   Modelo = arrmodelo(cboModelo.ListIndex + 1)
  
    Erase arrmotor
  

    frmFabricaWeb.cboMotor.Clear
    frmFabricaWeb.cboMotor.Text = ""
    Motor = 0
   
  
   Exit Sub
   
Trata_Erro:
  If Err = 9 Then
    MsgBox "Clique sobre um modelo", vbInformation, "Aten��o"
    Exit Sub
   End If
End Sub

Private Sub cboModelo_DropDown()
Dim ss As Object

' Verifica se foi escolhido uma montadora, caso contr�rio n�o permite sele��o
' Se escolheu montadora executa o select que busca os veiculos
' N�o permite executar o select se o combo estiver cheio

If Veiculo = 0 And cboVeiculo = "" Then
  MsgBox "Escolha um ve�culo", 0, "ATEN��O"
  Exit Sub
Else
  If cboModelo.ListCount = 0 Then
    
'para destravar montadora e veiculo
'     If Veiculo <> 0 Then
'
'        vSql = "Begin producao.PCK_VDA320_NOVO.pr_BUSCA_montadora(:veiculo,:montadora);END;"
'
'        oradatabase.Parameters.Remove "veiculo"
'        oradatabase.Parameters.Add "veiculo", Trim(cboVeiculo.Text), 1
'
'        oradatabase.Parameters.Remove "montadora"
'        oradatabase.Parameters.Add "montadora", montadora, 2
'
'        oradatabase.ExecuteSQL vSql
'
'        montadora = oradatabase.Parameters("montadora").Value
'
'     End If
     'Select utilizado para preencher o combo de montadora
      vSql = "Begin producao.PCK_VDA320_NOVO.pr_busca_modelo(:montadora,:veiculo,:vCursor,:vErro);END;"
      
     oradatabase.Parameters.Remove "montadora"
     oradatabase.Parameters.Add "montadora", montadora, 1
     
     oradatabase.Parameters.Remove "veiculo"
     oradatabase.Parameters.Add "veiculo", Veiculo, 1
     
     
     Criar_Cursor
     oradatabase.ExecuteSQL vSql
  
     Set ss = oradatabase.Parameters("vCursor").Value
  
    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(Sql)
        Exit Sub
    End If

      Screen.MousePointer = 11
      If ss.EOF Then
         MsgBox "N�o existe modelo cadastrado para este veiculo", 0, "ATEN��O"
         Screen.MousePointer = 0
         Exit Sub
      Else
        'Enche o combo
        For cont = 1 To ss.RecordCount
          'Redefine array para guardar os c�digos de montadoras
          ReDim Preserve arrmodelo(1 To ss.RecordCount)
          cboModelo.AddItem ss.Fields("desc_modelo").Value
          arrmodelo(cont) = ss.Fields("cod_modelo").Value
          ss.MoveNext
        Next
        Screen.MousePointer = 0
      End If
   End If
 End If

End Sub

Private Sub cboModelo_KeyPress(KeyAscii As Integer)
   KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub cboMontadora_Change()
  
  If cboMontadora.ListCount = 0 Then
     Call cboMontadora_DropDown
 End If
If cboMontadora = "" Then
   montadora = 0
 End If
End Sub

Private Sub cboMontadora_Click()
  On Error GoTo Trata_Erro

   montadora = arrmontadora(cboMontadora.ListIndex + 1)
'   If subgrupo <> 0 Then
'     Call sscmdPesquisa_Click
'   End If
   
   'Limpa o array que guarda os veiculos
  Erase arrveiculo
  Erase arrmodelo
  Erase arrmotor
 
  
  'Limpa o combo e vari�vel do veiculos
  frmFabricaWeb.cboVeiculo.Clear
  frmFabricaWeb.cboVeiculo.Text = ""
  Veiculo = 0
  
  frmFabricaWeb.cboModelo.Clear
  frmFabricaWeb.cboModelo.Text = ""
  Modelo = 0
   
  frmFabricaWeb.cboMotor.Clear
  frmFabricaWeb.cboMotor.Text = ""
  Motor = 0
   
   Exit Sub
   
Trata_Erro:
  If Err = 9 Then
    MsgBox "Clique sobre uma montadora", vbInformation, "Aten��o"
    Exit Sub
   End If

End Sub

Private Sub cboMontadora_DropDown()

Dim ss As Object


' Verifica se foi escolhido um fornecedor, caso contr�rio n�o permite sele��o
' Se escolheu fornecedor executa o select que busca as montadoras
' N�o permite executar o select se o combo estiver cheio



'If frmVenda.TXTCOD_FORNECEDOR.Text = "" Then
'  MsgBox "Escolha um fornecedor", 0, "ATEN��O"
'  Exit Sub
'Else
  If cboMontadora.ListCount = 0 Then
     'Select utilizado para preencher o combo de montadora
     
      vSql = "Begin producao.PCK_VDA320_NOVO.pr_montadora(:vCursor,:vErro);END;"
     
     Criar_Cursor
     oradatabase.ExecuteSQL vSql
  
     Set ss = oradatabase.Parameters("vCursor").Value
  
    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(Sql)
        Exit Sub
    End If

      Screen.MousePointer = 11
      If ss.EOF Then
         MsgBox "N�o existe montadora cadastrada", 0, "ATEN��O"
         Exit Sub
      Else
        'Enche o combo
        For cont = 1 To ss.RecordCount
          'Redefine array para guardar os c�digos de montadoras
          ReDim Preserve arrmontadora(1 To ss.RecordCount)
          cboMontadora.AddItem ss.Fields("desc_montadora").Value
          arrmontadora(cont) = ss.Fields("cod_montadora").Value
          ss.MoveNext
        Next
        Screen.MousePointer = 0
      End If
   End If
 'End If
  Screen.MousePointer = 0
End Sub

Private Sub cboMontadora_KeyPress(KeyAscii As Integer)

 KeyAscii = Maiusculo(KeyAscii)

End Sub


Private Sub cboMontadora_LostFocus()
  
' Dim INICIO As Integer
' Dim CONTINUA As Integer
' Dim POSICAO As Integer
'
' CONTINUA = 1
'
'
' If cboMontadora.DataChanged Then
'   For INICIO = 1 To cboMontadora.ListCount
'      If InStr(cboMontadora.Text, cboMontadora.List(CONTINUA)) <> 0 Then
'        If CONTINUA >= cboMontadora.ListCount Then
'          MsgBox "Montadora n�o cadastrada", vbExclamation, "Aten��o"
'          cboMontadora.SetFocus
'          Exit Sub
'        End If
'        montadora = arrmontadora(CONTINUA + 1)
'        If subgrupo <> 0 Then
'          Call cboSubgrupo_Click
'          Exit Sub
'        End If
'        Exit Sub
'      End If
'      CONTINUA = CONTINUA + 1
'      If (CONTINUA > cboMontadora.ListCount) And cboMontadora <> "" Then
'        MsgBox "Montadora n�o cadastrada", vbExclamation, "Aten��o"
'        cboMontadora.SetFocus
'        Exit Sub
'      End If
'   Next
' End If
  
End Sub

Private Sub cboMotor_Change()
   If cboMotor.ListCount = 0 Then
     Call cboMotor_DropDown
 End If
 If cboMotor = "" Then
   Motor = 0
 End If
End Sub

Private Sub cboMotor_Click()
1     On Error GoTo Trata_Erro
 
2        Motor = arrmotor(cboMotor.ListIndex + 1)
3        If Motor <> 0 Then
4          Call cboMotor_DropDown
5        End If
6        Exit Sub
   
Trata_Erro:
7       If Err <> 0 Then
8         MsgBox Err.Number & vbCrLf & Err.Description & vbCrLf & Erl, vbInformation, "Aten��o"
9         Exit Sub
10       End If
End Sub

Private Sub cboMotor_DropDown()
      Dim ss As Object

1     On Error GoTo Trata_Erro

      ' Verifica se foi escolhido uma montadora, caso contr�rio n�o permite sele��o
      ' Se escolheu montadora executa o select que busca os veiculos
      ' N�o permite executar o select se o combo estiver cheio



'      If Modelo = 0 And cboModelo = "" Then
'        MsgBox "Escolha um modelo", 0, "ATEN��O"
'        Exit Sub
'      Else
2       If cboMotor.ListCount = 0 Then
           'Select utilizado para preencher o combo de montadora
3           vSql = "Begin producao.PCK_VDA320_NOVO.pr_BUSCA_motor(:montadora,:veiculo,:modelo,:vCursor,:vErro);END;"
            
4          oradatabase.Parameters.Remove "montadora"
5          oradatabase.Parameters.Add "montadora", montadora, 1
           
6          oradatabase.Parameters.Remove "veiculo"
7          oradatabase.Parameters.Add "veiculo", Veiculo, 1
           
8          oradatabase.Parameters.Remove "modelo"
9          oradatabase.Parameters.Add "modelo", Modelo, 1
           
           
10         Criar_Cursor
11         oradatabase.ExecuteSQL vSql
        
12         Set ss = oradatabase.Parameters("vCursor").Value
        
13        If oradatabase.Parameters("vErro").Value <> 0 Then
14            Call Process_Line_Errors(Sql)
15            Exit Sub
16        End If

17          Screen.MousePointer = 11
18          If ss.EOF Then
19             MsgBox "N�o existe motor cadastrado para este modelo", 0, "ATEN��O"
20             Screen.MousePointer = 0
21             Exit Sub
22          Else
              'Enche o combo
23            For cont = 1 To ss.RecordCount
                'Redefine array para guardar os c�digos de montadoras
24              ReDim Preserve arrmotor(1 To ss.RecordCount)
25              cboMotor.AddItem ss.Fields("DESC_motor").Value
26              arrmotor(cont) = ss.Fields("cod_motor").Value
27              ss.MoveNext
28            Next
29            Screen.MousePointer = 0
30          End If
31       End If
       'End If

Trata_Erro:
32      If Err <> 0 Then
33        MsgBox Err.Number & vbCrLf & Err.Description & vbCrLf & Erl, vbInformation, "Aten��o"
34        Exit Sub
35       End If
End Sub

Private Sub cboMotor_KeyPress(KeyAscii As Integer)
   KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub cboSubgrupo_DropDown()

Dim ss As Object
Dim cont As Long

 
' Verifica se foi escolhido um fornecedor e um grupo, caso contr�rio n�o permite sele��o
' Se escolheu fornecedor e grupo executa o select que busca os subgrupos
' N�o permite executar o select se o combo estiver cheio

'If frmVenda.TXTCOD_FORNECEDOR.Text = "" Then
'  MsgBox "Escolha um fornecedor", 0, "ATEN��O"
'  Exit Sub
'Else
  If frmFabricaWeb.cboGrupo.Text = "" Then
    MsgBox "Escolha um grupo", 0, "ATEN��O"
    frmFabricaWeb.cboGrupo.SetFocus
    Exit Sub
  Else
    If cboSubgrupo.ListCount = 0 Then
      Screen.MousePointer = 11
      'Select utilizado para preencher o combo de subgrupo
      OraParameters.Remove "forn"
     OraParameters.Add "forn", IIf(frmVenda.txtCOD_FORNECEDOR = "", 0, frmVenda.txtCOD_FORNECEDOR), 1
     OraParameters.Remove "grupo"
     OraParameters.Add "grupo", grupo, 1
    
     
  
     vSql = "Begin producao.PCK_VDA320_NOVO.pr_subgrupo(:vCursor,:forn,:grupo,:vErro);END;"

     Criar_Cursor
     oradatabase.ExecuteSQL vSql
  
     Set ss = oradatabase.Parameters("vCursor").Value
  
    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(Sql)
        Exit Sub
    End If

        If ss.EOF Then
           MsgBox "N�o exite Subgrupo cadastrado para o fornecedor " & frmVenda.txtCOD_FORNECEDOR, 0, "ATEN��O"
           Exit Sub
        Else
          'Enche o combo
          For cont = 1 To ss.RecordCount
            'Redefine array para guardar os c�digos de subgrupos
            ReDim Preserve arrsubgrupo(1 To ss.RecordCount)
            cboSubgrupo.AddItem ss.Fields("desc_subgrupo").Value
            arrsubgrupo(cont) = ss.Fields("cod_subgrupo").Value
            ss.MoveNext
          Next
          Screen.MousePointer = 0
        End If
    End If
  End If
'End If



End Sub


Private Sub cboSubgrupo_KeyPress(KeyAscii As Integer)

   'n�o permite alterar a informa��o do combo
  
 ' KeyAscii = 0
 ' Beep
 KeyAscii = Maiusculo(KeyAscii)


End Sub

Private Sub cboVeiculo_Change()
 If cboVeiculo.ListCount = 0 Then
     Call cboVeiculo_DropDown
 End If
 If cboVeiculo = "" Then
   Veiculo = 0
 End If
    
End Sub

Private Sub cboVeiculo_Click()
 On Error GoTo Trata_Erro
 
   Veiculo = arrveiculo(cboVeiculo.ListIndex + 1)
   
   Erase arrmodelo
   Erase arrmotor
   
  frmFabricaWeb.cboModelo.Clear
  frmFabricaWeb.cboModelo.Text = ""
  Modelo = 0
   
  frmFabricaWeb.cboMotor.Clear
  frmFabricaWeb.cboMotor.Text = ""
  Motor = 0
   
   
   
   Exit Sub
   
Trata_Erro:
  If Err = 9 Then
    MsgBox "Clique sobre um veiculo", vbInformation, "Aten��o"
    Exit Sub
   End If

End Sub

Private Sub cboVeiculo_DropDown()
Dim ss As Object

' Verifica se foi escolhido uma montadora, caso contr�rio n�o permite sele��o
' Se escolheu montadora executa o select que busca os veiculos
' N�o permite executar o select se o combo estiver cheio

If montadora = 0 Then
  MsgBox "Escolha uma montadora", 0, "ATEN��O"
  Exit Sub
Else
   
  If cboVeiculo.ListCount = 0 Then
     'Select utilizado para preencher o combo de montadora
      vSql = "Begin producao.PCK_VDA320_NOVO.pr_BUSCA_veiculo(:montadora,:vCursor,:vErro);END;"
      
     oradatabase.Parameters.Remove "montadora"
     oradatabase.Parameters.Add "montadora", montadora, 1
     
     Criar_Cursor
     oradatabase.ExecuteSQL vSql
  
     Set ss = oradatabase.Parameters("vCursor").Value
  
    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(Sql)
        Exit Sub
    End If

      Screen.MousePointer = 11
      If ss.EOF Then
         MsgBox "N�o existe veiculo cadastrado para esta montadora", 0, "ATEN��O"
         Screen.MousePointer = 0
         Exit Sub
      Else
        'Enche o combo
        For cont = 1 To ss.RecordCount
          'Redefine array para guardar os c�digos de montadoras
          ReDim Preserve arrveiculo(1 To ss.RecordCount)
          cboVeiculo.AddItem ss.Fields("DESC_veiculo").Value
          arrveiculo(cont) = ss.Fields("cod_veiculo").Value
          ss.MoveNext
        Next
        Screen.MousePointer = 0
      End If
   End If
 End If

End Sub


Private Sub cboVeiculo_KeyPress(KeyAscii As Integer)
   KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub cmdSair_Click()
    
    txtResposta = ""
    frmFabricaWeb.Hide
    
End Sub

Private Sub txtAno_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub WebFabrica_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, Flags As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)

    On Error GoTo Trata_Erro
    
    If Left(URL, 13) = "javascript:F(" Then
             
        frmVenda.txtCOD_FABRICA.Text = Mid(URL, 15, InStr(15, URL, ",") - 16)
        DoEvents
        txtResposta = Mid(URL, 15, InStr(15, URL, ",") - 16)
        frmVenda.txtCOD_FORNECEDOR.Text = Mid(URL, InStr(15, URL, ",") + 1, InStr(InStr(15, URL, ","), URL, ")") - InStr(15, URL, ",") - 1)
        Call frmVenda.txtCOD_FABRICA_LostFocus
        frmFabricaWeb.Hide
    
    End If
    
Trata_Erro:
    If Err.Number <> 0 Then
        MessageBox 0, "Sub WebFabrica_BeforeNavigate2" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If
    
End Sub



Private Sub WebFabrica_DocumentComplete(ByVal pDisp As Object, URL As Variant)

    If Me.picAguardarParametros.Visible = True Then Me.picAguardarParametros.Visible = False
    
End Sub

'TI-5291
Private Sub cmdKdApeca_Click()
   Dim r As Long
   frmFabricaWeb.WindowState = vbMinimized
   r = ShellExecute(0, "open", "http://kdapecaweb.dpk.com.br/Home/Contact", 0, 0, 1)
End Sub
'FIM TI5291


